# people-counting

# Edge Installation
1) Copy ./bounding_box.json to ./videos/bounding_box.json
2) Add below content to: ~/.config/lxsession/LXDE-pt/autostart

@lxterminal -e /home/pi/run.sh

3) Put below content to newly create file: ~/run.sh (with 777 permission)

(/opt/intel/openvino/bin/setupvars.sh && cd /home/pi/people-counting && sleep 25 && while true; do ./start_edge_ui.sh; done)

4) Restart device to see the system software running...

# UPDATE CERT FOR THE SERVER
Follow this: https://medium.com/@mashrur123/a-step-by-step-guide-to-securing-a-tomcat-server-with-letsencrypt-ssl-certificate-65cd26290b70
