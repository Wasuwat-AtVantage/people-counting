import os
import cv2
import json
import sys
from sys import platform
import numpy as np
import matplotlib.pyplot as plt

import keras
from keras import backend as K
from keras.layers.core import Dense, Activation
from keras.optimizers import Adam
from keras.metrics import categorical_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
from keras.models import Model
from keras.applications import imagenet_utils
from keras.layers import Dense,GlobalAveragePooling2D,Flatten,Dropout
from keras.applications import MobileNetV2
from keras.applications.mobilenet import preprocess_input
from IPython.display import Image
from keras.optimizers import Adam
from keras.utils import plot_model
from keras.regularizers import l1
from keras.callbacks import TensorBoard, ModelCheckpoint

# Age prediction is highly unbalaned data training (Adult~2000, Child~200, Elder~100),
# We use unbalanced class weight when calculating loss for this
from collections import Counter

'''
Download training dataset from:
https://drive.google.com/drive/folders/17cNfflgrb66dH8UBWtNFMyzkCegvM_3N?usp=sharing
'''

'''
mobile = keras.applications.mobilenet_v2.MobileNetV2()

def prepare_image(file):
    img_path = ''
    img = image.load_img(img_path + file, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    return keras.applications.mobilenet_v2.preprocess_input(img_array_expanded_dims)

Image(filename='per00007.jpg')
preprocessed_image = prepare_image('per00007.jpg')
predictions = mobile.predict(preprocessed_image)
results = imagenet_utils.decode_predictions(predictions)
print(results)
'''

mobilenetv2_base_model = MobileNetV2(weights='imagenet', 
  include_top=False,
  input_shape=(224,224,3)
  )

x = mobilenetv2_base_model.output
feature_extractor_output = x

x = GlobalAveragePooling2D()(x) # Nok: This will make us loss much feature information
#x = Flatten()(x) # Nok: This will not make us loss much information of extracted features from MobileNetV2

x = Dense(64, activation = 'linear', activity_regularizer=l1(0.001) )(x)
x = Activation('relu')(x)
#x = Dense(64, activation = 'linear', activity_regularizer=l1(0.001) )(x)
#x = Activation('relu')(x)

#x = Dense(64, activation = 'linear' )(x)
#x = Dropout(rate=0.2)(x)

#x = Dense(128, activation = 'linear', activity_regularizer=l1(0.001) )(x)
#x = Dense(1024, activation = 'linear' )(x)
#x = Activation('relu')(x)
#x = Dropout(rate=0.5)(x)

# We may not need more complex model if it's already fit the training data, so let's try from simpler one first.
#x = Dense(1024, activation = 'relu')(x)
#x = Dense(1024, activation = 'relu')(x)
#x = Dense(512, activation = 'relu')(x)

predictions = Dense(3, activation = 'softmax')(x)
model_base = Model(inputs=mobilenetv2_base_model.input, outputs=feature_extractor_output)
model = Model(inputs=mobilenetv2_base_model.input, outputs=predictions)

#for i, layer in enumerate(model.layers):
#    print(i, layer.name)
model.summary() # This give more information about the model

# Freeze weight of only feature extractor layers
'''
for layer in model_base.layers:
    print('Freeze weight of layer: ' + str(layer.name))
    layer.trainable=False
'''

train_datagen = ImageDataGenerator(
    rotation_range=10, 
    width_shift_range=0.2, 
    height_shift_range=0.2, 
    shear_range=0.2,
    brightness_range=[0.5, 1.0],
    zoom_range=[0.9, 1.0],
    fill_mode='nearest', 
    vertical_flip=False,
    horizontal_flip=True, 
    preprocessing_function=preprocess_input, 
    validation_split=0.2
    )

test_datagen = ImageDataGenerator(
    preprocessing_function=preprocess_input
    )

train_generator = train_datagen.flow_from_directory(
    './dataset/train', 
    target_size=(224,224), 
    color_mode='rgb', 
    batch_size=32, 
    class_mode='categorical')

val_generator = test_datagen.flow_from_directory(
    './dataset/validation', 
    target_size=(224,224), 
    color_mode='rgb', 
    batch_size=32, 
    class_mode='categorical')


counter = Counter(train_generator.classes)                          
max_val = float(max(counter.values()))       
class_weights = {class_id : max_val/num_images for class_id, num_images in counter.items()}      
print('class_weights = ' + str(class_weights))               

model_saver = ModelCheckpoint('best_model_weight.h5', save_best_only=True, save_weights_only=True)
tboard_log_saver = TensorBoard('tboard_logs', write_graph=True, write_images=True)

model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['accuracy'])

step_size_train = train_generator.n//train_generator.batch_size
history = model.fit_generator(generator=train_generator,
                    validation_data=val_generator,
                    steps_per_epoch=step_size_train,
                    epochs=100,
                    callbacks=[model_saver, tboard_log_saver]
                    )
                    # , class_weight=class_weights)

# Save the model and weights
# serialize model to JSON
model_json = model.to_json()
with open("age_model.json", "w") as json_file:
  json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("age_model_weight.h5")
print("Saved model to disk")
