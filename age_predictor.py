import cv2
import numpy as np
import os
from keras.preprocessing.image import img_to_array
from keras.models import load_model, model_from_json
import dlib

from object_classifier import ObjectClassifier


class AgePredictor(ObjectClassifier):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('AgePredictor::init_model')

        # load json and create model
        MODEL_JSON = '../age_mobilenetv2_ssd/age_model.json'
        MODEL_WEIGHTS = '../age_mobilenetv2_ssd/age_model_weight.h5'

        print("Loaded model from disk")
        json_file = open(MODEL_JSON, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)
        # load weights into new model
        self.model.load_weights(MODEL_WEIGHTS)

        self.img_size = 224
        self.class_label_map = {0: 'adult', 1: 'kid', 2: 'elder'}
        self.target_size = self.model.input_shape[1:3]
        self.confidence = 0.5
        if ("confidence" in config):
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    def preprocess_input(self, x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('GenderPredictor::perform_classification')
        image = cv2.resize(image, (self.img_size, self.img_size))
        rgb_face = self.preprocess_input(image, False)
        rgb_face = np.expand_dims(rgb_face, 0)
        age_prediction = self.model.predict(rgb_face)
        print('AGE PREDICTION => ' + str(age_prediction))
        age = np.argmax(age_prediction)
        return self.class_label_map[age]
