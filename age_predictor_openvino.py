import cv2
import numpy as np
import os
import dlib

from object_classifier import ObjectClassifier


class AgePredictorOpenVINO(ObjectClassifier):
    def __init__(self, fp_num):
        self.model = None
        self.data_precision = fp_num

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('AgePredictorOpenVINO::init_model')

        xml_file = "../age_mobilenetv2_ssd/openvino.xml"
        bin_file = "../age_mobilenetv2_ssd/openvino.bin"
        if self.data_precision == 'fp32':
            xml_file = "../age_mobilenetv2_ssd/openvino_fp32.xml"
            bin_file = "../age_mobilenetv2_ssd/openvino_fp32.bin"
        self.model = cv2.dnn.readNet(xml_file, bin_file)
        if "use_myriad" in config and config["use_myriad"] == True:
            # Use MyRiad to perform inference
            print('Using NCS2 MyRiad Device.')
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)
        else:
            # Use CPU
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

        self.img_size = 224
        self.class_label_map = {0: 'adult', 1: 'kid', 2: 'elder'}
        self.confidence = 0.5
        if ("confidence" in config):
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    def preprocess_input(self, x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('AgePredictorOpenVINO::perform_classification')
        blob = cv2.dnn.blobFromImage(image, size=(224, 224), swapRB=True)
        self.model.setInput(blob)
        age_prediction = self.model.forward()
        print('AGE PREDICTION => ' + str(age_prediction))
        age = np.argmax(age_prediction)
        return self.class_label_map[age]
