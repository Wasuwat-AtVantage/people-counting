import os
import sys
import json


class CSVExporter:

    def __init__(self):
        self.ROI_TEXT = ['None', 'Customer', 'Operation', 'Traffic', 'Other', 'Other2', 'Demo', 'Demo']

    def get_count(self, hotspot_stat, queries):
        count = 0
        for key_str in hotspot_stat:
            _key_str = key_str
            if len(queries) > 1:  # We do not want to handle overlap case if search query is single string
                key_str = ',' + key_str + ','  # For not findind overlap substring of key
            found = True
            for query in queries:
                if key_str.find(query) == -1:
                    # print('Not Found ' + query + ' in ' + key_str)
                    found = False
                    break
                # else:
                # print('Found ' + query + ' in ' + key_str)
            if found:
                count = count + hotspot_stat[_key_str]
        # print('QUERY: ' + str(queries) + ' -> ' + str(count))
        return count

    def init_file(self, csv_path):
        # Create directory if not existed.
        raw_csv_log_file_dir = csv_path[:csv_path.rfind('/')]
        if not os.path.exists(raw_csv_log_file_dir):
            os.makedirs(raw_csv_log_file_dir)
        # Write header.
        with open(csv_path, 'w', encoding='utf-8') as fout:
            header_row = [
                'File',
                'Date',
                'Time',
                'Meta Data',
                'Area',
                'ROI_Type',
                'Total',

                'Right-Total',
                'Right-M-Kids',
                'Right-M-Adults',
                'Right-M-Elders',
                'Right-W-Kids',
                'Right-W-Adults',
                'Right-W-Elders',
                'Right-Undefined',

                'Left-Total',
                'Left-M-Kids',
                'Left-M-Adults',
                'Left-M-Elders',
                'Left-W-Kids',
                'Left-W-Adults',
                'Left-W-Elders',
                'Left-Undefined',

                'Interest-Total',
                'Interest-M-Kids',
                'Interest-M-Adults',
                'Interest-M-Elders',
                'Interest-W-Kids',
                'Interest-W-Adults',
                'Interest-W-Elders',
                'Interest-Undefined',

                'Engage-Total',
                'Engage-M-Kids',
                'Engage-M-Adults',
                'Engage-M-Elders',
                'Engage-W-Kids',
                'Engage-W-Adults',
                'Engage-W-Elders',
                'Engage-Undefined',

                '% Occcupied',
            ]
            fout.write(','.join(header_row) + '\n')

    def get_roi_type_display(self, roi_type):
        if roi_type < len(self.ROI_TEXT):
            return self.ROI_TEXT[roi_type]
        else:
            return str(roi_type)

    def append_to_file(self, csv_path, data_model, file_path, date_string, time_string, meta_datum):

        with open(csv_path, 'a+', encoding='utf-8') as fout:

            report_json = data_model

            for hotspot_stat_id, hotspot_stat in enumerate(report_json['hotspot_stat']):
                percent_occupied = 0
                if hotspot_stat['interval_occupied_count'] + hotspot_stat['interval_unoccupied_count'] > 0:
                    percent_occupied = int(hotspot_stat['interval_occupied_count'] * 100 / (
                                hotspot_stat['interval_occupied_count'] + hotspot_stat['interval_unoccupied_count']))

                roi_type = hotspot_stat['roi_type']

                data_row = [
                    str(hotspot_stat["count_map"]["all"]),
                    str(self.get_count(hotspot_stat["count_map"], [',h+,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h+,', ',male,', ',kid,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h+,', ',male,', ',adult,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h+,', ',male,', ',elder,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h+,', ',female,', ',kid,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h+,', ',female,', ',adult,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h+,', ',female,', ',elder,'])),
                    '',

                    str(self.get_count(hotspot_stat["count_map"], [',h-,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h-,', ',male,', ',kid,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h-,', ',male,', ',adult,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h-,', ',male,', ',elder,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h-,', ',female,', ',kid,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h-,', ',female,', ',adult,'])),
                    str(self.get_count(hotspot_stat["count_map"], [',h-,', ',female,', ',elder,'])),
                    '',

                    str(hotspot_stat["interested_count_map"]["all"]),
                    str(self.get_count(hotspot_stat["interested_count_map"], [',male,', ',kid,'])),
                    str(self.get_count(hotspot_stat["interested_count_map"], [',male,', ',adult,'])),
                    str(self.get_count(hotspot_stat["interested_count_map"], [',male,', ',elder,'])),
                    str(self.get_count(hotspot_stat["interested_count_map"], [',female,', ',kid,'])),
                    str(self.get_count(hotspot_stat["interested_count_map"], [',female,', ',adult,'])),
                    str(self.get_count(hotspot_stat["interested_count_map"], [',female,', ',elder,'])),
                    '',

                    str(hotspot_stat["engaged_count_map"]["all"]),
                    str(self.get_count(hotspot_stat["engaged_count_map"], [',male,', ',kid,'])),
                    str(self.get_count(hotspot_stat["engaged_count_map"], [',male,', ',adult,'])),
                    str(self.get_count(hotspot_stat["engaged_count_map"], [',male,', ',elder,'])),
                    str(self.get_count(hotspot_stat["engaged_count_map"], [',female,', ',kid,'])),
                    str(self.get_count(hotspot_stat["engaged_count_map"], [',female,', ',adult,'])),
                    str(self.get_count(hotspot_stat["engaged_count_map"], [',female,', ',elder,'])),
                    '',

                    str(percent_occupied),
                ]
                # Calculate undefined column
                data_row[8] = str(
                    int(data_row[1]) - int(data_row[2]) - int(data_row[3]) - int(data_row[4]) - int(data_row[5]) - int(
                        data_row[6]) - int(data_row[7]))
                data_row[16] = str(int(data_row[9]) - int(data_row[10]) - int(data_row[11]) - int(data_row[12]) - int(
                    data_row[13]) - int(data_row[14]) - int(data_row[15]))
                data_row[24] = str(int(data_row[17]) - int(data_row[18]) - int(data_row[19]) - int(data_row[20]) - int(
                    data_row[21]) - int(data_row[22]) - int(data_row[23]))
                data_row[32] = str(int(data_row[25]) - int(data_row[26]) - int(data_row[27]) - int(data_row[28]) - int(
                    data_row[29]) - int(data_row[30]) - int(data_row[31]))

                fout.write(file_path + ',' + date_string + ',' + time_string + ',' + '|'.join(meta_datum) + ',' + str(
                    hotspot_stat_id) + ',' + self.get_roi_type_display(roi_type) + ',' + ','.join(data_row) + '\n')
