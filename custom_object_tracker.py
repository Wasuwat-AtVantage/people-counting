import numpy as np
import cv2
from object_tracker import ObjectTracker
from roi_area_config import ROIAreaConfig, AREATYPE_SALE, AREATYPE_OPERATION
from roi_area_stat import ROIAreaStat
import random


class CustomObjectTracker(ObjectTracker):
    def __init__(self):
        # Initialization internal structures

        # Hotspot Region (TOP, LEFT, BOTTOM, RIGHT, X-DIRECTION, Y-DIRECTION, AGE_TRACKING, COUNT_THRESHOLD, INTEREST_THREHOLD, ENGAGEMENT_THRESHOLD)
        # If object is not being detected again with in MAX_AGE_TRACKING iteration, it's removed from tracking_list.
        # If object is not tracked inside boundary to the specified number, we will not bother counting it (Remove glitch)
        self.hotspot_areas = [
            ROIAreaConfig(type_=AREATYPE_SALE, poly=[[0, 0], [0, 3000], [3000, 3000], [3000, 0]], direction_x='0',
                          direction_y='0',
                          tracking_age=5, count_threshold=3, interesting_threshold=10, engagement_threshold=20,
                          unoccupied_threshold=50, constraint_rois=[]
                          )
        ]

        # Intersection threshold. If object has lower % intersection than this value, it will be discarded.
        self.MINIMIM_INTERSECTION_THRESHOLD = 0.6

        # Map from hotspot_id tp boolean indicate that the Hotspot is being active in current frame or not.
        # Inactive frame is caused by too low movement inside the frame using background subtraction technic
        self.MOVEMENT_PERCENT_THRESHOLD = 0.05

        # Object tracking list. Hotspot will have pointer to object in this list.
        self.tracking_list = []

        # Map from hotspot_id to hotspot stat
        self.hotspot_status_map = {}

        # Map from hotspot_id to hotspot stat (realtime counting)
        self.hotspot_status_map_realtime = {}

        # Below are global count map (SUM count for every ROI area)
        # Map from class_id to count
        self.count_map = {
            'all': 0,
        }
        # Map from class_id to interested count
        self.interested_count_map = {
            'all': 0,
        }
        # Map from class_id to engaged count
        self.engaged_count_map = {
            'all': 0,
        }

        # Map from class_id to attended time bucket to count
        self.atteneded_count_map = {
            'all': {},
        }

        # For detecting same object, its centroid cannot be far apart in order to consider as same object
        self.CENTROID_DISTANCE_THRESHOLD = 70

        # Cass ID to string map
        self.CLASSID_TO_STRING_MAP = [
            'all', 'people'
        ]

        # Similarity threshold
        self.SIMILARITY_CORRELATION_THRESHOLD = 0.7

        # Store last object id
        self.last_object_id = 1

        # Class finalizer from statistic
        self.custom_class_finalizer = None

        # Notification callback for events:
        self.callback = None

    def get_tracked_objects(self):
        return self.tracking_list

    def set_class_finalizer(self, finalizer):
        self.custom_class_finalizer = finalizer

    def get_hotspot_areas(self):
        return self.hotspot_areas

    # Notification callback for events:
    def set_callback(self, callback):
        self.callback = callback

    def set_hotspot_areas(self, new_hotspot_areas):
        self.hotspot_areas = new_hotspot_areas
        self.hotspot_status_map = {}
        self.hotspot_status_map_realtime = {}
        for hotspot_id, hotspot in enumerate(self.hotspot_areas):
            hotspot_status = ROIAreaStat()
            self.hotspot_status_map[hotspot_id] = hotspot_status
            hotspot_status_realtime = ROIAreaStat()
            self.hotspot_status_map_realtime[hotspot_id] = hotspot_status_realtime

    # Get (list of) ID of hotspot the rect fall in (can be more than 1)
    def is_in_hotspot_area(self, top, bottom, right, left):
        indices = []
        # Nok: Remove Object Area Threshold because it is not possible in polygon ROI settings...
        # object_area = (right - left) * (bottom - top)
        object_max_edge = max(bottom - top, right - left)
        distance_threshold = -(1.0 - self.MINIMIM_INTERSECTION_THRESHOLD) * object_max_edge
        for idx, hotspotArea in enumerate(self.hotspot_areas):

            if (cv2.pointPolygonTest(hotspotArea.polyNP, (left, top), True) > distance_threshold and
                    cv2.pointPolygonTest(hotspotArea.polyNP, (right, top), True) > distance_threshold and
                    cv2.pointPolygonTest(hotspotArea.polyNP, (right, bottom), True) > distance_threshold and
                    cv2.pointPolygonTest(hotspotArea.polyNP, (left, bottom), True) > distance_threshold):

                if (self.is_hotspot_active(idx)):
                    indices.append(idx)

                '''
                # check intersection threshold
                intersection_area = (min(hotspotArea.right, right) - max(hotspotArea.left, left)) * (min(hotspotArea.bottom, bottom) - max(hotspotArea.top, top))
                if object_area > 0:
                  # print('Intersection % = ' + str(intersection_area * 100 / object_area))
                  if intersection_area >= self.MINIMIM_INTERSECTION_THRESHOLD * object_area:
                    if (self.is_hotspot_active(idx)):
                      indices.append(idx)
                '''

        return indices

    # Apply Foreground Mask from movement detection. Hotspot those contains too low movement is masked as inactive
    def apply_hotspot_status_update(self, fgmask):
        for idx, hotspotArea in enumerate(self.hotspot_areas):
            top = hotspotArea.top
            bottom = hotspotArea.bottom
            left = hotspotArea.left
            right = hotspotArea.right
            cropped_fgmask = fgmask[top:bottom, left:right]
            percent_movement = np.count_nonzero(cropped_fgmask) / (cropped_fgmask.shape[0] * cropped_fgmask.shape[1])
            # print("Hotspot: "+str(idx)+". Percent Movement: "+str(percent_movement))
            if percent_movement > self.MOVEMENT_PERCENT_THRESHOLD:
                self.hotspot_status_map[idx].is_active = True
                self.hotspot_status_map_realtime[idx].is_active = True
            else:
                self.hotspot_status_map[idx].is_active = False
                self.hotspot_status_map_realtime[idx].is_active = False

    # Get status of each hotspot
    def is_hotspot_active(self, hotspot_id, debug=False):
        return True  # This is for occupacy detection
        '''
        if debug:
          print('Hotspot: ' + str(hotspot_id) + ', Active = ' + str(not((hotspot_id in self.hotspot_status_map) and (self.hotspot_status_map[hotspot_id].is_active == False))))
        return not((hotspot_id in self.hotspot_status_map) and (self.hotspot_status_map[hotspot_id].is_active == False))
        '''

    # Get list of tracking objects in given hotspot
    def get_tracking_list(self, hotspot_id):
        if hotspot_id not in self.hotspot_status_map_realtime:
            hotspot_status = ROIAreaStat()
            self.hotspot_status_map_realtime[hotspot_id] = hotspot_status
        if hotspot_id in self.hotspot_status_map:
            return self.hotspot_status_map[hotspot_id].tracking_list
        else:
            hotspot_status = ROIAreaStat()
            self.hotspot_status_map[hotspot_id] = hotspot_status
            return self.hotspot_status_map[hotspot_id].tracking_list

    def create_class_count_map(self):
        return {}

    def add_object_to_tracking_list_frame(self, object_id, object_features, centroid, rect, detected_class_ids,
                                          timestamp):
        class_count = self.create_class_count_map()
        for detected_class_id in detected_class_ids:
            class_count[detected_class_id] = 1
        tracked_object = {
            'object_id': object_id,
            'object_features': object_features,
            'ts': timestamp,
            'age': 0,
            'count': 0,
            'centroid_list': [centroid],
            'last_rect': rect,
            'class_count': class_count,
            'last_classes': [],
            'first_centroid': centroid,
            'rois': set(),
            'rois_history': set()
        }
        self.tracking_list.append(tracked_object)
        return tracked_object

    def add_object_to_tracking_list_hotspot(self, tracked_object, tracking_list, object_id, object_features, centroid,
                                            rect, detected_class_ids, timestamp):
        class_count = self.create_class_count_map()
        for detected_class_id in detected_class_ids:
            class_count[detected_class_id] = 1
        tracking_list.append({
            'object_id': object_id,
            'object_features': object_features,
            'ts': timestamp,
            'age': 0,
            'count': 0,
            'centroid_list': [centroid],
            'last_rect': rect,
            'class_count': class_count,
            'last_classes': [],
            'first_centroid': centroid,
            'tracked_object': tracked_object
        })

    def is_the_same_object(self, feature_a, feature_b):
        distance = cv2.compareHist(feature_a, feature_b, cv2.HISTCMP_CORREL)
        return (distance > 0.5)

    def add_class_to_tracked_object(self, item, detected_class_ids):
        for detected_class_id in detected_class_ids:
            if detected_class_id in item['class_count']:
                item['class_count'][detected_class_id] = item['class_count'][detected_class_id] + 1
            else:
                item['class_count'][detected_class_id] = 1
        item['last_classes'] = detected_class_ids

    def check_and_track_object_in_frame(self, object_features, centroid, rect, detected_class_ids, image, timestamp):
        isFound = False

        # Calculate heatmap index once
        hy = int(centroid[0] * 10 / image.shape[0])
        hx = int(centroid[1] * 10 / image.shape[1])

        tracked_object = None

        for item in self.tracking_list:
            if self.is_the_same_object(item['object_features'], object_features):

                # If centroid is too apart then they are definitly not the same object
                if len(item['centroid_list']) > 0:
                    prev_centroid = item['centroid_list'][-1]

                if (abs(centroid[0] - prev_centroid[0]) > self.CENTROID_DISTANCE_THRESHOLD) or abs(
                        centroid[1] - prev_centroid[1]) > self.CENTROID_DISTANCE_THRESHOLD:
                    print('Detect almost similar objects those too far apart. Treat them as distinct ones.')
                    continue

                # print('- Detect same object: ' + str(item['object_id']))
                item['age'] = 0
                item['count'] = item['count'] + 1
                item['centroid_list'].append(centroid)
                item['centroid_list'] = item['centroid_list'][-5:]
                item['last_rect'] = rect
                for detected_class_id in detected_class_ids:
                    if detected_class_id in item['class_count']:
                        item['class_count'][detected_class_id] = item['class_count'][detected_class_id] + 1
                    else:
                        item['class_count'][detected_class_id] = 1
                isFound = True
                tracked_object = item
                break
        if isFound == False:
            object_id = self.last_object_id
            self.last_object_id = self.last_object_id + 1
            print('- !!!!Detect new object: ' + str(object_id))
            tracked_object = self.add_object_to_tracking_list_frame(object_id, object_features, centroid, rect,
                                                                    detected_class_ids, timestamp)

        return tracked_object

    def check_and_track_object_in_hotspot(self, tracked_object, tracking_list, object_features, centroid, rect,
                                          detected_class_ids, hotspot_id, image, timestamp):
        isFound = False

        # Calculate heatmap index once
        hy = int(centroid[0] * 10 / image.shape[0])
        hx = int(centroid[1] * 10 / image.shape[1])

        for item in tracking_list:

            if item['tracked_object'] == tracked_object:
                # print('- Detect same object: ' + str(item['object_id']))
                item['age'] = 0
                item['count'] = item['count'] + 1
                item['centroid_list'].append(centroid)
                item['centroid_list'] = item['centroid_list'][-5:]
                item['last_rect'] = rect
                for detected_class_id in detected_class_ids:
                    if detected_class_id in item['class_count']:
                        item['class_count'][detected_class_id] = item['class_count'][detected_class_id] + 1
                    else:
                        item['class_count'][detected_class_id] = 1
                isFound = True

                # If counting reach specific limit, we may want to fire notification and maked it as fired already
                try:
                    if (self.callback is not None) and ('nfired' not in item):
                        item_lifetime = timestamp - item['ts']
                        if self.hotspot_areas[hotspot_id].type == 6:  # Only for Demo purpose
                            is_engaged = item_lifetime > self.hotspot_areas[
                                hotspot_id].engagement_threshold  # engaged threshold
                            if is_engaged:
                                self.callback.send_notification(
                                    'Detect engaged people in area: ' + str(hotspot_id) + '.')
                                item['nfired'] = True
                        elif self.hotspot_areas[hotspot_id].type == 7:  # Only for Demo purpose
                            is_happy = (('happy' in item['class_count']) and (item['class_count']['happy']) > 1)
                            if is_happy:
                                self.callback.send_notification('Detect a happy guy in area: ' + str(hotspot_id) + '.')
                                item['nfired'] = True
                except:
                    print('[Error] Cannot sending notification for Demo app.')

                # Increase heatmap counter at the index
                self.hotspot_status_map[hotspot_id].heatmap[hy][hx] = self.hotspot_status_map[hotspot_id].heatmap[hy][
                                                                          hx] + 1

                break
        if isFound == False:
            object_id = self.last_object_id
            self.last_object_id = self.last_object_id + 1
            print('- !!!!Detect new object: ' + str(object_id))
            self.add_object_to_tracking_list_hotspot(tracked_object, tracking_list, object_id, object_features,
                                                     centroid, rect, detected_class_ids, timestamp)

            # Add this ROI to tracked object ROI history
            tracked_object['rois'].add(hotspot_id)
            tracked_object['rois_history'].add(hotspot_id)

            # Increase heatmap counter at the index
            self.hotspot_status_map[hotspot_id].heatmap[hy][hx] = self.hotspot_status_map[hotspot_id].heatmap[hy][
                                                                      hx] + 1

    # Function to perform object tracking
    def track_object(self, image, left, top, right, bottom, current_frame_number, detected_class_ids, timestamp):
        # print("Track Object")
        crop_img = image[top:bottom, left:right]

        # Calculate feature of image

        # 1st feature is Histogram
        histogram = cv2.calcHist([crop_img], [0, 1, 2], None, [8, 8, 8],
                                 [0, 256, 0, 256, 0, 256])
        histogram = cv2.normalize(histogram, histogram).flatten()
        feature = histogram

        # 2nd feature is Centroid
        centroid = [(top + bottom) / 2, (left + right) / 2]
        rect = (left, top, right, bottom)

        # Add object to tracking list
        tracked_object = self.check_and_track_object_in_frame(feature, centroid, rect, detected_class_ids, image,
                                                              timestamp)

        hotspot_ids = self.is_in_hotspot_area(top, bottom, right, left)
        # print('isInHotSpot = ' + str(isInHotSpot))

        staff_count = self.custom_class_finalizer.get_count_of(tracked_object['class_count'], 'staff')
        customer_count = self.custom_class_finalizer.get_count_of(tracked_object['class_count'], 'customer')

        for hotspot_id in hotspot_ids:

            # If constraint ROIs is set, we discard the object here if it is not satisfied the criterias
            constraint_rois = self.hotspot_areas[hotspot_id].constraint_rois
            if len(
                    constraint_rois) > 0 and customer_count > staff_count:  # We need to skip checking for Staff as they can roam freely.
                pass_roi_contraint = True
                for contraint_roi in constraint_rois:
                    if contraint_roi not in tracked_object['rois_history']:
                        print("[INFO]: Discard object as missing Contraint ROI Criterias")
                        pass_roi_contraint = False
                        break
                if not pass_roi_contraint:
                    continue

            tracking_list = self.get_tracking_list(hotspot_id)
            self.check_and_track_object_in_hotspot(tracked_object, tracking_list, feature, centroid, rect,
                                                   detected_class_ids, hotspot_id, image, timestamp)

    # Function to get tracked object
    def get_object_list(self):
        tracking_list_map = {}
        for hotspot_id in self.hotspot_status_map:
            tracking_list_map[hotspot_id] = self.hotspot_status_map[hotspot_id].tracking_list
        return tracking_list_map

    # Function to get count data:
    def get_count_data(self):
        return self.count_map

    # Function to get interested count data:
    def get_interested_count_data(self):
        return self.interested_count_map

    # Function to get engaged count data:
    def get_engaged_count_data(self):
        return self.engaged_count_map

    # Function to get count data:
    def get_count_data_for_hotspot(self, hotspot_id):
        # print(str(self.hotspot_status_map))
        return self.hotspot_status_map[hotspot_id].count_map

    # Function to get interested count data:
    def get_interested_count_data_for_hotspot(self, hotspot_id):
        return self.hotspot_status_map[hotspot_id].interested_count_map

    # Function to get engaged count data:
    def get_engaged_count_data_for_hotspot(self, hotspot_id):
        return self.hotspot_status_map[hotspot_id].engaged_count_map

    # Function to get attended count data:
    def get_attended_count_data_for_hotspot(self, hotspot_id):
        return self.hotspot_status_map[hotspot_id].atteneded_count_map

    # Function to get count data (realtime):
    def get_count_data_for_hotspot_realtime(self, hotspot_id):
        # print(str(self.hotspot_status_map))
        return self.hotspot_status_map_realtime[hotspot_id].count_map

    # Function to get interested count data (realtime):
    def get_interested_count_data_for_hotspot_realtime(self, hotspot_id):
        return self.hotspot_status_map_realtime[hotspot_id].interested_count_map

    # Function to get engaged count data (realtime):
    def get_engaged_count_data_for_hotspot_realtime(self, hotspot_id):
        return self.hotspot_status_map_realtime[hotspot_id].engaged_count_map

    # Function to get attended count data (realtime):
    def get_attended_count_data_for_hotspot_realtime(self, hotspot_id):
        return self.hotspot_status_map_realtime[hotspot_id].atteneded_count_map

    def calculate_rectangle_area(self, rect):
        try:
            left, top, right, bottom = rect[0], rect[1], rect[2], rect[3]
        except IndexError:
            return None
        return (right - left) * (bottom - top)

    # Function to flag attended time (For Customer Attendance Tracking)
    def flag_attended_time(self, hotspot_status, hotspot_area, timestamp):
        tracking_list = hotspot_status.tracking_list

        # TODO: Revise this to use new setting parameter?
        distance_threshold = hotspot_area.unoccupied_threshold
        size_diff_threshold = 0.75

        staff_list = []
        customer_list = []

        # Find list of staff in the ROI
        for item in tracking_list:
            if len(item['centroid_list']) == 0:  # Focus on those who has location data
                continue
            staff_count = self.custom_class_finalizer.get_count_of(item['class_count'], 'staff')
            customer_count = self.custom_class_finalizer.get_count_of(item['class_count'], 'customer')
            if staff_count > customer_count:  # That is we are sure he/she is staff
                staff_list.append(item)
                if 'attended_at' in item:
                    del item['attended_at']
            else:
                # We only interested in customer who hasn't been attended
                if 'attended_at' not in item:
                    customer_list.append(item)

        # Loop each customer anf flag if they got attention from any staff
        for customer in customer_list:
            customer_centroid = customer['centroid_list'][-1]
            # customer_rect = customer['last_rect']

            # For Unit Test
            '''
            if random.randint(0, 100) < 10:
              customer['attended_at'] = timestamp - customer['ts']
            '''

            for staff in staff_list:
                staff_centroid = staff['centroid_list'][-1]
                # staff_rect = staff['last_rect']
                ydiff = abs(customer_centroid[0] - staff_centroid[0])
                xdiff = abs(customer_centroid[1] - staff_centroid[1])
                if xdiff < distance_threshold and ydiff < distance_threshold:  # Use L1 for speed
                    customer['attended_at'] = timestamp - customer['ts']
                    print('[INFO]: Customer is attended at :' + str(customer['attended_at']) + 's')
                    break
                    # staff_area = self.calculate_rectangle_area(staff_rect)
                    # customer_area = self.calculate_rectangle_area(customer_rect)
                    # if (min(staff_area, customer_area) / max(staff_area, customer_area)) >= size_diff_threshold:
                    #     customer['attended_at'] = timestamp - customer['ts']
                    #     print('[INFO]: Customer is attended at :' + str(customer['attended_at']) + 's')
                    #     break

    def update_each_attended_count_map(self, count_map, bucket):

        if bucket not in count_map:
            count_map[bucket] = 1
        else:
            count_map[bucket] = count_map[bucket] + 1

    # Update related count map with bucketized interval of attendance
    def update_attended_count_map(self, hotspot_status_realtime, hotspot_status, count_map, item, classes):
        bucket = (int(item['attended_at'] / 5) + 1) * 5
        # Cap max bucket
        if bucket > 30:
            return
        bucket = str(bucket)
        count_map_hotspot = hotspot_status.atteneded_count_map
        count_map_hotspot_realtime = hotspot_status_realtime.atteneded_count_map
        self.update_each_attended_count_map(count_map['all'], bucket)
        self.update_each_attended_count_map(count_map_hotspot['all'], bucket)
        self.update_each_attended_count_map(count_map_hotspot_realtime['all'], bucket)

        # [TBD]: Implement attended count map for every classified classes too

    # Update state of tracker into next state (Normally it means T = T + 1)
    def step(self, frame_seq, minutes_passed, timestamp):

        # Update global frame tracking list
        max_age_tracking_frame = 5
        new_tracking_list_frame = []
        for item in self.tracking_list:
            if item['age'] < max_age_tracking_frame:
                item['age'] = item['age'] + 1
                new_tracking_list_frame.append(item)
        self.tracking_list = new_tracking_list_frame

        for hotspot_id, hotspot_status in self.hotspot_status_map.items():
            # Only run checking for Hotspot aress those have movement.
            if not self.is_hotspot_active(hotspot_id):
                continue

            # Also get the realtime counting map
            hotspot_status_realtime = self.hotspot_status_map_realtime[hotspot_id]

            tracking_list = hotspot_status.tracking_list
            hotspot_area = self.hotspot_areas[hotspot_id]
            max_age_tracking = hotspot_area.tracking_age  # MAX_AGE_TRACKING
            existence_count_threshold = hotspot_area.count_threshold  # EXISTENCE_COUNT_THRESHOLD
            unoccupied_threshold = hotspot_area.unoccupied_threshold  # Threshold for treating area as unoccupied

            # For unoccupied record
            # print('is_occupied = ' + str(hotspot_status.is_occupied) + ', occupied_count = ' + str(hotspot_status.occupied_count))
            if len(tracking_list) == 0:
                hotspot_status.occupied_timestamp = timestamp
                if hotspot_status.is_occupied:
                    if timestamp - hotspot_status.unoccupied_timestamp > unoccupied_threshold:
                        hotspot_status.is_occupied = False
                        hotspot_status_realtime.is_occupied = False
                        hotspot_status.unoccupied_timestamp = timestamp
                        # Issue log
                        print('Hotspot: ' + str(hotspot_id) + ' is unoccupied.')
                        hotspot_status.events.append(['out', minutes_passed])

                        if self.callback is not None:
                            if self.hotspot_areas[hotspot_id].type != 1 and self.hotspot_areas[hotspot_id].type != 3 and \
                                    self.hotspot_areas[hotspot_id].type <= 5:  # Only for Service area
                                self.callback.send_notification('Area ' + str(hotspot_id) + ' is Unoccupied')
            else:
                hotspot_status.unoccupied_timestamp = timestamp
                if not hotspot_status.is_occupied:
                    if timestamp - hotspot_status.occupied_timestamp > unoccupied_threshold:
                        hotspot_status.is_occupied = True
                        hotspot_status_realtime.is_occupied = True
                        hotspot_status.occupied_timestamp = timestamp
                        # Issue log
                        print('Hotspot: ' + str(hotspot_id) + ' is occupied.')
                        hotspot_status.events.append(['in', minutes_passed])

                        if self.callback is not None:
                            if self.hotspot_areas[hotspot_id].type != 1 and self.hotspot_areas[hotspot_id].type != 2 and \
                                    self.hotspot_areas[hotspot_id].type <= 5:  # Only for Queue Monitor area
                                self.callback.send_notification('Area ' + str(hotspot_id) + ' is Occupied')

            # Record occupied and unoccupied count ot hotspot stat
            if hotspot_status.is_occupied:
                hotspot_status.interval_occupied_count = hotspot_status.interval_occupied_count + 1
            else:
                hotspot_status.interval_unoccupied_count = hotspot_status.interval_unoccupied_count + 1

            # For flagging attended time of prople in tracking list
            if hotspot_area.type == 6:
                self.flag_attended_time(hotspot_status, hotspot_area, timestamp)

            new_tracking_list = []
            for item in tracking_list:
                item_ts = item['ts']
                item_lifetime = timestamp - item_ts
                if item['age'] < max_age_tracking:
                    item['age'] = item['age'] + 1
                    new_tracking_list.append(item)
                else:
                    print('Lost object: ' + str(item['object_id']) + ' => count = ' + str(item['count']))
                    # If object is being lost, check if count is exceed threshold to confirm existence of the object
                    if item['count'] >= existence_count_threshold:

                        # We will not include "staff" into counting
                        staff_count = self.custom_class_finalizer.get_count_of(item['class_count'], 'staff')
                        customer_count = self.custom_class_finalizer.get_count_of(item['class_count'], 'customer')
                        if staff_count > customer_count:
                            print('[INFO]: Discard STAFF from counting')
                            continue

                        # Also check if the count is exceeding interested and engaged threshold.
                        is_interested = item_lifetime > hotspot_area.interesting_threshold  # Interested threshold
                        is_engaged = item_lifetime > hotspot_area.engagement_threshold  # engaged threshold

                        # print('Centroid List: ' + str(item['centroid_list']))

                        # Check direction of movement to screen out if it is not matched
                        check_direction_passed = True
                        centroid_list = item['centroid_list']
                        x_plus_count = 0
                        x_minus_count = 0
                        y_plus_count = 0
                        y_minus_count = 0
                        prev_centroid = None
                        x_translate = 0
                        y_translate = 0

                        for centroid in centroid_list:
                            x_translate = centroid[1]
                            y_translate = centroid[0]
                            if prev_centroid != None:
                                ydiff = centroid[0] - prev_centroid[0]
                                if ydiff > 0:
                                    y_plus_count = y_plus_count + 1
                                elif ydiff < 0:
                                    y_minus_count = y_minus_count + 1
                                xdiff = centroid[1] - prev_centroid[1]
                                if xdiff > 0:
                                    x_plus_count = x_plus_count + 1
                                elif xdiff < 0:
                                    x_minus_count = x_minus_count + 1
                            prev_centroid = centroid

                        if hotspot_area.direction_x_constraint != '' or hotspot_area.direction_y_constraint != '':
                            first_centroid = item['first_centroid']
                            x_translate = x_translate - first_centroid[1]
                            y_translate = y_translate - first_centroid[0]
                            print("Translation = " + str(x_translate) + "," + str(y_translate))
                            print("Constraint = " + hotspot_area.direction_x_constraint + ',' + str(
                                hotspot_area.direction_y_constraint))

                            if hotspot_area.direction_y_constraint != '':
                                y_diff_required = int(hotspot_area.direction_y_constraint)

                                if y_diff_required > 0 and y_translate < y_diff_required:
                                    print("FAIL Y-")
                                    check_direction_passed = False
                                elif y_diff_required < 0 and y_translate > y_diff_required:
                                    print("FAIL Y+")
                                    check_direction_passed = False

                            if hotspot_area.direction_x_constraint != '':
                                x_diff_required = int(hotspot_area.direction_x_constraint)

                                if x_diff_required > 0 and x_translate < x_diff_required:
                                    print("FAIL X-")
                                    check_direction_passed = False
                                elif x_diff_required < 0 and x_translate > x_diff_required:
                                    print("FAIL X+")
                                    check_direction_passed = False

                        # Also store direction as a part of detected object property
                        h_class = 'h0'
                        v_class = 'v0'
                        if y_plus_count > y_minus_count: v_class = 'v+'
                        if y_plus_count < y_minus_count: v_class = 'v-'
                        if x_plus_count > x_minus_count: h_class = 'h+'
                        if x_plus_count < x_minus_count: h_class = 'h-'

                        self.add_class_to_tracked_object(item, [h_class, v_class])

                        # We move class confirmation to external class so that they are not depend on each other
                        class_strings = []
                        if self.custom_class_finalizer is not None:
                            class_strings = self.custom_class_finalizer.finalize_from_stat(item['class_count'])
                        else:
                            class_strings = [str(np.argmax(item['class_count']))]

                        print('[CONFIRM DETECTION]: ' + str(item['object_id']) + ' of CLASS: ' + str(class_strings))

                        ##################################################################
                        ##

                        '''
                        print("y+ = " + str(y_plus_count))
                        print("y- = " + str(y_minus_count))
                        print("x+ = " + str(x_plus_count))
                        print("x- = " + str(x_minus_count))
                        '''

                        if check_direction_passed == True:
                            print('[CHECK DIRECTION PASSED]: ' + str(item['object_id']) + ' of CLASS: ' + str(
                                class_strings))

                            fine_grained_class_string = ','.join(class_strings)

                            # Also record count for specific hotspot too
                            hotspot_count_map = hotspot_status.count_map
                            hotspot_interested_count_map = hotspot_status.interested_count_map
                            hotspot_engaged_count_map = hotspot_status.engaged_count_map

                            # Also record count for specific hotspot (realtime)
                            hotspot_count_map_realtime = hotspot_status_realtime.count_map
                            hotspot_interested_count_map_realtime = hotspot_status_realtime.interested_count_map
                            hotspot_engaged_count_map_realtime = hotspot_status_realtime.engaged_count_map

                            # If ROI is the type to count customer attendance, we perform additional count here
                            if hotspot_area.type == 6 and 'attended_at' in item:
                                self.update_attended_count_map(hotspot_status_realtime, hotspot_status,
                                                               self.atteneded_count_map, item, class_strings)

                            self.count_map['all'] = self.count_map['all'] + 1
                            hotspot_count_map['all'] = hotspot_count_map['all'] + 1
                            hotspot_count_map_realtime['all'] = hotspot_count_map_realtime['all'] + 1
                            for class_string in class_strings:
                                if class_string in self.count_map:
                                    self.count_map[class_string] = self.count_map[class_string] + 1
                                else:
                                    self.count_map[class_string] = 1
                                if class_string in hotspot_count_map:
                                    hotspot_count_map[class_string] = hotspot_count_map[class_string] + 1
                                else:
                                    hotspot_count_map[class_string] = 1
                                if class_string in hotspot_count_map_realtime:
                                    hotspot_count_map_realtime[class_string] = hotspot_count_map_realtime[
                                                                                   class_string] + 1
                                else:
                                    hotspot_count_map_realtime[class_string] = 1
                            # Also add count to fine grained class string
                            if fine_grained_class_string in hotspot_count_map:
                                hotspot_count_map[fine_grained_class_string] = hotspot_count_map[
                                                                                   fine_grained_class_string] + 1
                            else:
                                hotspot_count_map[fine_grained_class_string] = 1
                            if fine_grained_class_string in hotspot_count_map_realtime:
                                hotspot_count_map_realtime[fine_grained_class_string] = hotspot_count_map_realtime[
                                                                                            fine_grained_class_string] + 1
                            else:
                                hotspot_count_map_realtime[fine_grained_class_string] = 1

                            if is_interested:

                                self.interested_count_map['all'] = self.interested_count_map['all'] + 1
                                hotspot_interested_count_map['all'] = hotspot_interested_count_map['all'] + 1
                                hotspot_interested_count_map_realtime['all'] = hotspot_interested_count_map_realtime[
                                                                                   'all'] + 1
                                for class_string in class_strings:
                                    if class_string in self.interested_count_map:
                                        self.interested_count_map[class_string] = self.interested_count_map[
                                                                                      class_string] + 1
                                    else:
                                        self.interested_count_map[class_string] = 1
                                    if class_string in hotspot_interested_count_map:
                                        hotspot_interested_count_map[class_string] = hotspot_interested_count_map[
                                                                                         class_string] + 1
                                    else:
                                        hotspot_interested_count_map[class_string] = 1
                                    if class_string in hotspot_interested_count_map_realtime:
                                        hotspot_interested_count_map_realtime[class_string] = \
                                            hotspot_interested_count_map_realtime[class_string] + 1
                                    else:
                                        hotspot_interested_count_map_realtime[class_string] = 1
                                # Also add count to fine grained class string
                                if fine_grained_class_string in hotspot_interested_count_map:
                                    hotspot_interested_count_map[fine_grained_class_string] = \
                                        hotspot_interested_count_map[fine_grained_class_string] + 1
                                else:
                                    hotspot_interested_count_map[fine_grained_class_string] = 1
                                if fine_grained_class_string in hotspot_interested_count_map_realtime:
                                    hotspot_interested_count_map_realtime[fine_grained_class_string] = \
                                        hotspot_interested_count_map_realtime[fine_grained_class_string] + 1
                                else:
                                    hotspot_interested_count_map_realtime[fine_grained_class_string] = 1

                            if is_engaged:

                                self.engaged_count_map['all'] = self.engaged_count_map['all'] + 1
                                hotspot_engaged_count_map['all'] = hotspot_engaged_count_map['all'] + 1
                                hotspot_engaged_count_map_realtime['all'] = hotspot_engaged_count_map_realtime[
                                                                                'all'] + 1
                                for class_string in class_strings:
                                    if class_string in self.engaged_count_map:
                                        self.engaged_count_map[class_string] = self.engaged_count_map[class_string] + 1
                                    else:
                                        self.engaged_count_map[class_string] = 1
                                    if class_string in hotspot_engaged_count_map:
                                        hotspot_engaged_count_map[class_string] = hotspot_engaged_count_map[
                                                                                      class_string] + 1
                                    else:
                                        hotspot_engaged_count_map[class_string] = 1
                                    if class_string in hotspot_engaged_count_map_realtime:
                                        hotspot_engaged_count_map_realtime[class_string] = \
                                            hotspot_engaged_count_map_realtime[class_string] + 1
                                    else:
                                        hotspot_engaged_count_map_realtime[class_string] = 1
                                # Also add count to fine grained class string
                                if fine_grained_class_string in hotspot_engaged_count_map:
                                    hotspot_engaged_count_map[fine_grained_class_string] = hotspot_engaged_count_map[
                                                                                               fine_grained_class_string] + 1
                                else:
                                    hotspot_engaged_count_map[fine_grained_class_string] = 1
                                if fine_grained_class_string in hotspot_engaged_count_map_realtime:
                                    hotspot_engaged_count_map_realtime[fine_grained_class_string] = \
                                        hotspot_engaged_count_map_realtime[fine_grained_class_string] + 1
                                else:
                                    hotspot_engaged_count_map_realtime[fine_grained_class_string] = 1

            self.hotspot_status_map[hotspot_id].tracking_list = new_tracking_list
