import cv2
import numpy as np
import os
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from wide_resnet import WideResNet

from object_classifier import ObjectClassifier


class FaceAgePredictor(ObjectClassifier):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('AgePredictor::init_model')
        MODEL_PATH = '../age_models/weights.28-3.73.hdf5'
        self.img_size = 64
        self.model = WideResNet(self.img_size, depth=16, k=8)()
        self.model.load_weights(MODEL_PATH)

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        pass

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('AgePredictor::perform_classification')
        age_face = cv2.resize(image, (self.img_size, self.img_size))
        ages_result = self.model.predict(np.array([age_face]))
        ages = np.arange(0, 101).reshape(101, 1)
        age = ages_result[1].dot(ages).flatten()

        # Quantize quantification into 3 classes here
        age_class = 'adult'
        if age < 15:
            age_class = 'kid'
        elif age > 50:
            age_class = 'elder'

        return age_class
