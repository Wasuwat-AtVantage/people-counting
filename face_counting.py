import cv2
import numpy as np
from csv_export import CSVExporter
from rpi_utils.uploader import Uploader


# Custom class finalizer for Face Counting project
class FaceCountingClassFinalizer:
    def get_count_of(self, count_stat, class_name):
        if class_name in count_stat:
            return count_stat[class_name]
        else:
            return 0

    def finalize_from_stat(self, count_stat):
        finalized_class = []

        count_emotions = np.array([
            self.get_count_of(count_stat, 'angry'),
            self.get_count_of(count_stat, 'disgusted'),
            self.get_count_of(count_stat, 'fear'),
            self.get_count_of(count_stat, 'happy'),
            self.get_count_of(count_stat, 'neutral'),
            self.get_count_of(count_stat, 'sad'),
            self.get_count_of(count_stat, 'surprised'),
            self.get_count_of(count_stat, 'unknown')
        ])
        emotion_index = np.argmax(count_emotions)
        count_h0 = self.get_count_of(count_stat, 'h0')
        count_hp = self.get_count_of(count_stat, 'h+')
        count_hm = self.get_count_of(count_stat, 'h-')
        count_v0 = self.get_count_of(count_stat, 'v0')
        count_vp = self.get_count_of(count_stat, 'v+')
        count_vm = self.get_count_of(count_stat, 'v-')

        if emotion_index == 0:
            finalized_class.append('angry')
        elif emotion_index == 1:
            finalized_class.append('disgusted')
        elif emotion_index == 2:
            finalized_class.append('fear')
        elif emotion_index == 3:
            finalized_class.append('happy')
        elif emotion_index == 4:
            finalized_class.append('neutral')
        elif emotion_index == 5:
            finalized_class.append('sad')
        elif emotion_index == 6:
            finalized_class.append('surprised')
        elif emotion_index == 7:
            finalized_class.append('unknown')
        if count_h0 > 0:
            finalized_class.append('h0')
        if count_hp > 0:
            finalized_class.append('h+')
        if count_hm > 0:
            finalized_class.append('h-')
        if count_v0 > 0:
            finalized_class.append('v0')
        if count_vp > 0:
            finalized_class.append('v+')
        if count_vm > 0:
            finalized_class.append('v-')
        return finalized_class


class FaceCountingLogics:
    def __init__(self, args):
        self.args = args
        self.csv_exporter = CSVExporter()
        self.uploader = Uploader(args)

    def create_class_finalizer(self):
        return FaceCountingClassFinalizer()

    def set_roi_json(self, roi_json):
        self.uploader.set_roi_json(roi_json)

    def set_roi_areas(self, roi_areas):
        self.uploader.set_roi_areas(roi_areas)

    def set_roi_filename(self, roi_filename):
        self.uploader.set_roi_filename(roi_filename)

    def set_object_tracker(self, object_tracker):
        self.uploader.set_object_tracker(object_tracker)

    def create_detector(self):
        # Case of human detection, we have choices of ssd and yolov3
        if self.args["detector"] == "cpu":
            from face_detector import FaceDetector
            detector = FaceDetector()
        elif self.args["detector"] == "edge":
            from face_detector_tensorrt import FaceDetectorTensorRT
            detector = FaceDetectorTensorRT()
        return detector

    def create_custom_classifiers(self):
        custom_classifiers = []
        if self.args['USE_CUSTOM_CLASSIFIER']:
            emotion_predictor = None
            if self.args["detector"] == "cpu":
                from face_emotion_predictor import FaceEmotionPredictor
                emotion_predictor = FaceEmotionPredictor()
            elif self.args["detector"] == "edge":
                from face_emotion_predictor_tensorrt import FaceEmotionPredictorTensorRT
                emotion_predictor = FaceEmotionPredictorTensorRT()
            if emotion_predictor is not None:
                custom_classifiers.append(emotion_predictor)

        return custom_classifiers

    def draw_roi_areas(self, frame, object_tracker, roi_area_configs):
        for idx, roi in enumerate(roi_area_configs):
            roi_top = roi.top
            roi_left = roi.left
            roi_bottom = roi.bottom
            roi_right = roi.right

            is_occupied = object_tracker.hotspot_status_map[idx].is_occupied

            if roi.type == 1:  # Sale
                cv2.drawContours(frame, [roi.polyNP], -1, (0, 255, 255), 2)
                # cv2.rectangle(frame, (roi_left, roi_top), (roi_right, roi_bottom), (0, 255, 255), 2)
            else:  # Operation + Both
                if is_occupied:
                    cv2.drawContours(frame, [roi.polyNP], -1, (0, 255, 255), 2)
                    # cv2.rectangle(frame, (roi_left, roi_top), (roi_right, roi_bottom), (0, 255, 255), 2)
                else:
                    cv2.drawContours(frame, [roi.polyNP], -1, (0, 0, 255), 10)
                    # cv2.rectangle(frame, (roi_left, roi_top), (roi_right, roi_bottom), (0, 0, 255), 10)

            cv2.putText(frame, str(idx), (int((roi_left + roi_right) / 2), int((roi_top + roi_bottom) / 2)),
                        cv2.FONT_HERSHEY_PLAIN, 6, (255, 255, 0), 3)

    def draw_tracked_object_frame(self, frame, tracked_object, rect, class_ids, timestamp):
        (startX, startY, endX, endY) = rect
        centroid = tracked_object['centroid_list'][-1]
        cv2.rectangle(frame, (startX, startY), (endX, endY), (128, 128, 128), 2)
        cv2.putText(frame,
                    ','.join(map(lambda a: str(a), tracked_object['rois_history'])) + '(' + ','.join(class_ids) + ')',
                    (int(centroid[1]) - 10, int(centroid[0]) - 40),
                    cv2.FONT_HERSHEY_PLAIN, 1.5, (255, 255, 128), 2)

    def draw_tracked_object(self, frame, hotspot_area, tracked_object, rect, class_ids, timestamp, transparent_frame):
        (startX, startY, endX, endY) = rect
        item_lifetime = timestamp - tracked_object['ts']
        if hotspot_area.type != 2:  # If Sale or Both
            is_interested = item_lifetime > hotspot_area.interesting_threshold  # Interested threshold
            is_engaged = item_lifetime > hotspot_area.engagement_threshold  # engaged threshold
            if is_engaged:
                cv2.rectangle(frame, (startX, startY), (endX, endY), (0, 255, 0), 10)
            elif is_interested:
                cv2.rectangle(frame, (startX, startY), (endX, endY), (255, 0, 255), 6)
            else:
                cv2.rectangle(frame, (startX, startY), (endX, endY), (255, 255, 255), 2)
        else:
            cv2.rectangle(frame, (startX, startY), (endX, endY), (255, 255, 255), 2)

        object_id = str(tracked_object['object_id'])
        centroid = tracked_object['centroid_list'][-1]
        cv2.putText(frame, '(' + ','.join(class_ids) + ') ' + str(int(item_lifetime)) + 's',
                    (int(centroid[1]) - 10, int(centroid[0]) - 10),
                    cv2.FONT_HERSHEY_PLAIN, 1.5, (128, 128, 255), 2)

        # If has customer attended data, just draw it here
        if 'attended_at' in tracked_object:
            cv2.putText(frame, 'Attended in ' + str(int(tracked_object['attended_at'])) + 's',
                        (int(centroid[1]) - 10, int(centroid[0]) + 10),
                        cv2.FONT_HERSHEY_PLAIN, 1.5, (128, 255, 128), 2)

    def draw_additional_infos(self, frame, object_tracker, tracked_object_list_map, roi_area_configs, timestamp):
        (H, W) = frame.shape[:2]

        # Store latest frame to uploader object (so it can cache and upload to server)
        # In case of edge deployment, we log report data to server side
        if True or self.args['deployment_type'] == 'edge':
            self.uploader.cache_last_frame(frame)

        for idx, hotspot_id in enumerate(tracked_object_list_map):
            hotspot_area = roi_area_configs[hotspot_id]

            if hotspot_area.type != 2:

                # Additional engage / interesting count from tracked objects.
                # Their status is not finalized, but we want to include them in engage / interest display
                unconfirmed_count = 0
                unconfirmed_interested = 0
                unconfirmed_engaged = 0
                tracked_objects = tracked_object_list_map[hotspot_id]
                for tracked_object in tracked_objects:
                    item_lifetime = timestamp - tracked_object['ts']
                    if tracked_object['count'] > hotspot_area.count_threshold:  # Count threshold
                        unconfirmed_count = unconfirmed_count + 1
                    if item_lifetime > hotspot_area.interesting_threshold:  # Interested threshold
                        unconfirmed_interested = unconfirmed_interested + 1
                    if item_lifetime > hotspot_area.engagement_threshold:  # Engaged threshold
                        unconfirmed_engaged = unconfirmed_engaged + 1

                count_map = object_tracker.get_count_data_for_hotspot(hotspot_id)
                count_all = count_map['all'] + unconfirmed_count
                interested_count_map = object_tracker.get_interested_count_data_for_hotspot(hotspot_id)
                interested_count_all = interested_count_map['all'] + unconfirmed_interested
                engaged_count_map = object_tracker.get_engaged_count_data_for_hotspot(hotspot_id)
                engaged_count_all = engaged_count_map['all'] + unconfirmed_engaged

                '''    
                timer_text = "{:.2f}".format(time.clock())
                cv2.putText(frame, timer_text, (10, H - ((1 * 20) + 20)),
                    cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 1)
                '''
                engaged_count_text = 'ROI: ' + str(hotspot_id) + ', Engaged: ' + str(engaged_count_all)
                cv2.putText(frame, engaged_count_text, (10, H - (((idx * 3 + 0) * 20) + 20)),
                            cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), 1)
                interested_count_text = 'ROI: ' + str(hotspot_id) + ', Interested: ' + str(interested_count_all)
                cv2.putText(frame, interested_count_text, (10, H - (((idx * 3 + 1) * 20) + 20)),
                            cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), 1)
                count_text = 'ROI: ' + str(hotspot_id) + ', Count: ' + str(count_all)
                cv2.putText(frame, count_text, (10, H - (((idx * 3 + 2) * 20) + 20)),
                            cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), 1)

    def init_log(self, log_path):
        # Raw CSV log file to be accumulated later
        print('Log Raw CSV Path: ' + log_path)
        self.csv_exporter.init_file(log_path)

    def update_log(self, object_tracker, roi_area_configs, log_path, video_path, date_string, time_string, meta_datum):
        data_model = {
            'hotspot_stat': [],
        }

        # print(str(ROI_AREA_CONFIGS))
        for hotspot_id, hotspot in enumerate(roi_area_configs):
            roi_count_map = object_tracker.get_count_data_for_hotspot(hotspot_id)
            roi_interested_count_map = object_tracker.get_interested_count_data_for_hotspot(hotspot_id)
            roi_engaged_count_map = object_tracker.get_engaged_count_data_for_hotspot(hotspot_id)
            roi_events = object_tracker.hotspot_status_map[hotspot_id].events
            roi_attended_count_map = object_tracker.get_attended_count_data_for_hotspot(hotspot_id)
            roi_json_report = {
                'roi_type': hotspot.type,
                'count_map': roi_count_map,
                'interested_count_map': roi_interested_count_map,
                'engaged_count_map': roi_engaged_count_map,
                'attended_count_map': roi_attended_count_map,
                'events': roi_events,
                'interval_occupied_count': object_tracker.hotspot_status_map[hotspot_id].interval_occupied_count,
                'interval_unoccupied_count': object_tracker.hotspot_status_map[hotspot_id].interval_unoccupied_count,
            }

            data_model['hotspot_stat'].append(roi_json_report)

            # Reset counter states
            object_tracker.hotspot_status_map[hotspot_id].cut_off_interval()

        # In case of edge deployment, we log report data to server side
        if True or self.args['deployment_type'] == 'edge' or args['deployment_type'] == 'edge_noop':
            self.uploader.upload_file(data_model)

        self.csv_exporter.append_to_file(log_path, data_model, video_path, date_string, time_string, meta_datum)

    def update_log_realtime(self, object_tracker, roi_area_configs, log_path, video_path, date_string, time_string,
                            meta_datum):
        data_model = {
            'hotspot_stat': [],
        }

        tracked_object_list_map = object_tracker.get_object_list()

        # print(str(ROI_AREA_CONFIGS))
        for hotspot_id, hotspot in enumerate(roi_area_configs):

            # Additional engage / interesting count from tracked objects.
            # Their status is not finalized, but we want to include them in engage / interest display
            unconfirmed_count = 0
            unconfirmed_interested = 0
            unconfirmed_engaged = 0
            tracked_objects = tracked_object_list_map[hotspot_id]
            for tracked_object in tracked_objects:
                if tracked_object['count'] > hotspot.count_threshold:  # Count threshold
                    unconfirmed_count = unconfirmed_count + 1
                if tracked_object['count'] > hotspot.interesting_threshold:  # Interested threshold
                    unconfirmed_interested = unconfirmed_interested + 1
                if tracked_object['count'] > hotspot.engagement_threshold:  # Engaged threshold
                    unconfirmed_engaged = unconfirmed_engaged + 1

            roi_count_map = object_tracker.get_count_data_for_hotspot_realtime(hotspot_id)
            roi_interested_count_map = object_tracker.get_interested_count_data_for_hotspot_realtime(hotspot_id)
            roi_engaged_count_map = object_tracker.get_engaged_count_data_for_hotspot_realtime(hotspot_id)
            roi_events = object_tracker.hotspot_status_map_realtime[hotspot_id].events
            roi_attended_count_map = object_tracker.get_attended_count_data_for_hotspot_realtime(hotspot_id)

            if 'all' in roi_count_map:
                roi_count_map['all'] = roi_count_map['all'] + 0  # unconfirmed_count
            else:
                roi_count_map['all'] = unconfirmed_count

            if 'all' in roi_interested_count_map:
                roi_interested_count_map['all'] = roi_interested_count_map['all'] + 0  # unconfirmed_interested
            else:
                roi_interested_count_map['all'] = unconfirmed_interested

            if 'all' in roi_engaged_count_map:
                roi_engaged_count_map['all'] = roi_engaged_count_map['all'] + 0  # unconfirmed_engaged
            else:
                roi_engaged_count_map['all'] = unconfirmed_engaged

            roi_json_report = {
                'roi_type': hotspot.type,
                'count_map': roi_count_map,
                'interested_count_map': roi_interested_count_map,
                'engaged_count_map': roi_engaged_count_map,
                'attended_count_map': roi_attended_count_map,
                'events': roi_events,
                'time_string': time_string,
                'heatmap': json.dumps(object_tracker.hotspot_status_map[hotspot_id].heatmap)
            }

            data_model['hotspot_stat'].append(roi_json_report)

            # Reset counter states
            object_tracker.hotspot_status_map_realtime[hotspot_id].cut_off_interval()

        # In case of edge deployment, we log report data to server side
        if True or self.args['deployment_type'] == 'edge' or args['deployment_type'] == 'edge_noop':
            self.uploader.push_realtime_data(data_model)

    def finalize_log(self, object_tracker, roi_area_configs, finalize_log_path):
        # Write result file
        print('[INFO] Writing result file to: ' + finalize_log_path)
        count_map = object_tracker.get_count_data()
        interested_count_map = object_tracker.get_interested_count_data()
        engaged_count_map = object_tracker.get_engaged_count_data()
        json_report = {
            'count_map': count_map,
            'interested_count_map': interested_count_map,
            'engaged_count_map': engaged_count_map,
            'hotspot_stat': [],
        }
        # Also write result for each ROI
        for hotspot_id, hotspot in enumerate(roi_area_configs):
            roi_count_map = object_tracker.get_count_data_for_hotspot(hotspot_id)
            roi_interested_count_map = object_tracker.get_interested_count_data_for_hotspot(hotspot_id)
            roi_engaged_count_map = object_tracker.get_engaged_count_data_for_hotspot(hotspot_id)
            roi_events = object_tracker.hotspot_status_map[hotspot_id].events
            roi_json_report = {
                'count_map': roi_count_map,
                'interested_count_map': roi_interested_count_map,
                'engaged_count_map': roi_engaged_count_map,
                'events': roi_events,
                'interval_occupied_count': object_tracker.hotspot_status_map[hotspot_id].interval_occupied_count,
                'interval_unoccupied_count': object_tracker.hotspot_status_map[hotspot_id].interval_unoccupied_count,
            }
            json_report['hotspot_stat'].append(roi_json_report)
        with open(finalize_log_path, 'w', encoding='utf8') as outfile:
            json.dump(json_report, outfile)
