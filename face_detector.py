import cv2
import numpy as np
import os

from object_detector import ObjectDetector


class FaceDetector(ObjectDetector):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('FaceDetector::init_model')
        self.model = cv2.CascadeClassifier('../face_detector_model/haarcascade_frontalface_default.xml')

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        pass

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        # print(str(image.shape))
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # print(str(gray.shape))
        faces = self.model.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=5)

        rects = []
        for (x, y, w, h) in faces:
            rects.append((x, y, x + w, y + h, 0))  # 'person'))

        return rects
