import cv2
import numpy as np
import os

from object_detector import ObjectDetector

import platform

if platform.processor() == 'x86_64':
    from face_detector_model.x64.utils.mtcnn import TrtMtcnn
else:
    from face_detector_model.jetson.utils.mtcnn import TrtMtcnn


class FaceDetectorTensorRT(ObjectDetector):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('FaceDetectorTensorRT::init_model')
        self.model = TrtMtcnn()

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        pass

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        dets, landmarks = self.model.detect(image, minsize=40)
        h, w, _ = image.shape
        rects = []
        for (x, y, x2, y2, conf) in dets:
            ww = (x2 - x) / 8
            x = max(0, x - ww)
            x2 = min(w - 1, x2 + ww)
            rects.append((int(x), int(y), int(x2), int(y2), 0))  # 'person'))

        return rects
