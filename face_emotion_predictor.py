import cv2
import numpy as np
import os
from keras.preprocessing.image import img_to_array
from keras.models import load_model, model_from_json, Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D
from keras.layers import MaxPooling2D

from object_classifier import ObjectClassifier


class FaceEmotionPredictor(ObjectClassifier):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('FaceEmotionPredictor::init_model')

        # load and create model
        MODEL_FILE = '../face_emotion_cnn/emotion_model.h5'

        print("Loaded model from disk")
        # Create the model
        self.model = Sequential()

        self.model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(48, 48, 1)))
        self.model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))

        self.model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))
        self.model.add(MaxPooling2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))

        self.model.add(Flatten())
        self.model.add(Dense(1024, activation='relu'))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(7, activation='softmax'))

        self.model.load_weights(MODEL_FILE)

        self.img_size = 48
        self.class_label_map = {0: "angry", 1: "disgusted", 2: "fear", 3: "happy", 4: "neutral", 5: "sad",
                                6: "surprised"}
        self.confidence = 0.9
        if ("confidence" in config):
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('GenderPredictor::perform_classification')
        image = cv2.resize(image, (self.img_size, self.img_size))
        gray_face = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        gray_face = np.expand_dims(np.expand_dims(gray_face, 0), -1)
        # print(str(gray_face.shape))
        prediction = self.model.predict(gray_face)
        print('EMOTION PREDICTION => ' + str(prediction))
        emotion = np.argmax(prediction)
        conf = prediction[0][emotion]
        if prediction[0][emotion] < self.confidence:
            return 'unknown'
        return self.class_label_map[emotion]
