import cv2
import numpy as np
import os
from keras.preprocessing.image import img_to_array
from keras.models import load_model

from object_classifier import ObjectClassifier


class FaceGenderPredictor(ObjectClassifier):
    def __init__(self):

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('GenderPredictor::init_model')
        MODEL_PATH = '../gender_models/simple_CNN.81-0.96.hdf5'
        self.model = load_model(MODEL_PATH, compile=False)
        self.img_size = 48
        self.class_label_map = {0: 'female', 1: 'male'}
        self.target_size = self.model.input_shape[1:3]
        self.confidence = 0.5
        if ("confidence" in config):
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    def preprocess_input(self, x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('GenderPredictor::perform_classification')
        image = cv2.resize(image, (self.img_size, self.img_size))
        rgb_face = self.preprocess_input(image, False)
        rgb_face = np.expand_dims(rgb_face, 0)
        gender_prediction = self.model.predict(rgb_face)
        gender = np.argmax(gender_prediction)
        return self.class_label_map[gender]
