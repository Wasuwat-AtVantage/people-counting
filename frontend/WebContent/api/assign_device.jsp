<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  String placeId = request.getParameter("place_id");
  String deviceId = request.getParameter("device_id");

  if (placeId == null || placeId.trim().length() == 0
    || deviceId == null || deviceId.trim().length() == 0
  ) {
    %>
    {
      "s": false,
      "m": "Place ID and Device ID cannot be empty"
    }
    <%
    return;
  }

  try {
    PlaceService.getInstance().addDevice(deviceId, placeId);
  } catch(Exception e) {
    %>
    {
      "s": false,
      "m": "<%=e.getMessage() %>"
    }
    <%
    return;  
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", deviceId);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>