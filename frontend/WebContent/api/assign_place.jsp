<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  String placeId = request.getParameter("place_id");
  String projectId = request.getParameter("project_id");

  if (placeId == null || placeId.trim().length() == 0
    || projectId == null || projectId.trim().length() == 0
  ) {
    %>
    {
      "s": false,
      "m": "Place ID and Project ID cannot be empty"
    }
    <%
    return;
  }

  try {
    ProjectService.getInstance().addPlace(projectId, placeId);
  } catch(Exception e) {
    %>
    {
      "s": false,
      "m": "<%=e.getMessage() %>"
    }
    <%
    return;  
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", placeId);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>