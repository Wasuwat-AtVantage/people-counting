<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  String userId = request.getParameter("user_id");
  String projectId = request.getParameter("project_id");
  String role = request.getParameter("role");

  if (userId == null || userId.trim().length() == 0
    || projectId == null || projectId.trim().length() == 0
  ) {
    %>
    {
      "s": false,
      "m": "User ID and Project ID cannot be empty"
    }
    <%
    return;
  }

  try {
    ProjectService.getInstance().addUser(projectId, userId, role);
  } catch(Exception e) {
    %>
    {
      "s": false,
      "m": "<%=e.getMessage() %>"
    }
    <%
    return;  
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", userId);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>