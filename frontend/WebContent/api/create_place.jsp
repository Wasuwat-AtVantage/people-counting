<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  String extId = request.getParameter("ext_id");
  String placeName = request.getParameter("place_name");
  String placeType = request.getParameter("place_type");
  String info = request.getParameter("info");

  if (extId == null || extId.trim().length() == 0) {
    extId = placeName;
  }
  if (placeName == null || placeName.trim().length() == 0) {
    %>
    {
      "s": false,
      "m": "Place name cannot be empty"
    }
    <%
    return;
  }

  Place place = new Place();
  place.setExtId(extId);
  place.setPlaceName(placeName);
  place.setPlaceType(placeType);
  place.setInfo(info);
  try {
    PlaceService.getInstance().createPlace(place);
  } catch(Exception e) {
    %>
    {
      "s": false,
      "m": "<%=e.getMessage() %>"
    }
    <%
    return;
  }

  // If project is specified, add newly created place to the project
  String projectId = request.getParameter("project_id");
  if (projectId != null && projectId.trim().length() > 0) {
    try {
      ProjectService.getInstance().addPlace(projectId, place.getId());
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "Place was created, but another error occurred: <%=e.getMessage() %>"
      }
      <%
      return;  
    }
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", place.getId());
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>