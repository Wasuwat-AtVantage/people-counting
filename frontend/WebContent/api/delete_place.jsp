<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  String id = request.getParameter("id");

  if (id == null || id.trim().length() == 0) {
    %>
    {
      "s": false,
      "m": "ID cannot be empty"
    }
    <%
    return;
  }

  try {
    PlaceService.getInstance().delete(id);
  } catch(Exception e) {
    %>
    {
      "s": false,
      "m": "<%=e.getMessage() %>"
    }
    <%
    return;
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", id);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>