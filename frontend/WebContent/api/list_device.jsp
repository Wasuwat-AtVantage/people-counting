<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
  sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));

  PollingStateManager edgeDeviceManager = PollingStateManager.getInstance();

  List<Object> list = new ArrayList<>();

  // If place is specified, list device in the place
  String placeId = request.getParameter("place_id");
  if (placeId != null && placeId.trim().length() > 0) {
    try {
      List<String> listOfIds = PlaceService.getInstance().listAllDeviceInPlace(placeId);
      for (int i=0;i<listOfIds.size();i++) {
        String id = listOfIds.get(i);
        EdgeDevice device = edgeDeviceManager.getEdgeDevice(id);
        if (device == null) {
          // TODO: Better handle the prefix path.
          String summarizedReportPrefix = "/report_data/root/sharedfolder/edgedecvices/" + id + ".json";
          String reportFileName = "/report_data/root/sharedfolder/edgedecvices/" + id + ".json";
          File reportFile = new File(reportFileName);
          boolean hasReport = reportFile.exists();
          if (!hasReport) {
            reportFileName = "";
          }
          Map<String, Object> obj = new HashMap<>();
          obj.put("type", "device");
          obj.put("id", id);
          obj.put("name", "-");
          obj.put("report", reportFileName);
          obj.put("version", "-");
          obj.put("lastUpdated", "-");
          obj.put("watched", Boolean.FALSE);
      
          list.add(obj);  
        } else {
          // TODO: Better handle the prefix path.
          String summarizedReportPrefix = "/report_data/root/sharedfolder/edgedecvices/" + device.id + ".json";
          String reportFileName = "/report_data/root/sharedfolder/edgedecvices/" + device.id + ".json";
          File reportFile = new File(reportFileName);
          boolean hasReport = reportFile.exists();
          if (!hasReport) {
            reportFileName = "";
          }
          Map<String, Object> obj = new HashMap<>();
          obj.put("type", "device");
          obj.put("id", device.id);
          obj.put("name", device.name);
          obj.put("report", reportFileName);
          obj.put("version", device.version);
          obj.put("lastUpdated", sdf.format(device.lastUpdated));
          obj.put("watched", device.watched);
      
          list.add(obj);  
        }
      }
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "<%=e.getMessage() %>"
      }
      <%
      return;
    }
  } else {
    try {
      EdgeDevice[] listOfDevices = edgeDeviceManager.listEdgeDevices(null);
      for (int i=0;i<listOfDevices.length;i++) {

        // TODO: Better handle the prefix path.
        String summarizedReportPrefix = "/report_data/root/sharedfolder/edgedecvices/" + listOfDevices[i].id + ".json";
        String reportFileName = "/report_data/root/sharedfolder/edgedecvices/" + listOfDevices[i].id + ".json";
        File reportFile = new File(reportFileName);
        boolean hasReport = reportFile.exists();
        if (!hasReport) {
          reportFileName = "";
        }
        Map<String, Object> obj = new HashMap<>();
        obj.put("type", "device");
        obj.put("id", listOfDevices[i].id);
        obj.put("name", listOfDevices[i].name);
        obj.put("report", reportFileName);
        obj.put("version", listOfDevices[i].version);
        obj.put("lastUpdated", sdf.format(listOfDevices[i].lastUpdated));
        obj.put("watched", listOfDevices[i].watched);
    
        list.add(obj);
      }
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "<%=e.getMessage() %>"
      }
      <%
      return;
    }  
  }
  

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", list);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>