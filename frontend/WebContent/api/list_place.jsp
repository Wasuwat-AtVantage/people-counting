<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%
  List<Place> list = null;

  // If project is specified, list place in the project
  String projectId = request.getParameter("project_id");
  if (projectId != null && projectId.trim().length() > 0) {
    try {
      list = ProjectService.getInstance().listAllPlaceInProject(projectId);
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "<%=e.getMessage() %>"
      }
      <%
      return;
    }
  } else {
    try {
      list = PlaceService.getInstance().select();
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "<%=e.getMessage() %>"
      }
      <%
      return;
    }  
  }
  

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", list);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>