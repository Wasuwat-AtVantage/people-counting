<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  // Check session
  boolean isLoggedIn = false;
  String sessionId = (String)session.getAttribute("sessionId");
  String userId = null;	
  if (sessionId != null) {
    userId = UserService.getInstance().getUserIdFromSessionId(sessionId);		
    isLoggedIn = (userId != null);
  }
  if (!isLoggedIn) {
    %>
    {
      "s": false,
      "m": "Not logged in."
    }
    <%
    return;
  }

  List<Project> list = null;
  User user = UserService.getInstance().get(userId);
  if ("A".equals(user.getUserType())) {
    try {
      list = ProjectService.getInstance().select();
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "<%=e.getMessage() %>"
      }
      <%
      return;
    }      
  } else {
    try {
      list = UserService.getInstance().listProjectOfUser(userId);
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "<%=e.getMessage() %>"
      }
      <%
      return;
    }      
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", list);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>