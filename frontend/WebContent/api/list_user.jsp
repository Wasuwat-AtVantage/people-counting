<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%
  List<User> list = null;

  // If project is specified, list user in the project
  String projectId = request.getParameter("project_id");
  if (projectId != null && projectId.trim().length() > 0) {
    try {
      list = ProjectService.getInstance().listAllUserInProject(projectId);
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "<%=e.getMessage() %>"
      }
      <%
      return;
    }
  } else {
    try {
      list = UserService.getInstance().select();
    } catch(Exception e) {
      %>
      {
        "s": false,
        "m": "<%=e.getMessage() %>"
      }
      <%
      return;
    }  
  }

  // Temporary code to get role of user for the first project
  String userProjectId = "";
  String userProjectRole = "";
  for (int i=0;i<list.size();i++) {
    String userId = list.get(i).getId();
    List<Project> projects = UserService.getInstance().listProjectOfUser(userId);
    if (projects.size() > 0) {
      userProjectId = projects.get(0).getId();
      userProjectRole = UserService.getInstance().getProjectRoleOfUser(userId, projects.get(0).getId());
    }
    list.get(i).setProjectId(userProjectId);
    list.get(i).setUserType(userProjectRole);
  }
  
  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", list);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>