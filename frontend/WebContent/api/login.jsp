<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  String username = request.getParameter("username");
  String password = request.getParameter("password");
  if (
    username==null||username.trim().length()==0
    ||password==null||password.trim().length()==0
  ) {
    %>
    {
      "s": false,
      "m": "Invalid authentication."
    }
    <%
    return;
  }
  
  String sessionId = null;
  try {
    sessionId = UserService.getInstance().login(username, password);
  } catch(Exception e) {
    %>
    {
      "s": false,
      "m": "<%=e.getMessage() %>"
    }
    <%
    return;
  }

  if (sessionId != null) {
    session.setAttribute("sessionId", sessionId);
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", sessionId);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>