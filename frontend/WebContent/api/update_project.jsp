<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  String id = request.getParameter("id");
  String extId = request.getParameter("ext_id");
  String projectName = request.getParameter("project_name");
  String projectType = request.getParameter("project_type");
  String statusCode = request.getParameter("status_code");

  if (extId == null || extId.trim().length() == 0) {
    extId = projectName;
  }
  if (id == null || projectName == null || projectName.trim().length() == 0) {
    %>
    {
      "s": false,
      "m": "Project name cannot be empty"
    }
    <%
    return;
  }

  Project project = new Project();
  project.setId(id);
  project.setExtId(extId);
  project.setProjectName(projectName);
  project.setProjectType(projectType);
  project.setStatusCode(statusCode);
  try {
    ProjectService.getInstance().update(project);
  } catch(Exception e) {
    %>
    {
      "s": false,
      "m": "<%=e.getMessage() %>"
    }
    <%
    return;
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", project.getId());
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>