<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
  import="com.atv.edge.dto.*"
%><%

  String id = request.getParameter("id");
  String username = request.getParameter("username");
  String email = request.getParameter("email");
  String fullName = request.getParameter("full_name");
  String secretCode = request.getParameter("secret_code");
  String statusCode = request.getParameter("status_code");
  String userType = request.getParameter("user_type");

  if (fullName == null || fullName.trim().length() == 0) {
    fullName = username;
  }
  if (username == null || username.trim().length() == 0) {
    %>
    {
      "s": false,
      "m": "Username cannot be empty"
    }
    <%
    return;
  }

  User user = new User();
  user.setId(id);
  user.setUsername(username);
  user.setEmail(email);
  user.setFullName(fullName);
  user.setSecretCode(secretCode);
  user.setStatusCode(statusCode);
  user.setUserType(userType);

  try {
    UserService.getInstance().update(user);
  } catch(Exception e) {
    %>
    {
      "s": false,
      "m": "<%=e.getMessage() %>"
    }
    <%
    return;
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", user.getId());
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>