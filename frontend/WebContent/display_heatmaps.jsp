<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
  import="java.text.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
%><%
  EdgeDevice[] edgeDevices = PollingStateManager.getInstance().listEdgeDevices(null);
%><html>
  <head>
    <script src="./assets/global/plugins/jquery.min.js"
    type="text/javascript"></script>
    <script src="./assets/heatmap.js"
		type="text/javascript"></script>    
    <script>
      function timedRefresh(timeoutPeriod) {
        setTimeout("location.reload(true);",timeoutPeriod);
      }     

      var allHeatMaps = [
      <%
        // Get all heatmaps data
        for (int i=0;i<edgeDevices.length;i++) {
          if (i > 0) {
            out.print(",");
          }
          String[] heatmapsJSON = edgeDevices[i].heatmaps;
          if (heatmapsJSON == null) {
            out.print("[]");            
          } else {
            String heatmaps = "[";
            for (int j=0;j<heatmapsJSON.length;j++) {
              if (j > 0) {
                heatmaps += ",";
              }
              heatmaps += heatmapsJSON[j];
            }
            heatmaps += "]";            
            out.print(heatmaps);
          }
        }
      %>
      ];
      window.allHeatMaps = allHeatMaps;

      function zeros(dimensions) {
        var array = [];
        for (var i = 0; i < dimensions[0]; ++i) {
          array.push(dimensions.length == 1 ? 0 : zeros(dimensions.slice(1)));
        }
        return array;
      }

      $(function() {
        console.log("Document Ready");
        for (var i=0;i<allHeatMaps.length;i++) {
          heatmaps = allHeatMaps[i];
          if (heatmaps.length == 0) continue;
          console.log('Create heatmap for ' + i);
          var heatmapComp = h337.create({
            container: document.getElementById('img_' + i),
            radius: 40,
            maxOpacity: .5,
            minOpacity: 0,
            blur: .75            
          });          

          // Accumulate heatmap data of every ROIs
          var xydata = zeros([10, 10]);
          var maxVal = 0;
          for (var j=0;j<heatmaps.length;j++) {
            var heatmap = heatmaps[j];
            if (heatmap != null && heatmap.length == 10) {
              for (var y=0;y<10;y++) {
                for (var x=0;x<10;x++) {
                  xydata[y][x] = xydata[y][x] + heatmap[y][x];
                  if (xydata[y][x] > maxVal) {
                    maxVal = xydata[y][x];
                  }
                }
              }
            }
          }
          var heatmapData = [];
          for (var y=0;y<10;y++) {
            for (var x=0;x<10;x++) {
              if (xydata[y][x] > 0) {
                heatmapData.push({
                  x: x * 40 + 20,
                  y: y * 30 + 15,
                  value: xydata[y][x],
                });              
              }
            }
          }

          heatmapComp.setData({
            max: maxVal,
            min: 0,
	          data: heatmapData,
          });

          console.log('Heatmap set => max = ' + maxVal);
          console.log(heatmapData);                    
        }
      });

    </script>
  </head>
  <body onload="JavaScript:timedRefresh(10000);" >
    <%
      for (int i=0;i<edgeDevices.length;i++) {
        String imageDataUrl = edgeDevices[i].getCachedSnapshotImageUrl();        
        if (imageDataUrl == null) continue;
        String[] heatmapsJSON = edgeDevices[i].heatmaps;
        if (heatmapsJSON == null) continue;
    %>
      <div style="display: block; margin: 10px; padding: 10px;" >
        <div style="display: block;" >
          <div style="display: block;" ><b> </b></div>
          <div id="img_<%=i%>" style="display:inline-block; position: relative; width:400px; height:300px;" >
            <img width="400" height="300" src="data:image/jpeg;base64, <%=imageDataUrl %>" >
          </div>
        </div>
      </div>
    <%
      }
    %>
  </body>
</html>