<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.atv.tracking.*"
%><%
  String command = request.getParameter("command");  
  String content = request.getParameter("content");  
  String nodeId = request.getParameter("id");
  if (nodeId == null || nodeId.length() == 0) {
    %>
    {
      "s": false,
      "m": "Node ID is empty"
    }
    <%
    return;
  }

  System.out.println("Queue command signal to Node ID: " + nodeId + "(" + command + ")");
  System.out.println(content);
  String res = String.valueOf(PollingStateManager.getInstance().setEdgeDeviceCommand(nodeId, command, content));

%>{
  "s": true,
  "m": "<%=res %>"
}