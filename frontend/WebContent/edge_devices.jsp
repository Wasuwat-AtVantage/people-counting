<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.atv.tracking.*"
%><%
  SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
  sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));

	String projId = request.getParameter("projId");
	System.out.println("PROJ_ID = " + projId);
	if (projId == null)
    projId = "/";

  PollingStateManager edgeDeviceManager = PollingStateManager.getInstance();
	EdgeDevice[] listOfDevices = edgeDeviceManager.listEdgeDevices(projId);

	HashSet<String> reportSet = new HashSet<String>();

	out.println("[");
	int count = 0;
	for (int i = 0; i < listOfDevices.length; i++) {
    // TODO: Better handle the prefix path.
    String summarizedReportPrefix = "/report_data/root/sharedfolder/edgedecvices/" + listOfDevices[i].id + ".json";
    String reportFileName = "/report_data/root/sharedfolder/edgedecvices/" + listOfDevices[i].id + ".json";
    System.out.println(reportFileName);
    File reportFile = new File(reportFileName);
    boolean hasReport = reportFile.exists();
    if (!hasReport) {
      reportFileName = "";
    }
    if (i > 0) out.print(",");
    %>
      { "type": "device", "id": "<%=listOfDevices[i].id %>", "name": "<%=listOfDevices[i].name %>", "report": "<%=reportFileName %>", "version": "<%=listOfDevices[i].version %>", "lastUpdated": "<%=sdf.format(listOfDevices[i].lastUpdated) %>", "watched": <%=listOfDevices[i].watched %> }
    <%
    count = count + 1;
	}	
	out.println("]");
%>