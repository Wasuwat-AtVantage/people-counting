<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.atv.edge.*"
%><%
  String LOG_ROOT_PATH = "/report_data/";
  String content = request.getParameter("content");  
  String nodeId = request.getParameter("id");
  String dateTimeString = request.getParameter("d");
  String prevDateTimeString = request.getParameter("pd");

  if (content == null || content.length() == 0 || nodeId == null || nodeId.length() == 0 || dateTimeString == null || prevDateTimeString == null) {
    %>
    {
      "s": false,
      "m": "Content or Node ID is empty"
    }
    <%
    return;
  }

  // We seperate realtime data to power BI to different endpoint
  if (true /*!RealtimeDataService.getInstance().notifyNewDataPoint(nodeId, dateTimeString, content)*/) {
    // If we use realtime data service, we will ignore file writing.
    String filePath = LOG_ROOT_PATH + nodeId + "_" + dateTimeString + "_" + prevDateTimeString + ".json";

    PrintWriter pw = null;
    try {
      pw = new PrintWriter(filePath);
      pw.println(content);
    } catch(Exception e) {
      e.printStackTrace();
      %>
      {
        "s": false,
        "m": "Log file writing error."
      }
      <%
      return;
    } finally {
      try { pw.close(); } catch(Exception ee) {}
    }
  
    System.out.println("Collecting Log for Node ID: " + nodeId);
  }
  System.out.println(content);

%>{
  "s": true,
}