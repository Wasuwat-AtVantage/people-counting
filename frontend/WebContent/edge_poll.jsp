<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
%><%
  String content = request.getParameter("content");  
  String nodeId = request.getParameter("id");
  String version = request.getParameter("v");
  String status = request.getParameter("s");
  if (nodeId == null || nodeId.length() == 0) {
    %>
    {
      "s": false,
      "m": "Node ID is empty"
    }
    <%
    return;
  }

  System.out.println("Receiving Polling signal from Node ID: " + nodeId + " -> status = " + status + " @ " + new Date().toString());
  System.out.println("Content Length = " + content.length());
  String res = PollingStateManager.getInstance().receiveDevicePolling(nodeId, content, version);

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", res);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>