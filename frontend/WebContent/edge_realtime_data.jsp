<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.atv.edge.*"
%><%
  String content = request.getParameter("content");  
  String nodeId = request.getParameter("id");
  String dateTimeString = request.getParameter("d");

  if (content == null || content.length() == 0 || nodeId == null || nodeId.length() == 0 || dateTimeString == null) {
    %>
    {
      "s": false,
      "m": "Content or Node ID is empty"
    }
    <%
    return;
  }

  RealtimeDataService.getInstance().notifyNewDataPoint2(nodeId, dateTimeString, content);
  System.out.println("Receive realtime data for node: " + nodeId);

%>{
  "s": true,
}