<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
%><%
  String commandId = request.getParameter("commandId");  
  String nodeId = request.getParameter("id");
  if (nodeId == null || nodeId.length() == 0 || commandId == null || commandId.length() == 0) {
    %>
    {
      "s": false,
      "m": "Node ID or Command ID is empty"
    }
    <%
    return;
  }

  System.out.println("Receiving Result Polling for Node ID: " + nodeId + ", Command ID: " + commandId);
  Object res = PollingStateManager.getInstance().getEdgeDeviceResponse(nodeId, commandId);
  if (res != null) {
    PollingStateManager.getInstance().markEdgeDeviceResponseComplete(nodeId, commandId);
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", String.valueOf(res));
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>