<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.atv.tracking.*"
%><%
  String watch = request.getParameter("watch");  
  String nodeId = request.getParameter("id");
  if (nodeId == null || nodeId.length() == 0 || watch == null || watch.length() == 0) {
    %>
    {
      "s": false,
      "m": "Node ID or Watch is empty"
    }
    <%
    return;
  }

  boolean wVal = Boolean.parseBoolean(watch);
  System.out.println("Set watch of " + nodeId + " => " + wVal);
  PollingStateManager.getInstance().getEdgeDevice(nodeId, null).watched = wVal;

%>{
  "s": true,
  "m": "OK"
}