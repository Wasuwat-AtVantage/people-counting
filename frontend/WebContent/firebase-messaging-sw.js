importScripts('https://www.gstatic.com/firebasejs/7.0.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.0.0/firebase-messaging.js');

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyB5hEglWKLfLEANmK8Dk879M28pNeBl5e4",
  authDomain: "machinelearning-246206.firebaseapp.com",
  databaseURL: "https://machinelearning-246206.firebaseio.com",
  projectId: "machinelearning-246206",
  storageBucket: "",
  messagingSenderId: "951964675017",
  appId: "1:951964675017:web:788ecd44775f1c12e01618",
  measurementId: "G-B01ECBC6X8"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
