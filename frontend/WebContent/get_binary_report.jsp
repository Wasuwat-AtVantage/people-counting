<%@ page language="java" contentType="application/octet-stream"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
%><%
  response.setHeader("Content-Disposition","attachment; filename=\"summary.xlsx\"");   

	String path = request.getParameter("path");
	System.out.println("PATH = " + path);
	if (path == null)
		return;
	path = path.replace("/videos//report_data/", "/report_data/");
	String filePath = path;
	System.out.println("filePath = " + filePath);

	File file = new File(filePath);
	if (file.exists() && file.isFile()) {
    OutputStream o = response.getOutputStream();
		InputStream is = null;
		try {
			is = new FileInputStream(file);
			byte[] buf = new byte[32 * 1024]; // 32k buffer
			int nRead = 0;
			while( (nRead=is.read(buf)) != -1 ) {
				o.write(buf, 0, nRead);
			}
			o.flush();
			o.close();// *important* to ensure no more jsp output
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try { is.close(); } catch (Exception e) {}
		}
		return; 				
	}
%>