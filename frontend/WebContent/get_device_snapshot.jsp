 <%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.fasterxml.jackson.databind.*"
  import="com.atv.tracking.*"
%><%
  String nodeId = request.getParameter("id");
  if (nodeId == null || nodeId.length() == 0) {
    %>
    {
      "s": false,
      "m": "Node ID is empty"
    }
    <%
    return;
  }

  System.out.println("Get cached snapshot for Node ID: " + nodeId);
  String imageDataUrl = PollingStateManager.getInstance().getEdgeDevice(nodeId, null).getCachedSnapshotImageUrl();
  if (imageDataUrl == null || imageDataUrl.length() == 0) {
    %>
    {
      "s": false,
      "m": "Snapshot is empty"
    }
    <%
    return;
  }

  Map<String, Object> params = new HashMap<>();
  params.put("s", Boolean.TRUE);
  params.put("m", imageDataUrl);
  String payload = new ObjectMapper().writeValueAsString(params);
%><%=payload %>