<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
	import="java.util.*"
%><%
	String dir = request.getParameter("dir");
	System.out.println("DIR = " + dir);
	if (dir == null)
		dir = "/";

	File folder = new File(dir);
	File[] listOfFiles = folder.listFiles();

	HashSet<String> reportSet = new HashSet<String>();

	for (int i = 0; i < listOfFiles.length; i++) {
		if (listOfFiles[i].isFile() && 
			(listOfFiles[i].getName().endsWith(".json") || listOfFiles[i].getName().endsWith(".log") || listOfFiles[i].getName().endsWith(".jpg"))
		) {
			reportSet.add(listOfFiles[i].getName());
		}
	}

	out.println("[");
	int count = 0;
	for (int i = 0; i < listOfFiles.length; i++) {
		if (listOfFiles[i].isFile() && 
			(listOfFiles[i].getName().endsWith(".mp4") || listOfFiles[i].getName().endsWith(".avi"))
		) {
			if (count > 0) {
				out.print(",");
			}
			String reportPrefix = listOfFiles[i].getName().replace(".mp4", "").replace(".avi", "");
			// TODO: Better handle the prefix path.
			String summarizedReportPrefix = "/report_data/root/sharedfolder" + dir + listOfFiles[i].getName().replace(".mp4", "").replace(".avi", "");
			String reportFileName = summarizedReportPrefix + "_raw.csv";
			System.out.println(reportFileName);
			String heatmapLogFileName = reportPrefix + ".heatmap.jpg";
			String countLogFileName = reportPrefix + ".count.log";
			File reportFile = new File(reportFileName);
			boolean hasReport = reportFile.exists();
			boolean hasHeatmapLog = reportSet.contains(heatmapLogFileName);
			boolean hasCountLog = reportSet.contains(countLogFileName);
			if (!hasReport) {
				reportFileName = "";
			}
			if (!hasHeatmapLog) {
				heatmapLogFileName = "";
			}
			if (!hasCountLog) {
				countLogFileName = "";
			}
	  	%>
	  		{ "type": "file", "name": "<%=listOfFiles[i].getName() %>", "report": "<%=reportFileName %>", "heatmapLog": "<%=heatmapLogFileName %>", "countLog": "<%=countLogFileName %>" }
	  	<%
	  	count++;
	  } else if (listOfFiles[i].isDirectory()) {
			if (count > 0) {
				out.print(",");
			}
	  	%>
	  		{ "type": "folder", "name": "<%=listOfFiles[i].getName() %>" }
	  	<%
	  	count++;
	  }
	}	
	out.println("]");
%>
