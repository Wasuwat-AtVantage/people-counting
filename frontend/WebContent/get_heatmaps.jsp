<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
	import="java.text.*"
  import="java.util.*"
  import="com.atv.tracking.*"
%><%
  String nodeId = request.getParameter("id");
  if (nodeId == null || nodeId.length() == 0) {
    %>
    {
      "s": false,
      "m": "Node ID is empty"
    }
    <%
    return;
  }

  System.out.println("Get Heatmaps of " + nodeId);
  EdgeDevice edgeDevice = PollingStateManager.getInstance().getEdgeDevice(nodeId);
  if (edgeDevice == null) {
    %>
    {
      "s": false,
      "m": "Unknown device"
    }
    <%
    return;
  }
  String[] heatmapsJSON = edgeDevice.heatmaps;
  if (heatmapsJSON == null) {
    %>
    {
      "s": true,
      "m": "OK",
      "heatmaps": []
    }
    <%
    return;
  }

  String heatmaps = "[";
  for (int i=0;i<heatmapsJSON.length;i++) {
    if (i > 0) {
      heatmaps += ",";
    }
    heatmaps += heatmapsJSON[i];
  }
  heatmaps += "]";

%>{
  "s": true,
  "m": "OK",
  "heatmaps": <%=heatmaps%>
}