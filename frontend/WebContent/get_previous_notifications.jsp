<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
  import="java.text.*"
  import="com.atv.tracking.*"
  import="com.atv.edge.*"
%><%
  PushNotificationService service = PushNotificationService.getInstance();
  List<PushNotificationService.NotificationObject> list = service.getPreviousNotifications();    
%><html>
  <head>
    <script>
      function timedRefresh(timeoutPeriod) {
        setTimeout("location.reload(true);",timeoutPeriod);
      }      
    </script>
  </head>
  <body onload="JavaScript:timedRefresh(10000);" >
    <%
      for (int i=list.size()-1;i>=0;i--) {
    %>
      <div style="display: block; margin: 10px; padding: 10px; border-radius: 10px; border: 2px solid black;" >
        <div style="display: block;" >
          <div style="display: block;" ><b><%=list.get(i).body %></b></div>
          <img width="200" src="<%=list.get(i).iconUrl %>" >
        </div>
      </div>
    <%
      }
    %>
  </body>
</html>