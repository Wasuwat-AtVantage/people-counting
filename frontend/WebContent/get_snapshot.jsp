<%@ page language="java" contentType="image/png"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.io.*"
	import="java.text.*"
	import="java.awt.image.*"
	import="javax.imageio.*"
	import="org.bytedeco.javacv.*"
%><%
	String path = request.getParameter("path");
	//System.out.println("PATH = " + path);
	if (path == null)
		return;

	FFmpegFrameGrabber g = new FFmpegFrameGrabber(path);
	g.start();
	BufferedImage image = Java2DFrameUtils.toBufferedImage(g.grab());
	ImageIO.write(image, "png", response.getOutputStream());
	g.stop();
%>