<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.io.*" 
	import="java.util.*"
	import="java.text.*"
	import="com.atv.tracking.*"
	import="com.atv.edge.*"
	import="com.atv.edge.dto.*"
%><%
	// Start Edge Watch Dog thread
	EdgeWatchDog.getInstance();

	// Check session
	boolean isLoggedIn = false;
	String sessionId = (String)session.getAttribute("sessionId");
	String userId = null;	
	if (sessionId != null) {
		userId = UserService.getInstance().getUserIdFromSessionId(sessionId);		
		isLoggedIn = (userId != null);
	}
	if (isLoggedIn) {
		// If login, then forward to user's project, or project setting page depending on user type
		User user = UserService.getInstance().get(userId);
		List<Project> projects = UserService.getInstance().listProjectOfUser(userId);
		if ("A".equals(user.getUserType()) || projects.size() > 0) {
			response.sendRedirect("list_project.jsp");
		} else {
			response.sendRedirect("view_project.jsp");
		}
	}
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8" />
<title></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link
	href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" 
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">	
<link
	href="./assets/global/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css"
	rel="stylesheet" type="text/css" />
<link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link
	href="./assets/global/plugins/bootstrap-modal/2.2.6/css/bootstrap-modal.css"
	rel="stylesheet" type="text/css" />
<link
	href="./assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css"
	rel="stylesheet" type="text/css" />
<link
	href="./assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="./assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="./assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="./assets/global/plugins/bootstrap-table/bootstrap-table.min.css"
	rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="./assets/global/css/components.min.css" rel="stylesheet"
	id="style_components" type="text/css" />
<link href="./assets/global/css/plugins.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="./assets/layouts/layout/css/layout.min.css" rel="stylesheet"
	type="text/css" />
<link href="./assets/layouts/layout/css/themes/light2.min.css"
	rel="stylesheet" type="text/css" id="style_color" />
<link href="./assets/layouts/layout/css/custom.min.css" rel="stylesheet"
	type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<!-- END HEAD -->

</head>

<body style="background-color: white;"
	class="page-content-white">
	<!-- BEGIN HEADER -->
	<!--
	<div class="page-header navbar navbar-fixed-top"
		style="height: 100px; background-color: #F6F6F6;">
	</div>
	-->
	<!-- END HEADER -->
	<!-- BEGIN HEADER & CONTENT DIVIDER -->
	<div class="clearfix"></div>
	<!-- END HEADER & CONTENT DIVIDER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container" style="margin-top: 0px">
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper  m-scene">
			<!-- BEGIN CONTENT BODY -->
			<div class="scene_element scene_element--fadeinright ">
				<!-- PRE LOADER DIV -->
				<!--         			<div class="se-pre-con"></div> -->
				<!-- BEGIN PAGE HEADER-->
				<!-- BEGIN PAGE TITLE-->
				<!-- <h3 class="page-title"></h3> -->
				<!-- END PAGE TITLE-->

				<!-- END PAGE HEADER-->
				<!-- BEGIN DASHBOARD STATS 1-->


				<!-- START PAGE ------------------------------------------------------------------------------------------>
				<div id="control">
				</div>
				<!-- END DASHBOARD STATS 1-->
				<!-- END MENU ------------------------------------------------------------------------------------------>
			</div>
			<!-- END CONTENT BODY -->
		</div>
		<!-- END CONTENT -->

	</div>
	<!-- END CONTAINER -->
	<!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
	<!-- BEGIN CORE PLUGINS -->
	<script src="./assets/global/plugins/jquery.min.js"
		type="text/javascript"></script>
	<script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>
	<script src="./assets/global/plugins/js.cookie.min.js"
		type="text/javascript"></script>
	<script
		src="./assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
		type="text/javascript"></script>
	<!--         <script src="./assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script> -->
	<script src="./js/jquery.smoothState.min.js" type="text/javascript"></script>
	<script
		src="./assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
		type="text/javascript"></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="./assets/global/plugins/moment.min.js"
		type="text/javascript"></script>
	<script
		src="./assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
		type="text/javascript"></script>
	<script
		src="./assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
		type="text/javascript"></script>
	<script src="./assets/global/plugins/morris/morris.min.js"
		type="text/javascript"></script>
	<script src="./assets/global/plugins/morris/raphael-min.js"
		type="text/javascript"></script>
	<script src="./assets/global/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>

	<script
		src="./assets/global/plugins/bootstrap-modal/2.2.6/js/bootstrap-modalmanager.js"
		type="text/javascript"></script>
	<script
		src="./assets/global/plugins/bootstrap-modal/2.2.6/js/bootstrap-modal.js"
		type="text/javascript"></script>
	<script
		src="./assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js"
		type="text/javascript"></script>

	<script
		src="./assets/global/plugins/bootstrap-table/bootstrap-table.min.js"
		type="text/javascript"></script>
	<script src="./assets/global/plugins/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
	<script src="./assets/pages/scripts/ui-extended-modals.min.js"
		type="text/javascript"></script>

	<script
		src="./assets/global/plugins/highcharts/5.0.12/js/highcharts.js"
		type="text/javascript"></script>
	<script
		src="./assets/global/plugins/highcharts/5.0.12/js/highcharts-3d.js"
		type="text/javascript"></script>
	<script
		src="./assets/global/plugins/highcharts/5.0.12/js/highcharts-more.js"
		type="text/javascript"></script>
	<script
		src="./assets/global/plugins/highcharts/5.0.12/js/modules/drilldown.js"></script>

	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN THEME GLOBAL SCRIPTS -->
	<script src="./assets/global/scripts/app.min.js" type="text/javascript"></script>
	<!-- END THEME GLOBAL SCRIPTS -->
	<!-- BEGIN THEME LAYOUT SCRIPTS -->
	<script src="./assets/layouts/layout/scripts/layout.min.js"
		type="text/javascript"></script>
	<script src="./assets/layouts/layout/scripts/demo.min.js"
		type="text/javascript"></script>
	<script src="./assets/layouts/global/scripts/quick-sidebar.min.js"
		type="text/javascript"></script>
	<script src="./assets/global/plugins/bootstrap-table/extensions/export/table.export.js" type="text/javascript"></script>

	<script src="./assets/global/scripts/qrcode.min.js"
		type="text/javascript"></script>

	<!-- END THEME LAYOUT SCRIPTS -->
	
	<!-- BEGIN LOAD REACT app.js -->
	<%
		String reactCompBasePath = ".";
		System.out.println("READING reactCompBasePath = ");
		if ("true".equals(System.getProperty("LOCAL_DEV"))) {
			reactCompBasePath = "http://localhost:9090/PeopleCounting";
		}
		System.out.println("reactCompBasePath = "+reactCompBasePath);
	%>
	<script src="<%=reactCompBasePath %>/assets/react/vendor.js"></script>	
	<script src="<%=reactCompBasePath %>/assets/react/app.js"></script>	

	<script type="text/javascript">
		$(function() {
			let reactProps = {
			};

      ReactDOM.render(React.createElement(atv.react.comp.LoginControl, 
      	reactProps, null), $('#control')[0]);            			
		});
	</script>

	<!-- END LOAD REACT app.js -->

</body>

</html>