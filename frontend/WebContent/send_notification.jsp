<%@ page language="java" contentType="application/json"
	pageEncoding="UTF-8"
%><%@ page import="java.util.*"
	import="java.nio.file.*"
	import="java.io.*"
  import="java.text.*"
  import="com.atv.tracking.*"
%><%
	String key = request.getParameter("key");
	String topic = request.getParameter("topic");
	String title = request.getParameter("title");
	String body = request.getParameter("body");
	String clickAction = request.getParameter("clickAction");
	String iconUrl = request.getParameter("iconUrl");
	if (key == null || topic == null || title == null || body == null)
		return;

	System.out.println("Notification: " + key + ": " + body);

	// Save image to file
	String uuid = UUID.randomUUID().toString();
	String filePath = "/tmp/" + uuid + ".txt";
	BufferedWriter writer = null;
	try {
		writer = new BufferedWriter(new FileWriter(filePath));
		writer.write(iconUrl);
	} catch(Exception e) {
		e.printStackTrace();
	} finally {
		try { writer.close();	} catch(Exception e) {}
	}
	 
	System.out.println("Calling Firebase...");
  PushNotificationService.getInstance().sendPushNotification(key, topic,
		title, body, 
		"https://edge.bizcuit.co.th/PeopleCounting/show_snapshot.jsp?uuid=" + uuid, 
		"https://edge.bizcuit.co.th/PeopleCounting/show_snapshot_icon.jsp?uuid=" + uuid);
	System.out.println("Calling Firebase... => Fin");
%>{}