import React from 'react'

const AdminControl = React.createClass({

  // Members

  getInitialState: function() {
    return{
    }
  },
  
  componentWillMount: function() {
  	console.log('AdminControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('AdminControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('AdminControl::componentDidMount');
  },

  componentWillUnmount: function() {
  },


  restartDevice: function(deviceId) {
    let $this = this;
    console.log('RESTART ' + deviceId);
    let SERVER_ENDPOINT = 'edge_cmd.jsp';
    let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=RST&content=';

    fetch(apiEndpoint, {
      method: "GET",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      console.log(result);
      if (result.s && result.m !== 'null') {
        let commandId = result.m;
        let RESULT_ENDPOINT = 'edge_result.jsp';
        let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
        // Wait until finish loading Screenshot
        let tryLoading = ()=>{
          console.log('Fetching Response...');
          fetch(resultEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (rresponse) {
            return rresponse.json();
          }.bind(this)).then(function (rresult) {
            console.log(rresult);
            if (rresult.s && rresult.m !== 'null') {
              if (rresult.m == 'busy') {
                alert('Device is busy, please try again later.');
                return;
              }
              alert('Device is restarting...');
            } else {
              setTimeout(()=>{
                tryLoading();
              }, 1000);          
            }
          });
        };
        tryLoading();
      } else {
        alert('Device is not ready.')
      }
    });
  },

  updateDeviceSoftware: function(deviceId) {
    let $this = this;
    console.log('UPDATE SOFTWARE ' + deviceId);
    let SERVER_ENDPOINT = 'edge_cmd.jsp';
    let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=UPD&content=master';

    fetch(apiEndpoint, {
      method: "GET",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      console.log(result);
      if (result.s && result.m !== 'null') {
        let commandId = result.m;
        let RESULT_ENDPOINT = 'edge_result.jsp';
        let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
        // Wait until finish loading Screenshot
        let tryLoading = ()=>{
          console.log('Fetching Response...');
          fetch(resultEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (rresponse) {
            return rresponse.json();
          }.bind(this)).then(function (rresult) {
            console.log(rresult);
            if (rresult.s && rresult.m !== 'null') {
              if (rresult.m == 'busy') {
                alert('Device is busy, please try again later.');
                return;
              }
              alert('Device is updated...');
            } else {
              setTimeout(()=>{
                tryLoading();
              }, 1000);          
            }
          });
        };
        tryLoading();
      } else {
        alert('Device is not ready.')
      }
    });
  },


  render: function() {

    let $this = this;

    return (
    <div>
      <table>
        <tr>
          <td>
            <a onClick={this.restartDevice.bind(this, this.props.deviceId)} >Restart Device</a>
          </td>
        </tr>
        <tr>
          <td>
            <a onClick={this.updateDeviceSoftware.bind(this, this.props.deviceId)} >Update Device Software</a>
          </td>
        </tr>
      </table>
    </div>
    );

  },

})

export default AdminControl
