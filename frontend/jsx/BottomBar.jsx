import React from 'react'

const BottomBar = React.createClass({


  getInitialState: function() {
    return {
    }
  },
  
  componentWillMount: function() {
  	console.log('BottomBar::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('BottomBar::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('BottomBar::componentDidMount');
  },

  onLabelData: function() {
    window.location = 'index.jsp';
  },

  render: function() {

    let $this = this;

    return (
      <div className="row" style={{marginTop: '0px', backgroundColor: 'rgb(248,248,248)', color: '#4c4d4e' }} >
        <div className="col-md-4"
          style={{
            marginTop: '30px',
            marginBottom: '70px',
            display: 'inline-block',
          }}
        >
          <div style={{
            display: 'inline-block',
          }}>
            <img src="assets/global/img/bizcuit_logo.svg" width="120" style={{
              marginLeft: '20px',
            }} />

          </div>
          <div style={{marginLeft: '20px', marginTop: '10px',}} >
            <div>219/ 13-15 5th Floor, Asoke Towers, Soi Asoke,</div>
            <div>Sukhumvit 21 Road, North Khlongtoey, Wattana,</div>
            <div>Bangkok 10110</div>
          </div>            
        </div>

        <div className="col-md-8"
          style={{
            marginTop: '20px',
            display: 'inline-block',
          }}
        >
          <div style={{fontSize: '26px', fontWeight: '500',}} >Contact</div>
          <div style={{marginTop: '10px', verticalAlign: 'middle',}} ><span style={{fontSize: '24px', verticalAlign: 'middle',}} >&#9990;</span> : +6626641675-7</div>
          <div style={{marginTop: '10px', verticalAlign: 'middle',}} ><span style={{fontSize: '24px', verticalAlign: 'middle',}} >&#9993;</span> : cs@atvantage.co.th</div>
          <div style={{marginTop: '10px', verticalAlign: 'middle',}} ><span style={{fontSize: '24px', verticalAlign: 'middle',}} >&#9737;</span> : www.bizcuitsolution.com</div>
        </div>

          <div className="col-md-12"
            style={{              
              padding: '10px',
              color: 'white',
              backgroundColor: 'rgb(25,52,95)',
            }}
          >
            <center>
              &#x24B8; 2019 Bizcuit Your Intelligent Business Circuit All Right Reserved.
            </center>
          </div>

      </div>
    );
  },

})

export default BottomBar
