import React from 'react'
import firebase from 'firebase'
import AdminControl from './AdminControl.jsx';

const EdgeDeviceListControl = React.createClass({

  // Members

  getInitialState: function() {
    let projId = this.props.projId || '/';
    return{
      projId: projId,
      placeList: [],
      projIdList: [],
      deviceList: [],
      loading: false,
      currentDeviceId: null,
      boundingBoxLoaded: null,
      errorMessage: null,
      displayingLiveSnapshot: false,

      areaHeader: '',
      showingCreatePlaceDialog: false,
      place: null,
      currentPlace: null,

      showingAddDeviceDialog: false,
      addDeviceList: [],

      showingListRecordingDialog: false,
      recordingList: [],
    }
  },

  areaTypeToText: function(type) {
    return {
      '1': 'Shop',
      '2': 'Pop-Up',
    }[type];
  },
  
  componentWillMount: function() {
  	console.log('EdgeDeviceListControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('EdgeDeviceListControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('EdgeDeviceListControl::componentDidMount');
    this.fetchPlace(this.state.projId);
    // this.fetchFromServer(this.state.projId);

    new QRCode(this.refs["qrcode"], "https://edge.bizcuit.co.th/PeopleCounting/subscribe.jsp?topic=/topics/all");
  },

  componentWillUnmount: function() {
  },

  fetchPlace: function(projId) {
    let $this = this;
    this.setState({
        loading: true,
        errorMessage: null,
      },
      ()=>{
        let SERVER_ENDPOINT = 'api/list_place.jsp?project_id=' + encodeURI(projId);
        let apiEndpoint = SERVER_ENDPOINT;
  
        fetch(apiEndpoint, {
          method: "GET",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }).then(function (response) {
          return response.json();
        }.bind(this)).then(function (result) {
          $this.setState({
            loading: false,
          }, ()=> {
            if (!result.s) {
              $this.setState({
                errorMessage: result.m,
              });
            } else {
              $this.setState({
                placeList: result.m,
              });
            }
          });
        });
      }
    );
  },

  createPlace: function() {
    this.setState({
      showingCreatePlaceDialog: true,
      place: {
        id: null,
        placeType: "1",
        placeName: "",
        extId: "",
        info: "",
      },
    })
  },

  savePlace: function() {
    let place = this.state.place;
    place.placeType = this.refs.placeType.value;
    place.placeName = this.refs.placeName.value;
    place.info = this.refs.info.value;
    place.extId = this.refs.extId.value;
    if (place.id == null) {
      // Create new
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/create_place.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?place_name=' + encodeURI(place.placeName) 
            + '&place_type=' + encodeURI(place.placeType)
            + '&info=' + encodeURI(place.info)
            + '&ext_id=' + encodeURI(place.extId)
            + '&project_id=' + encodeURI($this.state.projId)
            ;
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  project: null,
                  showingCreatePlaceDialog: false,
                }, ()=> {
                  $this.fetchPlace($this.state.projId);
                });
              }
            });
          });
        }
      );
  
    } else {
      // Modify exising
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/update_place.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?place_name=' + encodeURI(place.placeName) 
            + '&place_type=' + encodeURI(place.placeType)
            + '&ext_id=' + encodeURI(place.extId)
            + '&info=' + encodeURI(place.info)
            + '&id=' + encodeURI(place.id)
            ;
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  project: null,
                  showingCreatePlaceDialog: false,
                }, ()=> {
                  $this.fetchPlace($this.state.projId);
                });
              }
            });
          });
        }
      );

    }
  },

  deletePlace: function() {
    let place = this.state.place;
    if (place.id == null) {
      // Cancel new
      this.setState({
        place: null,
        errorMessage: null,
        showingCreatePlaceDialog: false,
      });
    } else {
      // Delete exising
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/delete_place.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(place.id);
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  project: null,
                  showingCreatePlaceDialog: false,
                }, ()=> {
                  $this.fetchPlace($this.state.projId);
                });
              }
            });
          });
        }
      );

    }
  },

  viewPlace: function(idx) {
    this.fetchFromServer(this.state.placeList[idx]);
  },

  managePlace: function(idx) {
    let $this = this;
    let place = this.state.placeList[idx];
    this.setState({
      showingCreatePlaceDialog: true,
      place: place,
    }, ()=>{
      if (typeof $this.refs.placeName !== 'undefined') {
        $this.refs.extId.value = place.extId;
        $this.refs.placeName.value = place.placeName;
        $this.refs.placeType.value = place.placeType;
        $this.refs.info.value = place.info;
      }
    });
  },

  addDevice: function() {
    let $this = this;
    this.setState({
      showingAddDeviceDialog: true,
    }, ()=>{
      $this.fetchAddDeviceList();
    });
  },

  fetchFromServer: function(place) {
    let placeId = place.id;
    let areaTitle = place.extId + " : " + place.placeName + " (" + this.areaTypeToText(place.placeType) + ")";
    let SERVER_ENDPOINT = 'api/list_device.jsp';
    let $this = this;

    fetch(SERVER_ENDPOINT + '?place_id=' + encodeURI(placeId), {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        place_id: placeId,
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      if (!result.s) {
        $this.setState({
          errorMessage: result.m,
        })
      } else {
        result = result.m
        console.log(result);
        let deviceList = [];
        let projIdList = [];
        for (let i=0;i<result.length;i++) {
          let item = result[i];
          if (item.type == 'device')
            deviceList.push(item);
          else
            projIdList.push(item);
        }
        $this.setState({
          areaTitle: areaTitle,
          projIdList: projIdList,
          deviceList: deviceList,
          currentPlace: place,
        });
      }

    });
  },

  fetchAddDeviceList: function() {
    let SERVER_ENDPOINT = 'edge_devices.jsp';
    let $this = this;

    fetch(SERVER_ENDPOINT, {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      $this.setState({
        addDeviceList: result,
      });
    });
  },

  confirmAddDevice: function(addExisting) {
    let deviceId = null;
    if (addExisting) {
      deviceId = this.refs.addDeviceOnline.value;
    } else {
      deviceId = this.refs.addDeviceManual.value;
    }

    let SERVER_ENDPOINT = 'api/assign_device.jsp?device_id=' + encodeURI(deviceId) + '&place_id=' + encodeURI(this.state.currentPlace.id);
    let $this = this;

    fetch(SERVER_ENDPOINT, {
      method: "GET",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      $this.setState({
        showingAddDeviceDialog: false,
      }, ()=>{
        $this.fetchFromServer($this.state.currentPlace);
      });
    });
  },

  removeDevice: function(device) {
    let deviceId = device.id;
    let SERVER_ENDPOINT = 'api/unassign_device.jsp?device_id=' + encodeURI(deviceId) + '&place_id=' + encodeURI(this.state.currentPlace.id);
    let $this = this;

    fetch(SERVER_ENDPOINT, {
      method: "GET",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      $this.setState({
        showingAddDeviceDialog: false,
      }, ()=>{
        $this.fetchFromServer($this.state.currentPlace);
      });
    });
  },

  cancelAddDevice: function() {
    this.setState({
      showingAddDeviceDialog: false,
    });
  },

  fetchSnapshotFromServer: function(path) {
    let SERVER_ENDPOINT = 'get_snapshot.jsp';
    let $this = this;

    fetch(SERVER_ENDPOINT + '?path=' + encodeURI(path), {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        path: path,
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      console.log(result);
    });
  },

  fetchBoundingBoxFromDevice: function(deviceId) {

    let $this = this;
    this.setState({
      loading: true,
    },
    ()=>{
      let SERVER_ENDPOINT = 'edge_cmd.jsp';
      let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=GBB&content=';
      //alert('go-1');

      fetch(apiEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (response) {
        return response.json();
      }.bind(this)).then(function (result) {
        console.log(result);
        if (result.s && result.m !== 'null') {
          let commandId = result.m;
          let RESULT_ENDPOINT = 'edge_result.jsp';
          let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
          // Wait until finish loading Screenshot
          let tryLoading = ()=>{
            console.log('Try loading bounding box...');
            fetch(resultEndpoint, {
              method: "GET",
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            }).then(function (rresponse) {
              return rresponse.json();
            }.bind(this)).then(function (rresult) {
              console.log(rresult);
              //alert('go0');
              if (rresult.s && rresult.m !== 'null') {
                if (rresult.m == 'busy') {
                  $this.setState({
                    loading: false,
                  });
                  alert('Device is busy, please try again later.');
                  return;
                }  
                let boundingBoxes = JSON.parse(rresult.m);
                //alert('go1');
                if ($this.props.onBoundingBoxesUpdated) {
                  $this.props.onBoundingBoxesUpdated(boundingBoxes, deviceId);
                  $this.setState({
                    loading: false,
                    boundingBoxLoaded: true,                    
                  }, ()=>{
                    //alert('go');
                    if ($this.props.refresh === true || $this.state.displayingLiveSnapshot === false) {
                      $this.refreshSnapshot($this.state.currentDeviceId);
                    }
                  });                  
                } 
              } else {
                setTimeout(()=>{
                  tryLoading();
                }, 1000);          
              }
            });
          };
          tryLoading();
        } else {
          alert('Device is not ready.')
        }
      });

    });
  },

  refreshSnapshot: function(deviceId) {
    let $this = this;
    console.log('REFRESH: ' + deviceId);
    if (this.state.boundingBoxLoaded && this.state.currentDeviceId == deviceId) {
      let SERVER_ENDPOINT = 'edge_cmd.jsp';
      let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=SS&content=';

      fetch(apiEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (response) {
        return response.json();
      }.bind(this)).then(function (result) {
        console.log(result);
        if (result.s && result.m !== 'null') {
          let commandId = result.m;
          let RESULT_ENDPOINT = 'edge_result.jsp';
          let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
          // Wait until finish loading Screenshot
          let tryLoading = ()=>{
            console.log('Try loading screenshot...');
            fetch(resultEndpoint, {
              method: "GET",
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            }).then(function (rresponse) {
              return rresponse.json();
            }.bind(this)).then(function (rresult) {
              console.log(rresult);
              if (rresult.s && rresult.m !== 'null') {
                let imageUrl = rresult.m;
                imageUrl = 'data:image/jpeg;base64, ' + imageUrl;
                if ($this.props.onUpdateImage) {
                  $this.props.onUpdateImage(imageUrl, ()=>{
                    $this.setState({
                      displayingLiveSnapshot: true,
                    });
                    if ($this.props.refresh === true) {
                      $this.refreshSnapshot(deviceId);
                    }
                  });
                }
              } else {
                setTimeout(()=>{
                  tryLoading();
                }, 1000);          
              }
            });
          };
          tryLoading();
        } else {
          alert('Device is not ready.')
        }
      });

    }
  },

  saveBoundingBoxToCurrentDevice: function(boundingBoxList) {
    let deviceId = this.state.currentDeviceId;
    if (deviceId == null) return; // No device is selected.

    let $this = this;
    this.setState({
      loading: true,
    },
    ()=>{
      let SERVER_ENDPOINT = 'edge_cmd.jsp';
      let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=SBB&content=' + encodeURI(JSON.stringify(boundingBoxList));

      fetch(apiEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (response) {
        return response.json();
      }.bind(this)).then(function (result) {
        console.log(result);
        if (result.s && result.m !== 'null') {
          let commandId = result.m;
          let RESULT_ENDPOINT = 'edge_result.jsp';
          let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
          // Wait until finish loading Screenshot
          let tryLoading = ()=>{
            console.log('Try saving bounding box...');
            fetch(resultEndpoint, {
              method: "GET",
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            }).then(function (rresponse) {
              return rresponse.json();
            }.bind(this)).then(function (rresult) {
              console.log(rresult);
              if (rresult.s && rresult.m !== 'null') {
                if (rresult.m == 'busy') {
                  $this.setState({
                    loading: false,
                  });
                  alert('Device is busy, please try again later.');
                  return;
                }  
                alert('Bounding Box List file has been saved.');
                $this.setState({
                  loading: false,
                }, ()=>{
                });                  
              } else {
                setTimeout(()=>{
                  tryLoading();
                }, 1000);          
              }
            });
          };
          tryLoading();
        } else {
          alert('Device is not ready.')
        }
      });

    });
  },

  selectPlace: function(id) {
    alert(id);
  },

  selectProject: function(name) {
    let projId = this.state.projectId + name + '/';
    this.setState({
      projId: projId,
    });
    this.fetchFromServer(projId);
  },

  selectUpFolder: function() {
    let projId = this.state.projId;
    let idx = projId.substring(0, projId.length-1).lastIndexOf('/');
    if (idx != -1) {
      projId = projId.substring(0, idx + 1);
    }
    this.setState({
      projId: projId,
    });
    this.fetchFromServer(projId);
  },

  selectDevice: function(deviceId) {    
    let $this = this;
    this.setState({
      loading: true,
      currentDeviceId: deviceId,
      boundingBoxLoaded: false,
      displayingLiveSnapshot: false,
    },
    ()=>{
      // this.fetchSnapshotFromServer(filePath);
      let SERVER_ENDPOINT = 'get_device_snapshot.jsp';
      let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=SS&content=';

      fetch(apiEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (response) {
        return response.json();
      }.bind(this)).then(function (result) {
        console.log(result);
        if (result.s && result.m !== 'null') {
          let imageUrl = result.m;
          imageUrl = 'data:image/jpeg;base64, ' + imageUrl;
          if ($this.props.onFileSelected) {
            $this.props.onFileSelected(imageUrl, ()=>{
              $this.setState({
                loading: false,
              }, ()=>{
                $this.fetchBoundingBoxFromDevice(deviceId);
              });
            });
          }
        } else {
          console.log('Device does not have cached snapshot, try loading live feed from real device.');
          $this.selectLiveDevice(deviceId);
        }
      });
    });
  },

  selectLiveDevice: function(deviceId) {    
    let $this = this;
    this.setState({
      loading: true,
      currentDeviceId: deviceId,
      boundingBoxLoaded: false,
    },
    ()=>{
      // this.fetchSnapshotFromServer(filePath);
      let SERVER_ENDPOINT = 'edge_cmd.jsp';
      let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=SS&content=';

      fetch(apiEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (response) {
        return response.json();
      }.bind(this)).then(function (result) {
        console.log(result);
        if (result.s && result.m !== 'null') {
          let commandId = result.m;
          let RESULT_ENDPOINT = 'edge_result.jsp';
          let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
          // Wait until finish loading Screenshot
          let tryLoading = ()=>{
            console.log('Try loading screenshot...');
            fetch(resultEndpoint, {
              method: "GET",
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            }).then(function (rresponse) {
              return rresponse.json();
            }.bind(this)).then(function (rresult) {
              console.log(rresult);
              if (rresult.s && rresult.m !== 'null') {
                if (rresult.m == 'busy') {
                  $this.setState({
                    loading: false,
                  });
                  // alert('Device is busy, please try again later.');
                  return;
                }  
                let imageUrl = rresult.m;
                imageUrl = 'data:image/jpeg;base64, ' + imageUrl;
                if ($this.props.onFileSelected) {
                  $this.props.onFileSelected(imageUrl, ()=>{
                    $this.setState({
                      loading: false,
                      displayingLiveSnapshot: true,
                    }, ()=>{
                      $this.fetchBoundingBoxFromDevice(deviceId);
                    });
                  });
                }
              } else {
                setTimeout(()=>{
                  tryLoading();
                }, 1000);          
              }
            });
          };
          tryLoading();
        } else {
          alert('Device is not ready.')
        }
      });

    });

  },

  getRecordings: function(deviceId) {    
    let $this = this;
    this.setState({
      loading: true,
      currentDeviceId: deviceId,
      boundingBoxLoaded: false,
    },
    ()=>{
      // this.fetchSnapshotFromServer(filePath);
      let SERVER_ENDPOINT = 'edge_cmd.jsp';
      let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=LST&content=';

      fetch(apiEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (response) {
        return response.json();
      }.bind(this)).then(function (result) {
        console.log(result);
        if (result.s && result.m !== 'null') {
          let commandId = result.m;
          let RESULT_ENDPOINT = 'edge_result.jsp';
          let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
          // Wait until finish loading Screenshot
          let tryLoading = ()=>{
            console.log('Try loading recording list...');
            fetch(resultEndpoint, {
              method: "GET",
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            }).then(function (rresponse) {
              return rresponse.json();
            }.bind(this)).then(function (rresult) {
              console.log(rresult);
              if (rresult.s && rresult.m !== 'null') {
                if (rresult.m == 'busy') {
                  $this.setState({
                    loading: false,
                  });
                  // alert('Device is busy, please try again later.');
                  return;
                }  
                $this.setState({
                  recordingList: JSON.parse(rresult.m),
                  loading: false,
                  showingListRecordingDialog: true,
                });
              } else {
                setTimeout(()=>{
                  tryLoading();
                }, 1000);          
              }
            });
          };
          tryLoading();
        } else {
          alert('Device is not ready.')
        }
      });

    });

  },

  downloadRecording: function(deviceId, path) {
    let $this = this;
    this.setState({
      loading: true,
      currentDeviceId: deviceId,
      boundingBoxLoaded: false,
    },
    ()=>{
      // this.fetchSnapshotFromServer(filePath);
      let SERVER_ENDPOINT = 'edge_cmd.jsp';
      let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=URE&content=' + encodeURI(path);

      fetch(apiEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (response) {
        return response.json();
      }.bind(this)).then(function (result) {
        console.log(result);
        if (result.s && result.m !== 'null') {
          let commandId = result.m;
          let RESULT_ENDPOINT = 'edge_result.jsp';
          let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
          // Wait until finish loading Screenshot
          let tryLoading = ()=>{
            console.log('Try downloading recording list...');
            fetch(resultEndpoint, {
              method: "GET",
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            }).then(function (rresponse) {
              return rresponse.json();
            }.bind(this)).then(function (rresult) {
              console.log(rresult);
              if (rresult.s && rresult.m !== 'null') {
                if (rresult.m == 'busy') {
                  $this.setState({
                    loading: false,
                  });
                  // alert('Device is busy, please try again later.');
                  return;
                }  
                if (rresult.m === 'True') {
                  let url = 'https://storage.googleapis.com/atv_dataset/edge_computing/recordings/' + deviceId + '/' + path;
                  window.location = url;
                } else {
                  alert('Downloading failed.')
                }
                $this.setState({
                  loading: false,
                });
              } else {
                setTimeout(()=>{
                  tryLoading();
                }, 1000);          
              }
            });
          };
          tryLoading();
        } else {
          alert('Device is not ready.')
        }
      });

    });
  },

  deleteRecording: function(deviceId, path) {
    let $this = this;
    this.setState({
      loading: true,
      currentDeviceId: deviceId,
      boundingBoxLoaded: false,
    },
    ()=>{
      // this.fetchSnapshotFromServer(filePath);
      let SERVER_ENDPOINT = 'edge_cmd.jsp';
      let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(deviceId) + '&command=DRE&content=' + encodeURI(path);

      fetch(apiEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (response) {
        return response.json();
      }.bind(this)).then(function (result) {
        console.log(result);
        if (result.s && result.m !== 'null') {
          let commandId = result.m;
          let RESULT_ENDPOINT = 'edge_result.jsp';
          let resultEndpoint = RESULT_ENDPOINT + '?id=' + encodeURI(deviceId) + '&commandId=' + encodeURI(commandId);
          // Wait until finish loading Screenshot
          let tryLoading = ()=>{
            console.log('Try deleting recording list...');
            fetch(resultEndpoint, {
              method: "GET",
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            }).then(function (rresponse) {
              return rresponse.json();
            }.bind(this)).then(function (rresult) {
              console.log(rresult);
              if (rresult.s && rresult.m !== 'null') {
                if (rresult.m == 'busy') {
                  $this.setState({
                    loading: false,
                  });
                  // alert('Device is busy, please try again later.');
                  return;
                }  
                if (rresult.m === 'True') {
                  alert('Recording was deleted.')
                } else {
                  alert('Deleting failed.')
                }
                let recordingList = $this.state.recordingList;
                for (let i=0;i<recordingList.length;i++) {
                  if (recordingList[i].name == path) {
                    recordingList.splice(0, 1);
                    break;
                  }
                }
                $this.setState({
                  loading: false,
                  recordingList: recordingList,
                });
              } else {
                setTimeout(()=>{
                  tryLoading();
                }, 1000);          
              }
            });
          };
          tryLoading();
        } else {
          alert('Device is not ready.')
        }
      });

    });
  },

  changeWatchStatus: function(device) {
    if (this.props.userMode) return; // Not allow normal user to change this value.
    let $this = this;
    device.watched = !device.watched;
    this.setState({
        loading: true,
        deviceList: this.state.deviceList,
      },
      ()=>{
        // this.fetchSnapshotFromServer(filePath);
        let SERVER_ENDPOINT = 'edge_watch.jsp';
        let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(device.id) + '&watch=' + device.watched;
  
        fetch(apiEndpoint, {
          method: "GET",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }).then(function (response) {
          return response.json();
        }.bind(this)).then(function (result) {
          $this.setState({
            loading: false,
          });
        });
      }
    );
  },

  selectReport: function(name) {
    let $this = this;
    let filePath = this.state.dir + name;
    // this.fetchSnapshotFromServer(filePath);
    let SERVER_ENDPOINT = 'get_report.jsp';
    let reportUrl = SERVER_ENDPOINT + '?path=' + encodeURI(filePath);
    window.open(reportUrl, '_blank');
  },

  selectImageFile: function(name) {
    let $this = this;
    let filePath = this.state.dir + name;
    // this.fetchSnapshotFromServer(filePath);
    let SERVER_ENDPOINT = 'get_image_report.jsp';
    let reportUrl = SERVER_ENDPOINT + '?path=' + encodeURI(filePath);
    window.open(reportUrl, '_blank');
  },

  onDownloadSummaryReport: function() {
    let $this = this;
    let SERVER_ENDPOINT = 'edge_report.jsp';
    let reportUrl = SERVER_ENDPOINT;
    window.open(reportUrl, '_blank');
  },

  onDownloadUnoccupiedReport: function() {
    let $this = this;
    let SERVER_ENDPOINT = 'edge_report_unoccupied.jsp';
    let reportUrl = SERVER_ENDPOINT;
    window.open(reportUrl, '_blank');
  },

  onViewReport: function() {
    let $this = this;
    let SERVER_ENDPOINT = 'view_report.jsp';
    let reportUrl = SERVER_ENDPOINT;
    window.open(reportUrl, '_blank');
  },

  onSubscribePushNotification: async function() {
    let $this = this;
    try {
      this.setState({
        loading: true,
      });
      console.log('Generating push user token:');      
      const messaging = firebase.messaging();
      await messaging.requestPermission();
      const token = await messaging.getToken();
      console.log('Push user token:', token);      
      // alert(token);

      let SUBSCRIBE_ENDPOINT = 'subscribe_notification.jsp';
      let subscribeEndpoint = SUBSCRIBE_ENDPOINT + '?key=' + encodeURI(token) + 
        '&topic=' + encodeURI('/topics/all');
      fetch(subscribeEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (rresponse) {
        return rresponse.json();
      }.bind(this)).then(function (rresult) {
        $this.setState({
          loading: false,
        });        
        alert('Your device has been subscribed to notification alert.');
      });

      return token;
    } catch (error) {
      console.error(error);
      alert(error);
    }
  }, 

  closeListRecordingDialog: function() {
    this.setState({
      showingListRecordingDialog: false,
    });
  },

  render: function() {

    let $this = this;
    let placeList = this.state.placeList;
    let projId = this.state.projId;
    let projIdList = this.state.projIdList;
    let deviceList = this.state.deviceList;
    let loading = this.state.loading;

    let showingCreatePlaceDialog = this.state.showingCreatePlaceDialog;
    let place = this.state.place;
    let errorMessage = this.state.errorMessage;
    let areaTitle = this.state.areaTitle;
    let currentPlace = this.state.currentPlace;

    let addDeviceList = this.state.addDeviceList;
    let showingAddDeviceDialog = this.state.showingAddDeviceDialog;

    let showingListRecordingDialog = this.state.showingListRecordingDialog;


    // User mode for hiding admin component 
    let userMode = (this.props.userMode?true:false);

    return (
    <div>
      <div style={{marginTop: '0px', marginBottom: '30px', display: ((loading)?'block':'none'), }} >
        <img src='./assets/global/img/loading.gif' />
      </div>


      <div style={{
          backgroundColor: 'rgb(242,122,69)',
          color: 'white',
          display: (userMode?'none':'inline'),
          margin: '0px',
          marginLeft: '10px',
          paddingTop: '12px',
          paddingBottom: '12px',
          paddingLeft: '30px',
          paddingRight: '30px',
          cursor: 'pointer',
          border: 'none',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: '600',
          borderRadius: '20px !important',
          whiteSpace: 'nowrap',
        }} 
          onClick={$this.createPlace.bind($this)}
        >
          Create new Area
      </div>

      <div style={{display: (userMode?'none':'block'), marginTop: '20px', }} >&nbsp;</div>

      <table style={{
        backgroundColor: 'white',
        width: '100%',
        color: 'white',
      }} >
        <thead>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Area Code</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Area Name</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Type</th>
          <th style={{display: (userMode?'none':'block'), border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Manage</th>
        </thead>
        {
          (()=>{
            return placeList.map((item, idx)=>{
              let bgColor = 'rgb(184,192,205)';
              if (idx % 2 == 0) {
                bgColor = 'rgb(142,153,171)';
              }
              return (
                <tbody key={'dir_' + idx} >
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.viewPlace.bind($this, idx)} >
                      {item.extId}
                    </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.viewPlace.bind($this, idx)} >
                      {item.placeName}
                    </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.viewPlace.bind($this, idx)} >
                    {$this.areaTypeToText(item.placeType)}
                    </span>
                  </td>
                  <td style={{display: (userMode?'none':'block'), border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.managePlace.bind($this, idx)} >
                    [Edit]
                    </span>
                  </td>
                </tbody>
              );
            })
          })()
        }
      </table>

      <div style={{marginTop: '20px', }} ><b>{areaTitle}</b></div>
      <div style={{marginTop: '15px', }} >&nbsp;</div>
      <div style={{
          backgroundColor: 'rgb(242,122,69)',
          color: 'white',
          display: ((currentPlace!=null && !userMode)?'inline':'none'),
          margin: '0px',
          marginLeft: '10px',
          paddingTop: '12px',
          paddingBottom: '12px',
          paddingLeft: '30px',
          paddingRight: '30px',
          cursor: 'pointer',
          border: 'none',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: '600',
          borderRadius: '20px !important',
          whiteSpace: 'nowrap',
        }} 
          onClick={$this.addDevice.bind($this)}
        >
          Add Device
      </div>
      <div style={{display: (userMode?'none':'block'), marginTop: '10px', }} >&nbsp;</div>

      <table style={{
        backgroundColor: 'white',
        width: '100%',
        color: 'white',
      }} >
        <thead>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Version</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >ID</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Last Updated</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Watched</th>
          <th style={{display: (userMode?'none':'block'), border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Manage</th>
        </thead>
        {
          (()=>{
            if (false) {
              return (
              <tbody projId={'dir_up'} >
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}} >project</td>
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}} >
                  <span style={{color:'white', cursor:'pointer'}} onClick={$this.selectUpFolder}>
                  [..]
                  </span>
                </td>
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}}></td>
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}}></td>
                <td style={{display: (userMode?'none':'block'), border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}}></td>
              </tbody>
              );              
            }
          })()
        }
        {
          (()=>{
            return projIdList.map((item, idx)=>{
              let bgColor = 'rgb(184,192,205)';
              if (idx % 2 == 0) {
                bgColor = 'rgb(142,153,171)';
              }
              return (
                <tbody key={'dir_' + idx} >
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.type}</td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.selectFolder.bind($this, item.name)} >
                    [{item.name}]
                    </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}}></td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}}></td>
                  <td style={{display: (userMode?'none':'block'), border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}}></td>
                </tbody>
              );
            })
          })()
        }
        {
          (()=>{
            return deviceList.map((item, idx)=>{
              let bgColor = 'rgb(184,192,205)';
              if ((deviceList.length + idx) % 2 == 0) {
                bgColor = 'rgb(142,153,171)';
              }
              return (
                <tbody key={'file_' + idx} >
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.version}</td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                  <span style={{color:'white', cursor:'pointer'}} onClick={$this.selectDevice.bind($this, item.id)} >
                  {item.id}
                  </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                  <span style={{color:'white', cursor:'pointer'}} onClick={$this.selectDevice.bind($this, item.id)} >
                  {item.lastUpdated}
                  </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                  <span style={{color:'white', cursor:'pointer'}} onClick={$this.getRecordings.bind($this, item.id)} >
                    [Recordings]
                  </span>
                  </td>
                  <td style={{display: (userMode?'none':'block'), border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                  <span style={{color:'white', cursor:'pointer'}} onClick={$this.removeDevice.bind($this, item)} >
                    [Remove]
                  </span>
                  </td>

                </tbody>
              );
            })
          })()
        }
      </table>

      <div style={{
        display: ((userMode)?'none':'normal'),
        marginTop: '30px',
        width: '100%',
        textAlign: 'center',
        visibility: (($this.state.currentDeviceId != null)?'visible':'hidden'),
      }} >
        <div>
          <b>
          Scan QR Code below to receive realtime updates.
          </b>
        </div>
        <div style={{
          marginTop: '10px',
        }} >
          <a href="https://edge.bizcuit.co.th/PeopleCounting/subscribe.jsp?topic=/topics/all" target="_blank" >
            <span ref="qrcode" id="qrcode" style={{
            }} ></span>
          </a>     
        </div>
      </div>

      <div style={{marginTop: '20px', textAlign: 'center', display: 'none' }} >
        <div style={{
          display: ((userMode)?'none':'inline'),
          backgroundColor: 'rgb(242,122,69)',
          color: 'white',
          margin: '0px',
          marginLeft: '10px',
          paddingTop: '12px',
          paddingBottom: '12px',
          paddingLeft: '30px',
          paddingRight: '30px',
          cursor: 'pointer',
          border: 'none',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: '600',
          borderRadius: '20px !important',
        }} 
          onClick={$this.onSubscribePushNotification.bind($this)}
        >
          SUBSCRIBE PUSH NOTIFICATION
        </div>  
      </div>

      <div style={{marginTop: '30px', textAlign: 'center' }} >
        <div style={{
          display: ((userMode)?'none':'inline'),
          backgroundColor: 'rgb(242,122,69)',
          color: 'white',
          margin: '0px',
          marginLeft: '10px',
          paddingTop: '12px',
          paddingBottom: '12px',
          paddingLeft: '30px',
          paddingRight: '30px',
          cursor: 'pointer',
          border: 'none',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: '600',
          borderRadius: '20px !important',
          whiteSpace: 'nowrap',
        }} 
          onClick={$this.onDownloadSummaryReport.bind($this)}
        >
          DOWNLOAD SUMMARY DATA
        </div>  
      </div>

      <div style={{marginTop: '30px', textAlign: 'center' }} >
        <div style={{
          display: ((userMode)?'none':'inline'),
          backgroundColor: 'rgb(242,122,69)',
          color: 'white',
          margin: '0px',
          marginLeft: '10px',
          paddingTop: '12px',
          paddingBottom: '12px',
          paddingLeft: '30px',
          paddingRight: '30px',
          cursor: 'pointer',
          border: 'none',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: '600',
          borderRadius: '20px !important',
          whiteSpace: 'nowrap',
        }} 
          onClick={$this.onDownloadUnoccupiedReport.bind($this)}
        >
          DOWNLOAD UNOCCUPIED DATA
        </div>  
      </div>

      <div style={{marginTop: '30px', textAlign: 'center' }} >
        <div style={{
          display: ((userMode)?'none':'inline'),
          backgroundColor: 'rgb(242,122,69)',
          color: 'white',
          margin: '0px',
          marginLeft: '10px',
          paddingTop: '12px',
          paddingBottom: '12px',
          paddingLeft: '30px',
          paddingRight: '30px',
          cursor: 'pointer',
          border: 'none',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: '600',
          borderRadius: '20px !important',
        }} 
          onClick={$this.onViewReport.bind($this)}
        >
          VIEW ANALYSIS REPORT
        </div>  
      </div>

      <div style={{ 
        marginTop: '30px',
        display: (($this.state.currentDeviceId != null && !userMode)?'block':'none'),
        }}>
        <AdminControl deviceId={$this.state.currentDeviceId} />
      </div>

      {
        (()=>{
          console.log('showingCreatePlaceDialog = ' + showingCreatePlaceDialog);
          if (showingCreatePlaceDialog) {
            return (
              <div style={{
                backgroundColor: 'rgb(238,245,255)',
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                border: '2px solid rgb(201,201,201)',
                borderRadius: '8px !important',
                padding: '15px',
                zIndex: '100000',
              }} >
                <div style={{
                  color: 'black',
                }}>
                  <div>
                    <span style={{marginRight: '10px',}} >Type</span>
                    <select ref="placeType" defaultValue={place.placeType} >
                      <option value='1'>Shop</option>
                      <option value='2'>Pop-up</option>
                    </select><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Area Name</span><input type="text" style={{margin: '3px',}} ref="placeName" defaultValue={place.placetName} /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Area Code</span><input type="text" style={{margin: '3px',}} ref="extId" defaultValue={place.extId} /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Info</span><input type="text" style={{margin: '3px',}} ref="info" defaultValue={place.info} /><br/>
                  </div>
                </div>

                <div style={{marginLeft: '20px', marginTop: '10px', marginBottom: '10px', display: ((errorMessage != null)?'block':'none'), }} >
                  <span style={{color:'red', }} >{errorMessage}</span>
                </div>

                <div style={{
                  marginTop: '15px',
                }}>
                  <span style={{
                    display: 'inline-block',
                    width: '48%',
                    backgroundColor: 'rgb(242,122,69)',
                    padding: '8px 5px 8px 5px',            
                    cursor: 'pointer',  
                    margin: '1%',
                    color: 'white',
                    borderRadius: '20px !important',
                    textAlign: 'center',
                  }} onClick={$this.savePlace} >Save</span>
                  <span style={{
                    display: 'inline-block',
                    width: '48%',
                    backgroundColor: 'rgb(25,52,95)',
                    padding: '8px 5px 8px 5px',            
                    cursor: 'pointer',  
                    margin: '1%',
                    color: 'white',
                    borderRadius: '20px !important',
                    textAlign: 'center',
                  }} onClick={$this.deletePlace} >Delete</span>
                </div>
              </div>        
            );
          }
        })()
      }

      {
        (()=>{
          console.log('showingAddDeviceDialog = ' + showingAddDeviceDialog);
          if (showingAddDeviceDialog) {
            return (
              <div style={{
                backgroundColor: 'rgb(238,245,255)',
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                border: '2px solid rgb(201,201,201)',
                borderRadius: '8px !important',
                padding: '15px',
                zIndex: '100000',
              }} >
                <div style={{
                  color: 'black',
                }}>
                  <div>
                    <span style={{marginRight: '10px',}} >Currently On-Line</span>
                    <select ref="addDeviceOnline" >
                      {
                        (()=>{
                          return addDeviceList.map((device)=>{
                            return (
                            <option value={device.id} key={device.id} >{device.id}</option>
                            );
                          })
                        })()
                      }
                    </select><br/>

                    <span style={{
                      display: 'inline-block',
                      width: '48%',
                      backgroundColor: 'rgb(242,122,69)',
                      padding: '8px 5px 8px 5px',            
                      cursor: 'pointer',  
                      margin: '1%',
                      color: 'white',
                      borderRadius: '20px !important',
                      textAlign: 'center',
                    }} onClick={$this.confirmAddDevice.bind($this, true)} >Add Existing</span>

                  </div>

                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Manual Preset Device ID</span><input type="text" style={{margin: '3px',}} ref="addDeviceManual" /><br/>
                  </div>
                </div>

                <div style={{marginLeft: '20px', marginTop: '10px', marginBottom: '10px', display: ((errorMessage != null)?'block':'none'), }} >
                  <span style={{color:'red', }} >{errorMessage}</span>
                </div>

                <div style={{
                  marginTop: '15px',
                }}>
                  <span style={{
                    display: 'inline-block',
                    width: '48%',
                    backgroundColor: 'rgb(242,122,69)',
                    padding: '8px 5px 8px 5px',            
                    cursor: 'pointer',  
                    margin: '1%',
                    color: 'white',
                    borderRadius: '20px !important',
                    textAlign: 'center',
                  }} onClick={$this.confirmAddDevice.bind($this, false)} >Save</span>
                  <span style={{
                    display: 'inline-block',
                    width: '48%',
                    backgroundColor: 'rgb(25,52,95)',
                    padding: '8px 5px 8px 5px',            
                    cursor: 'pointer',  
                    margin: '1%',
                    color: 'white',
                    borderRadius: '20px !important',
                    textAlign: 'center',
                  }} onClick={$this.cancelAddDevice} >Cancel</span>
                </div>

              </div>        
            );
          }
        })()
      }

      {
        (()=>{
          let recordingList = $this.state.recordingList;
          console.log(recordingList);

          console.log('showingListRecordingDialog = ' + showingListRecordingDialog);
          if (showingListRecordingDialog) {
            return (
              <div style={{
                backgroundColor: 'rgb(238,245,255)',
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                border: '2px solid rgb(201,201,201)',
                borderRadius: '8px !important',
                padding: '15px',
                zIndex: '100000',
              }} >
                <div style={{
                  color: 'black',
                }}>
                  <div>
                    <div style={{marginRight: '10px',}} >On-Device Recordings</div>
                    <div style={{
                      height: '500px',
                      overflow: 'auto',
                    }} >
                      <table style={{
                        backgroundColor: 'white',
                        width: '100%',
                        color: 'white',
                      }} >
                        <thead>
                          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >File Name</th>
                          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Size</th>
                          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Last Modified</th>
                          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} ></th>
                        </thead>

                        {
                          (()=>{
                            return recordingList.map((item, idx)=>{
                              let bgColor = 'rgb(184,192,205)';
                              if (idx % 2 == 0) {
                                bgColor = 'rgb(142,153,171)';
                              }
                              return (
                                <tbody key={'dir_' + idx} >
                                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.name}</td>
                                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.size}</td>
                                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.modified}</td>
                                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.downloadRecording.bind($this, $this.state.currentDeviceId, item.name)} >
                                    [Download]
                                    </span>
                                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.deleteRecording.bind($this, $this.state.currentDeviceId, item.name)} >
                                    [Delete]
                                    </span>
                                  </td>
                                </tbody>
                              );
                            })
                          })()
                        }

                      </table>
                    </div>

                    <div style={{
                      marginTop: '15px',
                    }}>
                      <span style={{
                        display: 'inline-block',
                        width: '48%',
                        backgroundColor: 'rgb(242,122,69)',
                        padding: '8px 5px 8px 5px',            
                        cursor: 'pointer',  
                        margin: '1%',
                        color: 'white',
                        borderRadius: '20px !important',
                        textAlign: 'center',
                      }} onClick={$this.closeListRecordingDialog} >Close</span>
                    </div>

                  </div>
                </div>

              </div>        
            );
          }
        })()
      }

    </div>
    );

  },

})

export default EdgeDeviceListControl
