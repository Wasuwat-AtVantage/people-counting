import React from 'react'

// Now support both user and admin email configurations, conditioned on props.isAdmin
const EmailList = React.createClass({

  // Members
  getInitialState: function() {
    return{
      emailList: [],
    }
  },
  
  componentWillMount: function() {
  	console.log('EmailList::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('EmailList::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('EmailList::componentDidMount');
    this.fetchEmailListFromServer();
  },

  componentWillUnmount: function() {
  },

  fetchEmailListFromServer: function() {
    let isAdmin = this.props.isAdmin;
    let SERVER_ENDPOINT = (isAdmin?'get_email_admin_list.jsp':'get_email_list.jsp');
    let $this = this;

    fetch(SERVER_ENDPOINT, {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      console.log(result);
      $this.setState({
        emailList: result,
      });
    });
  },

  saveEmailListToServer: function() {
    let isAdmin = this.props.isAdmin;
    let SERVER_ENDPOINT = (isAdmin?'set_email_admin_list.jsp':'set_email_list.jsp');
    let $this = this;

    let newEmailList = this.refs.emailList.value.split('\n');

    fetch(SERVER_ENDPOINT + '?emailList=' + encodeURI(newEmailList.join(',')), {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        emailList: newEmailList.join(','),
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      $this.setState({
        emailList: newEmailList,
      });
      alert('Notification email list has been updated.');
    });
  },

  handleChange: function(event) {
    this.setState({emailList: event.target.value.split('\n')});
  },  

  render: function() {

    let $this = this;
    let emailList= this.state.emailList.join('\n');
    return (
    <div>
      <textarea key='emailList' ref='emailList' value={emailList} onChange={this.handleChange} >  
      </textarea><br/>
      <a onClick={this.saveEmailListToServer} >Update Email List</a>
    </div>
    );

  },

})

export default EmailList
