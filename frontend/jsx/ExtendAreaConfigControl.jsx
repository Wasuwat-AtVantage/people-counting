import React from 'react'
import firebase from 'firebase'
import AdminControl from './AdminControl.jsx';

const ExtendAreaConfigControl = React.createClass({

  // Members

  getInitialState: function() {
    return{
      deviceId: this.props.deviceId,
      roiList: this.props.roiList,
      extendedROIList: [],
      extendedROI: null,
      loading: false,
      errorMessage: null,

      showingAddExtendedROIDialog: false,
    }
  },

  thresholdToText: function(type) {
    return [
      'Pass-Through',
      'Interested',
      'Engaged',
    ][type];
  },
  
  componentWillMount: function() {
  	console.log('ExtendAreaConfigControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('ExtendAreaConfigControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('ExtendAreaConfigControl::componentDidMount');
    // this.fetchExtendedROIList(this.state.deviceId);
  },

  componentWillUnmount: function() {
  },

  fetchExtendedROIList: function(deviceId) {
    let $this = this;
    this.setState({
        loading: true,
        errorMessage: null,
        deviceId: deviceId,
      },
      ()=>{
        let SERVER_ENDPOINT = 'api/load_extended_roi.jsp?device_id=' + encodeURI(deviceId);
        let apiEndpoint = SERVER_ENDPOINT;
  
        fetch(apiEndpoint, {
          method: "GET",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }).then(function (response) {
          return response.json();
        }.bind(this)).then(function (result) {
          $this.setState({
            loading: false,
          }, ()=> {
            if (!result.s) {
              $this.setState({
                errorMessage: result.m,
              });
            } else {
              $this.setState({
                extendedROIList: result.m,
              });
            }
          });
        });
      }
    );
  },

  createExtendedROI: function() {
    this.setState({
      showingAddExtendedROIDialog: true,
      extendedROI: {
        pass_through_source: '',
        interest_source: '',
        engaged_source: '',
        attended_source: '',
        other_sources: '',
      },
    })
  },

  saveExtendedROI: function() {
    let extendedROI = this.state.extendedROI;
    let extendedROIList = this.state.extendedROIList;
    if (extendedROIList == null) extendedROIList = [];
    extendedROI.pass_through_source = this.refs.passThroughSrc.value;
    extendedROI.interest_source = this.refs.interestSrc.value;
    extendedROI.engaged_source = this.refs.engagedSrc.value;
    extendedROI.attended_source = this.refs.attendedSrc.value;
    extendedROI.other_sources = this.refs.otherSrc.value;
    let isNew = true;
    for (var i=0;i<extendedROIList.length;i++) {
      if (extendedROIList[i] == extendedROI) {
        isNew = false;
        break;
      }
    }
    if (isNew)
      extendedROIList.push(extendedROI);
    this.store(extendedROIList);
  },

  store: function(extendedROIList) {
    // Update to server
    let $this = this;
    let deviceId = this.state.deviceId;
    this.setState({
        loading: true,
        errorMessage: null,
      },
      ()=>{
        let SERVER_ENDPOINT = 'api/save_extended_roi.jsp';
        let apiEndpoint = SERVER_ENDPOINT + '?device_id=' + encodeURI(deviceId) 
          + '&data=' + encodeURIComponent(JSON.stringify(extendedROIList))
          ;
  
        fetch(apiEndpoint, {
          method: "GET",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }).then(function (response) {
          return response.json();
        }.bind(this)).then(function (result) {
          $this.setState({
            loading: false,
          }, ()=> {
            if (!result.s) {
              $this.setState({
                errorMessage: result.m,
              })
            } else {
              $this.setState({
                extendedROI: null,
                showingAddExtendedROIDialog: false,
              }, ()=> {
                $this.fetchExtendedROIList(deviceId);
              });
            }
          });
        });
      }
    );
  },

  deleteExtendedROI: function() {
    let extendedROI = this.state.extendedROI;
    let extendedROIList = this.state.extendedROIList;
    let newExtendedROIList = [];
    for (var i=0;i<extendedROIList.length;i++) {
      if (extendedROIList[i] != extendedROI)
        newExtendedROIList.push(extendedROIList[i]);
    }
    this.store(newExtendedROIList)
  },

  manageExtendedROI: function(idx) {
    let $this = this;
    let extendedROI = this.state.extendedROIList[idx];
    this.setState({
      showingAddExtendedROIDialog: true,
      extendedROI: extendedROI,
    }, ()=>{
      if (typeof $this.refs.pass_through_source !== 'undefined') {
        $this.refs.passThroughSrc.value = extendedROI.pass_through_source;
        $this.refs.interestSrc.value = extendedROI.interest_source;
        $this.refs.engagedSrc.value = extendedROI.engaged_source;
        if ('attended_source' in extendedROI)
          $this.refs.attendedSrc.value = extendedROI.attended_source;
        else
          $this.refs.attendedSrc.value = '';
        if ('other_sources' in extendedROI)
          $this.refs.otherSrc.value = extendedROI.other_sources;
        else
          $this.refs.otherSrc.value = '';
      }
    });
  },

  render: function() {

    let $this = this;
    let extendedROIList = this.state.extendedROIList;
    let deviceId = this.state.deviceId;
    let loading = this.state.loading;

    let showingAddExtendedROIDialog = this.state.showingAddExtendedROIDialog;
    let extendedROI = this.state.extendedROI;
    let errorMessage = this.state.errorMessage;

    // User mode for hiding admin component 
    let userMode = (this.props.userMode?true:false);

    return (
    <div>
      <div style={{marginTop: '0px', marginBottom: '30px', display: ((loading)?'block':'none'), }} >
        <img src='./assets/global/img/loading.gif' />
      </div>


      <div style={{
          backgroundColor: 'rgb(242,122,69)',
          color: 'white',
          display: ((userMode || loading)?'none':'inline'),
          margin: '0px',
          marginLeft: '10px',
          paddingTop: '12px',
          paddingBottom: '12px',
          paddingLeft: '30px',
          paddingRight: '30px',
          cursor: 'pointer',
          border: 'none',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: '600',
          borderRadius: '20px !important',
          whiteSpace: 'nowrap',
        }} 
          onClick={$this.createExtendedROI.bind($this)}
        >
          Create new Extended Area
      </div>

      <div style={{display: (userMode?'none':'block'), marginTop: '20px', }} >&nbsp;</div>

      <table style={{
        backgroundColor: 'white',
        width: '100%',
        color: 'white',
      }} >
        <thead>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >#</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Pass</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Interested</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Engaged</th>
          <th style={{display: (userMode?'none':'block'), border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Manage</th>
        </thead>
        {
          (()=>{
            if (!extendedROIList || extendedROIList == null) 
              return null;
            return extendedROIList.map((item, idx)=>{
              let bgColor = 'rgb(184,192,205)';
              if (idx % 2 == 0) {
                bgColor = 'rgb(142,153,171)';
              }
              return (
                <tbody key={'dir_' + idx} >
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.manageExtendedROI.bind($this, idx)} >
                      {(idx + $this.state.roiList.length)}
                    </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.manageExtendedROI.bind($this, idx)} >
                      {item.pass_through_source}
                    </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.manageExtendedROI.bind($this, idx)} >
                      {item.interest_source}
                    </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.manageExtendedROI.bind($this, idx)} >
                      {item.engaged_source}
                    </span>
                  </td>
                  <td style={{display: (userMode?'none':'block'), border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.manageExtendedROI.bind($this, idx)} >
                    [Edit]
                    </span>
                  </td>
                </tbody>
              );
            })
          })()
        }
      </table>

      {
        (()=>{
          console.log('showingAddExtendedROIDialog = ' + showingAddExtendedROIDialog);
          if (showingAddExtendedROIDialog) {
            return (
              <div style={{
                backgroundColor: 'rgb(238,245,255)',
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                border: '2px solid rgb(201,201,201)',
                borderRadius: '8px !important',
                padding: '15px',
                zIndex: '100000',
              }} >
                <div style={{
                  color: 'black',
                }}>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Pass Formula</span><input type="text" style={{margin: '3px',}} ref="passThroughSrc" defaultValue={extendedROI.pass_through_source} /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Interested Formula</span><input type="text" style={{margin: '3px',}} ref="interestSrc" defaultValue={extendedROI.interest_source} /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Engaged Formula</span><input type="text" style={{margin: '3px',}} ref="engagedSrc" defaultValue={extendedROI.engaged_source} /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Attended Formula</span><input type="text" style={{margin: '3px',}} ref="attendedSrc" defaultValue={extendedROI.attended_source} /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Other Formulas</span><input type="text" style={{margin: '3px',}} ref="otherSrc" defaultValue={extendedROI.other_sources} /><br/>
                  </div>
                </div>

                <div style={{marginLeft: '20px', marginTop: '10px', marginBottom: '10px', display: ((errorMessage != null)?'block':'none'), }} >
                  <span style={{color:'red', }} >{errorMessage}</span>
                </div>

                <div style={{
                  marginTop: '15px',
                }}>
                  <span style={{
                    display: 'inline-block',
                    width: '48%',
                    backgroundColor: 'rgb(242,122,69)',
                    padding: '8px 5px 8px 5px',            
                    cursor: 'pointer',  
                    margin: '1%',
                    color: 'white',
                    borderRadius: '20px !important',
                    textAlign: 'center',
                  }} onClick={$this.saveExtendedROI} >Save</span>
                  <span style={{
                    display: 'inline-block',
                    width: '48%',
                    backgroundColor: 'rgb(25,52,95)',
                    padding: '8px 5px 8px 5px',            
                    cursor: 'pointer',  
                    margin: '1%',
                    color: 'white',
                    borderRadius: '20px !important',
                    textAlign: 'center',
                  }} onClick={$this.deleteExtendedROI} >Delete</span>
                </div>
              </div>        
            );
          }
        })()
      }

    </div>
    );

  },

})

export default ExtendAreaConfigControl
