import React from 'react'

const FileListControl = React.createClass({

  // Members

  getInitialState: function() {
    let dir = this.props.dir || '/';
    return{
      dir: dir,
      dirList: [],
      fileList: [],
      loading: false,
    }
  },
  
  componentWillMount: function() {
  	console.log('FileListControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('FileListControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('FileListControl::componentDidMount');
    this.fetchFromServer(this.state.dir);
  },

  componentWillUnmount: function() {
  },

  fetchFromServer: function(dir) {
    let SERVER_ENDPOINT = 'get_file_list.jsp';
    let $this = this;

    fetch(SERVER_ENDPOINT + '?dir=' + encodeURI(dir), {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        dir: dir,
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      console.log(result);
      let fileList = [];
      let dirList = [];
      for (let i=0;i<result.length;i++) {
        let item = result[i];
        if (item.type == 'file')
          fileList.push(item);
        else
          dirList.push(item);
      }
      $this.setState({
        dirList: dirList,
        fileList: fileList,
      });
    });
  },

  fetchSnapshotFromServer: function(path) {
    let SERVER_ENDPOINT = 'get_snapshot.jsp';
    let $this = this;

    fetch(SERVER_ENDPOINT + '?path=' + encodeURI(path), {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        path: path,
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      console.log(result);
    });
  },

  fetchBoundingBoxFromServer: function(path) {
    let SERVER_ENDPOINT = 'get_boundingbox.jsp';
    let $this = this;

    fetch(SERVER_ENDPOINT + '?path=' + encodeURI(path), {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        path: path,
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      let boundingBoxes = result;
      if ($this.props.onBoundingBoxesUpdated) {
        $this.props.onBoundingBoxesUpdated(boundingBoxes, path);
      }      
    });
  },

  saveBoundingBoxToServer: function(boundingBoxList) {
    let path = this.state.dir;
    let SERVER_ENDPOINT = 'set_boundingbox.jsp';
    let $this = this;

    fetch(SERVER_ENDPOINT + '?path=' + encodeURI(path) + '&boundingBoxList=' + encodeURI(JSON.stringify(boundingBoxList)), {
      method: "POST",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        path: path,
        boundingBoxList: boundingBoxList,
      }),
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      alert('Bounding Box List file has been saved.');
    });
  },

  selectFolder: function(name) {
    let dir = this.state.dir + name + '/';
    this.setState({
      dir: dir,
    });
    this.fetchFromServer(dir);
  },

  selectUpFolder: function() {
    let dir = this.state.dir;
    let idx = dir.substring(0, dir.length-1).lastIndexOf('/');
    if (idx != -1) {
      dir = dir.substring(0, idx + 1);
    }
    this.setState({
      dir: dir,
    });
    this.fetchFromServer(dir);
  },

  selectFile: function(name) {
    let $this = this;
    let filePath = this.state.dir + name;
    // this.fetchSnapshotFromServer(filePath);
    let SERVER_ENDPOINT = 'get_snapshot.jsp';
    let imageUrl = SERVER_ENDPOINT + '?path=' + encodeURI(filePath);
    if (this.props.onFileSelected) {
      this.props.onFileSelected(imageUrl, ()=>{
        $this.fetchBoundingBoxFromServer($this.state.dir);
      });
    }
  },

  selectReport: function(name) {
    let $this = this;
    let filePath = this.state.dir + name;
    // this.fetchSnapshotFromServer(filePath);
    let SERVER_ENDPOINT = 'get_report.jsp';
    let reportUrl = SERVER_ENDPOINT + '?path=' + encodeURI(filePath);
    window.open(reportUrl, '_blank');
  },

  selectImageFile: function(name) {
    let $this = this;
    let filePath = this.state.dir + name;
    // this.fetchSnapshotFromServer(filePath);
    let SERVER_ENDPOINT = 'get_image_report.jsp';
    let reportUrl = SERVER_ENDPOINT + '?path=' + encodeURI(filePath);
    window.open(reportUrl, '_blank');
  },

  onDownloadSummary: function() {
    let $this = this;
    let filePath = '/videos//report_data/root/sharedfolder' + this.state.dir + 'summary.xlsx';
    // this.fetchSnapshotFromServer(filePath);
    let SERVER_ENDPOINT = 'get_binary_report.jsp';
    let reportUrl = SERVER_ENDPOINT + '?path=' + encodeURI(filePath);
    window.open(reportUrl, '_blank');
  },

  render: function() {

    let $this = this;
    let dir = this.state.dir;
    let dirList = this.state.dirList;
    let fileList = this.state.fileList;
    let loading = this.state.loading;

    return (
    <div>
      <div style={{marginTop: '0px', display: ((loading)?'block':'none'), }} >
        <img src='./assets/global/img/loading.gif' />
      </div>
      <table style={{
        backgroundColor: 'white',
        width: '100%',
        color: 'white',
      }} >
        <thead>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Type</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Name</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Report</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Heatmap</th>
          <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Count</th>
        </thead>
        {
          (()=>{
            if (dir != '/') {
              return (
              <tbody key={'dir_up'} >
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}} >folder</td>
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}} >
                  <span style={{color:'white', cursor:'pointer'}} onClick={$this.selectUpFolder}>
                  [..]
                  </span>
                </td>
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}}></td>
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}}></td>
                <td style={{border: '1px solid rgb(184,192,205)', padding: '2px 5px', backgroundColor: 'rgb(184,192,205)'}}></td>
              </tbody>
              );              
            }
          })()
        }
        {
          (()=>{
            return dirList.map((item, idx)=>{
              let bgColor = 'rgb(184,192,205)';
              if (idx % 2 == 0) {
                bgColor = 'rgb(142,153,171)';
              }
              return (
                <tbody key={'dir_' + idx} >
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.type}</td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                    <span style={{color:'white', cursor:'pointer'}} onClick={$this.selectFolder.bind($this, item.name)} >
                    [{item.name}]
                    </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}}></td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}}></td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}}></td>
                </tbody>
              );
            })
          })()
        }
        {
          (()=>{
            return fileList.map((item, idx)=>{
              let bgColor = 'rgb(184,192,205)';
              if ((dirList.length + idx) % 2 == 0) {
                bgColor = 'rgb(142,153,171)';
              }
              return (
                <tbody key={'file_' + idx} >
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.type}</td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                  <span style={{color:'white', cursor:'pointer'}} onClick={$this.selectFile.bind($this, item.name)} >
                  {item.name}
                  </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                  <span style={{display: ((item.report !== '')?'normal':'none'), color:'white', cursor:'pointer'  }} onClick={$this.selectReport.bind($this, item.report)} >
                    [Download]
                  </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                  <span style={{display: ((item.heatmapLog !== '')?'normal':'none'), color:'white', cursor:'pointer' }} onClick={$this.selectImageFile.bind($this, item.heatmapLog)} >
                    [Download]
                  </span>
                  </td>
                  <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                  <span style={{display: ((item.countLog !== '')?'normal':'none'), color:'white', cursor:'pointer' }} onClick={$this.selectReport.bind($this, item.countLog)} >
                    [Download]
                  </span>
                  </td>
                </tbody>
              );
            })
          })()
        }
      </table>

      <div style={{marginTop: '20px',}} >
        <div style={{
          backgroundColor: 'rgb(242,122,69)',
          color: 'white',
          display: 'inline',
          margin: '0px',
          marginLeft: '10px',
          paddingTop: '12px',
          paddingBottom: '12px',
          paddingLeft: '30px',
          paddingRight: '30px',
          cursor: 'pointer',
          border: 'none',
          textAlign: 'center',
          fontSize: '14px',
          fontWeight: '600',
          borderRadius: '20px !important',
        }} 
          onClick={$this.onDownloadSummary.bind($this)}
        >
          DOWNLOAD SUMMARY REPORT
        </div>  
      </div>

    </div>
    );

  },

})

export default FileListControl
