import React from 'react'
import TopBar from './TopBar.jsx'
import BottomBar from './BottomBar.jsx'
import FileListControl from './FileListControl.jsx'
import EdgeDeviceListControl from './EdgeDeviceListControl.jsx'
import EmailList from './EmailList.jsx'
import ExtendAreaConfigControl from './ExtendAreaConfigControl.jsx'

const LabelControl = React.createClass({

  // ROI Data Format Version
  VERSION: '_v2',

  // Members
  fileCache: {

  },
  isDrag: false,
  curX: 0,
  curY: 0,
  curPoly: [],
  isDirty: false,
  ctx: null,

  getInitialState: function() {
    let fileList = [
    /*
      {name: 'Filename1.jpeg', path: null, width: 500, height: 400, imageData: null },
    */
    ];
    let currentSelectedFileIdx = -1;
    let currentSelectedDetectionIdx = -1;
    let detectedObjects = {

    };
    let currentImageFile = null;

    return{
      fileList: fileList,
      currentSelectedFileIdx: currentSelectedFileIdx,
      detectedObjects: detectedObjects,
      currentSelectedDetectionIdx: currentSelectedDetectionIdx,
      currentImageFile: currentImageFile,
      currentImageDataUrl: null,
      loading: false,
      showFileList: false,
      deviceId: null,
      boundingBoxList: [],
    }
  },
  
  componentWillMount: function() {
  	console.log('LabelControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('LabelControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('LabelControl::componentDidMount');
    let dropZone = this.refs['dropZone'];
    if (!this.props.userMode) {
      dropZone.addEventListener('dragover', this.handleDragOver, false);
      dropZone.addEventListener('drop', this.handleFileSelect, false);    
    }

    let drawLayer = this.refs['drawLayer'];
    this.ctx = drawLayer.getContext('2d')
    this.ctx.strokeStyle = '#ff0000'; // this.props.strokeStyle
    this.ctx.lineWidth = '2'; // this.props.lineWidth

    if (!this.props.userMode) {
      drawLayer.addEventListener('click', this.onMouseDown, false);
      drawLayer.addEventListener('mousemove', this.onMouseMove, false);
      drawLayer.addEventListener('dblclick', this.onMouseUp, false);
    }
  },

  componentWillUnmount: function() {
    let drawLayer = this.refs['drawLayer'];

    if (!this.props.userMode) {
      drawLayer.removeEventListener('click', this.onMouseDown, false);
      drawLayer.removeEventListener('mousemove', this.onMouseMove, false);
      drawLayer.removeEventListener('dblclick', this.onMouseUp, false);
    }
  },

  onMouseDown: function(e) {
    console.log('onMouseDown');
    this.isDrag = true
    this.curX = e.offsetX
    this.curY = e.offsetY
    this.curPoly.push([this.curY, this.curX]);
    requestAnimationFrame(this.updateCanvas)
  },

  onMouseMove: function(e) {
    if (! this.isDrag) return
    this.curX = e.offsetX
    this.curY = e.offsetY
    console.log('offset = '+e.offsetX+', '+e.offsetY);
    this.isDirty = true
  },
  
  onMouseUp: function(e) {
    console.log('onMouseUp');
    this.isDrag = false
    this.isDirty = true
    
    const polygon = this.curPoly;
    this.curPoly = [];
    console.log(polygon);

    // Add new bounding box
    let fileList = this.state.fileList || [];
    let currentSelectedFileIdx = this.state.currentSelectedFileIdx;
    let detectedObjects = this.state.detectedObjects;
    let currentSelectedDetectionIdx = this.state.currentSelectedDetectionIdx;

    if (currentSelectedFileIdx != -1) {    

      detectedObjects[currentSelectedFileIdx].push({
        type: '1',
        direction_x: '',
        direction_y: '',
        age_tracking: '5',
        count_threshold: '3',
        interested_threshold: '30',
        engaged_threshold: '100',
        unoccupied_threshold: '20',
        confidence: 100.00,
        boundingBox: polygon,
        status: null,
        constraint_rois: '',
      });
      currentSelectedDetectionIdx = detectedObjects[currentSelectedFileIdx].length - 1 ;

      this.setState({
        currentSelectedDetectionIdx: currentSelectedDetectionIdx,
        detectedObjects: detectedObjects,
      });
    }
    /*
    this.props.onSelected(rect)
    */
  },

  updateCanvas: function() {
    if (this.isDrag) {
      requestAnimationFrame(this.updateCanvas)
    }

    if (! this.isDirty) {
      return
    }
    
    let drawLayer = this.refs['drawLayer'];
    let width = drawLayer.width;
    let height = drawLayer.height;
    console.log('width = '+width+', height='+height);

    this.ctx.clearRect(0, 0, width, height)

    let currentSelectedFileIdx = this.state.currentSelectedFileIdx;
    let detectedObjects = this.state.detectedObjects;

    let fileList = this.state.fileList || [];

    if (currentSelectedFileIdx != -1) {

      let imageWidth = 300;
      let imageHeight = 300;
      if (currentSelectedFileIdx != -1) {
        imageWidth = fileList[currentSelectedFileIdx].width;
        imageHeight = fileList[currentSelectedFileIdx].height;
      }

      imageHeight = 480 / imageWidth * imageHeight;
      imageWidth = 480;

      if (imageHeight > 480) {
        imageWidth = 480 / imageHeight * imageWidth;
        imageHeight = 480;
      }
      console.log('imageWidth = '+imageWidth);
      console.log('imageHeight = '+imageHeight);

      // console.log('detectedObjects.length = ' + detectedObjects.length);
      for (var i in detectedObjects[currentSelectedFileIdx]) {      
        let detectedObject = detectedObjects[currentSelectedFileIdx][i];
        console.log(detectedObject);
        var poly = detectedObject.boundingBox;
        console.log(poly);
        this.ctx.strokeStyle = '#008800'; // this.props.strokeStyle
        this.ctx.lineWidth = '2'; // this.props.lineWidth
        if (poly.length > 0) {
          this.ctx.beginPath();
          this.ctx.moveTo(poly[0][1], poly[0][0]);
          for (var j=1;j<poly.length;j++) {
            this.ctx.lineTo(poly[j][1], poly[j][0]);
          }
          this.ctx.closePath();
          this.ctx.stroke();
        }
      }

      if (this.isDrag) {      
        this.ctx.strokeStyle = '#ff0000'; // this.props.strokeStyle
        this.ctx.lineWidth = '2'; // this.props.lineWidth

        if (this.curPoly.length > 0) {
          this.ctx.beginPath();
          this.ctx.moveTo(this.curPoly[0][1], this.curPoly[0][0]);
          for (var j=1;j<this.curPoly.length;j++) {
            this.ctx.lineTo(this.curPoly[j][1], this.curPoly[j][0]);
          }
          this.ctx.lineTo(this.curX, this.curY);
          this.ctx.closePath();
          this.ctx.stroke();
        }
      }  

    }
    this.isDirty = false
  },

  handleDragOver: function(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  },

  handleFileSelect: function(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    // Get the FileList object that contains the list of files that were dropped
    var files = evt.dataTransfer.files;
    // this UI is only built for a single file so just dump the first one
    this.loadFile(files[0]);
  },

  onFileSelected: function() {
    var file = this.refs.theFile.files[0];
    this.loadFile(file);
  },

  onRemoteFileSelected: function(imageUrl, onFinished) {
    this.loadImage(imageUrl, imageUrl, onFinished);
  },

  onUpdateImage: function(dataUrl, onFinished) {
    let fileList = this.state.fileList;
    let idx = fileList.length - 1;
    let img = new Image();
    img.onload = function() {
      console.log('reader.onload (4)');

      if (typeof this.width !== 'undefined' && typeof this.height !== 'undefined') {
        fileList[idx].width = this.width;
        fileList[idx].height = this.height;
        console.log('Update image Width = ' + this.width + ', height = ' + this.height);
      }

      this.setState({
        fileList: fileList,
        currentImageDataUrl: dataUrl,
      }, ()=>{
        if (onFinished) {
          onFinished();
        }
      });
    }.bind(this);
    img.src = dataUrl;
  },

  loadImage: function(name, dataUrl, onFinished = null) {
    var width = 480;
    var height = 480;
    let $this = this;

    let dataObj = {
      name: name,
      path: name, 
      width: width, 
      height: height, 
      imageData: dataUrl, 
    };
    let fileList = $this.state.fileList;
    fileList.push(dataObj);

    let detectedObjects = $this.state.detectedObjects;
    detectedObjects[fileList.length - 1] = [];

    console.log('reader.onload (2)');


    let idx = fileList.length - 1;
    let img = new Image();
    img.onload = function() {
      console.log('reader.onload (4)');

      fileList[idx].width = this.width;
      fileList[idx].height = this.height;

      $this.setState({
        fileList: fileList,
        detectedObjects: detectedObjects,
        currentImageFile: name,
        currentSelectedFileIdx: idx,
        currentSelectedDetectionIdx: -1,
        currentImageDataUrl: dataUrl,
        loading: false,
      }, ()=>{
        if (onFinished) {
          onFinished();
        }
      });
    }
    img.src = dataUrl;

    console.log('reader.onload (3)');
  },

  loadFile: function(file) {
    console.log('Loading: ' + file.name);
    var reader = new FileReader();
    var name = file.name;
    var _file = file;
    var $this = this;

    $this.setState({
      loading: true,
    });

    reader.onload = function(file) {
      console.log('reader.onload');
      var dataUrl = reader.result;

      $this.loadImage(name, dataUrl);

      /*
      ((pos)=>{
        setTimeout(()=>{
          detectedObjects[pos] = 
            [
            ];

          $this.setState({
            detectedObjects: detectedObjects,
            loading: false,
          });
        }, 100);
      })(fileList.length - 1);
      */

    }.bind(this);
    reader.readAsDataURL(file);
    // reader.readAsArrayBuffer(file);    
  },

  selectDetection(selectedIdx) {    
    console.log('selectedIdx = ' + selectedIdx);

    let fileList = this.state.fileList || [];
    let currentSelectedFileIdx = this.state.currentSelectedFileIdx;
    let detectedObjects = this.state.detectedObjects;
    let currentSelectedDetectionIdx = selectedIdx;

    let selectType = this.refs['selectType'];
    let editBoxDirX = this.refs['editBoxDirX'];
    let editBoxDirY = this.refs['editBoxDirY'];
    let editBoxAgeTracking = this.refs['editBoxAgeTracking'];
    let editBoxCountThreshold = this.refs['editBoxCountThreshold'];
    let editBoxInterestedThreshold = this.refs['editBoxInterestedThreshold'];
    let editBoxEngagedThreshold = this.refs['editBoxEngagedThreshold'];
    let editBoxUnoccupiedThreshold = this.refs['editBoxUnoccupiedThreshold'];
    let editBoxConstraintROIs = this.refs['editBoxConstraintROIs'];
    if (typeof selectType !== 'undefined') {
      selectType.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].type;
    }
    if (typeof editBoxDirX !== 'undefined') {
      editBoxDirX.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].direction_x;
    }
    if (typeof editBoxDirY !== 'undefined') {
      editBoxDirY.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].direction_y;
    }
    if (typeof editBoxAgeTracking !== 'undefined') {
      editBoxAgeTracking.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].age_tracking;
    }
    if (typeof editBoxCountThreshold !== 'undefined') {
      editBoxCountThreshold.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].count_threshold;
    }
    if (typeof editBoxInterestedThreshold !== 'undefined') {
      editBoxInterestedThreshold.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].interested_threshold;
    }
    if (typeof editBoxEngagedThreshold !== 'undefined') {
      editBoxEngagedThreshold.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].engaged_threshold;
    }
    if (typeof editBoxUnoccupiedThreshold !== 'undefined') {
      editBoxUnoccupiedThreshold.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].unoccupied_threshold;
    }
    if (typeof editBoxConstraintROIs !== 'undefined') {
      if (constraint_rois in detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx])
        editBoxConstraintROIs.value = detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].constraint_rois;
      else
        editBoxConstraintROIs.value = '';
    }

    this.setState({
      currentSelectedDetectionIdx: selectedIdx,
    });
  },

  onBoundingBoxesUpdated(boundingBoxList, deviceId) {
    // alert(JSON.stringify(boundingBoxList));
    console.log(boundingBoxList);

    let fileList = this.state.fileList || [];
    let currentSelectedFileIdx = this.state.currentSelectedFileIdx;
    let detectedObjects = this.state.detectedObjects;
    let $this = this;

    if (currentSelectedFileIdx != -1) {

      let imageWidth = 300;
      let imageHeight = 300;
      if (currentSelectedFileIdx != -1) {
        imageWidth = fileList[currentSelectedFileIdx].width;
        imageHeight = fileList[currentSelectedFileIdx].height;
      }

      imageHeight = 480 / imageWidth * imageHeight;
      imageWidth = 480;

      if (imageHeight > 480) {
        imageWidth = 480 / imageHeight * imageWidth;
        imageHeight = 480;
      }
      console.log('imageWidth = '+imageWidth);
      console.log('imageHeight = '+imageHeight);

      console.log('Convert bounding box');

      while (detectedObjects.length <= currentSelectedFileIdx) {
        detectedObjects.push([]);
      }

      detectedObjects[currentSelectedFileIdx] = [];
      console.log(detectedObjects);

      for (let i=0;i<boundingBoxList.length;i++) {
        let obj = {};
  
        let boundingBox = boundingBoxList[i];
        let version = boundingBox[0];
        if (version !== '_v2') {
          // For backward compatible
          let type = boundingBox[0];
          let top = boundingBox[1];
          let left = boundingBox[2];
          let bottom = boundingBox[3];
          let right = boundingBox[4];
          let uiBoundingBox = [
            [
              String(Math.round(top * imageHeight / fileList[currentSelectedFileIdx].height)),
              String(Math.round(left * imageWidth / fileList[currentSelectedFileIdx].width)),
            ],
            [
              String(Math.round(top * imageHeight / fileList[currentSelectedFileIdx].height)),
              String(Math.round(right * imageWidth / fileList[currentSelectedFileIdx].width)),
            ],
            [
              String(Math.round(bottom * imageHeight / fileList[currentSelectedFileIdx].height)),
              String(Math.round(right * imageWidth / fileList[currentSelectedFileIdx].width)),
            ],
            [
              String(Math.round(bottom * imageHeight / fileList[currentSelectedFileIdx].height)),
              String(Math.round(left * imageWidth / fileList[currentSelectedFileIdx].width)),
            ],
          ];
          let direction_x = boundingBox[5];
          let direction_y = boundingBox[6];
          let age_tracking = String(boundingBox[7]);
          let count_threshold = String(boundingBox[8]);
          let interested_threshold = String(boundingBox[9]);
          let engaged_threshold = String(boundingBox[10]);
          let unoccupied_threshold = String(boundingBox[11]);
          let constraint_rois = '';
          if (boundingBox.length > 12)
            constraint_rois = String(boundingBox[12]);
  
          obj.boundingBox = uiBoundingBox;
          obj.type = type;
          obj.direction_x = direction_x;
          obj.direction_y = direction_y;
          obj.age_tracking = age_tracking;
          obj.count_threshold = count_threshold;
          obj.interested_threshold = interested_threshold;
          obj.engaged_threshold = engaged_threshold;
          obj.unoccupied_threshold = unoccupied_threshold;
          obj.constraint_rois = constraint_rois;
  
          detectedObjects[currentSelectedFileIdx].push(obj);  
        } else {
          // For Data Format _v2
          let type = boundingBox[1];
          let polyAr = boundingBox[2];
          let uiBoundingBox = polyAr.map((vertex)=>{
            return [
              String(Math.round(vertex[0] * imageHeight / fileList[currentSelectedFileIdx].height)),
              String(Math.round(vertex[1] * imageWidth / fileList[currentSelectedFileIdx].width)),  
            ];
          })          
          let direction_x = boundingBox[3];
          let direction_y = boundingBox[4];
          let age_tracking = String(boundingBox[5]);
          let count_threshold = String(boundingBox[6]);
          let interested_threshold = String(boundingBox[7]);
          let engaged_threshold = String(boundingBox[8]);
          let unoccupied_threshold = String(boundingBox[9]);
          let constraint_rois = '';
          if (boundingBox.length > 10)
            constraint_rois = String(boundingBox[10]);
  
          obj.boundingBox = uiBoundingBox;
          obj.type = type;
          obj.direction_x = direction_x;
          obj.direction_y = direction_y;
          obj.age_tracking = age_tracking;
          obj.count_threshold = count_threshold;
          obj.interested_threshold = interested_threshold;
          obj.engaged_threshold = engaged_threshold;
          obj.unoccupied_threshold = unoccupied_threshold;
          obj.constraint_rois = constraint_rois;
  
          detectedObjects[currentSelectedFileIdx].push(obj);  
        }
      }

    }

    console.log(fileList);
    console.log(currentSelectedFileIdx);
    console.log(detectedObjects);

    this.setState({
      fileList: fileList,
      detectedObjects: detectedObjects,
      deviceId: deviceId,
      boundingBoxList: boundingBoxList,
    }, ()=> {
      $this.refs.extendedROIConfig.fetchExtendedROIList(deviceId);
    });    

  },

  selectImage(selectedIdx) {
    console.log('selectImage = ' + selectedIdx);
    let currentImageFile = this.state.fileList[selectedIdx].path;
    let dataUrl = this.state.fileList[selectedIdx].imageData;
    console.log(file);
    this.setState({
      currentSelectedFileIdx: selectedIdx,
      currentSelectedDetectionIdx: -1,
      currentImageFile: currentImageFile,
      currentImageDataUrl: dataUrl,
    });
  },

  deleteImage(selectedIdx) {
    console.log('deleteImage = ' + selectedIdx);
    let fileList = this.state.fileList;
    fileList.splice(selectedIdx, 1);
    let detectedObjects = this.state.detectedObjects;
    let newDetectedObjects = {};
    for (let idx in detectedObjects) {
      if (idx >= selectedIdx) {
        newDetectedObjects[idx - 1] = detectedObjects[idx];
      } else {
        newDetectedObjects[idx] = detectedObjects[idx];
      }
    }
    this.setState({
      fileList: fileList,
      detectedObjects: detectedObjects,
      currentSelectedFileIdx: -1,
      currentSelectedDetectionIdx: -1,
      currentImageFile: null,
      currentImageDataUrl: null,
    });
  },

  onConfirm(status) {
    console.log('status = ' + status);

    let fileList = this.state.fileList || [];
    let currentSelectedFileIdx = this.state.currentSelectedFileIdx;
    let detectedObjects = this.state.detectedObjects;
    let currentSelectedDetectionIdx = this.state.currentSelectedDetectionIdx;

    if (currentSelectedFileIdx != -1
          && currentSelectedDetectionIdx != -1 
          && detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].boundingBox 
          && detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].boundingBox != null) {    

      if (status === 'Y') {

        let selectType = this.refs['selectType'];
        let val_Type = selectType.value.trim();

        let editBoxDirX = this.refs['editBoxDirX'];
        let val_DirX = editBoxDirX.value.trim();

        let editBoxDirY = this.refs['editBoxDirY'];
        let val_DirY = editBoxDirY.value.trim();

        let editBoxAgeTracking = this.refs['editBoxAgeTracking'];
        let val_AgeTracking = editBoxAgeTracking.value.trim();

        let editBoxCountThreshold = this.refs['editBoxCountThreshold'];
        let val_CountThreshold = editBoxCountThreshold.value.trim();

        let editBoxInterestedThreshold = this.refs['editBoxInterestedThreshold'];
        let val_InterestedThreshold = editBoxInterestedThreshold.value.trim();

        let editBoxEngagedThreshold = this.refs['editBoxEngagedThreshold'];
        let val_EngagedThreshold = editBoxEngagedThreshold.value.trim();

        let editBoxUnoccupiedThreshold = this.refs['editBoxUnoccupiedThreshold'];
        let val_UnoccupiedThreshold = editBoxUnoccupiedThreshold.value.trim();

        let editBoxConstraintROIs = this.refs['editBoxConstraintROIs'];
        let val_ConstraintROIs = editBoxConstraintROIs.value.trim();

        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].type = val_Type;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].direction_x = val_DirX;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].direction_y = val_DirY;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].age_tracking = val_AgeTracking;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].count_threshold = val_CountThreshold;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].interested_threshold = val_InterestedThreshold;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].engaged_threshold = val_EngagedThreshold;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].unoccupied_threshold = val_UnoccupiedThreshold;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].status = status;
        detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].constraint_rois = val_ConstraintROIs;

        this.setState({
          currentSelectedDetectionIdx: -1,
          detectedObjects: detectedObjects,
        });

      } else {
        // Delete the selected bounding box
        detectedObjects[currentSelectedFileIdx].splice(currentSelectedDetectionIdx, 1);
        currentSelectedDetectionIdx = -1;

        this.setState({
          currentSelectedDetectionIdx: currentSelectedDetectionIdx,
          detectedObjects: detectedObjects,
        });

      }

    }
  },

  onClear: function() {
    this.onBoundingBoxesUpdated([]);
  },

  onFinish: function() {
    let fileList = this.state.fileList;
    let finished = true;

    // Confirm that every box has been saved.
    for (let i=0;i<fileList.length;i++) {
      let detectionList = this.state.detectedObjects[i];
      for (let j=0;j<detectionList.length;j++) {
        if (detectionList[j].status == null) {
          finished = false;
          break;
        }
      }
    }

    // Ignore confirmation for now
    finished = true;

    if (!finished) {
      alert('Please confirm all detection in every image.');
    } else {

      let fileList = this.state.fileList || [];
      let currentSelectedFileIdx = this.state.currentSelectedFileIdx;
      let detectedObjects = this.state.detectedObjects;

      if (currentSelectedFileIdx == -1)
        return;

      let imageWidth = 300;
      let imageHeight = 300;
      if (currentSelectedFileIdx != -1) {
        imageWidth = fileList[currentSelectedFileIdx].width;
        imageHeight = fileList[currentSelectedFileIdx].height;
      }

      imageHeight = 480 / imageWidth * imageHeight;
      imageWidth = 480;

      if (imageHeight > 480) {
        imageWidth = 480 / imageHeight * imageWidth;
        imageHeight = 480;
      }
      console.log('imageWidth = '+imageWidth);
      console.log('imageHeight = '+imageHeight);

      let boundingBoxList = [];
      for (let i=0;i<detectedObjects[currentSelectedFileIdx].length;i++) {
        if (detectedObjects[currentSelectedFileIdx][i].boundingBox 
          && detectedObjects[currentSelectedFileIdx][i].boundingBox != null) {
          let type = detectedObjects[currentSelectedFileIdx][i].type;
          let boundingBox = detectedObjects[currentSelectedFileIdx][i].boundingBox;
          let direction_x = detectedObjects[currentSelectedFileIdx][i].direction_x;
          let direction_y = detectedObjects[currentSelectedFileIdx][i].direction_y;
          let age_tracking = detectedObjects[currentSelectedFileIdx][i].age_tracking;
          let count_threshold = detectedObjects[currentSelectedFileIdx][i].count_threshold;
          let interested_threshold = detectedObjects[currentSelectedFileIdx][i].interested_threshold;
          let engaged_threshold = detectedObjects[currentSelectedFileIdx][i].engaged_threshold;
          let unoccupied_threshold = detectedObjects[currentSelectedFileIdx][i].unoccupied_threshold;
          let constraint_rois = detectedObjects[currentSelectedFileIdx][i].constraint_rois;
          boundingBoxList.push([
            this.VERSION, // Data Format Version
            parseInt(type),
            boundingBox.map((vertex)=> {
              return [
                Math.round(vertex[0] / imageHeight * fileList[currentSelectedFileIdx].height), 
                Math.round(vertex[1] / imageWidth * fileList[currentSelectedFileIdx].width),     
              ];
            }),
            direction_x,
            direction_y,
            parseInt(age_tracking),
            parseInt(count_threshold),
            parseInt(interested_threshold),
            parseInt(engaged_threshold),
            parseInt(unoccupied_threshold),
            constraint_rois,
          ]);
        }
      }
      let str = JSON.stringify(boundingBoxList); // , null, 2);
      let el = document.createElement('textarea');
      el.value = str;
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);      
      //alert(str + '\nThe text has been copied to clipboard.');
      if (this.state.showFileList ==  true) {
        this.refs.fileListControl.saveBoundingBoxToServer(boundingBoxList);
      } else {
        this.refs.deviceListControl.saveBoundingBoxToCurrentDevice(boundingBoxList);
      }
      //this.setState(this.getInitialState());
    }
  },

  render: function() {

    let $this = this;

    // User mode for hiding admin component 
    let userMode = (this.props.userMode?true:false);

    let fileName = this.props.projectName;
    let lastModified = '';
    
    let fileList = this.state.fileList || [];
    let currentSelectedFileIdx = this.state.currentSelectedFileIdx;
    let detectedObjects = this.state.detectedObjects;
    let currentSelectedDetectionIdx = this.state.currentSelectedDetectionIdx;
    let currentImageFile = this.state.currentImageFile || null;
    let currentImageDataUrl = this.state.currentImageDataUrl || null;
    let loading = this.state.loading || false;

    let deviceId = this.state.deviceId;
    let boundingBoxList_ = this.state.boundingBoxList;

    let imageWidth = 300;
    let imageHeight = 300;
    if (currentSelectedFileIdx != -1) {
      imageWidth = fileList[currentSelectedFileIdx].width;
      imageHeight = fileList[currentSelectedFileIdx].height;
      // fileName = fileList[currentSelectedFileIdx].name.split('=')[1];
      // if (fileName.length > 20)
      //  fileName = fileName.substring(0, 20) + '...';
      lastModified = new Date(fileList[currentSelectedFileIdx].imageData.lastModified).toLocaleString();
    }

    imageHeight = 480 / imageWidth * imageHeight;
    imageWidth = 480;

    if (imageHeight > 480) {
      imageWidth = 480 / imageHeight * imageWidth;
      imageHeight = 480;
    }
    console.log('imageWidth = '+imageWidth);
    console.log('imageHeight = '+imageHeight);

    let boundingBoxList = [];
    let detailBox = null;
    if (currentSelectedFileIdx != -1) {
      for (let i=0;i<detectedObjects[currentSelectedFileIdx].length;i++) {
        if (detectedObjects[currentSelectedFileIdx][i].boundingBox 
          && detectedObjects[currentSelectedFileIdx][i].boundingBox != null) {
          let boundingBox = detectedObjects[currentSelectedFileIdx][i].boundingBox;
          let status = detectedObjects[currentSelectedFileIdx][i].status;
          let borderColor = '#aaaaaa';
          if (currentSelectedDetectionIdx == i) {
            borderColor = '#ffff00';
            if (status === 'Y') {
              borderColor = '#00ff00';
            } else if (status === 'N') {
              borderColor = '#ff0000';
            }
          }

          // Find top / left / width / height of polygon
          let top = 65535;
          let left = 65535;
          let right = 0;
          let bottom = 0;
          for (var j=0;j<boundingBox.length;j++) {
            if (boundingBox[j][0] < top) top = boundingBox[j][0];
            if (boundingBox[j][0] > bottom) bottom = boundingBox[j][0];
            if (boundingBox[j][1] < left) left = boundingBox[j][1];
            if (boundingBox[j][1] > right) right = boundingBox[j][1];
          }

          let currentBoundingBox = (
            <div style={{
              position: 'absolute',
              top: top,
              left: left,
              height: (bottom - top),
              width: (right - left),
              border: '3px solid ' + borderColor,
            }} >
            </div>
          );
          boundingBoxList.push(currentBoundingBox);

          if (currentSelectedDetectionIdx == i) {
            // Calculate position of detail box
            let center_h = (top + bottom) / 2;      
            let detail_box_top = imageHeight + 10; // boundingBox[2] + 50;
            let detail_box_left = left - 15;
            //if (center_h > imageHeight / 2) {
            //  detail_box_top = boundingBox[0] - 80;
            //}
            if (detail_box_left < 0) {
              detail_box_left = 0;
            }
            detailBox = (
              <div style={{
                backgroundColor: 'rgb(238,245,255)',
                position: 'absolute',
                top: detail_box_top,
                left: detail_box_left,
                border: '2px solid rgb(201,201,201)',
                borderRadius: '8px !important',
                padding: '15px',
                zIndex: '100000',
              }} >
                <div style={{
                  color: 'black',
                }}>
                  <div>
                    <span style={{marginRight: '10px',}} >Type</span>
                    <select ref="selectType" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].type} >
                      <option value='1'>Sale</option>
                      <option value='2'>Operation</option>
                      <option value='3'>Queue Alert</option>
                      <option value='6'>Customer Attendant</option>
                    </select><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Dir-X</span><input type="text" ref="editBoxDirX" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].direction_x } /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Dir-Y</span><input type="text" ref="editBoxDirY" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].direction_y } /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Age Tracking</span><input type="text" ref="editBoxAgeTracking" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].age_tracking } /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Count Threshold</span><input type="text" ref="editBoxCountThreshold" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].count_threshold } /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Interested Threshold</span><input type="text" ref="editBoxInterestedThreshold" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].interested_threshold } /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Engaged Threshold</span><input type="text" ref="editBoxEngagedThreshold" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].engaged_threshold } /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Unoccupied / Distance Threshold</span><input type="text" ref="editBoxUnoccupiedThreshold" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].unoccupied_threshold } /><br/>
                  </div>
                  <div style={{marginTop: '5px',}}>
                    <span style={{marginRight: '10px',}} >Constraint ROIs</span><input type="text" ref="editBoxConstraintROIs" defaultValue={detectedObjects[currentSelectedFileIdx][currentSelectedDetectionIdx].constraint_rois } /><br/>
                  </div>
                </div>
                <div style={{
                  marginTop: '15px',
                }}>
                  <span style={{
                    display: 'inline-block',
                    width: '48%',
                    backgroundColor: 'rgb(242,122,69)',
                    padding: '8px 5px 8px 5px',            
                    cursor: 'pointer',  
                    margin: '1%',
                    color: 'white',
                    borderRadius: '20px !important',
                    textAlign: 'center',
                  }} onClick={$this.onConfirm.bind($this, 'Y')} >Save</span>
                  <span style={{
                    display: 'inline-block',
                    width: '48%',
                    backgroundColor: 'rgb(25,52,95)',
                    padding: '8px 5px 8px 5px',            
                    cursor: 'pointer',  
                    margin: '1%',
                    color: 'white',
                    borderRadius: '20px !important',
                    textAlign: 'center',
                  }} onClick={$this.onConfirm.bind($this, 'N')} >Delete</span>
                </div>
              </div>
            );
          }
        }
      }
    }

    let imageLayer = null;
    if(currentImageFile != null) {
      imageLayer = (
        <div id="imageContainer" 
          style={{
            backgroundColor: 'black',
            width: String(imageWidth)+'px',
            height: String(imageHeight)+'px',              
          }}
        >
          <img src={currentImageDataUrl}
            ref="theImg"
            style={{
              position: 'absolute',
              backgroundColor: 'black',
              width: String(imageWidth)+'px',
              height: String(imageHeight)+'px',              
            }}
          />
        </div>
      );
    }

    return (
    <div className="container-full">
      <TopBar title="Vision Analytic" userMode={userMode} />
      <div className="row" style={{marginTop: '0px', backgroundColor: 'white', }} >

        <div className="col-md-12">

          <div className="row" style={{
            height: '50px',
            backgroundColor: 'rgb(25,52,95)',
          }} >

            <div className="col-md-12" style={{
            }} >

              <div style={{
                marginTop: '12px',
                marginLeft: '5px',
                height: '39px',
                color: 'white',
                fontSize: '16px',
              }} >
                <div style={{marginLeft: '15px',}} >Project Name : {fileName} </div>
              </div>

              <div style={{
                marginTop: '40px',
                float: 'right',
                display: 'none',
              }} >
                <input ref="theFile" type="file" style={{
                  backgroundColor: 'white',
                  color: 'black',
                  display: 'inline',
                  margin: '0px',
                  paddingTop: '8px',
                  paddingBottom: '8px',
                  paddingLeft: '30px',
                  paddingRight: '30px',
                  cursor: 'pointer',
                  border: '2px solid #000',
                  textAlign: 'center',
                  fontSize: '16px',
                  fontWeight: '600',
                  borderRadius: '5px !important',
                }} 
                  onChange={$this.onFileSelected}
                >
                </input>            
              </div>              

            </div>

          </div>

          <div className="row" style={{
            marginTop: '40px',
          }} >

            <div className="col-md-4" style={{
            }} >

              <div style={{marginLeft: '20px', marginRight: '20px', marginBottom: '20px',
                display: ($this.state.showFileList?'normal':'none'),
              }} >
                <FileListControl 
                  ref='fileListControl'
                  dir='/videos/'
                  userMode={$this.props.userMode}
                  onFileSelected={$this.onRemoteFileSelected} 
                  onBoundingBoxesUpdated={$this.onBoundingBoxesUpdated}
                />
              </div>

              <div style={{marginLeft: '20px', marginRight: '20px', marginBottom: '20px',
                display: ($this.state.showFileList?'none':'normal'),
              }} >
                <EdgeDeviceListControl 
                  ref='deviceListControl'
                  projId={$this.props.projId}
                  userMode={$this.props.userMode}
                  refresh={$this.props.refresh}
                  onFileSelected={$this.onRemoteFileSelected} 
                  onUpdateImage={$this.onUpdateImage}
                  onBoundingBoxesUpdated={$this.onBoundingBoxesUpdated}
                />
              </div>

            </div>              

            <div className="col-md-4" style={{
            }} >
              <div 
                ref='dropZone'
                style={{
                  margin: 'auto',
                  position: 'relative',
                  backgroundColor: 'rgb(216,216,216)',
                  width: String(imageWidth) + 'px',
                  height: String(imageHeight) + 'px',
                  border: '2px solid rgb(201,201,201)',
                  marginBottom: '20px',
                }}>
              <div style={{
                position: 'absolute',
                left: '50%',
                top: '50%',
                transform: 'translate(-50%, -50%)',
                textAlign: 'center',
                color: 'black',
              }} >
                {((fileList.length==0)?'':'')}
              </div>
                {imageLayer}
                {boundingBoxList}
                <canvas id="drawLayer"
                  ref="drawLayer"
                  width={imageWidth}
                  height={imageHeight}
                  style={{
                    position: 'absolute',
                    top: '0px',
                    left: '0px',
                    width: String(imageWidth) + 'px' ,
                    height: String(imageHeight) + 'px' ,  
                  }}
                >
                </canvas>          
                {detailBox}
              </div>

            </div>
            <div className="col-md-4" style={{
              minHeight: '500px',
            }} >
              <div style={{marginLeft: '20px', marginRight: '20px'}} >
                <div style={{marginTop: '0px',display: 'none' }} >
                  <table style={{borderCollapse: 'collapse', display: ((fileList.length == 0)?'none':'table'), width:'100%' }} >
                    <thead>
                      <th style={{border: '1px solid black', padding: '5px',}} >Filename</th>
                      <th style={{border: '1px solid black', padding: '5px',}} ></th>
                    </thead>
                    <tbody>
                    {
                      (()=>{
                        return fileList.map((file, idx)=>{
                          let finished = true;
                          let detectionList = detectedObjects[idx];
                          for (let j=0;j<detectionList.length;j++) {
                            if (detectionList[j].status == null) {
                              finished = false;
                              break;
                            }
                          }
                          let iconClass = 'fa-question';
                          let color = '#888800';
                          if (finished) {
                            iconClass = 'fa-check';
                            color = '#008800';
                          }                    
                          return (
                            <tr key={'d1_'+idx} style={{
                              backgroundColor:(currentSelectedFileIdx == idx)?'#ddddff':'#ffffff',
                            }}                         
                            >
                              <td style={{
                                border: '1px solid black', padding: '5px',
                                cursor: 'pointer',
                              }} 
                                onClick={$this.selectImage.bind($this, idx)}
                              >
                                <span><i style={{color: color}} className={'fas '+iconClass}></i> {file.name}</span>
                              </td>
                              <td style={{border: '1px solid black', padding: '5px',}} >
                                <span><a href="#" onClick={$this.deleteImage.bind($this, idx)} >X</a></span>
                              </td>
                            </tr>
                          );
                        });
                      })()
                    }
                    </tbody>
                  </table>
                </div>

                <div style={{marginTop: '0px', display: ((loading)?'block':'none'), }} >
                  <img src='./assets/global/img/loading.gif' />
                </div>
                
                <div style={{marginTop: '0px', display: ((loading || userMode)?'none':'block'), }} >
                  <table style={{borderCollapse: 'collapse', display: ((fileList.length == 0)?'none':'table'),
                    borderRadius: '5px !important', 
                    width: '100%',
                  }} >
                    <thead style={{
                      backgroundColor: 'rgb(25,52,95)',
                      color: 'white',
                      fontWeight: '600',
                    }} >
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center', }} >No.</th>
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center',}} >Type</th>
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center',}} >Dir-X</th>
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center',}} >Dir-Y</th>
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center',}} >Age Tr.</th>
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center',}} >Count Th.</th>
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center',}} >Inter. Th.</th>
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center',}} >Engag. Th.</th>
                      <th style={{border: '1px solid rgb(25,52,95)', padding: '10px', textAlign: 'center',}} >Unocc. Th.</th>
                    </thead>
                    <tbody>
                    {                
                      (()=>{
                        if (currentSelectedFileIdx == -1)
                          return null;
                        return detectedObjects[currentSelectedFileIdx].map((detection, idx)=>{

                          let bgColor = 'rgb(184,192,205)';
                          if (idx % 2 == 0) {
                            bgColor = 'rgb(142,153,171)';
                          }
                          if (currentSelectedDetectionIdx == idx) {
                            bgColor = 'rgb(242,122,69)';
                          }
            
                          let iconClass = 'fa-question';
                          let color = '#ffff60';
                          let statusText = '???';
                          if (detection.status === 'Y') {
                            iconClass = 'fa-check';
                            statusText = 'Yes';
                            color = '#60ff60';
                          } else if (detection.status === 'N') {
                            iconClass = 'fa-times';
                            statusText = 'No';
                            color = '#ff6060';
                          }
                          return (
                            <tr key={'d2_'+idx} style={{
                              backgroundColor: bgColor,
                            }} 
                              onClick={$this.selectDetection.bind($this, idx)}
                            >
                              <td style={{color: 'white', border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',}} >
                                <span>{String(idx)}</span>
                              </td>
                              <td style={{
                                color: 'white',
                                border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',
                                cursor: 'pointer',
                              }} >
                                <span>{((detection.type==1)?'Sale':((detection.type==2)?'Oper':'Both'))} </span>
                              </td>
                              <td style={{
                                color: 'white',
                                border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',
                                cursor: 'pointer',
                              }} >
                                <span>{detection.direction_x} </span>
                              </td>
                              <td style={{
                                color: 'white',
                                border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',
                                cursor: 'pointer',
                              }} >
                                <span>{detection.direction_y} </span>
                              </td>
                              <td style={{
                                color: 'white',
                                border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',
                                cursor: 'pointer',
                              }} >
                                <span>{detection.age_tracking} </span>
                              </td>
                              <td style={{
                                color: 'white',
                                border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',
                                cursor: 'pointer',
                              }} >
                                <span>{detection.count_threshold} </span>
                              </td>
                              <td style={{
                                color: 'white',
                                border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',
                                cursor: 'pointer',
                              }} >
                                <span>{detection.interested_threshold} </span>
                              </td>
                              <td style={{
                                color: 'white',
                                border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',
                                cursor: 'pointer',
                              }} >
                                <span>{detection.engaged_threshold} </span>
                              </td>
                              <td style={{
                                color: 'white',
                                border: '1px solid ' + bgColor, padding: '10px', textAlign: 'center',
                                cursor: 'pointer',
                              }} >
                                <span>{detection.unoccupied_threshold} </span>
                              </td>
                            </tr>
                          );
                        });
                      })()
                    }
                    </tbody>
                  </table>
                </div>
                <div style={{marginTop: '20px', display: ((fileList.length == 0)?'none':'block'), }} >

                <div style={{
                    backgroundColor: 'rgb(25,52,95)',
                    color: 'white',
                    display: (userMode?'none':'inline'),
                    margin: '0px',
                    paddingTop: '12px',
                    paddingBottom: '12px',
                    paddingLeft: '30px',
                    paddingRight: '30px',
                    cursor: 'pointer',
                    border: 'none',
                    textAlign: 'center',
                    fontSize: '14px',
                    fontWeight: '600',
                    borderRadius: '20px !important',
                    whiteSpace: 'nowrap',
                  }} 
                    onClick={$this.onClear.bind($this)}
                  >
                    DELETE EXISTING ROIs
                  </div>            

                  <div style={{
                    backgroundColor: 'rgb(242,122,69)',
                    color: 'white',
                    display: (userMode?'none':'inline'),
                    margin: '0px',
                    marginLeft: '10px',
                    paddingTop: '12px',
                    paddingBottom: '12px',
                    paddingLeft: '30px',
                    paddingRight: '30px',
                    cursor: 'pointer',
                    border: 'none',
                    textAlign: 'center',
                    fontSize: '14px',
                    fontWeight: '600',
                    borderRadius: '20px !important',
                    whiteSpace: 'nowrap',
                  }} 
                    onClick={$this.onFinish.bind($this)}
                  >
                    SAVE ROIs TO DEVICE
                  </div>         
                     
                </div>

                <div style={{marginTop: '40px', }} >&nbsp;</div>

                <div style={{
                  display: ((userMode || deviceId==null)?'none':'inline'),
                }} >
                  <ExtendAreaConfigControl ref="extendedROIConfig" deviceId={deviceId} roiList={boundingBoxList_} ></ExtendAreaConfigControl>
                </div>

                <div style={{marginTop: '20px', }} />


                <div style={{marginTop: '30px', display: ((userMode)?'none':'block')}} >
                  <div><b>Notification Email List</b></div>
                  <EmailList isAdmin={false} />
                </div>                  

                <div style={{marginTop: '30px', display: ((userMode)?'none':'block')}} >
                  <div><b>Admin Notification Email List</b></div>
                  <EmailList isAdmin={true} />
                </div>       

              </div>

            </div>

          </div>
        </div>

      </div>

      <BottomBar />

    </div>
    );
  },

})

export default LabelControl
