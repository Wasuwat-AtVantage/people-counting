import React from 'react'
import TopBar from './TopBar.jsx'
import BottomBar from './BottomBar.jsx'

const ListProjectControl = React.createClass({


  getInitialState: function() {
    return{
      projects: [],
      loading: false,
      errorMessage: null,
      showingCreateDialog: false,
      project: null,
    }
  },
  
  componentWillMount: function() {
  	console.log('ListProjectControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('ListProjectControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('ListProjectControl::componentDidMount');
    this.refresh();
  },

  componentWillUnmount: function() {
  },

  createProject: function() {
    this.setState({
      showingCreateDialog: true,
      project: {
        id: null,
        projectType: "1",
        projectName: "",
        extId: "",
      },
    })
  },

  refresh: function() {
    let $this = this;
    this.setState({
        loading: true,
        errorMessage: null,
      },
      ()=>{
        let SERVER_ENDPOINT = 'api/list_project.jsp';
        let apiEndpoint = SERVER_ENDPOINT;
  
        fetch(apiEndpoint, {
          method: "GET",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }).then(function (response) {
          return response.json();
        }.bind(this)).then(function (result) {
          $this.setState({
            loading: false,
          }, ()=> {
            if (!result.s) {
              $this.setState({
                errorMessage: result.m,
              });
            } else {
              $this.setState({
                projects: result.m,
              });
            }
          });
        });
      }
    );
  },

  onSave: function() {
    let project = this.state.project;
    project.projectType = this.refs.projectType.value;
    project.projectName = this.refs.projectName.value;
    project.extId = this.refs.extId.value;
    if (project.id == null) {
      // Create new
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/create_project.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?project_name=' + encodeURI(project.projectName) 
            + '&project_type=' + encodeURI(project.projectType)
            + '&ext_id=' + encodeURI(project.extId)
            ;
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  project: null,
                  showingCreateDialog: false,
                }, ()=> {
                  $this.refresh();
                });
              }
            });
          });
        }
      );
  
    } else {
      // Modify exising
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/update_project.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?project_name=' + encodeURI(project.projectName) 
            + '&project_type=' + encodeURI(project.projectType)
            + '&ext_id=' + encodeURI(project.extId)
            + '&id=' + encodeURI(project.id)
            ;
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  project: null,
                  showingCreateDialog: false,
                }, ()=> {
                  $this.refresh();
                });
              }
            });
          });
        }
      );

    }
  },

  onDelete: function() {
    let project = this.state.project;
    if (project.id == null) {
      // Cancel new
      this.setState({
        project: null,
        errorMessage: null,
        showingCreateDialog: false,
      });
    } else {
      // Delete exising
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/delete_project.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(project.id);
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  project: null,
                  showingCreateDialog: false,
                }, ()=> {
                  $this.refresh();
                });
              }
            });
          });
        }
      );

    }
  },

  viewProject: function(id) {
    window.location = 'view_project.jsp?id=' + encodeURI(id);
  },

  manageProject: function(idx) {
    let $this = this;
    let project = this.state.projects[idx];
    this.setState({
      showingCreateDialog: true,
      project: project,
    }, ()=>{
      if (typeof $this.refs.projectName !== 'undefined') {
        $this.refs.extId.value = project.extId;
        $this.refs.projectName.value = project.projectName;
        $this.refs.projectType.value = project.projectType;
      }
    });
  },

  render: function() {

    let $this = this;
    let loading = this.state.loading;
    let errorMessage = this.state.errorMessage;
    let projects = this.state.projects;
    let showingCreateDialog = this.state.showingCreateDialog;
    let project = this.state.project;
    let userMode = this.props.userMode;

    return (
    <div className="container-full">
      <TopBar title="Vision Analytic" userMode={userMode} />
      <div className="row" style={{marginTop: '0px', backgroundColor: 'white', }} >

        <div className="col-md-2">
        </div>

        <div className="col-md-8">

          <div style={{marginTop: '20px', }} >&nbsp;</div>

          <div style={{
              backgroundColor: 'rgb(242,122,69)',
              color: 'white',
              display: (userMode?'none':'inline'),
              margin: '0px',
              marginLeft: '10px',
              paddingTop: '12px',
              paddingBottom: '12px',
              paddingLeft: '30px',
              paddingRight: '30px',
              cursor: 'pointer',
              border: 'none',
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: '600',
              borderRadius: '20px !important',
              whiteSpace: 'nowrap',
            }} 
              onClick={$this.createProject.bind($this)}
            >
              Create new Project
          </div>

          <div style={{marginLeft: '20px', marginTop: '40px', marginBottom: '10px', display: ((loading)?'block':'none'), }} >
            <img src='./assets/global/img/loading.gif' />
          </div>

          <div style={{marginLeft: '20px', marginTop: '40px', marginBottom: '10px', display: ((errorMessage != null)?'block':'none'), }} >
            <span style={{color:'red', }} >{errorMessage}</span>
          </div>

          <center>
            <div>
  
              <div style={{marginTop: '20px', }} >&nbsp;</div>

              <table style={{
                backgroundColor: 'white',
                width: '100%',
                color: 'white',
              }} >
                <thead>
                  <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Project Code</th>
                  <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Project Name</th>
                  <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >View</th>
                  <th style={{display: (userMode?'none':'block'), border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Manage</th>
                </thead>
                {
                  (()=>{
                    return projects.map((item, idx)=>{
                      let bgColor = 'rgb(184,192,205)';
                      if (idx % 2 == 0) {
                        bgColor = 'rgb(142,153,171)';
                      }
                      return (
                        <tbody key={'file_' + idx} >
                          <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.extId}</td>
                          <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.projectName}</td>
                          <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                          <span style={{color:'white', cursor:'pointer'}} onClick={$this.viewProject.bind($this, item.id)} >
                          [View Project]
                          </span>
                          </td>
                          <td style={{display: (userMode?'none':'block'), border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                          <span style={{color:'white', cursor:'pointer'}} onClick={$this.manageProject.bind($this, idx)} >
                          [Edit Project]
                          </span>
                          </td>
                        </tbody>
                      );
                    })
                  })()
                }
              </table>

              <div style={{marginTop: '20px', }} >&nbsp;</div>

            </div>
          </center>
        </div>

        <div className="col-md-2">
        </div>

        {
          (()=>{
            console.log('showingCreateDialog = ' + showingCreateDialog);
            if (showingCreateDialog) {
              return (
                <div style={{
                  backgroundColor: 'rgb(238,245,255)',
                  position: 'absolute',
                  top: '50%',
                  left: '50%',
                  transform: 'translate(-50%, -50%)',
                  border: '2px solid rgb(201,201,201)',
                  borderRadius: '8px !important',
                  padding: '15px',
                  zIndex: '100000',
                }} >
                  <div style={{
                    color: 'black',
                  }}>
                    <div>
                      <span style={{marginRight: '10px',}} >Type</span>
                      <select ref="projectType" defaultValue={project.type} >
                        <option value='1'>Human Vision Analytic</option>
                      </select><br/>
                    </div>
                    <div style={{marginTop: '5px',}}>
                      <span style={{marginRight: '10px',}} >Project Name</span><input type="text" style={{margin: '3px',}} ref="projectName" defaultValue={project.projectName} /><br/>
                    </div>
                    <div style={{marginTop: '5px',}}>
                      <span style={{marginRight: '10px',}} >Project Code</span><input type="text" style={{margin: '3px',}} ref="extId" defaultValue={project.extId} /><br/>
                    </div>
                  </div>

                  <div style={{marginLeft: '20px', marginTop: '10px', marginBottom: '10px', display: ((errorMessage != null)?'block':'none'), }} >
                    <span style={{color:'red', }} >{errorMessage}</span>
                  </div>

                  <div style={{
                    marginTop: '15px',
                  }}>
                    <span style={{
                      display: 'inline-block',
                      width: '48%',
                      backgroundColor: 'rgb(242,122,69)',
                      padding: '8px 5px 8px 5px',            
                      cursor: 'pointer',  
                      margin: '1%',
                      color: 'white',
                      borderRadius: '20px !important',
                      textAlign: 'center',
                    }} onClick={$this.onSave} >Save</span>
                    <span style={{
                      display: 'inline-block',
                      width: '48%',
                      backgroundColor: 'rgb(25,52,95)',
                      padding: '8px 5px 8px 5px',            
                      cursor: 'pointer',  
                      margin: '1%',
                      color: 'white',
                      borderRadius: '20px !important',
                      textAlign: 'center',
                    }} onClick={$this.onDelete} >Delete</span>
                  </div>
                </div>        
              );
            }
          })()
        }

      </div>
      <BottomBar />
    </div>
    );
  },

})

export default ListProjectControl
