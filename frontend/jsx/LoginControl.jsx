import React from 'react'
import TopBar from './TopBar.jsx'
import BottomBar from './BottomBar.jsx'

const LoginControl = React.createClass({


  getInitialState: function() {
    return{
      loading: false,
      errorMessage: null,
    }
  },
  
  componentWillMount: function() {
  	console.log('LoginControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('LoginControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('LoginControl::componentDidMount');
  },

  componentWillUnmount: function() {
  },

  onLogin: function() {
    let username = this.refs.username.value;
    let password = this.refs.password.value;

    let $this = this;
    this.setState({
        loading: true,
        errorMessage: null,
      },
      ()=>{
        let SERVER_ENDPOINT = 'api/login.jsp';
        let apiEndpoint = SERVER_ENDPOINT + '?username=' + encodeURI(username) + '&password=' + encodeURI(password);
  
        fetch(apiEndpoint, {
          method: "GET",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }).then(function (response) {
          return response.json();
        }.bind(this)).then(function (result) {
          $this.setState({
            loading: false,
          }, ()=> {
            if (!result.s) {
              $this.setState({
                errorMessage: result.m,
              })
            } else {
              window.location = 'index.jsp';
            }
          });
        });
      }
    );

  },

  render: function() {

    let $this = this;
    let loading = this.state.loading;
    let errorMessage = this.state.errorMessage;

    return (
    <div className="container-full">
      <TopBar title="Vision Analytic" userMode={false} />
      <div className="row" style={{marginTop: '0px', backgroundColor: 'white', }} >

        <div className="col-md-12">
          <center>
            <div>
  
              <div style={{marginTop: '20px', }} >&nbsp;</div>

              <table>
                <tr>
                  <td><div style={{padding: '10px',}} >Username</div></td>
                  <td><div style={{padding: '10px',}} ><input type="text" id="username" ref="username" /></div></td>
                </tr>
                <tr>
                  <td><div style={{padding: '10px',}} >Password</div></td>
                  <td><div style={{padding: '10px',}} ><input type="password" id="password" ref="password" /></div></td>
                </tr>
              </table>

              <div style={{marginTop: '0px', marginBottom: '10px', display: ((loading)?'block':'none'), }} >
                <img src='./assets/global/img/loading.gif' />
              </div>

              <div style={{marginTop: '0px', marginBottom: '10px', display: ((errorMessage != null)?'block':'none'), }} >
                <span style={{color:'red', }} >{errorMessage}</span>
              </div>

              <div style={{marginTop: '20px', }} >&nbsp;</div>

              <div style={{
                backgroundColor: 'rgb(242,122,69)',
                color: 'white',
                display: 'inline',
                margin: '0px',
                marginLeft: '10px',
                paddingTop: '12px',
                paddingBottom: '12px',
                paddingLeft: '30px',
                paddingRight: '30px',
                cursor: 'pointer',
                border: 'none',
                textAlign: 'center',
                fontSize: '14px',
                fontWeight: '600',
                borderRadius: '20px !important',
                whiteSpace: 'nowrap',
              }} 
                onClick={$this.onLogin.bind($this)}
              >
                Login
              </div>

              <div style={{marginTop: '20px', }} >&nbsp;</div>

            </div>
          </center>
        </div>

      </div>
      <BottomBar />
    </div>
    );
  },

})

export default LoginControl
