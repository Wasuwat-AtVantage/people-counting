import React from 'react'
import TopBar from './TopBar.jsx'
import BottomBar from './BottomBar.jsx'

const ReportControl = React.createClass({


  getInitialState: function() {
    return{
    }
  },
  
  componentWillMount: function() {
  	console.log('ReportControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('ReportControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('ReportControl::componentDidMount');
  },

  componentWillUnmount: function() {
  },

  render: function() {

    let $this = this;

    return (
    <div className="container-full">
      <TopBar title="Vision Analytic" userMode={false} />
      <div className="row" style={{marginTop: '0px', backgroundColor: 'white', }} >

        <div className="col-md-12">
          <center>
            <iframe width="1140" height="541.25" 
              src="https://app.powerbi.com/reportEmbed?reportId=7ac89475-d25c-48a0-a672-8f166e7dc652&autoAuth=true&ctid=103b20d8-265e-400d-8786-2791bc707674&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXNvdXRoLWVhc3QtYXNpYS1iLXByaW1hcnktcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D"
              frameborder="0" 
              allowFullScreen="true">
            </iframe>          
          </center>
        </div>

      </div>
      <BottomBar />
    </div>
    );
  },

})

export default ReportControl
