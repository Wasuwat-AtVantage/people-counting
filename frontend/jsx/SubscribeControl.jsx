import React from 'react'
import firebase from 'firebase'
import TopBar from './TopBar.jsx'
import BottomBar from './BottomBar.jsx'

const SubscribeControl = React.createClass({

  // Members

  getInitialState: function() {
    return{
      loading: false,
    }
  },
  
  componentWillMount: function() {
  	console.log('SubscribeControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('SubscribeControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('SubscribeControl::componentDidMount');
  },

  componentWillUnmount: function() {
  },

  onSubscribePushNotification: async function() {
    let $this = this;
    try {
      this.setState({
        loading: true,
      });
      console.log('Generating push user token:');      
      const messaging = firebase.messaging();
      await messaging.requestPermission();
      const token = await messaging.getToken();
      console.log('Push user token:', token);      
      // alert(token);

      let SUBSCRIBE_ENDPOINT = 'subscribe_notification.jsp';
      let subscribeEndpoint = SUBSCRIBE_ENDPOINT + '?key=' + encodeURI(token) + 
        '&topic=' + encodeURI('/topics/all');
      fetch(subscribeEndpoint, {
        method: "GET",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then(function (rresponse) {
        return rresponse.json();
      }.bind(this)).then(function (rresult) {
        $this.setState({
          loading: false,
        });        
        alert('Your device has been subscribed to notification alert.');
      });

      return token;
    } catch (error) {
      console.error(error);
      alert(error);
    }
  }, 

  render: function() {

    let $this = this;
    let loading = this.state.loading;

    return (
      <div className="container-full">
        <TopBar title="Vision Analytic" userMode={userMode} />
        <div style={{marginTop: '50px'}}></div>
        <div className="row" style={{marginTop: '0px', backgroundColor: 'white', }} >
          <div style={{marginTop: '0px', marginBottom: '10px', display: ((loading)?'block':'none'), }} >
            <center>
              <img src='./assets/global/img/loading.gif' />
            </center>
          </div>

          <div style={{marginTop: '20px', textAlign: 'center' }} >
            <div style={{
              backgroundColor: 'rgb(242,122,69)',
              color: 'white',
              display: 'inline',
              margin: '0px',
              marginLeft: '10px',
              paddingTop: '12px',
              paddingBottom: '12px',
              paddingLeft: '30px',
              paddingRight: '30px',
              cursor: 'pointer',
              border: 'none',
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: '600',
              borderRadius: '20px !important',
            }} 
              onClick={$this.onSubscribePushNotification.bind($this)}
            >
              SUBSCRIBE PUSH NOTIFICATION
            </div>  
          </div>

        </div>

        <div style={{marginTop: '70px'}}></div>

        <BottomBar />

      </div>
    );

  },

})

export default SubscribeControl
