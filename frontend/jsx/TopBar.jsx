import React from 'react'

const TopBar = React.createClass({


  getInitialState: function() {
    return{
      showingMenu: false,
    }
  },
  
  componentWillMount: function() {
  	console.log('TopBar::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('TopBar::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('TopBar::componentDidMount');
  },

  onBack: function() {
    if (this.props.onBack) {
      this.props.onBack();
    } else {
      window.location = 'home.jsp';
    }
  },

  onLogOut: function() {
    let $this = this;
    let SERVER_ENDPOINT = 'api/logout.jsp';
    let apiEndpoint = SERVER_ENDPOINT;

    fetch(apiEndpoint, {
      method: "GET",
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(function (response) {
      return response.json();
    }.bind(this)).then(function (result) {
      $this.setState({
        loading: false,
      }, ()=> {
        window.location = 'index.jsp';
      });
    });
    
  },

  onListProject: function() {
    window.location = 'list_project.jsp';
  },

  onUserManagement: function() {
    window.location = 'user_management.jsp';
  },

  toggleMenu: function() {
    this.setState({
      showingMenu: (!this.state.showingMenu)
    });
  },

  render: function() {

    let $this = this;
    let showingMenu = this.state.showingMenu;
    let title = this.props.title || '';
    let userMode = this.props.userMode;

    return (
      <div className="row" style={{marginTop: '0px', backgroundColor: 'white', }} >
        <div className="col-md-12"
          style={{
            backgroundColor: 'white',
            height: '80px',
            display: 'inline-block',
          }}
        >
          <div style={{
            display: 'inline-block',
          }}>
            <img src="assets/global/img/bizcuit_logo.svg" width="150" style={{
              marginTop: '10px',
              marginLeft: '20px',
            }} />

            <div style={{
              marginLeft: '20px',
              marginTop: '10px',
              display: 'inline-block',
              width: '2px',
              height: '60px',
              backgroundColor: 'rgb(216,216,216)',
              verticalAlign: 'middle',
            }}>
            </div>

            <div style={{
              marginLeft: '10px',
              marginTop: '10px',
              display: 'inline-block',
              verticalAlign: 'middle',
              fontSize: '18px',
              fontWeight: '600',
            }}>
              Vision Analytic
            </div>

          </div>


          <div style={{
            backgroundColor: 'white',
            margin: '10px',
            marginLeft: '30px',
            padding: '5px',
            paddingRight: '10px',
            cursor: 'pointer',
            color: 'white',
            display: (this.props.onBack)?'inline-block':'none',
          }} 
            onClick={this.onBack}
          >
            <img height="30" src="assets/global/img/icon_back.png" style={{
              marginTop: '-5px',
              marginRight: '15px',
            }} />
            Back
          </div>

          <div style={{
            marginLeft: '20px',
            marginTop: '15px',
            color: 'white',
            display: 'inline-block',
          }} >
            | {title}
          </div>

          <div style={{
            position: 'fixed',
            right: '0px',
            top: '0px',
            zIndex: '9999',
            backgroundColor: 'white',
          }} >

            <div style={{
              margin: '10px',
              marginTop: '15px',
              padding: '5px',
              fontSize: '18px',
              paddingRight: '10px',
              color: 'white',
              position: 'relative',
            }} 
            >
              <img height="20" src="assets/global/img/user.svg" style={{
                marginTop: '-3px',
                marginRight: '10px',
              }} />
              <div style={{
                marginLeft: '0px',
                marginTop: '0px',
                display: 'inline-block',
                width: '2px',
                height: '20px',
                backgroundColor: 'rgb(216,216,216)',
                verticalAlign: 'middle',
              }}>
              </div>
              <img height="20" src="assets/global/img/line-menu.svg" style={{
                marginTop: '-2px',
                marginLeft: '10px',
                marginRight: '25px',
                cursor: 'pointer',                
              }} 
                onClick={$this.toggleMenu}
              />

              <div style={{
                position: 'absolute',
                top: '40px',
                zIndex: '1000',
                right: '30px',
                backgroundColor: 'rgb(119,123,132)',
                paddingTop: '2px',
                paddingBottom: '3px',
                paddingLeft: '20px',
                paddingRight: '20px',
                width: '200px',
                fontSize: '14px',
                color: 'white',
                borderRadius: '5px !important',
                border: '1px solid rgb(255,255,255)',
                cursor: 'pointer',
                display: (showingMenu)?'inline':'none',
              }} 
              >

                <div style={{
                  margin: '10px',
                }} 
                  onClick={$this.onListProject}
                >
                  Project List
                </div>


                <div style={{
                  margin: '10px',
                  display: (userMode?'none':'block'),
                }} 
                  onClick={$this.onUserManagement}
                >
                  User Management
                </div>

                <div style={{
                  margin: '10px',
                }} 
                  onClick={$this.onLogOut}
                >
                  Log out
                </div>

              </div>

            </div>

          </div>

        </div>
        <div className="col-md-12" style={{
          backgroundColor: 'rgb(24,44,74)',
          height: '2px',  
        }}
        >
        </div>
      </div>
    );
  },

})

export default TopBar
