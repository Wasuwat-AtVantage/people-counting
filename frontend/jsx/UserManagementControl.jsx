import React from 'react'
import TopBar from './TopBar.jsx'
import BottomBar from './BottomBar.jsx'

const UserManagementControl = React.createClass({


  getInitialState: function() {
    return{
      users: [],
      loading: false,
      errorMessage: null,
      showingCreateDialog: false,
      user: null,
      projects: [],
      projectMap: {},
    }
  },
  
  componentWillMount: function() {
  	console.log('UserManagementControl::componentWillMount');
  },

  componentWillReceiveProps(nextProps) {
  	console.log('UserManagementControl::componentWillReceiveProps');
  	console.log(nextProps);
  },

  componentDidMount: function() {
    console.log('UserManagementControl::componentDidMount');
    this.fetchProjects();
  },

  componentWillUnmount: function() {
  },

  projectNameOf: function(projectId) {
    console.log(projectId);
    if (projectId in this.state.projectMap) {
      return this.state.projectMap[projectId].projectName;
    } else {
      return "";
    }
  },

  userTypeNameOf: function(userType) {
    if (!userType || userType === "") return "";
    return {'A': 'Admin', 'U': 'User'}[userType];
  },

  createUser: function() {
    this.setState({
      showingCreateDialog: true,
      user: {
        id: null,
        username: "",
        email: "",
        fullName: "",
        secretCode: "",
        userType: "",
      },
    })
  },

  fetchProjects: function() {
    let $this = this;
    this.setState({
        loading: true,
        errorMessage: null,
      },
      ()=>{
        let SERVER_ENDPOINT = 'api/list_project.jsp';
        let apiEndpoint = SERVER_ENDPOINT;
  
        fetch(apiEndpoint, {
          method: "GET",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }).then(function (response) {
          return response.json();
        }.bind(this)).then(function (result) {
          $this.setState({
            loading: false,
          }, ()=> {
            if (!result.s) {
              $this.setState({
                errorMessage: result.m,
              });
            } else {
              let projects = result.m;
              let projectMap = {};
              for (let i=0;i<projects.length;i++) {
                projectMap[projects[i].id] = projects[i];
              }
              $this.setState({
                projects: projects,
                projectMap: projectMap,
              }, ()=>{
                $this.refresh();
              });
            }
          });
        });
      }
    );
  },

  refresh: function() {
    let $this = this;
    this.setState({
        loading: true,
        errorMessage: null,
      },
      ()=>{
        let SERVER_ENDPOINT = 'api/list_user.jsp';
        let apiEndpoint = SERVER_ENDPOINT;
  
        fetch(apiEndpoint, {
          method: "GET",
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }).then(function (response) {
          return response.json();
        }.bind(this)).then(function (result) {
          $this.setState({
            loading: false,
          }, ()=> {
            if (!result.s) {
              $this.setState({
                errorMessage: result.m,
              });
            } else {
              console.log(result.m);
              $this.setState({
                users: result.m,
              });
            }
          });
        });
      }
    );
  },

  onSave: function() {
    let user = this.state.user;

    user.username = this.refs.username.value;
    user.email = this.refs.email.value;
    user.fullName = this.refs.fullName.value;
    user.secretCode = this.refs.secretCode.value;
    user.userType = this.refs.userType.value;
    let projectId = this.refs.projectId.value;
    if (user.id == null) {
      // Create new
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/create_user.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?username=' + encodeURI(user.username) 
            + '&email=' + encodeURI(user.email)
            + '&full_name=' + encodeURI(user.fullName)
            + '&secret_code=' + encodeURI(user.secretCode)
            + '&user_type=' + encodeURI(user.userType)
            + '&status_code=A'
            + '&project_id=' + encodeURI(projectId)
            ;
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  user: null,
                  showingCreateDialog: false,
                }, ()=> {
                  $this.refresh();
                });
              }
            });
          });
        }
      );
  
    } else {
      // Modify exising
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/update_user.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?username=' + encodeURI(user.username) 
            + '&email=' + encodeURI(user.email)
            + '&full_name=' + encodeURI(user.fullName)
            + '&secret_code=' + encodeURI(user.secretCode)
            + '&user_type=' + encodeURI(user.userType)
            + '&project_id=' + encodeURI(projectId)
            + '&status_code=A'
            + '&id=' + encodeURI(user.id)
            ;
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  user: null,
                  showingCreateDialog: false,
                }, ()=> {
                  $this.refresh();
                });
              }
            });
          });
        }
      );

    }
  },

  onDelete: function() {
    let user = this.state.user;
    if (user.id == null) {
      // Cancel new
      this.setState({
        user: null,
        errorMessage: null,
        showingCreateDialog: false,
      });
    } else {
      // Delete exising
      let $this = this;
      this.setState({
          loading: true,
          errorMessage: null,
        },
        ()=>{
          let SERVER_ENDPOINT = 'api/delete_user.jsp';
          let apiEndpoint = SERVER_ENDPOINT + '?id=' + encodeURI(user.id);
    
          fetch(apiEndpoint, {
            method: "GET",
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          }).then(function (response) {
            return response.json();
          }.bind(this)).then(function (result) {
            $this.setState({
              loading: false,
            }, ()=> {
              if (!result.s) {
                $this.setState({
                  errorMessage: result.m,
                })
              } else {
                $this.setState({
                  user: null,
                  showingCreateDialog: false,
                }, ()=> {
                  $this.refresh();
                });
              }
            });
          });
        }
      );

    }
  },

  manageUser: function(idx) {
    let $this = this;
    let user = this.state.users[idx];
    this.setState({
      showingCreateDialog: true,
      user: user,
    }, ()=>{
      if (typeof $this.refs.username !== 'undefined') {    
        $this.refs.username.value = user.username;
        $this.refs.email.value = user.email;
        $this.refs.fullName.value = user.fullName;
        $this.refs.secretCode.value = user.secretCode;
        $this.refs.userType.value = user.userType;
        $this.refs.projectId.value = user.projectId;
      }
    });
  },

  render: function() {

    let $this = this;
    let loading = this.state.loading;
    let errorMessage = this.state.errorMessage;
    let users = this.state.users;
    let showingCreateDialog = this.state.showingCreateDialog;
    let user = this.state.user;
    let userMode = this.props.userMode;
    let projects = this.state.projects;

    return (
    <div className="container-full">
      <TopBar title="Vision Analytic" userMode={userMode} />
      <div className="row" style={{marginTop: '0px', backgroundColor: 'white', }} >

        <div className="col-md-2">
        </div>

        <div className="col-md-8">

          <div style={{marginTop: '20px', }} >&nbsp;</div>

          <div style={{
              backgroundColor: 'rgb(242,122,69)',
              color: 'white',
              display: (userMode?'none':'inline'),
              margin: '0px',
              marginLeft: '10px',
              paddingTop: '12px',
              paddingBottom: '12px',
              paddingLeft: '30px',
              paddingRight: '30px',
              cursor: 'pointer',
              border: 'none',
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: '600',
              borderRadius: '20px !important',
              whiteSpace: 'nowrap',
            }} 
              onClick={$this.createUser.bind($this)}
            >
              Create new User
          </div>

          <div style={{marginLeft: '20px', marginTop: '40px', marginBottom: '10px', display: ((loading)?'block':'none'), }} >
            <img src='./assets/global/img/loading.gif' />
          </div>

          <div style={{marginLeft: '20px', marginTop: '40px', marginBottom: '10px', display: ((errorMessage != null)?'block':'none'), }} >
            <span style={{color:'red', }} >{errorMessage}</span>
          </div>

          <center>
            <div>
  
              <div style={{marginTop: '20px', }} >&nbsp;</div>

              <table style={{
                backgroundColor: 'white',
                width: '100%',
                color: 'white',
              }} >
                <thead>
                  <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Username</th>
                  <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Full Name</th>
                  <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >E-Mail</th>
                  <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Project</th>
                  <th style={{border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >User Type</th>
                  <th style={{display: (userMode?'none':'block'), border: '1px solid rgb(25,52,95)', padding: '2px 5px', backgroundColor: 'rgb(25,52,95)'}} >Manage</th>
                </thead>
                {
                  (()=>{
                    return users.map((item, idx)=>{
                      let bgColor = 'rgb(184,192,205)';
                      if (idx % 2 == 0) {
                        bgColor = 'rgb(142,153,171)';
                      }
                      return (
                        <tbody key={'file_' + idx} >
                          <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.username}</td>
                          <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.fullName}</td>
                          <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{item.email}</td>
                          <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{$this.projectNameOf(item.projectId)}</td>
                          <td style={{border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >{$this.userTypeNameOf(item.userType)}</td>
                          <td style={{display: (userMode?'none':'block'), border: '1px solid ' + bgColor, padding: '2px 5px', backgroundColor: bgColor}} >
                          <span style={{color:'white', cursor:'pointer'}} onClick={$this.manageUser.bind($this, idx)} >
                          [Edit User]
                          </span>
                          </td>
                        </tbody>
                      );
                    })
                  })()
                }
              </table>

              <div style={{marginTop: '20px', }} >&nbsp;</div>

            </div>
          </center>
        </div>

        <div className="col-md-2">
        </div>

        {
          (()=>{
            console.log('showingCreateDialog = ' + showingCreateDialog);
            if (showingCreateDialog) {
              return (
                <div style={{
                  backgroundColor: 'rgb(238,245,255)',
                  position: 'absolute',
                  top: '50%',
                  left: '50%',
                  transform: 'translate(-50%, -50%)',
                  border: '2px solid rgb(201,201,201)',
                  borderRadius: '8px !important',
                  padding: '15px',
                  zIndex: '100000',
                }} >
                  <div style={{
                    color: 'black',
                  }}>
                    <div>
                      <span style={{marginRight: '10px',}} >User Type</span>
                      <select ref="userType" defaultValue={user.userType} disabled={(user.id != null)? true : null} >
                        <option value='A'>Admin</option>
                        <option value='U'>User</option>
                      </select><br/>
                    </div>
                    <div style={{marginTop: '5px',}}>
                      <span style={{marginRight: '10px',}} >Username</span><input type="text" style={{margin: '3px',}} ref="username" defaultValue={user.username} /><br/>
                    </div>
                    <div style={{marginTop: '5px',}}>
                      <span style={{marginRight: '10px',}} >Password</span><input type="text" style={{margin: '3px',}} ref="secretCode" defaultValue={user.secretCode} /><br/>
                    </div>
                    <div style={{marginTop: '5px',}}>
                      <span style={{marginRight: '10px',}} >E-Mail</span><input type="text" style={{margin: '3px',}} ref="email" defaultValue={user.email} /><br/>
                    </div>
                    <div style={{marginTop: '5px',}}>
                      <span style={{marginRight: '10px',}} >Full Name</span><input type="text" style={{margin: '3px',}} ref="fullName" defaultValue={user.fullName} /><br/>
                    </div>
                    <div>
                      <span style={{marginRight: '10px',}} >Project</span>
                      <select ref="projectId" defaultValue={user.projectId} disabled={(user.id != null)? true : null} >
                        {
                          (()=>{
                            return projects.map((project)=>{
                              return (
                              <option value={project.id} key={project.id} >{project.projectName}</option>
                              );
                            })
                          })()
                        }
                      </select><br/>
                    </div>
                  </div>

                  <div style={{marginLeft: '20px', marginTop: '10px', marginBottom: '10px', display: ((errorMessage != null)?'block':'none'), }} >
                    <span style={{color:'red', }} >{errorMessage}</span>
                  </div>

                  <div style={{
                    marginTop: '15px',
                  }}>
                    <span style={{
                      display: 'inline-block',
                      width: '48%',
                      backgroundColor: 'rgb(242,122,69)',
                      padding: '8px 5px 8px 5px',            
                      cursor: 'pointer',  
                      margin: '1%',
                      color: 'white',
                      borderRadius: '20px !important',
                      textAlign: 'center',
                    }} onClick={$this.onSave} >Save</span>
                    <span style={{
                      display: ((user.id==='0')?'none':'inline-block'),
                      width: '48%',
                      backgroundColor: 'rgb(25,52,95)',
                      padding: '8px 5px 8px 5px',            
                      cursor: 'pointer',  
                      margin: '1%',
                      color: 'white',
                      borderRadius: '20px !important',
                      textAlign: 'center',
                    }} onClick={$this.onDelete} >Delete</span>
                  </div>
                </div>        
              );
            }
          })()
        }

      </div>
      <BottomBar />
    </div>
    );
  },

})

export default UserManagementControl
