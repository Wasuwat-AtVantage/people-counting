import React from 'react'
import ReactDOM from 'react-dom'
import firebase from 'firebase'

import LabelControl from './LabelControl.jsx'
import TopBar from './TopBar.jsx'
import BottomBar from './BottomBar.jsx'
import FileListControl from './FileListControl.jsx'
import EdgeDeviceListControl from './EdgeDeviceListControl.jsx'
import SubscribeControl from './SubscribeControl.jsx'
import AdminControl from './AdminControl.jsx'
import EmailList from './EmailList.jsx'
import ReportControl from './ReportControl.jsx'
import LoginControl from './LoginControl.jsx'
import ListProjectControl from './ListProjectControl.jsx'
import UserManagementControl from './UserManagementControl.jsx'
import ExtendAreaConfigControl from './ExtendAreaConfigControl.jsx'

console.log('REACT LOADED!');

window.React = React;
window.ReactDOM = ReactDOM;

window.atv = {};
window.atv.react = {};
window.atv.react.util = {};

window.atv.react.comp = {};
window.atv.react.comp['LabelControl'] = LabelControl;
window.atv.react.comp['TopBar'] = TopBar;
window.atv.react.comp['BottomBar'] = BottomBar;
window.atv.react.comp['FileListControl'] = FileListControl;
window.atv.react.comp['EdgeDeviceListControl'] = EdgeDeviceListControl;
window.atv.react.comp['SubscribeControl'] = SubscribeControl;
window.atv.react.comp['AdminControl'] = AdminControl;
window.atv.react.comp['EmailList'] = EmailList;
window.atv.react.comp['ReportControl'] = ReportControl;
window.atv.react.comp['LoginControl'] = LoginControl;
window.atv.react.comp['ListProjectControl'] = ListProjectControl;
window.atv.react.comp['UserManagementControl'] = UserManagementControl;
window.atv.react.comp['ExtendAreaConfigControl'] = ExtendAreaConfigControl;

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyB5hEglWKLfLEANmK8Dk879M28pNeBl5e4",
  authDomain: "machinelearning-246206.firebaseapp.com",
  databaseURL: "https://machinelearning-246206.firebaseio.com",
  projectId: "machinelearning-246206",
  storageBucket: "",
  messagingSenderId: "951964675017",
  appId: "1:951964675017:web:788ecd44775f1c12e01618",
  measurementId: "G-B01ECBC6X8"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
console.log('Firebase is inialized.');

navigator.serviceWorker
  .register('/PeopleCounting/firebase-messaging-sw.js')
  .then((registration) => {
    firebase.messaging().useServiceWorker(registration);
  });

