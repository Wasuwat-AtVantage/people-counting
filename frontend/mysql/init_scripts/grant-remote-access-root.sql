CREATE DATABASE IF NOT EXISTS edge CHARACTER SET UTF8 COLLATE utf8_bin;

USE edge;

CREATE TABLE IF NOT EXISTS _users (
  id VARCHAR(64) NOT NULL,
  username VARCHAR(64),
  email VARCHAR(64),
  full_name VARCHAR(256),
  secret_code VARCHAR(64),
  status_code VARCHAR(64),
  user_type VARCHAR(64),
  
  project_id VARCHAR(64),
  sub_project_id VARCHAR(64),
  json_data TEXT,

  PRIMARY KEY (id), 
  KEY(username),
  KEY(email),
  KEY(project_id),
  KEY(sub_project_id)
);

CREATE TABLE IF NOT EXISTS _projects (
  id VARCHAR(64) NOT NULL,
  ext_id VARCHAR(256),

  project_name VARCHAR(256),
  project_type VARCHAR(64),
  status_code VARCHAR(64),
  valid_from DATETIME,
  valid_to DATETIME,
  valid_data_from DATETIME,
  valid_data_to DATETIME,

  PRIMARY KEY (id), 
  KEY(ext_id),
  KEY(project_name),
  KEY(valid_from),
  KEY(status_code),
  KEY(valid_data_from)
);

CREATE TABLE IF NOT EXISTS _places (
  id VARCHAR(64) NOT NULL,
  ext_id VARCHAR(256),

  place_name VARCHAR(256),
  place_type VARCHAR(64),
  info TEXT,

  PRIMARY KEY (id), 
  KEY(ext_id),
  KEY(place_name)
);

CREATE TABLE IF NOT EXISTS _project_user_map (
  id VARCHAR(64) NOT NULL,
  project_id VARCHAR(64) NOT NULL,
  user_id VARCHAR(64) NOT NULL,
  user_role VARCHAR(64),
  PRIMARY KEY (id), 
  KEY(project_id),
  KEY(user_id)
);

CREATE TABLE IF NOT EXISTS _project_place_map (
  id VARCHAR(64) NOT NULL,
  project_id VARCHAR(64) NOT NULL,
  place_id VARCHAR(64) NOT NULL,
  PRIMARY KEY (id), 
  KEY(project_id),
  KEY(place_id)
);

CREATE TABLE IF NOT EXISTS _place_device_map (
  id VARCHAR(64) NOT NULL,
  device_id VARCHAR(64) NOT NULL,
  place_id VARCHAR(64) NOT NULL,
  PRIMARY KEY (id), 
  KEY(device_id),
  KEY(place_id)
);

GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'password';

INSERT INTO _users (id, username, email, full_name, secret_code, status_code, user_type, project_id, sub_project_id, json_data) VALUES (
  '0','webadmin','chulayuth@atvantage.co.th','Global Admin','webadmin','A','A',NULL,NULL,''
);
