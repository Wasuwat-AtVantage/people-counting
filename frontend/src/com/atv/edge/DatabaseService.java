package com.atv.edge;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseService {

  protected static DatabaseService instance;

  protected DatabaseService() {
    String jdbcDriverString = "com.mysql.jdbc.Driver";

    try {
      Class.forName(jdbcDriverString);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }

  }

  public static DatabaseService getInstance() {
    if (instance == null) {
      instance = new DatabaseService();
    }
    return instance;
  }

  public Connection createDBConnection() {
    String jdbcConnectionString = "jdbc:mysql://mysql-people-counting:3306/edge?useUnicode=true&characterEncoding=UTF-8";
    String databaseUsername = "root";
    String databasePassword = "password";

    Connection conn = null;
    try {
      conn = DriverManager.getConnection(jdbcConnectionString, databaseUsername, databasePassword);
    } catch(Exception e) {
      e.printStackTrace();
    }
    return conn;
  }


}