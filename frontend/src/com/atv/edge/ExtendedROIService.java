package com.atv.edge;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Hashtable;
import java.util.List;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExtendedROIService {

  protected static ExtendedROIService instance;

  public static final String BASE_PATH = "/extended_rois/";
  private Hashtable<String, List<Object>> extendedROIMap;

  protected ExtendedROIService() {
    extendedROIMap = new Hashtable<>();
  }

  public static ExtendedROIService getInstance() {
    if (instance == null) {
      instance = new ExtendedROIService();
    }
    return instance;
  }

  public String loadExtenedROIList(String deviceId) {
    String path = BASE_PATH + deviceId + ".json";
    String content = null;
    if (new File(path).exists()) {
      try {
        content = new String(Files.readAllBytes(Paths.get(path)), "utf-8");
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
    return content;
  }

  public void saveExtenedROIList(String deviceId, String confString) {
    PrintWriter pr = null;
    FileOutputStream fout = null;
    String path = BASE_PATH + deviceId + ".json";
    try {
      fout = new FileOutputStream(path);
      pr = new PrintWriter(new OutputStreamWriter(fout, "utf-8"));
      pr.print(confString);
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      try { pr.close(); } catch(Exception ee) {}
      try { fout.close(); } catch(Exception ee) {}
    }

    // Invalidate the cached map
    if (extendedROIMap.containsKey(deviceId)) {
      extendedROIMap.remove(deviceId);
    }
  }

  public List<Object> getExtendedROI(String deviceId) {
    if (extendedROIMap.containsKey(deviceId)) {
      return extendedROIMap.get(deviceId);
    } else {
      System.out.println("[INFO]: Loadin Extended ROI for device: " + deviceId);
      String content = loadExtenedROIList(deviceId);
      System.out.println("[INFO]: Extended ROI = " + content);
      if (content == null) {
        // extendedROIMap.put(deviceId, null); => throw NULLPointerException for Hashtable
        return null;
      } else {
        try {
          ObjectMapper mapper = new ObjectMapper();
          List<Object> list = mapper.readValue(content.getBytes("utf-8"), new TypeReference<List<Object>>(){}); 
          extendedROIMap.put(deviceId, list);
          return list;       
        } catch(Exception e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

}
