package com.atv.edge;

import com.atv.edge.dto.*;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.UUID;

public class PlaceService {

  protected static PlaceService instance;
  private SimpleDateFormat dateFormatter;

  protected PlaceService() {
    dateFormatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
  }

  public static PlaceService getInstance() {
    if (instance == null) {
      instance = new PlaceService();
    }
    return instance;
  }

  public void createPlace(Place object) throws Exception {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;

    PreparedStatement checkProjectStmt = null;    
    PreparedStatement createProjectStmt = null;    
    ResultSet rs = null;


    String checkProjectSql = "SELECT id FROM _places WHERE ext_id=? OR place_name=?";

    String createProjectSql = "INSERT INTO _places "
      + "( "+
        "id,"+
        "ext_id,"+
        "place_name,"+
        "place_type,"+
        "info"
      + ") VALUES ( "
      + "?,?,?,?,?"
      + ") ";
    try {
      conn = dbService.createDBConnection();    
      conn.setAutoCommit(false);

      checkProjectStmt = conn.prepareStatement(checkProjectSql);
      createProjectStmt = conn.prepareStatement(createProjectSql);

      checkProjectStmt.setString(1, object.getExtId());
      checkProjectStmt.setString(2, object.getPlaceName());

      rs = checkProjectStmt.executeQuery();
      if (rs.next()) {
        throw new Exception("Place name or code is duplicated.");
      }

      String id = UUID.randomUUID().toString();
      object.setId(id);

      createProjectStmt.setString(1, id);
      createProjectStmt.setString(2, object.getExtId());
      createProjectStmt.setString(3, object.getPlaceName());
      createProjectStmt.setString(4, object.getPlaceType());
      createProjectStmt.setString(5, object.getInfo());

      System.out.println(createProjectStmt);

      createProjectStmt.executeUpdate();

      conn.setAutoCommit(true);
    } catch (Exception e) {      
      e.printStackTrace();
      try { conn.rollback(); } catch (Exception ee) {}
      throw e;
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { checkProjectStmt.close(); } catch (Exception ee) {}
      try { createProjectStmt.close(); } catch (Exception ee) {}
      try { conn.setAutoCommit(true); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public void delete(String id) {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "DELETE FROM _places WHERE id = ? ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, id);
      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public void update(Place object) {    
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "UPDATE _places SET "+
                    "id=?,"+
                    "ext_id=?,"+
                    "place_name=?,"+
                    "place_type=?,"+
                    "info=? "+
                    "WHERE id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);

      stmt.setString(1, object.getId());
      stmt.setString(2, object.getExtId());
      stmt.setString(3, object.getPlaceName());
      stmt.setString(4, object.getPlaceType());
      stmt.setString(5, object.getInfo());

      stmt.setString(6, object.getId());      

      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public Date convertStringToDate(String str) {
    if (str == null) {
      return null;
    }
    try {
      return dateFormatter.parse(str);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  public String convertDateToString(Date date) {
    if (date == null) {
      return null;
    }
    try {
      return dateFormatter.format(date);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  private Date convertTimestampToDate(java.sql.Timestamp ts) {
    if (ts == null) {
      return null;
    }    
    return new Date(ts.getTime());
  }
  private java.sql.Timestamp convertDateToTimestamp(Date date) {
    if (date == null) {
      return null;
    }    
    return new java.sql.Timestamp(date.getTime());
  }
  public String createUUID() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }
  public String createCode() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }
  public Place get(String id) {
    Place ret = null;
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _places WHERE id = ? ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, id);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      if (rs.next()) {
        ret = new Place();
        ret.setId(rs.getString("id"));
        ret.setExtId(rs.getString("ext_id"));
        ret.setPlaceName(rs.getString("place_name"));
        ret.setPlaceType(rs.getString("place_type"));
        ret.setInfo(rs.getString("info"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return ret;
  }
  public List<Place> select() {
    ArrayList<Place> arr = new ArrayList<Place>();
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _places ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      while (rs.next()) {
        Place ret = new Place();
        ret.setId(rs.getString("id"));
        ret.setExtId(rs.getString("ext_id"));
        ret.setPlaceName(rs.getString("place_name"));
        ret.setPlaceType(rs.getString("place_type"));
        ret.setInfo(rs.getString("info"));

        arr.add(ret);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return arr;
  }

  public void addDevice(String deviceId, String placeId) throws Exception {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;

    PreparedStatement checkStmt = null;    
    PreparedStatement createStmt = null;    
    ResultSet rs = null;


    String checkSql = "SELECT id FROM _place_device_map WHERE device_id=? AND place_id=?";

    String createSql = "INSERT INTO _place_device_map "
      + "( "+
        "id,"+
        "device_id,"+
        "place_id"
      + ") VALUES ( "
      + "?,?,?"
      + ") ";
    try {
      conn = dbService.createDBConnection();    
      conn.setAutoCommit(false);

      checkStmt = conn.prepareStatement(checkSql);
      createStmt = conn.prepareStatement(createSql);

      checkStmt.setString(1, deviceId);
      checkStmt.setString(2, placeId);

      rs = checkStmt.executeQuery();
      if (rs.next()) {
        throw new Exception("Device is already in the place.");
      }

      String id = UUID.randomUUID().toString();
      
      createStmt.setString(1, id);
      createStmt.setString(2, deviceId);
      createStmt.setString(3, placeId);

      System.out.println(createStmt);

      createStmt.executeUpdate();

      conn.setAutoCommit(true);
    } catch (Exception e) {      
      e.printStackTrace();
      try { conn.rollback(); } catch (Exception ee) {}
      throw e;
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { checkStmt.close(); } catch (Exception ee) {}
      try { createStmt.close(); } catch (Exception ee) {}
      try { conn.setAutoCommit(true); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }

  }

  public void removeDevice(String deviceId, String placeId) {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "DELETE FROM _place_device_map WHERE device_id=? AND place_id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, deviceId);
      stmt.setString(2, placeId);
      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }

  public List<String> listAllDeviceInPlace(String placeId) {
    ArrayList<String> arr = new ArrayList<String>();
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _place_device_map WHERE place_id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, placeId);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      while (rs.next()) {
        arr.add(rs.getString("device_id"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return arr;
  }

}