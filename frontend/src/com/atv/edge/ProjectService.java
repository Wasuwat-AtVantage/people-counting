package com.atv.edge;

import com.atv.edge.dto.*;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.UUID;

public class ProjectService {

  protected static ProjectService instance;
  private SimpleDateFormat dateFormatter;

  protected ProjectService() {
    dateFormatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
  }

  public static ProjectService getInstance() {
    if (instance == null) {
      instance = new ProjectService();
    }
    return instance;
  }

  public void createProject(Project object) throws Exception {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;

    PreparedStatement checkProjectStmt = null;    
    PreparedStatement createProjectStmt = null;    
    ResultSet rs = null;


    String checkProjectSql = "SELECT id FROM _projects WHERE ext_id=? OR project_name=?";

    String createProjectSql = "INSERT INTO _projects "
      + "( "+
        "id,"+
        "ext_id,"+
        "project_name,"+
        "project_type,"+
        "status_code,"+
        "valid_from,"+
        "valid_to,"+
        "valid_data_from,"+
        "valid_data_to"
      + ") VALUES ( "
      + "?,?,?,?,?,?,?,?,?"
      + ") ";
    try {
      conn = dbService.createDBConnection();    
      conn.setAutoCommit(false);

      checkProjectStmt = conn.prepareStatement(checkProjectSql);
      createProjectStmt = conn.prepareStatement(createProjectSql);

      checkProjectStmt.setString(1, object.getExtId());
      checkProjectStmt.setString(2, object.getProjectName());

      rs = checkProjectStmt.executeQuery();
      if (rs.next()) {
        throw new Exception("Project name or project code is duplicated.");
      }

      String id = UUID.randomUUID().toString();
      object.setId(id);

      createProjectStmt.setString(1, id);
      createProjectStmt.setString(2, object.getExtId());
      createProjectStmt.setString(3, object.getProjectName());
      createProjectStmt.setString(4, object.getProjectType());
      createProjectStmt.setString(5, object.getStatusCode());
      createProjectStmt.setTimestamp(6, convertDateToTimestamp(object.getValidFrom()));
      createProjectStmt.setTimestamp(7, convertDateToTimestamp(object.getValidTo()));
      createProjectStmt.setTimestamp(8, convertDateToTimestamp(object.getValidDataFrom()));
      createProjectStmt.setTimestamp(9, convertDateToTimestamp(object.getValidDataTo()));

      System.out.println(createProjectStmt);

      createProjectStmt.executeUpdate();

      conn.setAutoCommit(true);
    } catch (Exception e) {      
      e.printStackTrace();
      try { conn.rollback(); } catch (Exception ee) {}
      throw e;
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { checkProjectStmt.close(); } catch (Exception ee) {}
      try { createProjectStmt.close(); } catch (Exception ee) {}
      try { conn.setAutoCommit(true); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public void delete(String id) {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "DELETE FROM _projects WHERE id = ? ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, id);
      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public void update(Project object) {    
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "UPDATE _projects SET "+
                    "id=?,"+
                    "ext_id=?,"+
                    "project_name=?,"+
                    "project_type=?,"+
                    "status_code=?,"+
                    "valid_from=?,"+
                    "valid_to=?,"+
                    "valid_data_from=?,"+
                    "valid_data_to=? "+
                    "WHERE id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);

      stmt.setString(1, object.getId());
      stmt.setString(2, object.getExtId());
      stmt.setString(3, object.getProjectName());
      stmt.setString(4, object.getProjectType());
      stmt.setString(5, object.getStatusCode());
      stmt.setTimestamp(6, convertDateToTimestamp(object.getValidFrom()));
      stmt.setTimestamp(7, convertDateToTimestamp(object.getValidTo()));
      stmt.setTimestamp(8, convertDateToTimestamp(object.getValidDataFrom()));
      stmt.setTimestamp(9, convertDateToTimestamp(object.getValidDataTo()));

      stmt.setString(10, object.getId());      

      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public Date convertStringToDate(String str) {
    if (str == null) {
      return null;
    }
    try {
      return dateFormatter.parse(str);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  public String convertDateToString(Date date) {
    if (date == null) {
      return null;
    }
    try {
      return dateFormatter.format(date);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  private Date convertTimestampToDate(java.sql.Timestamp ts) {
    if (ts == null) {
      return null;
    }    
    return new Date(ts.getTime());
  }
  private java.sql.Timestamp convertDateToTimestamp(Date date) {
    if (date == null) {
      return null;
    }    
    return new java.sql.Timestamp(date.getTime());
  }
  public String createUUID() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }
  public String createCode() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }
  public Project get(String id) {
    Project ret = null;
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _projects WHERE id = ? ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, id);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      if (rs.next()) {
        ret = new Project();
        ret.setId(rs.getString("id"));
        ret.setExtId(rs.getString("ext_id"));
        ret.setProjectName(rs.getString("project_name"));
        ret.setProjectType(rs.getString("project_type"));
        ret.setStatusCode(rs.getString("status_code"));
        ret.setValidFrom(convertTimestampToDate(rs.getTimestamp("valid_from")));
        ret.setValidTo(convertTimestampToDate(rs.getTimestamp("valid_to")));
        ret.setValidDataFrom(convertTimestampToDate(rs.getTimestamp("valid_data_from")));
        ret.setValidDataTo(convertTimestampToDate(rs.getTimestamp("valid_data_to")));
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return ret;
  }
  public Project getByExtId(String extId) {
    Project ret = null;
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _projects WHERE ext_id = ? ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, extId);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      if (rs.next()) {
        ret = new Project();
        ret.setId(rs.getString("id"));
        ret.setExtId(rs.getString("ext_id"));
        ret.setProjectName(rs.getString("project_name"));
        ret.setProjectType(rs.getString("project_type"));
        ret.setStatusCode(rs.getString("status_code"));
        ret.setValidFrom(convertTimestampToDate(rs.getTimestamp("valid_from")));
        ret.setValidTo(convertTimestampToDate(rs.getTimestamp("valid_to")));
        ret.setValidDataFrom(convertTimestampToDate(rs.getTimestamp("valid_data_from")));
        ret.setValidDataTo(convertTimestampToDate(rs.getTimestamp("valid_data_to")));
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return ret;
  }
  public List<Project> select() {
    ArrayList<Project> arr = new ArrayList<Project>();
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _projects ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      while (rs.next()) {
        Project ret = new Project();
        ret.setId(rs.getString("id"));
        ret.setExtId(rs.getString("ext_id"));
        ret.setProjectName(rs.getString("project_name"));
        ret.setProjectType(rs.getString("project_type"));
        ret.setStatusCode(rs.getString("status_code"));
        ret.setValidFrom(convertTimestampToDate(rs.getTimestamp("valid_from")));
        ret.setValidTo(convertTimestampToDate(rs.getTimestamp("valid_to")));
        ret.setValidDataFrom(convertTimestampToDate(rs.getTimestamp("valid_data_from")));
        ret.setValidDataTo(convertTimestampToDate(rs.getTimestamp("valid_data_to")));

        arr.add(ret);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return arr;
  }

  public void addUser(String projectId, String userId, String role) throws Exception {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;

    PreparedStatement checkStmt = null;    
    PreparedStatement createStmt = null;    
    ResultSet rs = null;


    String checkSql = "SELECT id FROM _project_user_map WHERE project_id=? AND user_id=?";

    String createSql = "INSERT INTO _project_user_map "
      + "( "+
        "id,"+
        "project_id,"+
        "user_id,"+
        "user_role"
      + ") VALUES ( "
      + "?,?,?,?"
      + ") ";
    try {
      conn = dbService.createDBConnection();    
      conn.setAutoCommit(false);

      checkStmt = conn.prepareStatement(checkSql);
      createStmt = conn.prepareStatement(createSql);

      checkStmt.setString(1, projectId);
      checkStmt.setString(2, userId);

      rs = checkStmt.executeQuery();
      if (rs.next()) {
        throw new Exception("User is already in the project.");
      }

      String id = UUID.randomUUID().toString();
      
      createStmt.setString(1, id);
      createStmt.setString(2, projectId);
      createStmt.setString(3, userId);
      createStmt.setString(4, role);

      System.out.println(createStmt);

      createStmt.executeUpdate();

      conn.setAutoCommit(true);
    } catch (Exception e) {      
      e.printStackTrace();
      try { conn.rollback(); } catch (Exception ee) {}
      throw e;
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { checkStmt.close(); } catch (Exception ee) {}
      try { createStmt.close(); } catch (Exception ee) {}
      try { conn.setAutoCommit(true); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }

  }

  public void removeUser(String projectId, String userId) {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "DELETE FROM _project_user_map WHERE project_id=? AND user_id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, projectId);
      stmt.setString(2, userId);
      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }

  public List<User> listAllUserInProject(String projectId) {
    ArrayList<User> arr = new ArrayList<User>();
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT a.* FROM _users a INNER JOIN _project_user_map b ON a.id=b.user_id WHERE b.project_id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, projectId);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      while (rs.next()) {
        User ret = new User();
        ret.setId(rs.getString("id"));
        ret.setUsername(rs.getString("username"));
        ret.setEmail(rs.getString("email"));
        ret.setFullName(rs.getString("full_name"));
        ret.setSecretCode(rs.getString("secret_code"));
        ret.setStatusCode(rs.getString("status_code"));
        ret.setUserType(rs.getString("user_type"));
        ret.setProjectId(rs.getString("project_id"));
        ret.setSubProjectId(rs.getString("sub_project_id"));
        ret.setJsonData(rs.getString("json_data"));

        arr.add(ret);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return arr;
  }

  public void addPlace(String projectId, String placeId) throws Exception {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;

    PreparedStatement checkStmt = null;    
    PreparedStatement createStmt = null;    
    ResultSet rs = null;


    String checkSql = "SELECT id FROM _project_place_map WHERE project_id=? AND place_id=?";

    String createSql = "INSERT INTO _project_place_map "
      + "( "+
        "id,"+
        "project_id,"+
        "place_id"
      + ") VALUES ( "
      + "?,?,?"
      + ") ";
    try {
      conn = dbService.createDBConnection();    
      conn.setAutoCommit(false);

      checkStmt = conn.prepareStatement(checkSql);
      createStmt = conn.prepareStatement(createSql);

      checkStmt.setString(1, projectId);
      checkStmt.setString(2, placeId);

      rs = checkStmt.executeQuery();
      if (rs.next()) {
        throw new Exception("Place is already in the project.");
      }

      String id = UUID.randomUUID().toString();
      
      createStmt.setString(1, id);
      createStmt.setString(2, projectId);
      createStmt.setString(3, placeId);

      System.out.println(createStmt);

      createStmt.executeUpdate();

      conn.setAutoCommit(true);
    } catch (Exception e) {      
      e.printStackTrace();
      try { conn.rollback(); } catch (Exception ee) {}
      throw e;
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { checkStmt.close(); } catch (Exception ee) {}
      try { createStmt.close(); } catch (Exception ee) {}
      try { conn.setAutoCommit(true); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }

  }

  public void removePlace(String projectId, String placeId) {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "DELETE FROM _project_place_map WHERE project_id=? AND place_id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, projectId);
      stmt.setString(2, placeId);
      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }

  public List<Place> listAllPlaceInProject(String projectId) {
    ArrayList<Place> arr = new ArrayList<Place>();
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT a.* FROM _places a INNER JOIN _project_place_map b ON a.id=b.place_id WHERE b.project_id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, projectId);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      while (rs.next()) {
        Place ret = new Place();
        ret.setId(rs.getString("id"));
        ret.setExtId(rs.getString("ext_id"));
        ret.setPlaceName(rs.getString("place_name"));
        ret.setPlaceType(rs.getString("place_type"));
        ret.setInfo(rs.getString("info"));

        arr.add(ret);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return arr;
  }

}