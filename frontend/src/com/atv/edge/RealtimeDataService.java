package com.atv.edge;

import com.atv.edge.dto.*;
import com.atv.tracking.EdgeDevice;
import com.atv.tracking.PollingStateManager;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.fasterxml.jackson.core.type.TypeReference;
import java.net.*;

public class RealtimeDataService {

  protected static RealtimeDataService instance;

  public static final String BASE_PATH = "/realtime_data/";
  public static final String HEATMAP_BASE_PATH = "/heatmap_data/";

  private HashMap<String, String> endpointMap;


  protected RealtimeDataService() {
    System.out.println("Loading RealtimeDataService...");
    endpointMap = new HashMap<>();
    loadAllPushEndPoints();
  }

  public static RealtimeDataService getInstance() {
    if (instance == null) {
      instance = new RealtimeDataService();      
    }
    return instance;
  }

  public void loadAllPushEndPoints() {
    System.out.println("Loading all Power BI Push Endpoint...");
    endpointMap.clear();
    try (Stream<Path> walk = Files.walk(Paths.get(BASE_PATH))) {

      List<String> result = walk.map(x -> x.toString())
          .filter(f -> f.endsWith(".json")).collect(Collectors.toList());
        
      result.forEach(System.out::println);
      
      for (int i=0;i<result.size();i++) {
        String path = result.get(i);
        String[] tokens = path.split("/");        
        BufferedReader bis = null;
        FileInputStream fis = null;
        try {
          String deviceId = tokens[tokens.length-1].replaceAll(".json", "");
          System.out.println("Found Endpoint for DEVICE ID = " + deviceId);
          fis = new FileInputStream(path);
          bis = new BufferedReader(new InputStreamReader(fis, "utf-8"));
          String line = bis.readLine();
          int roiId = 0;
          while(line != null) {
            System.out.println(" - ROI " + roiId + " => " + line);
            line = line.trim();
            if ("null".equals(line))
              endpointMap.put(deviceId + "_" + roiId, "");
            else
              endpointMap.put(deviceId + "_" + roiId, line.trim());
            roiId++;
            line = bis.readLine();
          }
        } catch(Exception e) {
          e.printStackTrace();
        } finally {
          try {bis.close();} catch(Exception ee) {}
          try {fis.close();} catch(Exception ee) {}
        }
      }
  
    } catch (IOException e) {
      e.printStackTrace();
    }    
  }

  public String loadPushEndPoint(String deviceId) {
    String path = BASE_PATH + deviceId + ".json";
    String content = "";
    if (new File(path).exists()) {
      try {
        content = new String(Files.readAllBytes(Paths.get(path)), "utf-8");
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
    return content;
  }

  public String loadPushEndPoint(String deviceId, int roiId) {
    String key = BASE_PATH + deviceId + "_" + roiId + ".json";
    if (endpointMap.containsKey(key)) {
      return endpointMap.get(key);
    }
    return "";
  }

  public void savePushEndPoint(String deviceId, String confString) {
    PrintWriter pr = null;
    FileOutputStream fout = null;
    String path = BASE_PATH + deviceId + ".json";
    try {
      fout = new FileOutputStream(path);
      pr = new PrintWriter(new OutputStreamWriter(fout, "utf-8"));
      pr.print(confString);
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      try { pr.close(); } catch(Exception ee) {}
      try { fout.close(); } catch(Exception ee) {}
    }

    loadAllPushEndPoints();
  }

  /*
    {"hotspot_stat": 
      [{
        "roi_type": 1, 
        "count_map": {"all": 3, "unknown": 2, "adult": 3, "h0": 1, "v0": 1, "unknown,adult,h0,v0": 1, "h+": 1, "v+": 1, "unknown,adult,h+,v+": 1, "male": 1, "h-": 1, "v-": 1, "male,adult,h-,v-": 1}, 
        "interested_count_map": {"all": 1, "male": 1, "adult": 1, "h-": 1, "v-": 1, "male,adult,h-,v-": 1}, 
        "engaged_count_map": {"all": 0}, 
        "events": [["in", 0.011666666666666665]], 
        "interval_occupied_count": 80, 
        "interval_unoccupied_count": 20
      }]}

      CURL:
        curl --include \
        --request POST \
        --header "Content-Type: application/json" \
        --data-binary "[
        {
        \"Device ID\" :\"AAAAA555555\",
        \"ROI\" :\"AAAAA555555\",
        \"ROI Type\" :\"AAAAA555555\",
        \"Datetime\" :\"2020-01-20T13:56:53.535Z\",
        \"Interest\" :98.6,
        \"Engage\" :98.6,
        \"Male\" :98.6,
        \"Female\" :98.6,
        \"Kid\" :98.6,
        \"Adult\" :98.6,
        \"Elder\" :98.6,
        \"Neutral\" :98.6,
        \"Happy\" :98.6,
        \"Angry\" :98.6,
        \"Pass Through\" :98.6
        }
        ]" \
        "https://api.powerbi.com/beta/103b20d8-265e-400d-8786-2791bc707674/datasets/57f763b9-30ba-42d9-aef6-3dc56692d3e8/rows?noSignUpCheck=1&key=Ai7a5r6%2Bo7gfGaWOwDLHHAsWg1Ro4asgeuzUCo6aDQbIxdhidmUO91FiWiPVXqVq8FY2o3sWVB9QFYO6S%2FAbKA%3D%3D"      
  */
  public boolean notifyNewDataPoint(String deviceId, String dateTimeString, String content) {
    System.out.println("Realtime Log: " + deviceId + " @ " + dateTimeString + " => " + content);
    boolean ret = false;
    try {
      ObjectMapper mapper = new ObjectMapper();
      Map<String, Object> map = mapper.readValue(content.getBytes("utf-8"), new TypeReference<Map<String, Object>>(){});
      List<Object> roiCountDataList = (List<Object>)map.get("hotspot_stat");

      SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd_hhmmss");
      SimpleDateFormat jsonDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS+07:00");

      Date dateTime = dateFormatter.parse(dateTimeString);
      String jsonDatetime = jsonDateFormatter.format(dateTime);

      System.out.println(" Found data of " + roiCountDataList.size() + " ROI(s).");
      for (int i=0;i<roiCountDataList.size();i++) {
        int roiId = i;
        String key = deviceId + "_" + roiId;
        if (!endpointMap.containsKey(key)) 
          continue;
        
        String biStreamingEndpoint = endpointMap.get(key);
        if (biStreamingEndpoint.length() == 0) continue;

        ret |= true;
  
        Map<String, Object> roiCountData = (Map<String, Object>)roiCountDataList.get(i);
        Map<String, Object> countMap = (Map<String, Object>)roiCountData.get("count_map");
        Map<String, Object> interestedCountMap = (Map<String, Object>)roiCountData.get("interested_count_map");
        Map<String, Object> engagedCountMap = (Map<String, Object>)roiCountData.get("engaged_count_map");
        int passThrough = (Integer)countMap.get("all");
        int interested = (Integer)interestedCountMap.get("all");
        int engaged = (Integer)engagedCountMap.get("all");
        int male = 0;
        int female = 0;
        int kid = 0;
        int adult = 0;
        int elder = 0;
        int angry = 0;
        int neutral = 0;
        int happy = 0;
        if (countMap.containsKey("male")) {
          male = (Integer)countMap.get("male");
        }
        if (countMap.containsKey("female")) {
          female = (Integer)countMap.get("female");
        }
        if (countMap.containsKey("kid")) {
          kid = (Integer)countMap.get("kid");
        }
        if (countMap.containsKey("adult")) {
          adult = (Integer)countMap.get("adult");
        }
        if (countMap.containsKey("elder")) {
          elder = (Integer)countMap.get("elder");
        }
        if (countMap.containsKey("angry")) {
          angry = (Integer)countMap.get("angry");
        }
        if (countMap.containsKey("neutral")) {
          neutral = (Integer)countMap.get("neutral");
        }
        if (countMap.containsKey("happy")) {
          happy = (Integer)countMap.get("happy");
        }
        System.out.println("  Count Funnel = " + passThrough + ", " + interested + ", " + engaged);

        URL url = new URL(biStreamingEndpoint);
        HttpURLConnection con = null;
        try {
          con = (HttpURLConnection)url.openConnection();
          con.setRequestMethod("POST");
          con.setRequestProperty("Content-Type", "application/json; utf-8");
          con.setRequestProperty("Accept", "application/json");
          con.setDoOutput(true);

          String jsonInputString = "[{"
            +"\"Device ID\":\"" + deviceId + "\","
            +"\"ROI\":\"" + roiId + "\","
            +"\"ROI Type\":\"0\","
            +"\"Datetime\":\""+jsonDatetime+"\","
            +"\"Interest\":"+interested+","
            +"\"Engage\":"+engaged+","
            +"\"Male\":"+male+","
            +"\"Female\":"+female+","
            +"\"Kid\":"+kid+","
            +"\"Adult\":"+adult+","
            +"\"Elder\":"+elder+","
            +"\"Neutral\":"+neutral+","
            +"\"Happy\":"+happy+","
            +"\"Angry\":"+angry+","
            +"\"Pass Through\":"+passThrough
            +"}]";

          try(OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);           
          }
          
          try(BufferedReader br = new BufferedReader(
            new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println("RESPONSE => " + response.toString());
          }          
        } catch(Exception ee) {
          ee.printStackTrace();
        } finally {
          try {con.disconnect();} catch(Exception eee) {}
        }
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
    return ret;
  }

  // Stream Push data to power BI in this format:
  /*
  [
    {
      "Device ID" :"AAAAA555555",
      "ROI" :98.6,
      "Timestamp" :"2020-03-10T11:44:59.579Z",
      "Hour" :"AAAAA555555",
      "Pass-Through" :98.6,
      "Interest" :98.6,
      "Engage" :98.6,
      "Percent Interest" :"AAAAA555555",
      "Percent Engage" :"AAAAA555555",
      "Pass-Through Men" :98.6,
      "Pass-Through Women" :98.6,
      "Interest Men" :98.6,
      "Interest Women" :98.6,
      "Engage Men" :98.6,
      "Engage Women" :98.6,
      "Percent Interest Men" :"AAAAA555555",
      "Percent Interest Women" :"AAAAA555555",
      "Percent Engage Men" :"AAAAA555555",
      "Percent Engage Women" :"AAAAA555555",
      "Date" :"AAAAA555555",
      "In Store" :98.6,
      "Staff <15s" :98.6,
      "16-30s" :98.6,
      "31-60s" :98.6,
      "1-3m" :98.6,
      "3-5m" :98.6,
      "5-10m" :98.6,
      ">10m" :98.6
    }
  ]
  */
  private void streamDataToPowerBI(
      String biStreamingEndpoint, 
      String deviceId,
      int roiId,
      String jsonDatetime,
      String dateString,
      String hourString,
      int passThrough,
      int interested,
      int engaged,
      int passThroughMale,
      int interestedMale,
      int engagedMale,
      int passThroughFemale,
      int interestedFemale,
      int engagedFemale,
      Map<String, Integer> attendedIntervalMap,
      Map<String, Integer> otherValuesMap
    ) {
    HttpURLConnection con = null;
    try {
      URL url = new URL(biStreamingEndpoint);
      con = (HttpURLConnection)url.openConnection();
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type", "application/json; utf-8");
      con.setRequestProperty("Accept", "application/json");
      con.setDoOutput(true);

      // [TBD]: Remove this
      // Custom flow to make Male and Female not different than 30%
      if (passThroughMale > 0 && passThroughFemale > 0) {
        if ((double)passThroughMale/(double)passThroughFemale > 1.3) {
          int newPassThroughMale = (int)((double)passThroughFemale * 1.3);
          double ratio = ((double)newPassThroughMale/(double)passThroughMale);
          interestedMale = (int)((double)interestedMale * ratio);
          engagedMale = (int)((double)engagedMale * ratio);
          passThroughMale = newPassThroughMale;
        } else if ((double)passThroughFemale/(double)passThroughMale > 1.3) {
          int newPassThroughFemale = (int)((double)passThroughMale * 1.3);
          double ratio = ((double)newPassThroughFemale/(double)passThroughFemale);
          interestedFemale = (int)((double)interestedFemale * ratio);
          engagedFemale = (int)((double)engagedFemale * ratio);
          passThroughFemale = newPassThroughFemale;
        }
      }

      double percentInterest = 0.0;
      double percentEngage = 0.0;
      if (passThrough > 0) {
        percentInterest = (double)interested * 100.00 / (double)passThrough;
        percentEngage = (double)engaged * 100.00 / (double)passThrough;
      }

      double percentInterestMale = 0.0;
      double percentEngageMale = 0.0;
      if (passThroughMale > 0) {
        percentInterestMale = (double)interestedMale * 100.00 / (double)passThroughMale;
        percentEngageMale = (double)engagedMale * 100.00 / (double)passThroughMale;
      }

      double percentInterestFemale = 0.0;
      double percentEngageFemale = 0.0;
      if (passThroughFemale > 0) {
        percentInterestFemale = (double)interestedFemale * 100.00 / (double)passThroughFemale;
        percentEngageFemale = (double)engagedFemale * 100.00 / (double)passThroughFemale;
      }

      String attendedIntervalString = "";
      for (String classStr : attendedIntervalMap.keySet()) {
        attendedIntervalString += "\"" + classStr + "\":" + attendedIntervalMap.get(classStr) + ",";
      }

      String otherValuesString = "";
      for (String classStr : otherValuesMap.keySet()) {
        otherValuesString += "\"" + classStr + "\":" + otherValuesMap.get(classStr) + ",";
      }

      String jsonInputString = "[{"
        +"\"Device ID\":\"" + deviceId + "\","
        +"\"ROI\":\"" + roiId + "\","
        +"\"Timestamp\":\""+jsonDatetime+"\","
        +"\"Date\":\""+dateString+"\","
        +"\"Hour\":\""+hourString+"\","
        +"\"Interest\":"+interested+","
        +"\"Engage\":"+engaged+","
        + attendedIntervalString
        + otherValuesString
        +"\"Interest Men\":"+interestedMale+","
        +"\"Engage Men\":"+engagedMale+","
        +"\"Interest Women\":"+interestedFemale+","
        +"\"Engage Women\":"+engagedFemale+","
        +"\"Pass-Through\":"+passThrough+","
        +"\"Percent Interest\":"+percentInterest+","
        +"\"Percent Engage\":"+percentEngage+","
        +"\"Pass-Through Men\":"+passThroughMale+","
        +"\"Percent Interest Men\":"+percentInterestMale+","
        +"\"Percent Engage Men\":"+percentEngageMale+","
        +"\"Pass-Through Women\":"+passThroughFemale+","
        +"\"Percent Interest Women\":"+percentInterestFemale+","
        +"\"Percent Engage Women\":"+percentEngageFemale
        +"}]";

      System.out.println(jsonInputString);

      try(OutputStream os = con.getOutputStream()) {
        byte[] input = jsonInputString.getBytes("utf-8");
        os.write(input, 0, input.length);           
      }
      
      try(BufferedReader br = new BufferedReader(
        new InputStreamReader(con.getInputStream(), "utf-8"))) {
        StringBuilder response = new StringBuilder();
        String responseLine = null;
        while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
        }
        System.out.println("RESPONSE => " + response.toString());
      }          
    } catch(Exception ee) {
      ee.printStackTrace();
    } finally {
      try {con.disconnect();} catch(Exception eee) {}
    }
  }

  private int getCountFromMap(Map<String, Object> countMap, String key) {
    if (countMap.containsKey(key)) {
      return (Integer)countMap.get(key);
    } else {
      return 0;
    }
  }

  private Map<String, Integer> constructAttendedIntervalMap(Map<String, Object> attendedCountMap, String key) {
    HashMap<String, Integer> ret = new HashMap<>();
    // Put initial data to the map
    ret.put("Staff <15s", 0);
    ret.put("16-30s", 0);
    ret.put("31-60s", 0);
    ret.put("1-3m", 0);
    ret.put("3-5m", 0);
    ret.put("5-10m", 0);
    ret.put(">10m", 0);
    // Init all bucket to be zero
    if (attendedCountMap.containsKey(key)) {
      Map<String, Object> allCountMap = (Map<String, Object>)attendedCountMap.get(key);
      for (String classStr : allCountMap.keySet()) {
        try {
          int time = Integer.parseInt(classStr);
          if (time <= 15) {
            ret.put("Staff <15s", (Integer)allCountMap.get(classStr));
          } else if (time <= 30) {
            ret.put("16-30s", (Integer)allCountMap.get(classStr));
          } else if (time <= 60) {
            ret.put("31-60s", (Integer)allCountMap.get(classStr));
          } else if (time <= 180) {
            ret.put("1-3m", (Integer)allCountMap.get(classStr));
          } else if (time <= 300) {
            ret.put("3-5m", (Integer)allCountMap.get(classStr));
          } else if (time <= 600) {
            ret.put("5-10m", (Integer)allCountMap.get(classStr));
          } else {
            ret.put(">10m", (Integer)allCountMap.get(classStr));
          }
        } catch(Exception e) {
          e.printStackTrace();
        }
      }
    }
    return ret;
  }

  private Map<String, Integer> combineAttendedCountMap(Map<String, Integer> map1, Map<String, Integer> map2, int sign) {
    HashMap<String, Integer> ret = new HashMap<>();
    for (String classStr : map2.keySet()) {
      int val = map2.get(classStr) * sign;
      if (map1 != null && map1.containsKey(classStr)) {
        val += map1.get(classStr);
      }
      ret.put(classStr, val);
    }
    return ret;
  }

  private int parseValue(String formula, List<Object> roiCountDataList, String key) {
    int pos = formula.indexOf(":");
    if (pos == -1) return 0;
    String idx_str = formula.substring(0, pos);
    String field = formula.substring(pos + 1, formula.length());
    try {
      int i = Integer.parseInt(idx_str);
      Map<String, Object> roiCountData = (Map<String, Object>)roiCountDataList.get(i);      
      Map<String, Object> countMap = null;
      if ("pass".equals(field))
        countMap = (Map<String, Object>)roiCountData.get("count_map");
      else if ("interest".equals(field))
        countMap = (Map<String, Object>)roiCountData.get("interested_count_map");
      else if ("engage".equals(field))
        countMap = (Map<String, Object>)roiCountData.get("engaged_count_map");
      else
        return 0;
      
      return getCountFromMap(countMap, key);
  
    } catch(Exception e) {
      System.out.println("[ERROR]: Parsing Value: " + formula);
      e.printStackTrace();
      return 0;
    }
  }

  private int parseExtendedROIFormula(String formula, List<Object> roiCountDataList, String key) {
    int start = 0;
    int val = 0;
    int sign = 1;
    for (int i=0;i<formula.length();i++) {
      char c = formula.charAt(i);
      if(c == '+') {
        val = val + sign * parseValue(formula.substring(start, i), roiCountDataList, key);
        sign = 1;
        start = i + 1;
      } else if (c == '-') {
        val = val + sign * parseValue(formula.substring(start, i), roiCountDataList, key);
        sign = -1;
        start = i + 1;
      }    
    }
    if (start < formula.length()) {
      val = val + sign * parseValue(formula.substring(start, formula.length()), roiCountDataList, key);
    }
    return Math.max(val, 0); // Prevent minus value on count, ultimately.
  }

  private Map<String, Integer> parseAttenedROIFormula(String formula, List<Object> roiCountDataList, String key) {
    int start = 0;
    Map<String, Integer> val = new HashMap<>();
    if (formula == null) return val;
    int sign = 1;
    for (int i=0;i<formula.length();i++) {
      char c = formula.charAt(i);
      if(c == '+') {
        int roi_id = Integer.parseInt(formula.substring(start, i));
        Map<String, Object> roiCountData = (Map<String, Object>)roiCountDataList.get(roi_id);      
        val = combineAttendedCountMap(val, constructAttendedIntervalMap((Map<String, Object>)roiCountData.get("attended_count_map"), key), sign);
        sign = 1;
        start = i + 1;
      } else if (c == '-') {
        int roi_id = Integer.parseInt(formula.substring(start, i));
        Map<String, Object> roiCountData = (Map<String, Object>)roiCountDataList.get(roi_id);      
        val = combineAttendedCountMap(val, constructAttendedIntervalMap((Map<String, Object>)roiCountData.get("attended_count_map"), key), sign);
        sign = -1;
        start = i + 1;
      }    
    }
    if (start < formula.length()) {
      int roi_id = Integer.parseInt(formula.substring(start, formula.length()));
      Map<String, Object> roiCountData = (Map<String, Object>)roiCountDataList.get(roi_id);      
      val = combineAttendedCountMap(val, constructAttendedIntervalMap((Map<String, Object>)roiCountData.get("attended_count_map"), key), sign);
    }
    return val; // Prevent minus value on count, ultimately.
  }

  private Map<String, Integer> parseOtherValuesFormula(String formula, List<Object> roiCountDataList, String key) {
    HashMap<String, Integer> ret = new HashMap<>();
    if (formula == null) return ret;
    String[] formulas = formula.split(",");
    for (int i=0;i<formulas.length;i++) {
      String each_formula = formulas[i];
      String[] tokens = each_formula.split("=");
      if (tokens.length == 2) {
        String name = tokens[0];
        String text = tokens[1];
        int val = parseExtendedROIFormula(text, roiCountDataList, key);
        ret.put(name, val);
      }
    }
    return ret;
  }

  public boolean notifyNewDataPoint2(String deviceId, String dateTimeString, String content) {
    System.out.println("Realtime Log: " + deviceId + " @ " + dateTimeString + " => " + content);
    boolean ret = false;
    try {
      ObjectMapper mapper = new ObjectMapper();
      Map<String, Object> map = mapper.readValue(content.getBytes("utf-8"), new TypeReference<Map<String, Object>>(){});
      List<Object> roiCountDataList = (List<Object>)map.get("hotspot_stat");

      SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd_hhmmss");
      SimpleDateFormat jsonDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS+07:00");
      SimpleDateFormat hourFormatter = new SimpleDateFormat("HH");
      SimpleDateFormat datePartFormatter = new SimpleDateFormat("dd/MM/yyyy");

      Date dateTime = dateFormatter.parse(dateTimeString);
      String jsonDatetime = jsonDateFormatter.format(dateTime);

      System.out.println(" Found data of " + roiCountDataList.size() + " ROI(s).");

      // Store heatmaps of all ROIs as an array of JSON string
      String[] heatmaps = new String[roiCountDataList.size()];

      for (int i=0;i<roiCountDataList.size();i++) {
        int roiId = i;
        String key = deviceId + "_" + roiId;
        if (!endpointMap.containsKey(key)) 
          continue;
        
        String biStreamingEndpoint = endpointMap.get(key);
        if (biStreamingEndpoint.length() == 0) continue;

        ret |= true;
    
        Map<String, Object> roiCountData = (Map<String, Object>)roiCountDataList.get(i);
        Map<String, Object> countMap = (Map<String, Object>)roiCountData.get("count_map");
        Map<String, Object> interestedCountMap = (Map<String, Object>)roiCountData.get("interested_count_map");
        Map<String, Object> engagedCountMap = (Map<String, Object>)roiCountData.get("engaged_count_map");
        Map<String, Object> attendedCountMap = (Map<String, Object>)roiCountData.get("attended_count_map");
        String timeString = (String)roiCountData.get("time_string");
        String heatmap = (String)roiCountData.get("heatmap");
        heatmaps[i] = heatmap;
        String dateString = datePartFormatter.format(dateTime);
        String hourString = hourFormatter.format(dateTime);
        int passThrough = getCountFromMap(countMap, "all");
        int interested = getCountFromMap(interestedCountMap, "all");
        int engaged = getCountFromMap(engagedCountMap, "all");
        int passThroughMale = getCountFromMap(countMap, "male");
        int interestedMale = getCountFromMap(interestedCountMap, "male");
        int engagedMale = getCountFromMap(engagedCountMap, "male");
        int passThroughFemale = getCountFromMap(countMap, "female");
        int interestedFemale = getCountFromMap(interestedCountMap, "female");
        int engagedFemale = getCountFromMap(engagedCountMap, "female");
        // Process only "all" class for now [TBD]: Seperate them in future
        Map<String, Integer> attendedIntervalMap = constructAttendedIntervalMap(attendedCountMap, "all");
        System.out.println(" ROI: " + roiId+ " => Count Funnel[" + timeString + "] = " + passThrough + ", " + interested + ", " + engaged);

        streamDataToPowerBI(
          biStreamingEndpoint, 
          deviceId,
          roiId,
          jsonDatetime,
          dateString,
          hourString,
          passThrough,
          interested,
          engaged,
          passThroughMale,
          interestedMale,
          engagedMale,
          passThroughFemale,
          interestedFemale,
          engagedFemale,
          attendedIntervalMap,
          new HashMap<String, Integer>()
        );
      }

      // Also process aggregated ROIs and send their notification too.
      List<Object> extendedROIs = ExtendedROIService.getInstance().getExtendedROI(deviceId);
      if (extendedROIs != null) {
        System.out.println("Extended ROI Size = " + extendedROIs.size());
        // Get timeString from 1st ROI 
        // [TBD]: Do we have other choices?
        String timeString = "";
        if (roiCountDataList.size() > 0) {
          Map<String, Object> roiCountData = (Map<String, Object>)roiCountDataList.get(0);
          timeString = (String)roiCountData.get("time_string");          
        }
        for (int i=0;i<extendedROIs.size();i++) {
          int roiId = i + roiCountDataList.size(); // Extended ROI use number start from MAX(len(ROIs))
          String key = deviceId + "_" + roiId;
          if (!endpointMap.containsKey(key)) 
            continue;

          String biStreamingEndpoint = endpointMap.get(key);
          if (biStreamingEndpoint.length() == 0) continue;
    
          ret |= true;

          // Calculate Extended ROIs properties based on formula of normal ROIs
          String dateString = datePartFormatter.format(dateTime);
          String hourString = hourFormatter.format(dateTime);
          int passThrough = 0;
          int interested = 0;
          int engaged = 0;
          int passThroughMale = 0;
          int interestedMale = 0;
          int engagedMale = 0;
          int passThroughFemale = 0;
          int interestedFemale = 0;
          int engagedFemale = 0;
          Map<String, Integer> attendedIntervalMap = new HashMap<>();
          Map<String, Integer> otherValuesMap = new HashMap<>();

          Map<String, Object> roiProps = (Map<String, Object>)extendedROIs.get(i);
          String passThroughFormula = (String)roiProps.get("pass_through_source");
          String interestFormula = (String)roiProps.get("interest_source");
          String engageFormula = (String)roiProps.get("engaged_source");
          String attendedFormula = (String)roiProps.get("attended_source");
          String otherFomulars = (String)roiProps.get("other_sources");

          passThrough = parseExtendedROIFormula(passThroughFormula, roiCountDataList, "all");
          interested = parseExtendedROIFormula(interestFormula, roiCountDataList, "all");
          engaged = parseExtendedROIFormula(engageFormula, roiCountDataList, "all");

          passThroughMale = parseExtendedROIFormula(passThroughFormula, roiCountDataList, "male");
          interestedMale = parseExtendedROIFormula(interestFormula, roiCountDataList, "male");
          engagedMale = parseExtendedROIFormula(engageFormula, roiCountDataList, "male");

          passThroughFemale = parseExtendedROIFormula(passThroughFormula, roiCountDataList, "female");
          interestedFemale = parseExtendedROIFormula(interestFormula, roiCountDataList, "female");
          engagedFemale = parseExtendedROIFormula(engageFormula, roiCountDataList, "female");
          
          attendedIntervalMap = parseAttenedROIFormula(attendedFormula, roiCountDataList, "all");
          otherValuesMap = parseOtherValuesFormula(otherFomulars, roiCountDataList, "all");

          System.out.println(" Extended-ROI: " + roiId+ " => Count Funnel[" + timeString + "] = " + passThrough + ", " + interested + ", " + engaged);
  
          streamDataToPowerBI(
            biStreamingEndpoint, 
            deviceId,
            roiId,
            jsonDatetime,
            dateString,
            hourString,
            passThrough,
            interested,
            engaged,
            passThroughMale,
            interestedMale,
            engagedMale,
            passThroughFemale,
            interestedFemale,
            engagedFemale,
            attendedIntervalMap,
            otherValuesMap
          );  
        }
      }

      EdgeDevice edgeDevice = PollingStateManager.getInstance().getEdgeDevice(deviceId);
      if (edgeDevice != null) {
        edgeDevice.heatmaps = heatmaps;
      }

    } catch(Exception e) {
      e.printStackTrace();
    }
    return ret;
  }

}
