package com.atv.edge;

import com.atv.edge.dto.*;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.UUID;
import java.util.Hashtable;

public class UserService {

  protected static UserService instance;
  private SimpleDateFormat dateFormatter;
  private Hashtable<String, String> sessionToUserIDMap;

  protected UserService() {
    dateFormatter = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
    sessionToUserIDMap = new Hashtable<>();
  }

  public static UserService getInstance() {
    if (instance == null) {
      instance = new UserService();
    }
    return instance;
  }


  public void createUser(User object) throws Exception {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;

    PreparedStatement checkUserStmt = null;    
    PreparedStatement createUserStmt = null;    
    ResultSet rs = null;

    String checkUserSql = "SELECT id FROM _users WHERE username=? OR email=?";

    String createUserSql = "INSERT INTO _users "
      + "( "+
        "id,"+
        "username,"+
        "email,"+
        "full_name,"+
        "secret_code,"+
        "status_code,"+
        "user_type,"+
        "project_id,"+
        "sub_project_id,"+
        "json_data"
      + ") VALUES ( "
      + "?,?,?,?,?,?,?,?,?,?"
      + ") ";
    try {
      conn = dbService.createDBConnection();    
      conn.setAutoCommit(false);

      checkUserStmt = conn.prepareStatement(checkUserSql);
      createUserStmt = conn.prepareStatement(createUserSql);

      checkUserStmt.setString(1, object.getUsername());
      checkUserStmt.setString(2, object.getEmail());

      rs = checkUserStmt.executeQuery();
      if (rs.next()) {
        throw new Exception("Username or email is duplicated.");
      }

      String id = UUID.randomUUID().toString();
      object.setId(id);
      
      createUserStmt.setString(1, id);
      createUserStmt.setString(2, object.getUsername());
      createUserStmt.setString(3, object.getEmail());
      createUserStmt.setString(4, object.getFullName());
      createUserStmt.setString(5, object.getSecretCode());
      createUserStmt.setString(6, object.getStatusCode());
      createUserStmt.setString(7, object.getUserType());
      createUserStmt.setString(8, object.getProjectId());
      createUserStmt.setString(9, object.getSubProjectId());
      createUserStmt.setString(10, object.getJsonData());

      System.out.println(createUserStmt);

      createUserStmt.executeUpdate();

      conn.commit();
    } catch (Exception e) {
      e.printStackTrace();
      try { conn.rollback(); } catch (Exception ee) {}
      throw e;
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { checkUserStmt.close(); } catch (Exception ee) {}
      try { createUserStmt.close(); } catch (Exception ee) {}
      try { conn.setAutoCommit(true); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public void delete(String id) {
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "DELETE FROM _users WHERE id = ? ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, id);
      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public void update(User object) {    
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    String sql = "UPDATE _users SET "+
                    "id=?,"+
                    "username=?,"+
                    "email=?,"+
                    "full_name=?,"+
                    "secret_code=?,"+
                    "status_code=?,"+
                    "user_type=?,"+
                    "project_id=?,"+
                    "sub_project_id=?,"+
                    "json_data=? "+
                    "WHERE id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);

      stmt.setString(1, object.getId());
      stmt.setString(2, object.getUsername());
      stmt.setString(3, object.getEmail());
      stmt.setString(4, object.getFullName());
      stmt.setString(5, object.getSecretCode());
      stmt.setString(6, object.getStatusCode());
      stmt.setString(7, object.getUserType());
      stmt.setString(8, object.getProjectId());
      stmt.setString(9, object.getSubProjectId());
      stmt.setString(10, object.getJsonData());

      stmt.setString(11, object.getId());      

      System.out.println(stmt);

      stmt.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
  }
  public Date convertStringToDate(String str) {
    if (str == null) {
      return null;
    }
    try {
      return dateFormatter.parse(str);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  public String convertDateToString(Date date) {
    if (date == null) {
      return null;
    }
    try {
      return dateFormatter.format(date);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
  private Date convertTimestampToDate(java.sql.Timestamp ts) {
    if (ts == null) {
      return null;
    }    
    return new Date(ts.getTime());
  }
  private java.sql.Timestamp convertDateToTimestamp(Date date) {
    if (date == null) {
      return null;
    }    
    return new java.sql.Timestamp(date.getTime());
  }
  public String createUUID() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }
  public String createCode() {
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }
  public User get(String id) {
    User ret = null;
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _users WHERE id = ? ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, id);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      if (rs.next()) {
        ret = new User();
        ret.setId(rs.getString("id"));
        ret.setUsername(rs.getString("username"));
        ret.setEmail(rs.getString("email"));
        ret.setFullName(rs.getString("full_name"));
        ret.setSecretCode(rs.getString("secret_code"));
        ret.setStatusCode(rs.getString("status_code"));
        ret.setUserType(rs.getString("user_type"));
        ret.setProjectId(rs.getString("project_id"));
        ret.setSubProjectId(rs.getString("sub_project_id"));
        ret.setJsonData(rs.getString("json_data"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return ret;
  }
  public User getByUsername(String username) {
    User ret = null;
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _users WHERE username = ? ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, username);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      if (rs.next()) {
        ret = new User();
        ret.setId(rs.getString("id"));
        ret.setUsername(rs.getString("username"));
        ret.setEmail(rs.getString("email"));
        ret.setFullName(rs.getString("full_name"));
        ret.setSecretCode(rs.getString("secret_code"));
        ret.setStatusCode(rs.getString("status_code"));
        ret.setUserType(rs.getString("user_type"));
        ret.setProjectId(rs.getString("project_id"));
        ret.setSubProjectId(rs.getString("sub_project_id"));
        ret.setJsonData(rs.getString("json_data"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return ret;
  }
  public List<User> select() {
    ArrayList<User> arr = new ArrayList<User>();
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _users ";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      while (rs.next()) {
        User ret = new User();
        ret.setId(rs.getString("id"));
        ret.setUsername(rs.getString("username"));
        ret.setEmail(rs.getString("email"));
        ret.setFullName(rs.getString("full_name"));
        ret.setSecretCode(rs.getString("secret_code"));
        ret.setStatusCode(rs.getString("status_code"));
        ret.setUserType(rs.getString("user_type"));
        ret.setProjectId(rs.getString("project_id"));
        ret.setSubProjectId(rs.getString("sub_project_id"));
        ret.setJsonData(rs.getString("json_data"));

        arr.add(ret);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return arr;
  }

  public String getUserIdFromSessionId(String sessionId) {
    if (sessionToUserIDMap.containsKey(sessionId)) {
      return sessionToUserIDMap.get(sessionId);
    } else {
      return null;
    }
  }

  public boolean isLoggedIn(String sessionId) {
    return (getUserIdFromSessionId(sessionId) != null);
  }

  public String login(String username, String password) throws Exception {
    User user = getByUsername(username);
    if (user == null) {
      throw new Exception("Invalid username.");
    }
    if (!password.equals(user.getSecretCode())) {
      throw new Exception("Invalid authentication.");
    }
    String sessionId = UUID.randomUUID().toString();
    sessionToUserIDMap.put(sessionId, user.getId());
    return sessionId;
  }

  public void logout(String sessionId) {
    if (sessionToUserIDMap.containsKey(sessionId)) {
      sessionToUserIDMap.remove(sessionId);
    }
  }

  public List<Project> listProjectOfUser(String userId) {
    ArrayList<Project> arr = new ArrayList<Project>();
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT a.* FROM _projects a INNER JOIN _project_user_map b ON a.id=b.project_id WHERE b.user_id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, userId);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      while (rs.next()) {
        Project ret = new Project();
        ret.setId(rs.getString("id"));
        ret.setExtId(rs.getString("ext_id"));
        ret.setProjectName(rs.getString("project_name"));
        ret.setProjectType(rs.getString("project_type"));
        ret.setStatusCode(rs.getString("status_code"));
        ret.setValidFrom(convertTimestampToDate(rs.getTimestamp("valid_from")));
        ret.setValidTo(convertTimestampToDate(rs.getTimestamp("valid_to")));
        ret.setValidDataFrom(convertTimestampToDate(rs.getTimestamp("valid_data_from")));
        ret.setValidDataTo(convertTimestampToDate(rs.getTimestamp("valid_data_to")));

        arr.add(ret);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }
    return arr;
  }

  public String getProjectRoleOfUser(String userId, String projectId) {
    String ret = null;
    DatabaseService dbService = DatabaseService.getInstance();
    Connection conn = null;
    PreparedStatement stmt = null;    
    ResultSet rs = null;    
    String sql = "SELECT * FROM _project_user_map WHERE user_id=? AND project_id=?";
    try {
      conn = dbService.createDBConnection();    
      stmt = conn.prepareStatement(sql);
      stmt.setString(1, userId);
      stmt.setString(2, projectId);
      System.out.println(stmt);

      rs = stmt.executeQuery();

      if (rs.next()) {
        ret = rs.getString("user_role");
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try { rs.close(); } catch (Exception ee) {}
      try { stmt.close(); } catch (Exception ee) {}
      try { conn.close(); } catch (Exception ee) {}
    }

    return ret;
  }

}