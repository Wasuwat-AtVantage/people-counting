package com.atv.edge.dto;

import java.util.Date;

public class Project {
  private String id;
  private String extId;
  private String projectName;
  private String projectType;
  private String statusCode;
  private Date validFrom;
  private Date validTo;
  private Date validDataFrom;
  private Date validDataTo;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getExtId() {
    return extId;
  }

  public void setExtId(String extId) {
    this.extId = extId;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getProjectType() {
    return projectType;
  }

  public void setProjectType(String projectType) {
    this.projectType = projectType;
  }

  public String getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(String statusCode) {
    this.statusCode = statusCode;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public Date getValidDataFrom() {
    return validDataFrom;
  }

  public void setValidDataFrom(Date validDataFrom) {
    this.validDataFrom = validDataFrom;
  }

  public Date getValidDataTo() {
    return validDataTo;
  }

  public void setValidDataTo(Date validDataTo) {
    this.validDataTo = validDataTo;
  }  

}
