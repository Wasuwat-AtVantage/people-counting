package com.atv.tracking;

import java.util.*;

public class EdgeDevice {

  private static int lastCommandId;
  private boolean readFromDevice;
  protected String queuedCommandId;
  protected String queuedCommand;
  protected String queuedCommandParams;
  protected String queuedCommandResponse;
  public String id;
  public String name;
  public String version;
  public String[] heatmaps;
  public Date lastUpdated;
  public boolean watched;
  protected String cachedSnapshotImageUrl; // Read-only value

  public EdgeDevice() {
    readFromDevice = false;
    id = "";
    name = "";
    version = "0";
    lastUpdated = new Date();
    watched = false;
  }

  public boolean isWaitingDeviceResponse() {
    synchronized(this) { // Sync set / clear operation
      return readFromDevice;
    }
  }

  public boolean isReadyToSendNewCommand() {
    synchronized(this) { // Sync set / clear operation
      return (readFromDevice == false && queuedCommandId == null);
    }
  }

  public String getCachedSnapshotImageUrl() {
    return cachedSnapshotImageUrl;
  }

  public String queueCommand(String command, String commandParams) {    
    synchronized(this) { // Sync set / clear operation
      /* Nok: Now we always discard previous unfinished command.
      if (!isReadyToSendNewCommand()) {
        return null; // Not ready
      }
      */
      queuedCommandId = String.valueOf(lastCommandId);
      queuedCommand = command;
      queuedCommandParams = commandParams;
      queuedCommandResponse = null;
      lastCommandId++;
      readFromDevice = true;
      return queuedCommandId;
    }
  }

  public void setCommandResponse(String commandId, String commandResponse) {
    synchronized(this) {
      System.out.println("[INFO]: commandId="+commandId+", queuedCommandId="+queuedCommandId + " @ " + new Date().toString());
      if (!commandId.equals(queuedCommandId)) {
        System.out.println("[WARNING]: Attempt to set unmatched command response: " + commandId + " @ " + new Date().toString());
      } else {
        queuedCommandResponse = commandResponse;

        // In case of command ID is "SS" (Screenshot), we want to store the image into memory (or on disk as TBD)
        if ("SS".equals(queuedCommand)) {
          cachedSnapshotImageUrl = queuedCommandResponse;
        }

      }
      readFromDevice = false;
    }
  }

  public String getCommandResponse(String commandId) {
    synchronized(this) {
      if (!commandId.equals(queuedCommandId)) {
        System.out.println("[WARNING]: Attempt to get unmatched command response: " + commandId + " @ " + new Date().toString());
        return "busy";
      } else {
        return queuedCommandResponse;
      }
    }
  }

  public void markAsComplete(String commandId) {
    synchronized(this) {
      if (!commandId.equals(queuedCommandId)) {
        System.out.println("[WARNING]: Attempt to set unmatched command response: " + commandId + " @ " + new Date().toString());
      } else {
        queuedCommandId = null;
        queuedCommand = null;
        queuedCommandParams = null;
        queuedCommandResponse = null;
      }
    }
  }

}