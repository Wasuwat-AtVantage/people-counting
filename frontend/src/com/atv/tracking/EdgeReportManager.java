package com.atv.tracking;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.atv.edge.ExtendedROIService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EdgeReportManager {

  //public static final String BASE_RAW_JSON_REPORT_PATH = "C:\\Users\\nok\\work\\atvantage\\people-counting\\report_data";
  public static final String BASE_RAW_JSON_REPORT_PATH = "/report_data/";
  //public static final String BASE_CSV_REPORT_PATH = "C:\\Users\\nok\\work\\atvantage\\people-counting\\report_data";
  public static final String BASE_CSV_REPORT_PATH = "/report_data/";

  public static String[] HEADER_COLUMNS = new String[] {
    "File",
    "Date",
    "Time",
    "Hours",

    "Device ID",
    "ROI",
    "ROI Type",

    "Total",
    "Total Men",
    "Total Women",

    "Right-Total",
    "Right-M-Kids",
    "Right-M-Adults",
    "Right-M-Elders",
    "Right-W-Kids",
    "Right-W-Adults",
    "Right-W-Elders",
    "Right-Undefined",

    "Left-Total",
    "Left-M-Kids",
    "Left-M-Adults",
    "Left-M-Elders",
    "Left-W-Kids",
    "Left-W-Adults",
    "Left-W-Elders",
    "Left-Undefined",

    "Pass-Through-Total",
    "Pass-Through-M",
    "Pass-Through-W",

    "Interest-Total",
    "Interest-M-Total",
    "Interest-M-Kids",
    "Interest-M-Adults",
    "Interest-M-Elders",
    "Interest-W-Total",
    "Interest-W-Kids",
    "Interest-W-Adults",
    "Interest-W-Elders",
    "Interest-Undefined",

    "Engage-Total",
    "Engage-M-Total",
    "Engage-M-Kids",
    "Engage-M-Adults",
    "Engage-M-Elders",
    "Engage-W-Total",
    "Engage-W-Kids",
    "Engage-W-Adults",
    "Engage-W-Elders",
    "Engage-Undefined",

    "% Occupied",
    "Mins of Unoccupied"
  };

  public static String[] HEADER_UNOCCUPIED_COLUMNS = new String[] {
    "Device ID",
    "ROI",
    "ROI Type",
    "Date",
    "Unoccupied",
    "Unoccupied Period"
  };


  private static final String[] ROI_TEXT = new String[] {
    "None", "Customer", "Operation", "Traffic", "Special 1", "Special 2", "Demo", "Demo"
  };

  private static String getROITextDisplay(int roiType) {
    if (roiType < ROI_TEXT.length) {
      return EdgeReportManager.ROI_TEXT[roiType];
    } else {
      return String.valueOf(roiType);
    }
  }

  private static int get_count(Object hotspot_stat_, String[] queries) {
    Map<String, Integer> hotspot_stat = (Map<String, Integer>)hotspot_stat_;
    int count = 0;
    String[] keys = (String[])hotspot_stat.keySet().toArray(new String[0]);
    for (int i=0;i<keys.length;i++) {
      String key_str = keys[i];
      String _key_str = key_str;
      if (queries.length > 1) { // We do not want to handle overlap case if search query is single string
        key_str = "," + key_str + ","; // For not finding overlap substring of key
      }
      boolean found = true;
      for (int j=0;j<queries.length;j++) {
        String query = queries[j];
        // System.out.println(" :: " + key_str + " in " + query);
        if (key_str.indexOf(query) == -1) {
          found = false;
          break;
        }
      }
      if (found) {
        count = count + hotspot_stat.get(_key_str);
      }
    }
    // if (count > 0)
    //   System.out.println("QUERY: " + queries + " -> " + count);
    return count;
  }

  private static int get_exact_count(Object hotspot_stat_, String query) {
    Map<String, Integer> hotspot_stat = (Map<String, Integer>)hotspot_stat_;
    int count = 0;
    String[] keys = (String[])hotspot_stat.keySet().toArray(new String[0]);
    for (int i=0;i<keys.length;i++) {
      String key_str = keys[i];
      String _key_str = key_str;
      boolean found = true;
      // System.out.println(" :: " + key_str + " in " + query);
      if (!key_str.equals(query)) {
        found = false;
      }
      if (found) {
        count = count + hotspot_stat.get(_key_str);
      }
    }
    // if (count > 0)
    //   System.out.println("QUERY: " + query + " -> " + count);
    return count;
  }

  public static String minutePassedToTimeString(String baseTime, double minutes_passed) {

    int baseHour = Integer.parseInt(baseTime.substring(0,2));
    int baseMinute = Integer.parseInt(baseTime.substring(3,5));
    // int baseSecond = Integer.parseInt(baseTime.substring(6,8));

    int log_minute_ = (int)(Math.round(minutes_passed)) + baseHour * 60 + baseMinute;
    String log_hour = String.valueOf(log_minute_ / 60);
    while (log_hour.length() < 2) { 
      log_hour = '0' + log_hour;
    }
    String log_minute = String.valueOf(log_minute_ % 60);
    while (log_minute.length() < 2) { 
      log_minute = '0' + log_minute;
    }
    String this_time_string = log_hour + ':' + log_minute;
    return this_time_string;  
  }

  public static String percentUnoccupiedToUnoccupiedMinutes(String timeString, String prevTimeString, int percent_unoccupied) {
    if (prevTimeString == null) return "";
    int hour = Integer.parseInt(timeString.substring(0, 2));
    int min = Integer.parseInt(timeString.substring(3, 5));
    int prevHour = Integer.parseInt(prevTimeString.substring(0, 2));
    int prevMin = Integer.parseInt(prevTimeString.substring(3, 5));
    min = (hour * 60 + min) - (prevHour * 60 + prevMin);
    int ret = min * percent_unoccupied / 100;
    return String.valueOf(ret);
  }

  public static String timeDiffToMinuteCount(String timeString, String prevTimeString) {
    int hour = Integer.parseInt(timeString.substring(0, 2));
    int min = Integer.parseInt(timeString.substring(3, 5));
    int prevHour = Integer.parseInt(prevTimeString.substring(0, 2));
    int prevMin = Integer.parseInt(prevTimeString.substring(3, 5));
    min = (hour * 60 + min) - (prevHour * 60 + prevMin);
    return String.valueOf(min);
  }

  public static String timeStringToHourString(String timeString) {
    int hour = Integer.parseInt(timeString.substring(0, 2));
    int min = Integer.parseInt(timeString.substring(3, 5));
    // if (min >= 30) hour += 1;
    hour = hour % 24;
    boolean isPM = (hour >= 12);
    if (hour > 12) hour = hour - 12;
    if (hour == 0) hour = 12;
    String ret = String.valueOf(hour) + ((isPM)?" PM":" AM");
    return ret;
  }

  public static class SummaryDataRow {
    public String filePath;
    public String dateString;
    public String prevTimeString;
    public String timeString;
    public String hourString;
    public String deviceId;
    public int hotspot_stat_id;
    public int roi_type;
    public int interval_occupied_count;
    public int interval_unoccupied_count;
    public int[] dataRow;
  }

  public static void updateDataRowToSummaryMap(
    String filePath,
    String dateString,
    String prevTimeString,
    String timeString,
    String hourString,
    String deviceId,
    int hotspot_stat_id,
    int roi_type,
    int interval_occupied_count,
    int interval_unoccupied_count,
    String[] dataRow,
    HashMap<String, SummaryDataRow> map
  ) {
    String key = deviceId + "_" + dateString + "_" + hourString + "_" + hotspot_stat_id;
    SummaryDataRow summary = null;
    if (map.containsKey(key)) {
      summary = map.get(key);
      summary.timeString = timeString;
      summary.interval_occupied_count += interval_occupied_count;
      summary.interval_unoccupied_count += interval_unoccupied_count;
    } else {
      summary = new SummaryDataRow();
      map.put(key, summary);
      summary.filePath = filePath; // Store only first file path?
      summary.dateString = dateString;
      summary.deviceId = deviceId;
      summary.hotspot_stat_id = hotspot_stat_id;
      summary.hourString = hourString;
      summary.prevTimeString = prevTimeString;
      summary.roi_type = roi_type;
      summary.interval_occupied_count = interval_occupied_count;
      summary.interval_unoccupied_count = interval_unoccupied_count;
      summary.timeString = timeString;
      summary.dataRow = new int[dataRow.length];
    }
    for (int i=0;i<dataRow.length;i++) {
      summary.dataRow[i] += Integer.parseInt(dataRow[i]);
    }
  }

  public static String[] getSummarizedRow(SummaryDataRow summaryData) {
    int percent_occupied = 0;
    if (summaryData.interval_occupied_count + summaryData.interval_unoccupied_count > 0) {
      percent_occupied = (int)((double)summaryData.interval_occupied_count * 100.00 / (summaryData.interval_occupied_count + summaryData.interval_unoccupied_count));
    }
    String[] ret = new String[summaryData.dataRow.length];
    for (int j=0;j<summaryData.dataRow.length;j++) {
      ret[j] = String.valueOf(summaryData.dataRow[j]);
    }
    // Recalculate the percent occupied
    ret[42] = String.valueOf(percent_occupied);
    return ret;
  }

  public static ReportFileLocation generateCSVSummaryReport() {
    // Output Filepath
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HH");
    sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
    String csvOutputFilePath = BASE_CSV_REPORT_PATH + "\\" + sdf.format(new Date()) + ".csv";
    String csvUnoccupiedOutputFilePath = BASE_CSV_REPORT_PATH + "\\" + sdf.format(new Date()) + ".unoccupied.csv";
    File csvOutputFile = new File(csvOutputFilePath);
    File csvUnoccupiedOutputFile = new File(csvUnoccupiedOutputFilePath);

    FileOutputStream csvFOut = null;
    PrintWriter csvOut = null;
    FileOutputStream csvOcFOut = null;
    PrintWriter csvOcOut = null;
    try {
      if (csvOutputFile.exists() && csvUnoccupiedOutputFile.exists()) { // Reuse cached file
        return new ReportFileLocation(csvOutputFilePath, csvUnoccupiedOutputFilePath);
      }
      csvFOut = new FileOutputStream(csvOutputFile);
      csvOut = new PrintWriter(new OutputStreamWriter(csvFOut, "utf-8"));
      csvOcFOut = new FileOutputStream(csvUnoccupiedOutputFile);
      csvOcOut = new PrintWriter(new OutputStreamWriter(csvOcFOut, "utf-8"));

      for (int i=0;i<HEADER_COLUMNS.length;i++) {
        if (i>0) csvOut.print(",");
        csvOut.print(HEADER_COLUMNS[i]);
      }
      csvOut.println();

      for (int i=0;i<HEADER_UNOCCUPIED_COLUMNS.length;i++) {
        if (i>0) csvOcOut.print(",");
        csvOcOut.print(HEADER_UNOCCUPIED_COLUMNS[i]);
      }
      csvOcOut.println();

      // Map from DeviceID_DateString to TimeString
      HashMap<String, String> deviceAndDateToTimeMap = new HashMap<String, String>();

      // Map from DeviceID_DateString_HotspotID to UnoccupiedFrom
      HashMap<String, String> deviceDateAndHotspotIDToUnoccupiedFromMap = new HashMap<String, String>();

      // Map from DeviceID_DateString_HotspotID_HourString to List of Data Row
      HashMap<String, SummaryDataRow> deviceDateHourAndHotspotIDMap = new HashMap<String, SummaryDataRow>();

      // Map from DeviceID to Extended Area Formula
      HashMap<String, Object> deviceToExtendedAreaMap = new HashMap<>();

      ObjectMapper mapper = new ObjectMapper();

      // Structure for caching data when writing to unoccupied CSV file.
      // They are used to combine adjacent row those can be merged together.
      // We only need to cache 1 row of data
      Object[] ucLastRowCache = null;
      String ucLastKeyCache = null;

      try (Stream<Path> walk = Files.walk(Paths.get(BASE_RAW_JSON_REPORT_PATH))) {
        List<String> result = walk.map(x -> x.toString())
            .filter(f -> f.endsWith(".json")).collect(Collectors.toList());
        Collections.sort(result); // Sort alphabethly
        for (int i=0;i<result.size();i++) {
          String filePath = result.get(i);
          System.out.println("Start parsing report: " + filePath);

          try {
            Map<String, Object> map = new HashMap<>();
            byte[] json = Files.readAllBytes(new File(filePath).toPath());
            map = mapper.readValue(json, new TypeReference<Map<String, Object>>(){});
            
            // Count number of '_' in filePath
            int underscoreCount = 0;
            for (int k=filePath.length()-1;k>=0;k--) {
              char c = filePath.charAt(k);
              if (c == '/' || c == '\\') break;
              if (c == '_') underscoreCount++;
            }
            int suffice = 0;
            if (underscoreCount == 4)
              suffice = 16; // Both fromDate and toDate are presented in path
            else if (underscoreCount == 3)
              suffice = 1; // Only toDate is presented in path

            // Extract date / time and deviceId
            String timeString = filePath.substring(filePath.length()-6-suffice-5,filePath.length()-0-suffice-5);
            String dateString = filePath.substring(filePath.length()-15-suffice-5, filePath.length()-7-suffice-5);
            String deviceId = filePath.substring(filePath.length()-17-16-suffice-5, filePath.length()-16-suffice-5);

            timeString = timeString.substring(0,2) + ":" + timeString.substring(2,4) + ":" + timeString.substring(4,6);
            dateString = dateString.substring(0,4) + "/" + dateString.substring(4,6) + "/" + dateString.substring(6,8);

            // Get latest timeString for same device and same date
            String deviceAndDate = deviceId + "_" + dateString;
            String prevTimeString = null;
            if (deviceAndDateToTimeMap.containsKey(deviceAndDate)) {
              prevTimeString = deviceAndDateToTimeMap.get(deviceAndDate);
            }

            // If fromDate is existed in path, we extract and use it here
            if (underscoreCount == 4) {
              String _prevTimeString = filePath.substring(filePath.length()-6-5,filePath.length()-0-5);
              String _prevDateString = filePath.substring(filePath.length()-15-5, filePath.length()-7-5);
              _prevTimeString = _prevTimeString.substring(0,2) + ":" + _prevTimeString.substring(2,4) + ":" + _prevTimeString.substring(4,6);
              _prevDateString = _prevDateString.substring(0,4) + "/" + _prevDateString.substring(4,6) + "/" + _prevDateString.substring(6,8);
              if (dateString.equals(_prevDateString)) {
                prevTimeString = _prevTimeString;
              }
            }

            // Fill out data columns
            try {
              ArrayList<Object> hotspot_stats = (ArrayList<Object>)map.get("hotspot_stat");
              // System.out.println("hotspot_stats.length ->" + hotspot_stats.size());
              for (int j=0;j<hotspot_stats.size();j++) {
                Map<String, Object> hotspot_stat = (Map<String, Object>)hotspot_stats.get(j);

                // Get Snapshot statistic data

                int percent_occupied = 0;
                if ((Integer)hotspot_stat.get("interval_occupied_count") + (Integer)hotspot_stat.get("interval_unoccupied_count") > 0) {
                  percent_occupied = (int)((double)(Integer)hotspot_stat.get("interval_occupied_count") * 100.00 / ((Integer)hotspot_stat.get("interval_occupied_count") + (Integer)hotspot_stat.get("interval_unoccupied_count")));
                }
        
                int roi_type = (Integer)hotspot_stat.get("roi_type");
        
                // 0 -> 21 : Pass
                // 22 -> 31 : Interest
                // 32 -> 41 : Engaged
                String[] data_row = new String[] {
                  String.valueOf(((Map<String, Object>)hotspot_stat.get("count_map")).get("all")), // 0

                  String.valueOf(get_exact_count(hotspot_stat.get("count_map"), "male")), // 1
                  String.valueOf(get_exact_count(hotspot_stat.get("count_map"), "female")), // 2

                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h+,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h+,",",male,",",kid,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h+,",",male,",",adult,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h+,",",male,",",elder,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h+,",",female,",",kid,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h+,",",female,",",adult,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h+,",",female,",",elder,"})),
                  "",
        
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h-,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h-,",",male,",",kid,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h-,",",male,",",adult,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h-,",",male,",",elder,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h-,",",female,",",kid,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h-,",",female,",",adult,"})),
                  String.valueOf(get_count(hotspot_stat.get("count_map"), new String[]{",h-,",",female,",",elder,"})),
                  "",  // 18

                  // Pass-Through gang
                  String.valueOf((Integer)(((Map<String, Object>)hotspot_stat.get("count_map")).get("all"))), // 19 // - (Integer)(((Map<String, Object>)hotspot_stat.get("interested_count_map")).get("all"))),
                  String.valueOf(get_exact_count(hotspot_stat.get("count_map"), "male")), // - get_exact_count(hotspot_stat.get("interested_count_map"), "male")),
                  String.valueOf(get_exact_count(hotspot_stat.get("count_map"), "female")), // - get_exact_count(hotspot_stat.get("interested_count_map"), "female")),
        
                  String.valueOf(((Map<String, Object>)hotspot_stat.get("interested_count_map")).get("all")), // 22
                  String.valueOf(get_exact_count(hotspot_stat.get("interested_count_map"), "male")),
                  String.valueOf(get_count(hotspot_stat.get("interested_count_map"), new String[]{",male,",",kid,"})),
                  String.valueOf(get_count(hotspot_stat.get("interested_count_map"), new String[]{",male,",",adult,"})),
                  String.valueOf(get_count(hotspot_stat.get("interested_count_map"), new String[]{",male,",",elder,"})),
                  String.valueOf(get_exact_count(hotspot_stat.get("interested_count_map"), "female")),
                  String.valueOf(get_count(hotspot_stat.get("interested_count_map"), new String[]{",female,",",kid,"})),
                  String.valueOf(get_count(hotspot_stat.get("interested_count_map"), new String[]{",female,",",adult,"})),
                  String.valueOf(get_count(hotspot_stat.get("interested_count_map"), new String[]{",female,",",elder,"})),
                  "", // 31
        
                  String.valueOf(((Map<String, Object>)hotspot_stat.get("engaged_count_map")).get("all")), // 32
                  String.valueOf(get_exact_count(hotspot_stat.get("engaged_count_map"), "male")),
                  String.valueOf(get_count(hotspot_stat.get("engaged_count_map"), new String[]{",male,",",kid,"})),
                  String.valueOf(get_count(hotspot_stat.get("engaged_count_map"), new String[]{",male,",",adult,"})),
                  String.valueOf(get_count(hotspot_stat.get("engaged_count_map"), new String[]{",male,",",elder,"})),
                  String.valueOf(get_exact_count(hotspot_stat.get("engaged_count_map"), "female")),
                  String.valueOf(get_count(hotspot_stat.get("engaged_count_map"), new String[]{",female,",",kid,"})),
                  String.valueOf(get_count(hotspot_stat.get("engaged_count_map"), new String[]{",female,",",adult,"})),
                  String.valueOf(get_count(hotspot_stat.get("engaged_count_map"), new String[]{",female,",",elder,"})),
                  "", // 41
        
                  String.valueOf(percent_occupied),
                  percentUnoccupiedToUnoccupiedMinutes(timeString, prevTimeString, 100 - percent_occupied)
                };

                // Calculate undefined column
                data_row[10] = String.valueOf(Integer.parseInt(data_row[3]) - Integer.parseInt(data_row[4]) - Integer.parseInt(data_row[5]) - Integer.parseInt(data_row[6]) - Integer.parseInt(data_row[7]) - Integer.parseInt(data_row[8]) - Integer.parseInt(data_row[9]));
                data_row[18] = String.valueOf(Integer.parseInt(data_row[11]) - Integer.parseInt(data_row[12]) - Integer.parseInt(data_row[13]) - Integer.parseInt(data_row[14]) - Integer.parseInt(data_row[15]) - Integer.parseInt(data_row[16]) - Integer.parseInt(data_row[17]));
                data_row[31] = String.valueOf(Integer.parseInt(data_row[22]) - Integer.parseInt(data_row[24]) - Integer.parseInt(data_row[25]) - Integer.parseInt(data_row[26]) - Integer.parseInt(data_row[28]) - Integer.parseInt(data_row[29]) - Integer.parseInt(data_row[30]));
                data_row[41] = String.valueOf(Integer.parseInt(data_row[32]) - Integer.parseInt(data_row[34]) - Integer.parseInt(data_row[35]) - Integer.parseInt(data_row[36]) - Integer.parseInt(data_row[38]) - Integer.parseInt(data_row[39]) - Integer.parseInt(data_row[40]));
                
                if (prevTimeString != null && timeString != null) { // Log only the period that we have both start-end time.
                  String hourString = timeStringToHourString(prevTimeString);
                  updateDataRowToSummaryMap(filePath, dateString, prevTimeString, timeString, 
                    hourString, deviceId, j, roi_type, 
                    (Integer)hotspot_stat.get("interval_occupied_count"),
                    (Integer)hotspot_stat.get("interval_unoccupied_count"),
                    data_row, deviceDateHourAndHotspotIDMap);
                  // Summarize hourly before write to CSV file
                  // csvOut.println(filePath + "," + dateString + "," + prevTimeString + "-" + timeString + "," + hourString + "," + deviceId + "," + /*hotspot_stat_id*/ j + ',' + ROI_TEXT[roi_type] + ',' + String.join(",", data_row));

                  // Also look into every Extended HotSpot ID
                  // Search key in the hash map
                  List<Map<String, String>> extendAreaList = null;
                  if (deviceToExtendedAreaMap.containsKey(deviceId)) {
                    extendAreaList = (List<Map<String, String>>)deviceToExtendedAreaMap.get(deviceId);
                  } else {
                    String listJSONStr = ExtendedROIService.getInstance().loadExtenedROIList(deviceId);
                    extendAreaList = null;
                    try {
                      extendAreaList = Arrays.asList(mapper.readValue(listJSONStr, new TypeReference<Map<String, String>>(){}));
                    } catch(Exception eee) {
                      eee.printStackTrace();
                    }
                    if (extendAreaList != null) {
                      deviceToExtendedAreaMap.put(deviceId, extendAreaList);
                    }
                  }
                  if (extendAreaList != null) {
                    // Iterate every formula
                    // TBD: This flow is broken, we need to rewrite it with new logics.
                    /*
                    for (int z=0;z<extendAreaList.size();z++) {
                      String passFormula = extendAreaList.get(z).get("pass_through_source");
                      String interestFormula = extendAreaList.get(z).get("interest_source");
                      String engagedFormula = extendAreaList.get(z).get("engaged_source");
                      try {
                        ExtendedAreaFormula formula = ExtendedAreaFormula.parseFromString(passFormula);
                        for (int y=0;y<formula.roiIds.size();y++) {
                          if (formula.roiIds.get(y) == j) { // If the formula contain source from this ROI
                            int factor = formula.factors.get(y);
                            int source_id = formula.sourceTypes.get(y);
                            String[] extended_data_row = ExtendedAreaFormula.produceExtendedRow(data_row, 0, source_id, factor);
                            updateDataRowToSummaryMap(filePath, dateString, prevTimeString, timeString, 
                              hourString, deviceId, -z, 0, // ROI_ID for extended ROI is minus value 
                              0,
                              0,
                              extended_data_row, deviceDateHourAndHotspotIDMap);
                          }
                        }
                      } catch(Exception e) {}
                      try {
                        ExtendedAreaFormula formula = ExtendedAreaFormula.parseFromString(interestFormula);
                        for (int y=0;y<formula.roiIds.size();y++) {
                          if (formula.roiIds.get(y) == j) { // If the formula contain source from this ROI
                            int factor = formula.factors.get(y);
                            int source_id = formula.sourceTypes.get(y);
                            String[] extended_data_row = ExtendedAreaFormula.produceExtendedRow(data_row, 1, source_id, factor);
                            updateDataRowToSummaryMap(filePath, dateString, prevTimeString, timeString, 
                              hourString, deviceId, -z, 0, // ROI_ID for extended ROI is minus value 
                              0,
                              0,
                              extended_data_row, deviceDateHourAndHotspotIDMap);
                          }
                        }
                      } catch(Exception e) {}
                      try {
                        ExtendedAreaFormula formula = ExtendedAreaFormula.parseFromString(engagedFormula);
                        for (int y=0;y<formula.roiIds.size();y++) {
                          if (formula.roiIds.get(y) == j) { // If the formula contain source from this ROI
                            int factor = formula.factors.get(y);
                            int source_id = formula.sourceTypes.get(y);
                            String[] extended_data_row = ExtendedAreaFormula.produceExtendedRow(data_row, 2, source_id, factor);
                            updateDataRowToSummaryMap(filePath, dateString, prevTimeString, timeString, 
                              hourString, deviceId, -z, 0, // ROI_ID for extended ROI is minus value 
                              0,
                              0,
                              extended_data_row, deviceDateHourAndHotspotIDMap);
                          }
                        }
                      } catch(Exception e) {}

                    }
                    */
                    
                  }
                }

                // Get Occupied / Unoccupied event data
                if (prevTimeString != null) {
                  List<Object> eventList = (List<Object>)hotspot_stat.get("events");
                  if (eventList != null) {
                    String deviceDateAndHotspotID = deviceId + "_" + dateString + "_" + j;
                    System.out.println("Event Count = " + eventList.size());
                    String unoccupiedFrom = null;                
                    if (deviceDateAndHotspotIDToUnoccupiedFromMap.containsKey(deviceDateAndHotspotID)) {
                      unoccupiedFrom = deviceDateAndHotspotIDToUnoccupiedFromMap.get(deviceDateAndHotspotID);
                    }
                    for (int k=0;k<eventList.size();k++) {
                      List<Object> event = (List<Object>)eventList.get(k);                  
                      if (event.size() > 1) {
                        String eventName = (String)event.get(0);
                        Double eventValue = (Double)event.get(1);
                        System.out.println(" : Event: " + eventName + ", " + eventValue);
                        if ("out".equals(eventName)) {
                          unoccupiedFrom = minutePassedToTimeString(prevTimeString, eventValue.doubleValue());
                        } else if ("in".equals(eventName)) {
                          if (unoccupiedFrom != null) {
                            String unoccupiedTo = minutePassedToTimeString(prevTimeString, eventValue.doubleValue());
                            String unoccupiedPeriod = timeDiffToMinuteCount(unoccupiedTo, unoccupiedFrom);
                            // Prevent abnormal data
                            if (unoccupiedPeriod.length() > 0 && unoccupiedPeriod.charAt(0) == '-')
                              continue;
                            // See if it can be combined with latest cache
                            boolean canMerge = true;
                            if (ucLastRowCache == null) {
                              canMerge = false;
                            } else {
                              if (!deviceDateAndHotspotID.equals(ucLastKeyCache))
                                canMerge = false;
                              else if (!ucLastRowCache[2].equals(unoccupiedFrom))
                                canMerge = false;
                            }
                            // If cannot merge, log the last entry
                            if (!canMerge) {
                              if (ucLastRowCache != null && !"0".equals(ucLastRowCache[3]))
                                csvOcOut.println(ucLastRowCache[0] + "" + ucLastRowCache[1] + "-" + ucLastRowCache[2] + "," + ucLastRowCache[3]);
                              Object[] ucCache = new Object[] {
                                deviceId + "," + j + "," + ROI_TEXT[roi_type] + "," + dateString + ",",
                                unoccupiedFrom,
                                unoccupiedTo,
                                unoccupiedPeriod
                              };                            
                              ucLastRowCache = ucCache;
                              ucLastKeyCache = deviceDateAndHotspotID;
                            } else {
                              int sumOcPeriod = Integer.parseInt((String)ucLastRowCache[3]) + Integer.parseInt(unoccupiedPeriod);
                              ucLastRowCache[3] = String.valueOf(sumOcPeriod);
                              ucLastRowCache[2] = unoccupiedTo;
                            }
                            // csvOcOut.println(deviceId + "," + j + "," + ROI_TEXT[roi_type] + "," + dateString + "," + unoccupiedFrom + " - " + unoccupiedTo + "," + unoccupiedPeriod);
                            unoccupiedFrom = null;
                          }
                        }
                      }
                    }
                    deviceDateAndHotspotIDToUnoccupiedFromMap.put(deviceDateAndHotspotID, unoccupiedFrom);
                  }
                }

              }

              deviceAndDateToTimeMap.put(deviceAndDate, timeString);

            } catch(Exception ie) {
              ie.printStackTrace();
              System.exit(0);
            }

            System.out.println("Finished parsing report for deviceId: " + deviceId + " @ " + dateString + " " + timeString);
          } catch(Exception e) {
            System.out.println("[Error] - cannot parse: " + filePath);
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      // Write the latest entry of the unoccupied report
      if (ucLastRowCache != null)
        if (!"0".equals(ucLastRowCache[3]))
          csvOcOut.println(ucLastRowCache[0] + "" + ucLastRowCache[1] + "-" + ucLastRowCache[2] + "," + ucLastRowCache[3]);


      // Actual write to csv goes here
      SummaryDataRow[] summaries = (SummaryDataRow[])deviceDateHourAndHotspotIDMap.values().toArray(new SummaryDataRow[0]);
      for (int i=0;i<summaries.length;i++) {
        SummaryDataRow summary = summaries[i];
        csvOut.println(summary.filePath + "," + 
          summary.dateString + "," + 
          summary.prevTimeString + "-" + summary.timeString + "," + 
          summary.hourString + "," + 
          summary.deviceId + "," + 
          summary.hotspot_stat_id + "," + 
          getROITextDisplay(summary.roi_type) + ',' + 
          String.join(",", getSummarizedRow(summary)));
      }

    } catch (Exception ee) {
      ee.printStackTrace();
    } finally {
      try {csvOut.close();} catch (Exception e) {}
      try {csvFOut.close();} catch (Exception e) {}
      try {csvOcOut.close();} catch (Exception e) {}
      try {csvOcFOut.close();} catch (Exception e) {}
    }
    return new ReportFileLocation(csvOutputFilePath, csvUnoccupiedOutputFilePath);
  }

  public static class ExtendedAreaFormula {
    public ArrayList<Integer> roiIds; // 0, 1, 2
    public ArrayList<Integer> sourceTypes; // 0 - Pass, 1 - Interest, 2 - Engage
    public ArrayList<Integer> factors; // -1 => minus, 1 => plus

    private ExtendedAreaFormula() {
      roiIds = new ArrayList<>();
      sourceTypes = new ArrayList<>();
      factors = new ArrayList<>();
    }

    public static ExtendedAreaFormula parseFromString(String str) throws Exception {
      ExtendedAreaFormula ret = new ExtendedAreaFormula();
      try {
        StringBuffer sb = new StringBuffer();
        int lastFactor = 1;
        boolean storeIndex = false;
        for (int i=0;i<str.length();i++) {
          if (str.charAt(0) == '-') {
            lastFactor = -1;
            continue;
          }
          char ch = str.charAt(i);
          if (ch == '+') {
            if (!storeIndex) {
              throw new Exception("Error parsing formula: " + str + " at character: " + ch + " (Unexpected token at this position)");
            }
            ret.sourceTypes.add(Integer.parseInt(sb.toString()));
            ret.factors.add(lastFactor);
            sb = new StringBuffer();
            lastFactor = 1;
          } else if (ch == '-') {
            if (!storeIndex) {
              throw new Exception("Error parsing formula: " + str + " at character: " + ch + " (Unexpected token at this position)");
            }
            ret.sourceTypes.add(Integer.parseInt(sb.toString()));
            ret.factors.add(lastFactor);
            sb = new StringBuffer();
            lastFactor = -1;
          } else if (ch >= '0' && ch <= 9) {
            sb.append(ch);
          } else if (ch == ':') {
            if (storeIndex) {
              throw new Exception("Error parsing formula: " + str + " at character: " + ch + " (Unexpected token at this position)");
            } else {
              ret.roiIds.add(Integer.parseInt(sb.toString()));
              storeIndex = true;
              sb = new StringBuffer();
            }
          } else {
            throw new Exception("Error parsing formula: " + str + " at character: " + ch);
          }
        }
        ret.sourceTypes.add(Integer.parseInt(sb.toString()));
        ret.factors.add(lastFactor);
        if (ret.sourceTypes.size() != ret.factors.size() || ret.sourceTypes.size() != ret.roiIds.size()) {
          throw new Exception("Error parsing formula: " + str + " (Size check error: " + ret.sourceTypes.size() + ", " + ret.roiIds.size() + ", " + ret.factors.size());
        }
      } catch(Exception e) {
        throw e;
      }
      return ret;
    }

    public static String[] produceExtendedRow(String[] datarow, int targetId, int sourceId, int factor) {
      String[] ret = new String[datarow.length];

      // This will be supported later, now just copy every row
      // 0 -> 21 : Pass
      // 22 -> 31 : Interest
      // 32 -> 41 : Engaged
      int[] starts = new int[] {0, 22, 32};
      int[] ends = new int[] {21, 31, 41};

      for (int i=0;i<=ret.length;i++) {
        ret[i] = "0"; // Default value
      }
      for (int i=0;i<=41;i++) {
        ret[i] = String.valueOf(Integer.parseInt(datarow[i]) * factor);
      }

      return ret;
    }
  }

  public static class ReportFileLocation {
    private String summaryReportLocation;
    private String unoccupiedReportLocation;
    public ReportFileLocation(String summaryReportLocation, String unoccupiedReportLocation) {
      this.summaryReportLocation = summaryReportLocation;
      this.unoccupiedReportLocation = unoccupiedReportLocation;
    }
    public String getSummaryReportLocation() {
      return summaryReportLocation;
    }
    public String getUnoccupiedReportLocation() {
      return unoccupiedReportLocation;
    }
  }

  // Unit Test 
  public static void main(String[] args) {
    System.out.println("Running Unit Test of EdgeReportManager...");
    System.out.println(generateCSVSummaryReport());
  }

}