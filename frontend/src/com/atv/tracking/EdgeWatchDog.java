package com.atv.tracking;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.net.*;
import java.text.*;

public class EdgeWatchDog implements Runnable {
  protected static EdgeWatchDog instance;

  private static final long ALERT_LOST_MINUTE = 5;
  private SimpleDateFormat sdf;

  public static EdgeWatchDog getInstance() {
    if (instance == null) {
      instance = new EdgeWatchDog();
    }
    return instance;
  }

  protected EdgeWatchDog() {
    sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));

    new Thread(this).start();
  }

  public void run() {
    System.out.println("Edge Watch Dog thread is started.");
    while (true) {
      try {
        Thread.sleep(60 * 1000); // Sleep for 60 secs
        Date currentTime = new Date();        
        long currentTimeLong = currentTime.getTime();
        System.out.println(">> Edge Watch Dog thread starts checking at " + sdf.format(currentTime));
        EdgeDevice[] deviceList = PollingStateManager.getInstance().listEdgeDevices(null); // List devices of all projects
        for (int i=0;i<deviceList.length;i++) {
          if (deviceList[i].watched) {
            Date lastUpdated = deviceList[i].lastUpdated;
            long lastUpdatedLong = lastUpdated.getTime();
            long diffInMillies = Math.abs(lastUpdatedLong - currentTimeLong);
            long diffInMinutes = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if (diffInMinutes > ALERT_LOST_MINUTE) {
              String alertMessage = "Edge Device: " + deviceList[i].id + " is lost from " + sdf.format(lastUpdated);
              System.out.println(">>> " + alertMessage);
              PushNotificationService.getInstance().sendAdminPushNotification("", "edgeLost", alertMessage, alertMessage);
            }
          }
        }
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
  }
}