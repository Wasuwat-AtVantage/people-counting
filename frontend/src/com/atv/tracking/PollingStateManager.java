package com.atv.tracking;
import java.util.*;
import com.atv.edge.*;

public class PollingStateManager {
  
  protected static PollingStateManager instance;
  private Hashtable<String, EdgeDevice> deviceStatusMap;

  public static PollingStateManager getInstance() {
    if (instance == null) {
      instance = new PollingStateManager();
    }
    return instance;
  }

  protected PollingStateManager() {
    deviceStatusMap = new Hashtable<String, EdgeDevice>();

    // Start Realtime Report Service
    RealtimeDataService.getInstance();
  }

  // For polling service to get device, register as new device if not existed.
  public EdgeDevice getEdgeDevice(String deviceId, String version) {
    EdgeDevice device = null;
    if (deviceStatusMap.containsKey(deviceId)) {
      device = deviceStatusMap.get(deviceId);
    } else {
      device = new EdgeDevice();
      device.id = deviceId;
      device.name = "";
      deviceStatusMap.put(deviceId, device);
      System.out.println("[INFO] Register new Edge Device: " + deviceId + " @ " + new Date().toString());

      // Get first snapshot from device
      setEdgeDeviceCommand(deviceId, "SS", "");
    }
    if (version != null)
      device.version = version;
    return device;
  }

  // For API to get device, return null if not existed.
  public EdgeDevice getEdgeDevice(String deviceId) {
    EdgeDevice device = null;
    if (deviceStatusMap.containsKey(deviceId)) {
      device = deviceStatusMap.get(deviceId);
    }
    return device;
  }

  public EdgeDevice[] listEdgeDevices(String projId) {
    // TODO: Implement Proj ID filtering
    EdgeDevice[] devices = (EdgeDevice[])deviceStatusMap.values().toArray(new EdgeDevice[deviceStatusMap.values().size()]);
    return devices;
  }

  public String receiveDevicePolling(String deviceId, String params, String version) {
    EdgeDevice device = getEdgeDevice(deviceId, version);

    // Update last update, so we know when is the last time device get connected to server
    Date lastUpdateTime = new Date();
    device.lastUpdated = lastUpdateTime;
    // [TODO]: We also can write log file here to log device uptime chart

    // If in read from device mode, then reading response, otherwise write new command
    if (device.isWaitingDeviceResponse()) {
      String[] paramsAr = params.split("\t");
      if (paramsAr.length > 1 &&  paramsAr[0].length() > 0) {
        String commandId = paramsAr[0];
        String commandResponse = paramsAr[1];
        System.out.println("[INFO] Set command response: commandId=" + commandId + ", length=" + commandResponse.length() + " @ " + new Date().toString());
        device.setCommandResponse(commandId, commandResponse);
        // Return status to clear command response from device memory
        return device.queuedCommandId + "|FIN|";
      }  
      // Resend last command until we get response
      if (device.queuedCommandId != null && device.queuedCommandResponse == null) {
        String response = device.queuedCommandId + "|" + device.queuedCommand + "|" + device.queuedCommandParams;
        return response;
      } else {
        return "";
      }      
    } else {
      // Send new command
      if (device.queuedCommandId != null && device.queuedCommandResponse == null) {
        String response = device.queuedCommandId + "|" + device.queuedCommand + "|" + device.queuedCommandParams;
        return response;
      } else {
        return "";
      }      
    }  
  }

  public String setEdgeDeviceCommand(String deviceId, String command, String commandParams) {
    EdgeDevice device = getEdgeDevice(deviceId, null);
    /* Nok: Now we always discard previous unfinished command.
    if (!device.isReadyToSendNewCommand()) { // Not ready
      return null;
    }
    */
    return device.queueCommand(command, commandParams);
  }

  public String getEdgeDeviceResponse(String deviceId, String commandId) {
    EdgeDevice device = getEdgeDevice(deviceId, null);
    return device.getCommandResponse(commandId);
  }

  public void markEdgeDeviceResponseComplete(String deviceId, String commandId) {
    EdgeDevice device = getEdgeDevice(deviceId, null);
    device.markAsComplete(commandId);
  }

}
