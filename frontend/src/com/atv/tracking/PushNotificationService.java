package com.atv.tracking;

import java.util.*;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.TopicManagementResponse;
import com.google.firebase.messaging.WebpushConfig;
import com.google.firebase.messaging.WebpushNotification;

import java.io.*;
import java.net.*;
import java.text.*;

public class PushNotificationService {

  public static class NotificationObject {
    public String clientKey;
    public String topic;
    public String title;
    public String body;
    public String clickAction;
    public String iconUrl;
  }

  protected static PushNotificationService instance;

  private ArrayList<String> recipientEmailList;
  private ArrayList<String> adminRecipientEmailList;
  private static final String EMAIL_FILE = "/tmp/notification_reciever.txt";
  private static final String ADMIN_EMAIL_FILE = "/tmp/admin_notification_reciever.txt";

  // Store list of previous 10 notifications
  private ArrayList<NotificationObject> previousNotification;

  public static PushNotificationService getInstance() {
    if (instance == null) {
      instance = new PushNotificationService();
    }
    return instance;
  }

  protected PushNotificationService() {
    previousNotification = new ArrayList<>();
    try {
      System.out.println("Initialize Firebase Admin Module...");
      FileInputStream serviceAccount =
        new FileInputStream("/firebase_cfg/service_account.json");
    
      FirebaseOptions options = new FirebaseOptions.Builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .setDatabaseUrl("https://machinelearning-246206.firebaseio.com")
        .build();
      
      FirebaseApp.initializeApp(options);
    } catch(Exception e) {
      e.printStackTrace();
    }

    // Read email target from configuration file
    recipientEmailList = new ArrayList<String>();
    BufferedReader br = null;
    try {
      br = new BufferedReader(new InputStreamReader(new FileInputStream(EMAIL_FILE), "utf-8"));
      String str = null;
      do {
        str = br.readLine();
        if (str != null && str.length() > 0) {
          str = str.trim();
          System.out.println("Adding Notification Receiver Email: " + str);
          recipientEmailList.add(str);          
        }
      } while (str != null && str.length() > 0);
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      try { br.close(); } catch(Exception ee){}
    }

    // Read admin email target from configuration file
    adminRecipientEmailList = new ArrayList<String>();
    br = null;
    try {
      br = new BufferedReader(new InputStreamReader(new FileInputStream(ADMIN_EMAIL_FILE), "utf-8"));
      String str = null;
      do {
        str = br.readLine();
        if (str != null && str.length() > 0) {
          str = str.trim();
          System.out.println("Adding Notification Receiver Admin Email: " + str);
          adminRecipientEmailList.add(str);          
        }
      } while (str != null && str.length() > 0);
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      try { br.close(); } catch(Exception ee){}
    }

  }
  
  public ArrayList<String> getNotificationRecieverEmailList() {
    return recipientEmailList;
  }

  public ArrayList<String> getNotificationRecieverAdminEmailList() {
    return adminRecipientEmailList;
  }

  public void setNotificationRecieverEmailList(ArrayList<String> list) {
    recipientEmailList = list;
  }

  public void setNotificationRecieverAdminEmailList(ArrayList<String> list) {
    adminRecipientEmailList = list;
  }

  public List<NotificationObject> getPreviousNotifications() {
    return previousNotification;
  }

  public void updateNotificationRecieverEmailList() {
    PrintWriter pw = null;
    try {
      pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(EMAIL_FILE), "utf-8"));
      for (int i=0;i<recipientEmailList.size();i++) {
        pw.println(recipientEmailList.get(i));
      }
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      try { pw.close(); } catch(Exception ee){}
    }
  }

  public void updateNotificationRecieverAdminEmailList() {
    PrintWriter pw = null;
    try {
      pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(ADMIN_EMAIL_FILE), "utf-8"));
      for (int i=0;i<adminRecipientEmailList.size();i++) {
        pw.println(adminRecipientEmailList.get(i));
      }
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      try { pw.close(); } catch(Exception ee){}
    }
  }

  public void registerPushReceiver(String clientKey, String topic) {
    try {
      // These registration tokens come from the client FCM SDKs.
      List<String> registrationTokens = Arrays.asList(
        clientKey
      );

      // Subscribe the devices corresponding to the registration tokens to the
      // topic.
      TopicManagementResponse response = FirebaseMessaging.getInstance().subscribeToTopic(
        registrationTokens, topic);
      // See the TopicManagementResponse reference documentation
      // for the contents of response.
      System.out.println(response.getSuccessCount() + " tokens were subscribed successfully");  
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  public void unregisterPushReceiver(String clientKey, String topic) {
    try {
      // These registration tokens come from the client FCM SDKs.
      List<String> registrationTokens = Arrays.asList(
        clientKey
      );

      // Subscribe the devices corresponding to the registration tokens to the
      // topic.
      TopicManagementResponse response = FirebaseMessaging.getInstance().unsubscribeFromTopic(
        registrationTokens, topic);
      // See the TopicManagementResponse reference documentation
      // for the contents of response.
      System.out.println(response.getSuccessCount() + " tokens were unsubscribed successfully");  
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  public void sendPushNotification(String clientKey, String topic,
    String title, String body, String clickAction, String iconUrl
  ) {
    NotificationObject no = new NotificationObject();
    no.clientKey = clientKey;
    no.topic = topic;
    no.title = title;
    no.body = body;
    no.clickAction = clickAction;
    no.iconUrl = iconUrl;
    previousNotification.add(no);
    while (previousNotification.size() > 10)
      previousNotification.remove(0);
    try {
      // See documentation on defining a message payload.
      WebpushNotification noti = WebpushNotification.builder().setTitle(title)
        .setBody(body)
        .setIcon(iconUrl)
        .putCustomData("click_action", clickAction).build();
      Message message = Message.builder()
          .setWebpushConfig(WebpushConfig.builder()
            .setNotification(noti).build()
          ).setTopic(topic)
          .build();
      // Send a message to the devices subscribed to the provided topic.
      String response = FirebaseMessaging.getInstance().send(message);
      // Response is a message ID string.
      System.out.println("Successfully sent message: " + response);
    } catch(Exception e) {
      e.printStackTrace();
    }

    try {
      String[] emails = recipientEmailList.toArray(new String[recipientEmailList.size()]);
      String emailBody = "<html><body><h3>" + body + "</h3><br><a href=\"" + clickAction + "\">View snapshot from camera.</a></body></html>";
      EmailSender.generateAndSendEmail(emails, "[BIZCuit] - " + title, emailBody);
      System.out.println("Successfully sent email");
    } catch(Exception e) {
      e.printStackTrace();
    }

  }

  public void sendAdminPushNotification(String clientKey, String topic,
    String title, String body
  ) {
    try {
      // See documentation on defining a message payload.
      WebpushNotification noti = WebpushNotification.builder().setTitle(title)
        .setBody(body)
        .build();
      Message message = Message.builder()
          .setWebpushConfig(WebpushConfig.builder()
            .setNotification(noti).build()
          ).setTopic(topic)
          .build();
      // Send a message to the devices subscribed to the provided topic.
      String response = FirebaseMessaging.getInstance().send(message);
      // Response is a message ID string.
      System.out.println("Successfully sent message: " + response);
    } catch(Exception e) {
      e.printStackTrace();
    }

    try {
      String[] emails = adminRecipientEmailList.toArray(new String[adminRecipientEmailList.size()]);
      String emailBody = "<html><body><h4>" + body + "</h4></body></html>";
      EmailSender.generateAndSendEmail(emails, "[BIZCuit Admin] - " + title, emailBody);
      System.out.println("Successfully sent email");
    } catch(Exception e) {
      e.printStackTrace();
    }

  }

}
