import os
import cv2
import json
import sys
from sys import platform
import numpy as np
import matplotlib.pyplot as plt

import keras
from keras import backend as K
from keras.layers.core import Dense, Activation
from keras.optimizers import Adam
from keras.metrics import categorical_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
from keras.models import Model
from keras.applications import imagenet_utils
from keras.layers import Dense,GlobalAveragePooling2D,Flatten,Dropout
from keras.applications import MobileNetV2
from keras.applications.mobilenet import preprocess_input
from IPython.display import Image
from keras.optimizers import Adam
from keras.utils import plot_model
from keras.regularizers import l1
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.models import load_model, model_from_json

# loading keras model
K.set_learning_phase(0)

# load json and create model
MODEL_JSON = 'gender_model.json'
MODEL_WEIGHTS = 'gender_model_weight.h5'

print("Loaded model from disk")    
json_file = open(MODEL_JSON, 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)
# load weights into new model
model.load_weights(MODEL_WEIGHTS)

#for i, layer in enumerate(model.layers):
#    print(i, layer.name)
model.summary() # This give more information about the model

test_datagen = ImageDataGenerator(
    preprocessing_function=preprocess_input
    )

val_generator = test_datagen.flow_from_directory(
    './dataset/validation', 
    shuffle=False,
    target_size=(224,224), 
    color_mode='rgb', 
    batch_size=1, 
    class_mode='categorical')

rev_class_map = dict(map(reversed, val_generator.class_indices.items()))
count = 0
correct_count = 0
counts = [0, 0]
correct_counts = [0, 0]
with open('eval_report.csv', 'w', encoding='utf-8') as fout:
  fout.write('ID,File Path,Label,Predicted,Score\n')
  for val_data in val_generator:
    #if count > 10:
    #  break
    try:
      file_path = val_generator.filepaths[count]
      input_data = val_data[0]
      label = val_data[1]
      predicted = model.predict(input_data)
      print(file_path)
      print(predicted)
      print(label)

      predicted_idx = np.argmax(predicted[0])
      label_idx = np.argmax(label[0])
      print(str(predicted_idx) + ' <=> ' + str(label_idx))

      fout.write(str(count + 1) + ',' + file_path + ',' + rev_class_map[label_idx] + ',' + rev_class_map[predicted_idx] + ',' + ('1' if label_idx == predicted_idx else '0') + '\n' )

      if (label_idx == predicted_idx):
        correct_count = correct_count + 1
        correct_counts[label_idx] = correct_counts[label_idx] + 1
      count = count + 1
      counts[label_idx] = counts[label_idx] + 1
    except:
      break

print('---------------------------------------------------')
print('Score = ' + str(correct_count) + '/' + str(count))
percent = 100.00 * correct_count / count
print('Percent Accuracy = ' + str(percent) + '%')
for i in range(len(counts)):
  print('---------------------------------------------------')
  print('Score for class: ' + rev_class_map[i] + ' = ' + str(correct_counts[i]) + '/' + str(counts[i]))
  if counts[i] == 0:
    percent = 0
  else:
    percent = 100.00 * correct_counts[i] / counts[i]
  print('Percent Accuracy = ' + str(percent) + '%')
print('---------------------------------------------------')
