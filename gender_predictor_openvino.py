import cv2
import numpy as np
import os
import dlib

from object_classifier import ObjectClassifier


class GenderPredictorOpenVINO(ObjectClassifier):
    def __init__(self, fp_num):
        self.model = None
        self.data_precision = fp_num

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('GenderPredictorOpenVINO::init_model')

        xml_file = "../gender_mobilenetv2_ssd/openvino.xml"
        bin_file = "../gender_mobilenetv2_ssd/openvino.bin"
        if self.data_precision == 'fp32':
            xml_file = "../gender_mobilenetv2_ssd/openvino_fp32.xml"
            bin_file = "../gender_mobilenetv2_ssd/openvino_fp32.bin"
        self.model = cv2.dnn.readNet(xml_file, bin_file)
        if "use_myriad" in config and config["use_myriad"] == True:
            # Use MyRiad to perform inference
            print('Using NCS2 MyRiad Device.')
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)
        else:
            # Use CPU
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

        self.img_size = 224
        self.class_label_map = {0: 'female', 1: 'male'}
        self.confidence = 0.5
        if ("confidence" in config):
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('GenderPredictorOpenVINO::perform_classification')
        blob = cv2.dnn.blobFromImage(image, size=(224, 224), swapRB=True)
        self.model.setInput(blob)
        gender_prediction = self.model.forward()
        print('GENDER PREDICTION [OV] => ' + str(gender_prediction))
        gender = np.argmax(gender_prediction[0])
        conf = gender_prediction[0][gender]
        return self.class_label_map[gender]
        '''
        if gender == 1 and conf > 0.2:
          return 'male'
        if gender == 0 and conf > 0.90:
          return 'female'
        return 'unknown'
        #if gender_prediction[0][gender] < self.confidence:
        #  return 'unknown'
        #return self.class_label_map[gender]
        '''
