import cv2
import numpy as np
import os
from keras.preprocessing.image import img_to_array
from keras.models import load_model, model_from_json
import tensorflow as tf

from object_classifier import ObjectClassifier


class GenderPredictorTensorRT(ObjectClassifier):
    def __init__(self):
        self.model = None

    def get_frozen_graph(self, graph_file):
        """Read Frozen Graph file from disk."""
        with tf.gfile.FastGFile(graph_file, "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
        return graph_def

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('GenderPredictorTensorRT::init_model')

        # load frozen graph and create model
        MODEL_FROZEN_GRAPH = '../gender_mobilenetv2_ssd/tf_model.pb'

        print("Loaded model from disk")
        self.graph = tf.Graph()
        trt_graph = self.get_frozen_graph(MODEL_FROZEN_GRAPH)

        # Create session and load graph
        tf_config = tf.ConfigProto(
            device_count={'GPU': 0}
        )
        # tf_config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=tf_config, graph=self.graph)
        with self.graph.as_default():
            tf.import_graph_def(trt_graph, name='')

        self.img_size = 224
        self.class_label_map = {0: 'female', 1: 'male'}
        self.confidence = 0.5
        if ("confidence" in config):
            self.confidence = config["confidence"]

        # input and output tensor names.
        self.input_tensor_name = "input_1:0"
        output_tensor_name = "dense_2/Softmax:0"
        self.output_tensor = self.sess.graph.get_tensor_by_name(output_tensor_name)

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    def preprocess_input(self, x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('GenderPredictor::perform_classification')
        image = cv2.resize(image, (self.img_size, self.img_size))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        rgb_face = self.preprocess_input(image, False)
        rgb_face = np.expand_dims(rgb_face, 0)
        gender_prediction = self.sess.run(self.output_tensor, feed_dict={self.input_tensor_name: rgb_face})
        print('GENDER PREDICTION (2) => ' + str(gender_prediction))
        gender = np.argmax(gender_prediction[0])
        conf = gender_prediction[0][gender]

        return self.class_label_map[gender]

        '''
        if gender == 1 and conf > 0.2:
          return 'male'
        if gender == 0 and conf > 0.90:
          return 'female'
        return 'unknown'
        '''
        '''    
        if gender_prediction[0][gender] < self.confidence:
          return 'unknown'
        return self.class_label_map[gender]
        '''
