import os
import sys
import cv2
import time
import ctypes
import numpy as np
import pycuda.autoinit
import pycuda.driver as cuda

import uff
import tensorrt as trt
import graphsurgeon as gs
from rpi_utils.uploader import Uploader

from jetson_utils import gender_tensorrt_converter as model

from object_classifier import ObjectClassifier

import platform

if platform.processor() == 'x86_64':
    ctypes.CDLL("../jetson_utils/libflattenconcat_x64.so")
else:
    ctypes.CDLL("../jetson_utils/libflattenconcat.so")


class GenderPredictorTensorRT2(ObjectClassifier):
    def __init__(self):
        pass

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('GenderPredictorTensorRT2::init_model')

        TRT_LOGGER = trt.Logger(trt.Logger.INFO)
        trt.init_libnvinfer_plugins(TRT_LOGGER, '')
        runtime = trt.Runtime(TRT_LOGGER)

        # compile model into TensorRT
        if not os.path.isfile(model.TRTbin):
            print('Converting Tensorflow model to TensorRT model...')
            dynamic_graph = model.add_plugin(gs.DynamicGraph(model.path))
            uff_model = uff.from_tensorflow(dynamic_graph.as_graph_def(), model.output_name, output_filename='tmp.uff')

            with trt.Builder(TRT_LOGGER) as builder, builder.create_network() as network, trt.UffParser() as parser:
                builder.max_workspace_size = 1 << 28
                builder.max_batch_size = 1
                builder.fp16_mode = True

                parser.register_input('input_1', model.dims)
                parser.register_output('dense_2/Softmax')
                parser.parse('tmp.uff', network)
                engine = builder.build_cuda_engine(network)

                buf = engine.serialize()
                with open(model.TRTbin, 'wb') as f:
                    f.write(buf)

        # create engine
        print('Loading TensorRT model file...')
        with open(model.TRTbin, 'rb') as f:
            buf = f.read()
            engine = runtime.deserialize_cuda_engine(buf)

        # create buffer
        self.host_inputs = []
        self.cuda_inputs = []
        self.host_outputs = []
        self.cuda_outputs = []
        self.bindings = []
        self.stream = cuda.Stream()

        print('ENGINE.SIZE = ' + str(len(engine)))
        for binding in engine:
            print('BINDING: ' + str(binding))
            size = trt.volume(engine.get_binding_shape(binding)) * engine.max_batch_size
            host_mem = cuda.pagelocked_empty(size, np.float32)
            cuda_mem = cuda.mem_alloc(host_mem.nbytes)

            self.bindings.append(int(cuda_mem))
            if engine.binding_is_input(binding):
                print(' -> is_input')
                self.host_inputs.append(host_mem)
                self.cuda_inputs.append(cuda_mem)
            else:
                print(' -> is_output')
                self.host_outputs.append(host_mem)
                self.cuda_outputs.append(cuda_mem)
        self.context = engine.create_execution_context()

        self.img_size = 224
        self.class_label_map = {0: 'female', 1: 'male'}
        self.confidence = 0.5
        if ("confidence" in config):
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # inference
        W = image.shape[1]
        H = image.shape[0]

        # Resize and normalize image for network input
        frame = cv2.resize(image, (224, 224))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # frame = frame.astype('float32') / 255.0
        frame = (2.0 / 255.0) * frame - 1.0
        frame = frame.transpose((2, 0, 1))
        np.copyto(self.host_inputs[0], frame.ravel())

        # start_time = time.time()
        cuda.memcpy_htod_async(self.cuda_inputs[0], self.host_inputs[0], self.stream)
        self.context.execute_async(bindings=self.bindings, stream_handle=self.stream.handle)
        # cuda.memcpy_dtoh_async(self.host_outputs[1], self.cuda_outputs[1], self.stream)
        cuda.memcpy_dtoh_async(self.host_outputs[0], self.cuda_outputs[0], self.stream)
        self.stream.synchronize()
        # print("execute times "+str(time.time()-start_time))

        output = self.host_outputs[0]
        gender_prediction = [output[0], output[1]]

        print('GENDER PREDICTION (3) => ' + str(gender_prediction))
        gender = np.argmax(gender_prediction)
        conf = gender_prediction[gender]

        # Custom penalize male
        if gender == 1 and conf > 0.95:
            return 'male'
        elif gender == 0:
            return 'female'
        else:
            return 'unknown'
