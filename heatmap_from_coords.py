import sys, os
import numpy as np
import cv2
import argparse
import matplotlib
from PIL import Image
import matplotlib.pyplot as plt
from matplotlib.cm import colors
import seaborn as sns
import pandas as pd
import re

matplotlib.use("Agg")

os.chdir("videos")

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", type=str, default='',
                help="Video/Heatmap data File without extension")
ap.add_argument("-o", "--output", type=str, default='',
                help="Heatmap image output file path")
ap.add_argument("-bf", "--begin_frame", type=str, default='',
                help="Frame number (begin)")
ap.add_argument("-ef", "--end_frame", type=str, default='',
                help="Frame number (end)")
args = vars(ap.parse_args())


def video_to_frames(video):
    image = None
    # extract frames from a video and save to directory as 'x.png' where 
    # x is the frame index
    vidcap = cv2.VideoCapture(video)
    count = 0
    success, image = vidcap.read()
    cv2.destroyAllWindows()
    vidcap.release()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    print('Image size = ' + str(image.shape))
    return image


def chunks(list, n):
    new_list = []
    # Yield successive n-sized chunks from 1
    for i in range(0, len(list), n):
        new_list.append(list[i:i + n])
    return new_list


heatmap_file = args["file"].replace('.avi', '') + '.heatmap.log'
video_file = args["file"].replace('.avi', '') + '.avi'

output_path = args["file"].replace('.avi', '') + '.heatmap.jpg'
if args["output"] != '':
    output_path = args["output"]

print('Read heatmap.log file.')
# coordinates_file = open(os.path.join(BASE_FOLDER, BASE_FILE))
coordinates_list = None
with open(heatmap_file, 'r', encoding='utf-8') as f:
    coordinates_list = f.read()

print('Capture image from video')
# base image from video frame
map_img = video_to_frames(video_file)

# create pandas dataframe out of coordinates from txt file
print('Create pandas dataframe.')
numbers = re.findall(r'[+-]?\d+(?:\.\d+)?', coordinates_list)
numbers = [int(round(float(n))) for n in numbers]
# print(numbers)
numbers_list = chunks(numbers, 3)
# print(numbers_list)
numbers_frame = pd.DataFrame.from_records(numbers_list, index=['Frame'], columns=['Frame', 'X', 'Y'])
# print(numbers_frame)
print("Dataframe range: " + str(numbers_frame.index[0]) + "-" + str(numbers_frame.index[-1]))

fig = plt.figure(frameon=False)
print(map_img.shape)
plt.axis('off')
plt.xlim(0, map_img.shape[1])
plt.ylim(0, map_img.shape[0])
ax = fig.add_subplot(111, frameon=False, xticks=[], yticks=[])
fig.patch.set_visible(False)
ax.axis('off')
plt.imshow(map_img)

# accept input for range of frames to gather data from. Default to all of the frames
starting_frame = numbers_frame.index[0]
ending_frame = numbers_frame.index[-1]
if args['begin_frame'] != '':
    starting_frame = int(args['begin_frame'])
if args['end_frame'] != '':
    ending_frame = int(args['end_frame'])

print('Running from starting_frame ' + str(starting_frame) + ', ending_frame ' + str(ending_frame))

# draw kernel density estimate
x = [d[1] for d in numbers_list if d[0] >= starting_frame and d[0] <= ending_frame]
y = [d[2] for d in numbers_list if d[0] >= starting_frame and d[0] <= ending_frame]
# print(x)
# print(y)
ax = sns.kdeplot(x, y, cmap="coolwarm", shade=True, shade_lowest=False, alpha=0.6)

# Hide all axes & bounding boxes
ax.axes.get_xaxis().set_visible(False)
ax.axes.get_yaxis().set_visible(False)
ax.set_frame_on(False)
ax.axis('off')

plt.savefig(output_path, bbox_inches='tight')

# plt.show()
# cv2.waitKey()
