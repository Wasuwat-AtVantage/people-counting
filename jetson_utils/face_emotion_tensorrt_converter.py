import graphsurgeon as gs
import tensorflow as tf
import platform

path = '../face_emotion_cnn/tf_model.pb'

TRTbin = '../face_emotion_cnn/TRT_age.bin'
if platform.processor() == 'x86_64':
    TRTbin = '../face_emotion_cnn/TRT_x64_age.bin'

output_name = ['dense_2/Softmax']
dims = [1,48,48]

def add_plugin(graph):
    all_assert_nodes = graph.find_nodes_by_op("Assert")
    graph.remove(all_assert_nodes, remove_exclusive_dependencies=True)

    all_identity_nodes = graph.find_nodes_by_op("Identity")
    graph.forward_inputs(all_identity_nodes)

    Input = gs.create_plugin_node(
        name="conv2d_1_input",
        op="Placeholder",
        shape=[1, 1, 48, 48],
        dtype=tf.float32
    )

    namespace_plugin_map = {
        "Preprocessor": Input,
        "ToFloat": Input,
        "conv2d_1_input": Input,
    }

    graph.collapse_namespaces(namespace_plugin_map)
    return graph
