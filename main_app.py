import sys, os
import numpy as np
import cv2
import imutils
# import time
import pyis
import argparse
import csv
import datetime
import multiprocessing
import glob
import json
import traceback
import dlib

import atexit

from system_setting import SystemSetting

from imutils.video import FPS
from collections import defaultdict
from functools import partial

from custom_object_tracker import CustomObjectTracker

from roi_area_config import ROIAreaConfig
from time import time
from rpi_utils.uploader import Uploader

from staff_predictor import StaffPredictor

# from staff_predictor_with_cap import StaffPredictorWithCap as StaffPredictor


if getattr(sys, 'frozen', False):
    # If the application is run as a bundle, the pyInstaller bootloader
    # extends the sys module by a flag frozen=True and sets the app
    # path into variable _MEIPASS'.
    application_path = sys._MEIPASS
else:
    application_path = os.path.dirname(os.path.abspath(__file__))

# f = Figlet(font='doom')
# print(f.renderText('People Counter'))
print('People Counter')

print("[INFO] starting program...")

ap = argparse.ArgumentParser()

# Constant defining if we will use customized classifiers or not (Eg, Age, Gender, ...)
USE_CUSTOM_CLASSIFIER = True

# Constant control whether we will run custom classifier only once or every detected frame
RUN_CUSTOM_CLASSIFIER_EVERY = 20

# Constant defining number of frame skipping between each processing frame
RUNNING_FRAME_SKIP = 3

# Constant defining how frequent we should run detection using AI
DETECTION_FRAME_SKIP = 1

# Constant defining number of frame skipping between Heatmap and Count Logging
HEATMAP_LOGGING_FRAME_SKIP = 20

# Constant defining number of frame for force restart device conditionally
# (This is to prevent unvoidable edge cases, like library/OS memory leakage)
FORCE_REBOOT_UNCONDITIONALLY_EVERY = 30000

# Constant defining number of frame for split video recording files
SPLIT_VIDEO_RECORDING_EVERY = 4000

# These 2 parameters were changed to be supplied in configuration,
# which can hold multiple ROI areas.
# ap.add_argument("-ti", "--time_interest", type=int, default=15,
#    help="Customer interest measurement. Default is 15 seconds.")
# ap.add_argument("-te", "--time_engaged", type=int, default=90,
#    help="Customer engaged measurement. Default is 90 seconds.")

ap.add_argument("-a", "--detection_type", type=str, default='human',
                help="Specified type of detection mode (human | face | vehicle).")
ap.add_argument("-b", "--deployment_type", type=str, default='server',
                help="Specified type of deployment (server | edge).")
ap.add_argument("-f", "--file", type=str, default='',
                help="[Single video file | Camera connection string: picam, csi]")
ap.add_argument("-r", "--roi", type=str,
                help="Path to ROI configuration json file.")
ap.add_argument("-o", "--output", type=str,
                help="Path to optional output video file.")
ap.add_argument("-c", "--confidence", type=float, default=0.1,
                help="Minimum probability to filter weak detections.")
ap.add_argument("-s", "--skip-frames", type=int, default=30,
                help="Number of skip frames between detections. Default is 30.")
ap.add_argument("-d", "--detector", type=str, default='ssdm',
                help="Model to be used for object detection (ssdm | ssdmv2 | yolo | yolo_cuda)")
# ap.add_argument("-hp", "--hparameters", type=str, default=None,
#    help="If supplied, the inferencing will be performed against every combination of hyperparameters in supplied configuration file.")
ap.add_argument("-v", "--verbose", type=str, default='0',
                help="Save verbose log (Detected object and debug video) (0 = None, 1 = Debug Video, 2 = Classifier Outputs)")
ap.add_argument("-l", "--headless", type=str, default='monitor',
                help="Specify headless mode (monitor||headless)")
ap.add_argument("-n", "--network", type=str, default='wifi',
                help="Specify network mode (wifi||3g)")
ap.add_argument("-e", "--host", type=str, default='https://edge.bizcuit.co.th',
                help="Specify host to connect the device to")

args = vars(ap.parse_args())

# Load system setting
system_setting = SystemSetting()
if os.path.exists('system_setting.json'):
    system_setting.read_from_config_file('system_setting.json')

os.chdir("videos")

# We no not support custom classifier for Edge AI for a while
if args['deployment_type'] == 'edge':
    USE_CUSTOM_CLASSIFIER = True
    args['use_myriad'] = True  # Use NCS2 for inferencing
    RUN_CUSTOM_CLASSIFIER_EVERY = 20
elif args['deployment_type'] == 'edge_noop':
    USE_CUSTOM_CLASSIFIER = True
    args['use_myriad'] = False  # Not Using NCS2 for inferencing
    RUN_CUSTOM_CLASSIFIER_EVERY = 20
    if args["detector"] == "cpu":  # For simulation
        RUN_CUSTOM_CLASSIFIER_EVERY = 1

# Check network connect if we are using 3G modem to connect to internet
if args['deployment_type'] == '3g':
    print('TODO: Trying to connect to 3G')

# Store object detector here.
detector = None

# Store custom classifier object here. They will be used to classify object when they are detected.
custom_classifiers = []

# Store custom logics here
custom_logics = None

# Store custom classifier class finalizer here.
class_finalizer = None

# Set node ID from system setting file
args['node_id'] = system_setting.node_id
print('Device NODE ID = ' + str(args['node_id']))

args['USE_CUSTOM_CLASSIFIER'] = USE_CUSTOM_CLASSIFIER

if args['detection_type'] == 'human':
    from people_counting import PeopleCountingLogics

    custom_logics = PeopleCountingLogics(args)
    detector = custom_logics.create_detector()
    custom_classifiers = custom_logics.create_custom_classifiers()
    class_finalizer = custom_logics.create_class_finalizer()
elif args['detection_type'] == 'face':
    from face_counting import FaceCountingLogics

    custom_logics = FaceCountingLogics(args)
    detector = custom_logics.create_detector()
    custom_classifiers = custom_logics.create_custom_classifiers()
    class_finalizer = custom_logics.create_class_finalizer()
elif args['detection_type'] == 'vehicle':
    # Case of vehicle detection, we use custom model
    detector = None
    if USE_CUSTOM_CLASSIFIER:
        pass
    class_finalizer = None

print("[INFO] loading model...")
detector.init_model(args)
for custom_classifier in custom_classifiers:
    custom_classifier.init_model(args)
print("[INFO] Detector model was initialized...")

# Store video source here
video_source_adaptor = None
if args["file"] == 'picam':
    # Case of running from RPI Camera
    from rpi_utils.pi_camera_adaptor import PiCameraAdaptor

    video_source_adaptor = PiCameraAdaptor(system_setting, args)
    RUNNING_FRAME_SKIP = 1  # Case of live camera, we want to run every frame
    FORCE_REBOOT_UNCONDITIONALLY_EVERY = 30000  # For Gen 1 / Gen 2, we use 30000
elif args["file"] == 'csi':
    # Case of running from CSI Camera
    from jetson_utils.jetson_csi_camera_adaptor import JetsonCSICameraAdaptor

    video_source_adaptor = JetsonCSICameraAdaptor(system_setting, args)
    RUNNING_FRAME_SKIP = 1  # Case of live camera, we want to run every frame
    FORCE_REBOOT_UNCONDITIONALLY_EVERY = 60000  # For Gen3, we use 60000
elif args["file"] == 'webcam':
    # Case of running from Web Camera
    from standalone_utils.webcam_camera_adaptor import WebcamCameraAdaptor

    video_source_adaptor = WebcamCameraAdaptor(system_setting, args)
    RUNNING_FRAME_SKIP = 1  # Case of live camera, we want to run every frame
    FORCE_REBOOT_UNCONDITIONALLY_EVERY = sys.maxsize  # For not forcing restart att all
else:
    # Case of running against single video / image file.
    if args["file"].endswith('.jpg') or args["file"].endswith('.png'):
        from standalone_utils.image_file_adaptor import ImageFileAdaptor

        video_source_adaptor = ImageFileAdaptor(system_setting, args)
        FORCE_REBOOT_UNCONDITIONALLY_EVERY = sys.maxsize  # For not forcing restart at all
    else:
        from standalone_utils.video_file_adaptor import VideoFileAdaptor

        video_source_adaptor = VideoFileAdaptor(system_setting, args)
        FORCE_REBOOT_UNCONDITIONALLY_EVERY = sys.maxsize  # For not forcing restart at all

VERBOSE_LOG = (args["verbose"] == '1' or args["verbose"] == '2' or args["verbose"] == '3')
CREATE_TRAINING_DATA = (args["verbose"] == '2')
RECORD_VIDEO = (args["verbose"] == '3')

IS_WINDOWS_OS = (os.name == 'nt')

HEADLESS_MODE = (args["headless"] == 'headless')
if HEADLESS_MODE:
    print('Running in headless mode.')

# Path of every log and outputs must be get from video source adaptor
output_path_configs = video_source_adaptor.get_all_log_paths()

# Path for saving video recording
DETECTION_PLAYBACK_DIR = output_path_configs['detection_playback_dir']
DETECTED_OBJECTS_DIR = output_path_configs['detected_objects_dir']
Uploader.DETECTION_PLAYBACK_DIR = DETECTION_PLAYBACK_DIR
if VERBOSE_LOG:
    if not os.path.exists(DETECTION_PLAYBACK_DIR):
        os.makedirs(DETECTION_PLAYBACK_DIR)
    if not os.path.exists(DETECTED_OBJECTS_DIR):
        os.makedirs(DETECTED_OBJECTS_DIR)
RECORD_DETECTED_OBJECT_EVERY = 1

# ROIs configuration is array of below list:
# [TOP, LEFT, BOTTOM, RIGHT, X-DIRECTION, Y-DIRECTION, AGE_TRACKING, COUNT_THRESHOLD, INTEREST_THREHOLD, ENGAGEMENT_THRESHOLD]
ROI_AREA_CONFIGS = []
if 'roi' in args and args['roi'] is not None:
    rois_configuration_file = args['roi']
    print('Reading ROIs JSON configuration from: ' + rois_configuration_file)
    custom_logics.set_roi_filename(rois_configuration_file)
    with open(rois_configuration_file) as f:
        roi_datum = json.load(f)
        custom_logics.set_roi_json(roi_datum)
        for roi_data in roi_datum:
            print(roi_data)

            # Check for backward compatible ROI Data Format
            version = roi_data[0]
            if version != '_v2':
                # For V1 Data Format (2 vertices for rectangle ROIs)
                top = roi_data[1]
                left = roi_data[2]
                bottom = roi_data[3]
                right = roi_data[4]
                poly = [[top, left], [top, right], [bottom, right], [bottom, left]]
                constraint_rois = []
                if len(roi_data) > 12:
                    rois = roi_data[12].split(',')
                    for i in range(len(rois)):
                        try:
                            roi_id = int(rois[i].strip())
                            constraint_rois.append(roi_id)
                        except:
                            pass
                roi_config = ROIAreaConfig(type_=roi_data[0], poly=poly, direction_x=roi_data[5],
                                           direction_y=roi_data[6],
                                           tracking_age=roi_data[7], count_threshold=roi_data[8],
                                           interesting_threshold=roi_data[9], engagement_threshold=roi_data[10],
                                           unoccupied_threshold=roi_data[11],
                                           constraint_rois=constraint_rois)
                ROI_AREA_CONFIGS.append(roi_config)
            else:
                # For V2 Data Format (multi-vertices polygon)
                constraint_rois = []
                if len(roi_data) > 10:
                    rois = roi_data[10].split(',')
                    for i in range(len(rois)):
                        try:
                            roi_id = int(rois[i].strip())
                            constraint_rois.append(roi_id)
                        except:
                            pass
                roi_config = ROIAreaConfig(type_=roi_data[1], poly=roi_data[2], direction_x=roi_data[3],
                                           direction_y=roi_data[4],
                                           tracking_age=roi_data[5], count_threshold=roi_data[6],
                                           interesting_threshold=roi_data[7], engagement_threshold=roi_data[8],
                                           unoccupied_threshold=roi_data[9],
                                           constraint_rois=constraint_rois)
                ROI_AREA_CONFIGS.append(roi_config)

    print(ROI_AREA_CONFIGS)
    custom_logics.set_roi_areas(ROI_AREA_CONFIGS)

# If HParam search space is specified, we run inference for each combination of HParam and save result.
'''SCORE_THRESHOLD_MAP = {
  'person': 0.8,
}
hparam_search_cfg = None
if 'hparameters' in args and args["hparameters"] is not None:
  with open(args["hparameters"]) as hparam_search_cfg_file:
    hparam_search_cfg = json.load(hparam_search_cfg_file)

  # Construct search space for HParam
  search_array = np.arange(hparam_search_cfg['lower_bound'], 
    hparam_search_cfg['upper_bound'], 
    hparam_search_cfg['step_size'], dtype=float)
  print('Threshold Search Space = ' + str(search_array))
  hparam_search_space = []
  hparam_set = {}
  import itertools
  hparam_search_space = list(itertools.product(search_array, repeat=len(SCORE_THRESHOLD_MAP)))
  hparam_search_space_list = []
  key_list = list(SCORE_THRESHOLD_MAP.keys())
  for cfg in hparam_search_space:
    cfg_set = {}
    for i in range(len(cfg)):
      cfg_set[key_list[i]] = cfg[i]
    hparam_search_space_list.append(cfg_set)
  hparam_search_space = hparam_search_space_list
  print('Hyper-Parameter search space size = ' + str(len(hparam_search_space)))
else:
  hparam_search_space = [SCORE_THRESHOLD_MAP]'''

# [TBD]: Set as option
RUN_IN_FULLSCREEN = False

# create 'frame' window before while-loop in order to create mouse callback
if not HEADLESS_MODE:
    cv2.namedWindow('Window', cv2.WINDOW_NORMAL)
    # These two lines will force the window to be on top with focus.
    if RUN_IN_FULLSCREEN:
        cv2.setWindowProperty('Window', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    else:
        cv2.setWindowProperty('Window', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL)

print("[INFO] starting video stream...")

# start_time = time.clock()
# print("[INFO] start time: {:.2f}".format(start_time))

# initialize the video writer (we'll instantiate later if need be)
writer = None

# initialize the frame dimensions (we'll set them as soon as we read
# the first frame from the video)
W = None
H = None

# start the frames per second throughput estimator
fps = FPS().start()

# initialize the total number of frames processed thus far, along
# with the total number of objects that have moved either up or down
totalFrames = 0

# Frame ID for used to save detected object images periodicall
frame_id = 0

# Parsing Date/Time string from file path.
# We will skip the video if we cannot parse file path into designated format correctly.
# FORMAT:
#
# Case 1:
# <File Dir>/YYYYMMDD_HHMMSS.mp4
#
# Case 2:
# <File Dir>/YYYYMMDD/HHMMSS.mp4
#
date_string, time_string, base_minute, meta_datum = video_source_adaptor.get_date_and_time()
print(str([date_string, time_string, base_minute, meta_datum]))
if time_string is None:
    print('[WARNING] Cannot parse date/time from file path. Program is exited.')
    # exit(1)
    time_string = '00:00:00'
    date_string = ' '

'''for score_threshold_map in hparam_search_space:
  print('Running inference for HParam Config: ' + str(score_threshold_map))

  # Output filename is construct from HParam configuration:
  hparam_str = ''
  if hparam_search_cfg is not None:
    hparam_str = '_' + ('_').join([str(score_threshold_map[key]) for key in score_threshold_map])

  # Set HParam values to detector
  print('Set Confidence Level to: ' + str(score_threshold_map['person']))
  detector.setParameter('confidence', score_threshold_map['person'])'''

# If running verbose log, we will save processed video in detection playback directory
result_video_output = None


def reinit_video_recording():
    global result_video_output

    if result_video_output is not None:
        result_video_output.release()

    record_datetime = datetime.datetime.today().strftime('%Y%m%d_%H%M%S')
    DETECTION_PLAYBACK_PATH = os.path.join(DETECTION_PLAYBACK_DIR, record_datetime + '.mp4')
    print('DETECTION_PLAYBACK_PATH = ' + DETECTION_PLAYBACK_PATH)
    if IS_WINDOWS_OS:
        result_video_output = cv2.VideoWriter(DETECTION_PLAYBACK_PATH,
                                              cv2.VideoWriter_fourcc('X', 'V', 'I', 'D'), 20.0,
                                              (704, 576))
    else:
        result_video_output = cv2.VideoWriter(DETECTION_PLAYBACK_PATH,
                                              cv2.VideoWriter_fourcc('X', 'V', 'I', 'D'), 20.0,
                                              (704, 576))


if (VERBOSE_LOG and RECORD_VIDEO):
    reinit_video_recording()


    def system_exit():
        print('Release Video Output')
        result_video_output.release()


    atexit.register(system_exit)

# If there already is result file, then skip
result_file_path = output_path_configs['result_file_path']
if os.path.isfile(result_file_path):
    print('[INFO] Found existing result file, skip.')
    # exit(0) # Let's not exit the program

# Heatmap file path
heatmap_file_path = output_path_configs['heatmap_file_path']

# Count log file path
count_log_file_path = output_path_configs['count_log_file_path']

# Initialize the video stream
video_source_adaptor.initialize(args)

# Raw CSV log file to be accumulated later
raw_csv_log_file_path = output_path_configs['raw_csv_log_file_path']
custom_logics.init_log(raw_csv_log_file_path)

# Get frame rate to calculate log frequency in frame domain
fps_value = video_source_adaptor.get_fps()
if fps_value == 0:
    fps_value = 1
print('Video has FPS = ' + str(fps_value))
log_every_fps = int(fps_value * 60 * system_setting.log_frequency_minute)
realtime_update_every_fps = int(fps_value * 60 * system_setting.realtime_update_frequency_minute)
print('Log Raw CSV every ' + str(system_setting.log_frequency_minute) + ' minute(s) = ' + str(
    log_every_fps) + ' frame(s)')
print('Realtime Update every ' + str(system_setting.realtime_update_frequency_minute) + ' minute(s) = ' + str(
    realtime_update_every_fps) + ' frame(s)')

# Initialize new Object Tracker instance for every video 
object_tracker = None
object_tracker = CustomObjectTracker()
object_tracker.set_hotspot_areas(ROI_AREA_CONFIGS)
object_tracker.set_class_finalizer(class_finalizer)
custom_logics.set_object_tracker(object_tracker)

# DLib Correlation object tracker to reduce load on DNN
obj_trackers = []
obj_trackers_classes = []

# Frame number of current file -> Used to map back into time domain
frame_seq = 0

while True:
    frame = None
    try:
        frame = video_source_adaptor.get_next_frame()
    except:
        pass

    if frame is None:
        Uploader.running = False  # This will stop uploading thread.
        break

    frame_id = frame_id + 1
    frame_seq = frame_seq + 1

    if frame_seq % RUNNING_FRAME_SKIP != 0:
        continue

    # if frame_seq < 4000:
    #  continue

    seconds_passed = frame_seq / fps_value
    minutes_passed = seconds_passed / 60
    timestamp = time()

    # frame = imutils.resize(frame, width=600)
    # rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    # Detect objects and track them with object tracker
    start = time()
    boxes = []
    if frame_seq % DETECTION_FRAME_SKIP == 0:
        boxes = detector.perform_detection(frame)

        # Reset correlation tracker
        obj_trackers = []
        obj_trackers_classes = []

        for box in boxes:
            (startX, startY, endX, endY, class_id) = box
            obj_tracker = dlib.correlation_tracker()
            rect = dlib.rectangle(startX, startY, endX, endY)
            obj_tracker.start_track(frame, rect)
            obj_trackers.append(obj_tracker)
            obj_trackers_classes.append(class_id)
    else:
        for obj_tracker, class_id in zip(obj_trackers, obj_trackers_classes):
            # update the tracker and grab the upedated position
            obj_tracker.update(frame)
            pos = obj_tracker.get_position()
            # unpack the position object
            startX = int(pos.left())
            startY = int(pos.top())
            endX = int(pos.right())
            endY = int(pos.bottom())
            # add the bounding box coordinates to the rectangles list
            boxes.append((startX, startY, endX, endY, class_id))

    end = time()
    # print('>>> Object Detection: ' + str(end - start))

    start = time()
    for box in boxes:
        (startX, startY, endX, endY, class_id) = box
        object_tracker.track_object(frame, startX, startY, endX, endY, totalFrames, [class_id], timestamp)

        # Also write position of each detected object to heatmap log file if need
        if totalFrames % HEATMAP_LOGGING_FRAME_SKIP == 0:
            cx = (startX + endX) / 2
            cy = (startY + endY) / 2
            with open(heatmap_file_path, 'a+', encoding='utf8') as outfile:
                outfile.write('Frame:' + str(int(round(minutes_passed))) + ',X:' + str(cx) + ',Y:' + str(cy) + '\n')

    end = time()
    # print('>>> Object Tracking: ' + str(end - start))

    # Draw ROI areas, skip if creating training data
    if not CREATE_TRAINING_DATA and not RECORD_VIDEO:
        custom_logics.draw_roi_areas(frame, object_tracker, ROI_AREA_CONFIGS)

        # Also draw global frame level object tracked here too
        tracked_objects = object_tracker.get_tracked_objects()
        for tracked_object in tracked_objects:
            # Ensure that we crop detected object inside frame boundary
            (startX, startY, endX, endY) = tracked_object['last_rect']
            startX = max(startX, 0)
            startY = max(startY, 0)
            endX = min(endX, frame.shape[1] - 1)
            endY = min(endY, frame.shape[0] - 1)

            if (tracked_object['count'] % RUN_CUSTOM_CLASSIFIER_EVERY) == 0:

                detected_object_image = frame[startY:endY, startX:endX, :]

                # Skip case of bounding box partially exceed frame area.
                # if len(detected_object_image) == 0:
                #  continue
                # print((startX, startY, endX, endY))

                # Perform additional custom classifications and add result classes to tracked objects
                custom_classifier_classes = []
                for idx, custom_classifier in enumerate(custom_classifiers):
                    if isinstance(custom_classifier, StaffPredictor):
                        try:
                            start = time()
                            custom_classifier_class = custom_classifier.perform_classification(detected_object_image)
                            end = time()
                            # print('>>> Classifier: ' + str(idx) + ': ' + str(end - start))
                            custom_classifier_classes.append(custom_classifier_class)
                        except Exception as err:
                            print('Error!')
                            traceback.print_tb(err.__traceback__)
                        break
                object_tracker.add_class_to_tracked_object(tracked_object, custom_classifier_classes)

                custom_logics.draw_tracked_object_frame(frame, tracked_object,
                                                        (startX, startY, endX, endY), custom_classifier_classes,
                                                        timestamp)
            else:
                custom_logics.draw_tracked_object_frame(frame, tracked_object,
                                                        (startX, startY, endX, endY), tracked_object['last_classes'],
                                                        timestamp)

    # Get all tracked object from object tracker
    tracked_object_list_map = object_tracker.get_object_list()

    # Copy image to another layer for Transparent display
    transparent_frame = frame.copy()

    # Iterate over all tracked objects in each ROI
    for hotspot_id in tracked_object_list_map:
        # print('Print hotspot: ' + str(hotspot_id))
        tracked_objects = tracked_object_list_map[hotspot_id]
        for tracked_object in tracked_objects:

            # Detect effective age of tracked object, we use this to see if we should run custom detect for current frame.
            hotspot_area = ROI_AREA_CONFIGS[hotspot_id]
            effective_age = tracked_object['count'] - hotspot_area.count_threshold

            # Ensure that we crop detected object inside frame boundary
            (startX, startY, endX, endY) = tracked_object['last_rect']
            startX = max(startX, 0)
            startY = max(startY, 0)
            endX = min(endX, frame.shape[1] - 1)
            endY = min(endY, frame.shape[0] - 1)

            if (effective_age % RUN_CUSTOM_CLASSIFIER_EVERY) == 0:

                detected_object_image = frame[startY:endY, startX:endX, :]
                if VERBOSE_LOG and CREATE_TRAINING_DATA and (frame_id % RECORD_DETECTED_OBJECT_EVERY == 0):
                    ts = datetime.datetime.now().timestamp()
                    cv2.imwrite(os.path.join(DETECTED_OBJECTS_DIR, 'object_' + str(frame_id) + str(ts) + '.jpg'),
                                detected_object_image)

                    # Skip case of bounding box partially exceed frame area.
                # if len(detected_object_image) == 0:
                #  continue
                # print((startX, startY, endX, endY))

                # Perform additional custom classifications and add result classes to tracked objects
                custom_classifier_classes = []
                for idx, custom_classifier in enumerate(custom_classifiers):
                    try:
                        start = time()
                        custom_classifier_class = custom_classifier.perform_classification(detected_object_image)
                        end = time()
                        # print('>>> Classifier: ' + str(idx) + ': ' + str(end - start))
                        custom_classifier_classes.append(custom_classifier_class)
                    except Exception as err:
                        print('Error!')
                        traceback.print_tb(err.__traceback__)
                        continue
                object_tracker.add_class_to_tracked_object(tracked_object, custom_classifier_classes)

                # Draw track objects (bounding boxes and information), skip if creating training data
                if not CREATE_TRAINING_DATA and not RECORD_VIDEO:
                    custom_logics.draw_tracked_object(frame, hotspot_area, tracked_object,
                                                      (startX, startY, endX, endY), custom_classifier_classes,
                                                      timestamp,
                                                      transparent_frame
                                                      )
            else:
                # Draw track objects (bounding boxes and information), skip if creating training data
                if not CREATE_TRAINING_DATA and not RECORD_VIDEO:
                    custom_logics.draw_tracked_object(frame, hotspot_area, tracked_object,
                                                      (startX, startY, endX, endY), tracked_object['last_classes'],
                                                      timestamp,
                                                      transparent_frame
                                                      )

        # Also write current object count for each ROI to count log file if need
        if totalFrames % HEATMAP_LOGGING_FRAME_SKIP == 0:
            count = len(tracked_objects)
            with open(count_log_file_path, 'a+', encoding='utf8') as outfile:
                outfile.write('Frame:' + str(int(round(minutes_passed))) + ',ROI:' + str(hotspot_id) + ',Count:' + str(
                    count) + '\n')

    # Add transparent frame to original frame
    frame = cv2.addWeighted(frame, 0.8, transparent_frame, 0.2, 0)

    start = time()
    # Update object tracker state by calling step function
    object_tracker.step(frame_seq, base_minute + minutes_passed, timestamp)
    end = time()
    # print('>>> Step: ' + str(end - start))

    # Draw additional information (if needed), skip if creating training data
    if not CREATE_TRAINING_DATA and not RECORD_VIDEO:
        custom_logics.draw_additional_infos(frame, object_tracker,
                                            tracked_object_list_map, ROI_AREA_CONFIGS, timestamp)

    # If recording detection playback, we add frame to video writer here.
    # Note that H263 has limitted dimension, so we need to resize it to confrom the H263 specification
    if (VERBOSE_LOG and RECORD_VIDEO):
        record_frame = cv2.resize(frame, (704, 576))
        result_video_output.write(record_frame)

        if totalFrames % SPLIT_VIDEO_RECORDING_EVERY == 0:
            print('Split video recording file')
            reinit_video_recording()

    # Show current frame in non-headless mode
    if not HEADLESS_MODE:
        '''
        scale_percent = 30 # percent of original size
        width = int(frame.shape[1] * scale_percent / 100)
        height = int(frame.shape[0] * scale_percent / 100)
        dim = (width, height)
        # resize image
        resized = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA) 
        '''
        resized = frame
        cv2.imshow('Window', resized)
        cv2.waitKey(30)

    # Let's print something so that we know the program is running in headless mode
    if totalFrames % 100 == 0:
        print(':: Running at frame: ' + str(totalFrames))

    # See if we should log Raw CSV data or not
    # print('running frame: ' + str(frame_seq))
    # Have to divide because of skipping frame.
    Uploader.frame_seq = str(frame_seq)

    need_log = False
    need_realtime_update = False
    need_restart = False

    # Force restart every FORCE_REBOOT_UNCONDITIONALLY_EVERY frame (to prevent memory leakage issue in python runtime)
    # TBD: Disable force restart for now (To Demo on MktOops)
    if False:  # frame_seq % FORCE_REBOOT_UNCONDITIONALLY_EVERY == 0:
        if args['deployment_type'] == 'edge' or args['deployment_type'] == 'edge_noop':
            need_log = True
            need_restart = True

    if int(frame_seq / RUNNING_FRAME_SKIP) % int(log_every_fps / RUNNING_FRAME_SKIP) == 0:
        need_log = True

    if int(frame_seq / RUNNING_FRAME_SKIP) % int(realtime_update_every_fps / RUNNING_FRAME_SKIP) == 0:
        need_realtime_update = True

    if need_realtime_update:
        print('Realtime Update at frame: ' + str(frame_seq))

        log_minute = base_minute + int(round(minutes_passed))
        log_hour = str(int(log_minute / 60))
        while len(log_hour) < 2:
            log_hour = '0' + log_hour
        log_minute = str(log_minute % 60)
        while len(log_minute) < 2:
            log_minute = '0' + log_minute
        log_time_string = log_hour + ':' + log_minute

        print('log_time_string = ' + log_time_string)

        custom_logics.update_log_realtime(object_tracker, ROI_AREA_CONFIGS,
                                          raw_csv_log_file_path, output_path_configs['video_path'], date_string,
                                          log_time_string, meta_datum)

    if need_log:
        print('Logging Raw CSV at frame: ' + str(frame_seq))

        log_minute = base_minute + int(round(minutes_passed))
        log_hour = str(int(log_minute / 60))
        while len(log_hour) < 2:
            log_hour = '0' + log_hour
        log_minute = str(log_minute % 60)
        while len(log_minute) < 2:
            log_minute = '0' + log_minute
        this_time_string = log_hour + ':' + log_minute
        log_time_string = time_string + '-' + this_time_string

        print('log_time_string = ' + log_time_string)

        custom_logics.update_log(object_tracker, ROI_AREA_CONFIGS,
                                 raw_csv_log_file_path, output_path_configs['video_path'], date_string, log_time_string,
                                 meta_datum)

        time_string = this_time_string
    else:
        if frame_seq % 10 == 0:
            print('::: frame_seq: ' + str(frame_seq))

    if need_restart:
        print("========= ISSUE NORMAL RESTART SIGNAL ==========")
        os.system('sudo reboot now')
        # Use more gentle restart process?
        break

    totalFrames += 1
    fps.update()

    sys.stdout.flush()

    # caps_d[cap] = len(trackableObjects)

print('[INFO] Finish processing')
video_source_adaptor.dispose()

Uploader.running = False

# Write summary report
try:
    custom_logics.finalize_log(object_tracker, ROI_AREA_CONFIGS, result_file_path)
except:
    pass

if not HEADLESS_MODE:
    cv2.destroyAllWindows()

print('FINISH RUNNING main_app.py')
