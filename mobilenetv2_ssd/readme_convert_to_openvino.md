# THIS SECTION IS FOR SSDLITE+MOBILENETV2

# For OpenVINO on NCS2
python3 mo_tf.py --input_shape [1,300,300,3] --tensorflow_use_custom_operations_config ../model_optimizer/extensions/front/tf/ssd_v2_support.json --tensorflow_object_detection_api_pipeline_config ~/Downloads/ssdlite_mobilenet_v2_coco_2018_05_09/pipeline.config --data_type FP16 --input_model ~/Downloads/ssdlite_mobilenet_v2_coco_2018_05_09/frozen_inference_graph.pb

# For OpenVINO on CPU
python3 mo_tf.py --input_shape [1,300,300,3] --tensorflow_use_custom_operations_config ../model_optimizer/extensions/front/tf/ssd_v2_support.json --tensorflow_object_detection_api_pipeline_config ~/Downloads/ssdlite_mobilenet_v2_coco_2018_05_09/pipeline.config --data_type FP32 --input_model ~/Downloads/ssdlite_mobilenet_v2_coco_2018_05_09/frozen_inference_graph.pb

# THIS SECTION IS FOR SSD+MOBILENETV2

# For OpenVINO on NCS2
python3 mo_tf.py --input_shape [1,300,300,3] --tensorflow_use_custom_operations_config ../model_optimizer/extensions/front/tf/ssd_v2_support.json --tensorflow_object_detection_api_pipeline_config ~/Downloads/ssd_mobilenet_v2_coco_2018_03_29/pipeline.config --data_type FP16 --input_model ~/Downloads/ssd_mobilenet_v2_coco_2018_03_29/frozen_inference_graph.pb

# For OpenVINO on CPU
python3 mo_tf.py --input_shape [1,300,300,3] --tensorflow_use_custom_operations_config ../model_optimizer/extensions/front/tf/ssd_v2_support.json --tensorflow_object_detection_api_pipeline_config ~/Downloads/ssd_mobilenet_v2_coco_2018_03_29/pipeline.config --data_type FP32 --input_model ~/Downloads/ssd_mobilenet_v2_coco_2018_03_29/frozen_inference_graph.pb
