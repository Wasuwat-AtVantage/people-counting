import os
import cv2
import json
import sys
import numpy as np
import matplotlib.pyplot as plt

from sys import platform

from sklearn.model_selection import GridSearchCV

import keras
from keras import backend as K
from keras.layers.core import Dense, Activation
from keras.optimizers import Adam
from keras.metrics import categorical_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
from keras.models import Model
from keras.applications import imagenet_utils
from keras.layers import Dense, GlobalAveragePooling2D, Flatten
from keras.applications import MobileNetV2
from keras.applications.mobilenet import preprocess_input
from IPython.display import Image
from keras.optimizers import Adam
from keras.utils import plot_model
from keras.wrappers.scikit_learn import KerasClassifier

from keras import regularizers

import wandb
from wandb import magic
from wandb.keras import WandbCallback

wandb.init(project="mobilenetv2_model")

'''
Download training dataset from:
https://drive.google.com/drive/folders/17cNfflgrb66dH8UBWtNFMyzkCegvM_3N?usp=sharing
'''

'''
mobile = keras.applications.mobilenet_v2.MobileNetV2()

def prepare_image(file):
    img_path = ''
    img = image.load_img(img_path + file, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    return keras.applications.mobilenet_v2.preprocess_input(img_array_expanded_dims)

Image(filename='per00007.jpg')
preprocessed_image = prepare_image('per00007.jpg')
predictions = mobile.predict(preprocessed_image)
results = imagenet_utils.decode_predictions(predictions)
print(results)
'''

mobilenetv2_base_model = MobileNetV2(weights='imagenet',
                                     include_top=False,
                                     input_shape=(224, 224, 3)
                                     )

x = mobilenetv2_base_model.output
feature_extractor_output = x

# x = GlobalAveragePooling2D()(x) # Nok: This will make us loss much feature information
x = Flatten()(x)  # Nok: This will not make us loss much information of extracted features from MobileNetV2

x = Dense(64, activity_regularizer=regularizers.l1_l2(l1=0.001, l2=0.01))(x)
x = Dense(128, activity_regularizer=regularizers.l1_l2(l1=0.001, l2=0.001))(x)

# We may not need more complex model if it's already fit the training data, so let's try from simpler one first.
# x = Dense(1024, activation = 'relu')(x)
# x = Dense(512, activation = 'relu')(x)

predictions = Dense(2, activation='softmax')(x)
model_base = Model(inputs=mobilenetv2_base_model.input, outputs=feature_extractor_output)
model = Model(inputs=mobilenetv2_base_model.input, outputs=predictions)

# for i, layer in enumerate(model.layers):
#    print(i, layer.name)
model.summary()  # This give more information about the model

# Freeze weight of only feature extractor layers
for layer in model_base.layers:
    print('Freeze weight of layer: ' + str(layer.name))
    layer.trainable = True

train_datagen = ImageDataGenerator(
    rotation_range=90,
    width_shift_range=0.2,
    height_shift_range=0.2,
    brightness_range=[0.4, 1.0],
    zoom_range=[0.5, 1.0],
    channel_shift_range=0.2,
    horizontal_flip=True,
    vertical_flip=True,
    shear_range=0.5,
    preprocessing_function=preprocess_input,
    validation_split=0.2)

test_datagen = ImageDataGenerator(
    preprocessing_function=preprocess_input)

train_generator = train_datagen.flow_from_directory(
    './dataset/train',
    target_size=(224, 224),
    color_mode='rgb',
    batch_size=52,
    class_mode='categorical')

val_generator = test_datagen.flow_from_directory(
    './dataset/validation',
    target_size=(224, 224),
    color_mode='rgb',
    batch_size=52,
    class_mode='categorical')

model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['accuracy'])

step_size_train = train_generator.n // train_generator.batch_size
model.fit_generator(generator=train_generator,
                    validation_data=val_generator,
                    validation_steps=3,
                    validation_freq=2,
                    steps_per_epoch=step_size_train,
                    epochs=15,
                    callbacks=[WandbCallback()])

model.save(os.path.join(wandb.run.dir, "model.h5"))
'''
if __name__ == '__main__':

    cap = cv2.VideoCapture(params["video"])

    while cap.isOpened():
        ret_val, image = cap.read()
        fps = cap.get(cv2.CAP_PROP_POS_FRAMES)

        
        if frame % 100 == 0:

        frame += 1
        

        cv2.putText(image, str(fps), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 0, 0), thickness=2, lineType=cv2.LINE_AA)
        cv2.imshow('tf-pose-estimation result', image)
        if cv2.waitKey(1) == 27:
            break

    cv2.destroyAllWindows()
'''
