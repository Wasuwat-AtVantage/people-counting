class ObjectClassifier:

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('ObjectClassifier::init_model')

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        pass

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        print('ObjectClassifier::perform_classification')
        return []
