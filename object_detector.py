class ObjectDetector:

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('ObjectDetector::init_model')

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        pass

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        print('ObjectDetector::perform_detection')
        return []
