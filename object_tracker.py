class ObjectTracker:
    # Function to perform object tracking
    def track_object(self, image, left, top, right, bottom, current_frame_number, detected_class_id, timestamp):
        pass

    # Function to get tracked object:
    def get_object_list(self):
        pass

    # Function to get count data:
    def get_count_data(self):
        pass

    # Function to get interested count data:
    def get_interested_count_data(self):
        pass

    # Function to get engaged count data:
    def get_engaged_count_data(self):
        pass

    # Notification callback for events:
    def set_callback(self, callback):
        pass

    # Update state of tracker into next state (Normally it means T = T + 1)
    def step(self, frame_seq, minuted_passed, timestamp):
        pass
