import os
import numpy as np

AREATYPE_SALE = 1
AREATYPE_OPERATION = 2


class ROIAreaConfig:
    def __init__(self, type_=AREATYPE_SALE, poly=[[0, 0], [0, 3000], [3000, 3000], [3000, 0]],
                 direction_x='0', direction_y='0', tracking_age=5, count_threshold=3,
                 interesting_threshold=30, engagement_threshold=50, unoccupied_threshold=50,
                 constraint_rois=[]
                 ):
        # Type of ROI
        self.type = type_

        # Polyline of ROI
        self.poly = poly
        polyXY = [[v[1], v[0]] for v in poly]
        self.polyNP = np.array(polyXY)  # For speedup operation on OpenCV

        # Generate bounding area for tracking movement
        self.top = np.min([v[0] for v in poly])
        self.left = np.min([v[1] for v in poly])
        self.bottom = np.max([v[0] for v in poly])
        self.right = np.max([v[1] for v in poly])

        # Direction constraint of the ROI
        self.direction_x_constraint = direction_x
        self.direction_y_constraint = direction_y

        # Generic configuration fo the ROI, used for counting
        self.tracking_age = tracking_age
        self.count_threshold = count_threshold

        # Configuration for SALE type ROI
        self.interesting_threshold = interesting_threshold
        self.engagement_threshold = engagement_threshold

        # Configuration for OPERATION type ROI
        self.unoccupied_threshold = unoccupied_threshold

        # Constraint for this ROI to count only people who already pass other ROIs
        self.constraint_rois = constraint_rois
