class ROIAreaStat:

    def __init__(self):
        # Active status (Having movement inside it)
        self.is_active = False

        # Occupied status
        self.is_occupied = False
        self.unoccupied_timestamp = 0
        self.occupied_timestamp = 0

        # Accumulate of occupied status event, this is reseted when data for this interval is each record to report
        self.interval_occupied_count = 0
        self.interval_unoccupied_count = 0

        # Event as array of [event_name, frame_id]
        self.events = []

        # Map from class_id to count
        self.count_map = {
            'all': 0,
        }

        # Map from class_id to interested count
        self.interested_count_map = {
            'all': 0,
        }

        # Map from class_id to engaged count
        self.engaged_count_map = {
            'all': 0,
        }

        # Map from class_id to attended time bucket to count
        self.atteneded_count_map = {
            'all': {},
        }

        # List of objects being tracked
        self.tracking_list = []

        # Heat map array [10x10]
        self.heatmap = [[0 for _ in range(10)] for _ in range(10)]

    def cut_off_interval(self):
        # Reset states those mean to return to initial state when each interval is passed.
        self.interval_occupied_count = 0
        self.interval_unoccupied_count = 0

        # Event as array of [event_name, frame_id]
        self.events = []

        # Map from class_id to count
        self.count_map = {
            'all': 0,
        }

        # Map from class_id to interested count
        self.interested_count_map = {
            'all': 0,
        }

        # Map from class_id to engaged count
        self.engaged_count_map = {
            'all': 0,
        }

        # Map from class_id to attended time bucket to count
        self.atteneded_count_map = {
            'all': {},
        }

        self.heatmap = [[0 for _ in range(10)] for _ in range(10)]
