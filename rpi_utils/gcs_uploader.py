import os
import sys
from google.cloud import storage
from google.oauth2 import service_account

class GCSUploader:
  def __init__(self):
    pass

  def uploadFile(self, deviceId, sourcePath):
    try:
      destPath = sourcePath[sourcePath.rfind('/'):]
      destPath = 'edge_computing/recordings/' + deviceId + destPath
      credentials = service_account.Credentials.from_service_account_file('../gcs.json')
      storage_client = storage.Client(
        credentials=credentials, project='machinelearning')
      bucket = storage_client.get_bucket('atv_dataset')
      file_blob = bucket.blob(destPath)
      with open(sourcePath, 'rb') as fin:
        file_blob.upload_from_file(fin)
      print('Finished uploading to GCS')
      return True
    except Exception as ex:
      print('Error while uploading upi logs: %s', ex)
      return False

# Unit Test
if __name__ == '__main__':
  gcs = GCSUploader()
  gcs.uploadFile("test", "../detection_playback/20200225_142414.mp4")
  print('Finished.')


