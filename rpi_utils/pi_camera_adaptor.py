import os
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
from rpi_utils.uploader import Uploader

class PiCameraAdaptor:

  def __init__(self, system_setting, args):
    try:
      self.camera = PiCamera()
      self.camera.resolution = (640, 480)
      self.camera.framerate = 5
      self.rawCapture = PiRGBArray(self.camera, size=(640, 480))
      time.sleep(0.5) # Wait for camera hardware to be initialized.
      self.system_setting = system_setting
      Uploader.system_status = 'CY'
    except Exception as err:
      print("========= ERROR IN CAMERA INITIALIZATION ==========")
      print(err)  
      #os.system('sudo reboot now')
      Uploader.system_status = 'EC:' + str(err)[:100]
      return
      # os._exit(1) # This will trigger shell script to restart the program

  # Do initalization stuffs
  def initialize(self, args):
    self.generator = self.camera.capture_continuous(self.rawCapture, format='bgr', use_video_port=True)
    pass

  # Get next frame from input source
  def get_next_frame(self):
    frame = next(self.generator)
    image = frame.array
    self.rawCapture.truncate(0)
    return image

  # Do dispose stuffs
  def dispose(self):
    pass 

  # Get FPS of video source
  def get_fps(self):
    return 5
    
  # Get Date and Time string of video source
  def get_date_and_time(self):
    #TODO: Return Date/Time based on current system date and time
    return ('00/00/0000', '00:00:00', 0, [])

  # Get all logging path
  def get_all_log_paths(self):    
    DETECTION_PLAYBACK_DIR = '../detection_playback'
    DETECTED_OBJECTS_DIR = '../detected_objects'
    result_file_path = 'picam_result.json'
    # Heatmap file path
    heatmap_file_path = 'picam_result.heatmap.log'
    # Count log file path
    count_log_file_path = 'picam_result.count.log'
    # Raw CSV log file to be accumulated later
    raw_csv_log_file_path = '../' + self.system_setting.report_dir + 'picam_result_raw.csv'

    DETECTION_PLAYBACK_PATH = os.path.join(DETECTION_PLAYBACK_DIR, 'picam_result.mp4')

    return {
      'detection_playback_dir': DETECTION_PLAYBACK_DIR, 
      'detected_objects_dir': DETECTED_OBJECTS_DIR,
      'detection_playback_path': DETECTION_PLAYBACK_PATH,
      'result_file_path': result_file_path,
      'heatmap_file_path': heatmap_file_path,
      'count_log_file_path': count_log_file_path,
      'raw_csv_log_file_path': raw_csv_log_file_path,
      'video_path': 'picam.mp4',
    }
