import sys
import os
import numpy as np
import requests
import json
from threading import Thread
import time
import base64
import cv2
from datetime import datetime
from roi_area_config import ROIAreaConfig
from streaming.streaming_server import StreamingServer
from rpi_utils.gcs_uploader import GCSUploader

class Uploader:
  system_status = ''
  frame_seq = ''
  last_uploaded_date_time_str = ''
  running = True
  DETECTION_PLAYBACK_DIR = ''

  def __init__(self, args):
    self.VERSION_NUMBER = '61'

    # self.SERVER_ADDRESS = 'http://localhost:8080'
    # self.SERVER_ADDRESS = 'https://edge.bizcuit.co.th'
    self.SERVER_ADDRESS = args['host']
    self.POLL_SERVER_API = self.SERVER_ADDRESS + '/PeopleCounting/edge_poll.jsp'
    self.UPLOAD_SERVER_API = self.SERVER_ADDRESS + '/PeopleCounting/edge_log.jsp'
    self.CAPTURE_SERVER_API = self.SERVER_ADDRESS + '/PeopleCounting/edge_capture.jsp'
    self.NOTIFICATION_API = self.SERVER_ADDRESS + '/PeopleCounting/send_notification.jsp'
    self.REALTIMEDATA_SERVER_API = self.SERVER_ADDRESS + '/PeopleCounting/edge_realtime_data.jsp'

    t = Thread(target=self.start_straming_server_t, args=())
    t.start()    

    # Store command to be execute
    self.command_id = ""
    self.command = ""
    self.command_params = ""
    self.command_executed = False
    self.command_response = ""

    self.last_frame = None
    self.roi_json = None
    self.roi_areas = None
    self.roi_filename = None
    self.object_tracker = None

    self.sending_notification = False

    self.node_id = self.get_mac_address()
    self.start_polling_thread()

  def set_roi_json(self, roi_json):
    self.roi_json = roi_json
  def set_roi_areas(self, roi_areas):
    self.roi_areas = roi_areas
  def set_roi_filename(self, roi_filename):
    self.roi_filename = roi_filename
  def set_object_tracker(self, object_tracker):
    self.object_tracker = object_tracker
    self.object_tracker.set_callback(self)

  def get_network_inferface_name(self):
    try:
      for root, dirs, files in os.walk('/sys/class/net'):
        for dir in dirs:
          if dir[:3] == 'enx' or dir[:3] == 'eth' or dir[:3] == 'wlp':
            interface = dir
      return interface
    except:
      return None

  def get_mac_address(self, interface=None):
    if interface is None:
      interface = self.get_network_inferface_name()
      if interface is None:
        print('Warning: No network interface is found.')
        return "node-default"
      print('Get MAC Address of interface: ' + str(interface))
      mac_address = ''
      try:
        mac_address = open('/sys/class/net/%s/address' % interface).read()
      except:
        mac_address = '00:00:00:00:00:00'
      print('MAC Address => ' + str(mac_address))
      return mac_address[0:17]

  def cache_last_frame(self, frame):
    self.last_frame = frame

  
  def send_notification_t(self, data):
    message = data
    print('Sending Notification: ' + message)
    ret = None
    try:
      # capture image of last frame
      _, buffer = cv2.imencode('.jpg', self.last_frame)
      image_dataurl = 'data:image/jpeg;base64, ' + base64.b64encode(buffer).decode('utf-8')

      # data to be sent to api 
      data = { 
        'topic': '/topics/all',
        'key': self.node_id,
        'title': self.node_id, 
        'body': message,
        'iconUrl': image_dataurl
      }   
      # sending post request and saving response as response object 
      r = requests.post(url = self.NOTIFICATION_API, data = data, timeout=10) 
      # extracting response text  
      ret = r.text
      print('Sending Notification Result: ' + str(ret))
    except Exception as err:
      print('Error sending notification')
      print(err)      
    self.sending_notification = False     
    return ret

  def send_notification(self, message):
    if self.sending_notification == True:
      print('Notification is called while being busy, skipped.')
      return
    self.sending_notification = True     
    t = Thread(target=self.send_notification_t, args=({message}))
    t.start()    
  
  def poll_command_t(self):
    while (True):
      try:
        if Uploader.running == False:
          break

        time.sleep(5)

        print('[POLL] - Poll the server for command')

        ret = None    

        if self.command_id is None:
          self.command_id = ''
        if self.command_response is None:
          self.command_response = ''
        if self.command_executed is None:
          self.command_executed = False

        # data to be sent to api 
        content = (self.command_id + '\t' + self.command_response) if self.command_executed else ''
        #print(content)
        data = { 'id': self.node_id, 'content': content, 'v': self.VERSION_NUMBER, 's': Uploader.frame_seq + ':' + Uploader.system_status }   
        # sending post request and saving response as response object 
        r = requests.post(url = self.POLL_SERVER_API, data = data, timeout=10) 
        # extracting response text  
        ret = r.text
        print(ret)

        # If Restart command is handshaked, perform restarting
        if self.command_response == '=>RST':
          print("========= ISSUE RESTART SIGNAL ==========")
          os.system('sudo reboot now')
          return
          #os._exit(1) # This will trigger shell script to restart the program

        # If ret.length > 0, then we receive new command and store it for execution
        if len(ret) > 0:
          ret = json.loads(ret)
          if ret['s'] == True:
            ret = ret['m']
            res_tokens = ret.split('|')
            if len(res_tokens) > 1:
              if res_tokens[0] != self.command_id and len(res_tokens) > 2: # Skip if the command has already been recrived.
                self.command_id = res_tokens[0]
                print('[POLL] - Got command_id: ' + self.command_id)
                self.command = res_tokens[1]
                print('[POLL] - Got command: ' + self.command)
                self.command_params = res_tokens[2]
                print('[POLL] - Got command params: ' + self.command_params)
                self.command_executed = False
              elif res_tokens[0] == self.command_id and res_tokens[1] == 'FIN': # If response indicate finish of latest command, then clear memory.
                print('[POLL] - Finished command: ' + self.command)
                self.command_id = ''
                self.command = ''
                self.command_params = ''
                self.command_executed = False

      except Exception as err:
        print('Error poll result')
        print(err)      
        # In case of error polling, we just exit the process
        # os._exit(1)  # Let's not exit the program

      # Execute command as per server response
      try:
        if len(self.command_id) > 0:
          print("[POLL] - Execute Command: " + self.command)
          if self.command == 'SS' and self.last_frame is not None: # Upload screenshot command
            _, buffer = cv2.imencode('.jpg', self.last_frame)
            self.command_response = base64.b64encode(buffer).decode('utf-8')
            self.command_executed = True
          elif self.command == 'GBB': # Get Bounding Box
            self.command_response = json.dumps(self.roi_json)
            self.command_executed = True
          elif self.command == 'SBB': # Set Bounding Box
            print("Set new bounding boxes: " + str(self.command_params))
            self.command_response = "true"
            self.command_executed = True
            if self.roi_filename is not None:
              print('Write to: ' + str(self.roi_filename))
              with open(self.roi_filename, 'w', encoding='utf-8') as f:
                f.write(self.command_params)
            self.roi_json = json.loads(self.command_params)
            self.roi_areas.clear()
            for roi_data in self.roi_json:
              print(roi_data)
              # Check for backward compatible ROI Data Format
              version = roi_data[0]
              if version != '_v2':
                # For V1 Data Format (2 vertices for rectangle ROIs)
                top = roi_data[1]
                left = roi_data[2]
                bottom = roi_data[3]
                right = roi_data[4]
                poly = [[top, left], [top, right], [bottom, right], [bottom, left]]
                constraint_rois = []
                if len(roi_data) > 12:          
                  rois = roi_data[12].split(',')
                  for i in range(len(rois)):
                    try:
                      roi_id = int(rois[i].strip())
                      constraint_rois.append(roi_id)
                    except:
                      pass
                roi_config = ROIAreaConfig(type_=roi_data[0], poly=poly, direction_x=roi_data[5], direction_y=roi_data[6],
                    tracking_age=roi_data[7], count_threshold=roi_data[8], interesting_threshold=roi_data[9], engagement_threshold=roi_data[10], unoccupied_threshold=roi_data[11],
                    constraint_rois=constraint_rois)
                self.roi_areas.append(roi_config)
              else:
                # For V2 Data Format (multi-vertices polygon)
                constraint_rois = []
                if len(roi_data) > 10:          
                  rois = roi_data[10].split(',')
                  for i in range(len(rois)):
                    try:
                      roi_id = int(rois[i].strip())
                      constraint_rois.append(roi_id)
                    except:
                      pass
                roi_config = ROIAreaConfig(type_=roi_data[1], poly=roi_data[2], direction_x=roi_data[3], direction_y=roi_data[4],
                    tracking_age=roi_data[5], count_threshold=roi_data[6], interesting_threshold=roi_data[7], engagement_threshold=roi_data[8], unoccupied_threshold=roi_data[9],
                    constraint_rois=constraint_rois)
                self.roi_areas.append(roi_config)

            if self.object_tracker is not None:
              print('SET NEW HOTSPOT AREAS => LENGTH = ' + str(len(self.roi_areas)))
              self.object_tracker.set_hotspot_areas(self.roi_areas)

          elif self.command == 'RST': # Restart the program
            self.command_response = '=>RST'
            self.command_executed = True
            # The restart will be performed once device send response back to server

          elif self.command == 'UPD': # Update device system software
            print('===== UPDATE DEVICE SOFTWARE =====> START')
            res = os.popen('git pull origin master').read()
            print('===== UPDATE DEVICE SOFTWARE =====> END')
            print(res)            
            self.command_response = 'OK'
            self.command_executed = True

          elif self.command == 'LST': # List Recording Videos
            print('Listing file in ' + str(Uploader.DETECTION_PLAYBACK_DIR))
            files = os.listdir(Uploader.DETECTION_PLAYBACK_DIR)
            print(files)
            file_list = []
            for f in files:
              (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(str(os.path.join(Uploader.DETECTION_PLAYBACK_DIR, f)))
              file_list.append({
                "name": str(f),
                "size": size,
                "modified": str(time.ctime(mtime))
              })
            self.command_response = json.dumps(file_list)
            self.command_executed = True

          elif self.command == 'URE': # Upload Recording Videos
            file_path = self.command_params
            file_path = os.path.join(Uploader.DETECTION_PLAYBACK_DIR, file_path)
            print('Uploading file: ' + file_path)

            try: 
              res = GCSUploader().uploadFile(self.node_id, file_path)
            except Exception as e:
              res = False
              print(e)

            self.command_response = str(res)
            self.command_executed = True

          elif self.command == 'DRE': # Delete Recording Videos
            file_path = self.command_params
            file_path = os.path.join(Uploader.DETECTION_PLAYBACK_DIR, file_path)
            print('Deleting file: ' + file_path)

            res = False
            if (os.path.exists(file_path)):
              try :
                os.remove(file_path)
                res = True
              except Exception as e:
                res = False
                print(e)

            self.command_response = str(res)
            self.command_executed = True


      except Exception as err:
        print('Error executing command: ' + str(self.command))
        print(err)      

  def upload_file_t(self, json_data):
    ret = None
    log_datetime = datetime.today().strftime('%Y%m%d_%H%M%S')
    prev_log_datetime = self.last_uploaded_date_time_str
    self.last_uploaded_date_time_str = log_datetime

    # print('Upload: ' + str(json_data))

    # data to be sent to api 
    data = { 'id': self.node_id, 'content': json.dumps(json_data).encode('utf-8'), 'd': log_datetime.encode('utf-8'), 'pd': prev_log_datetime.encode('utf-8') }   

    # [INTERRIM: Always save the result to local disk to prevent data loss]
    try:      
      with open(log_datetime + '.fin', 'w') as outfile:
        json.dump({ 'id': self.node_id, 'content': json.dumps(json_data), 'd': log_datetime, 'pd': prev_log_datetime }, outfile)
    except Exception as err:
      print('Error saving local file')
      print(err)      

    try:
      # sending post request and saving response as response object 
      r = requests.post(url = self.UPLOAD_SERVER_API, data = data, timeout=120) 
      # extracting response text  
      ret = r.text
      print(ret)
    except Exception as err:
      print('Error uploading result')
      print(err)      

      # In case of failed to upload, save the log to local disk
      try:
        with open(log_datetime + '.ful', 'w') as outfile:
          json.dump({ 'id': self.node_id, 'content': json.dumps(json_data), 'd': log_datetime, 'pd': prev_log_datetime }, outfile)
      except Exception as err:
        print('Saving log error')
        print(err)

    # Try to find and upload failed attempts
    files = []
    # r=root, d=directories, f = files
    for r_, d_, f_ in os.walk('.'):
      for file in f_:
        if '.ful' in file:
          files.append(os.path.join(r_, file))
    for file in files:
      print('Retry uploading: ' + str(file)) 
      data = None
      with open(file) as json_file:
        data = json.load(json_file)
      if data is not None:
        try:
          # sending post request and saving response as response object 
          r = requests.post(url = self.UPLOAD_SERVER_API, data = data, timeout=120) 
          # extracting response text  
          ret_ = r.text
          print(ret_)
          print('Deleting: ' + str(file))
          os.remove(file)
        except Exception as err:
          print('Error retry uploading result')
          print(err)      

    return ret

  def push_realtime_data_t(self, json_data):
    ret = None
    log_datetime = datetime.today().strftime('%Y%m%d_%H%M%S')

    # print('Upload: ' + str(json_data))

    # data to be sent to api 
    data = { 'id': self.node_id, 'content': json.dumps(json_data).encode('utf-8'), 'd': log_datetime.encode('utf-8') }   

    try:
      # sending post request and saving response as response object 
      r = requests.post(url = self.REALTIMEDATA_SERVER_API, data = data, timeout=30) 
      # extracting response text  
      ret = r.text
      print(ret)
    except Exception as err:
      print('Error push realtime data')
      print(err)      

      # In case of failed to upload, save the log to local disk
      try:
        with open(log_datetime + '.rul', 'w') as outfile:
          json.dump({ 'id': self.node_id, 'content': json.dumps(json_data), 'd': log_datetime }, outfile)
      except Exception as err:
        print('Saving log error')
        print(err)

    # Try to find and upload failed attempts
    files = []
    # r=root, d=directories, f = files
    for r_, d_, f_ in os.walk('.'):
      for file in f_:
        if '.rul' in file:
          files.append(os.path.join(r_, file))
    for file in files:
      print('Retry pushing realtime data: ' + str(file)) 
      data = None
      with open(file) as json_file:
        data = json.load(json_file)
      if data is not None:
        try:
          # sending post request and saving response as response object 
          r = requests.post(url = self.REALTIMEDATA_SERVER_API, data = data, timeout=30) 
          # extracting response text  
          ret_ = r.text
          print(ret_)
          print('Deleting: ' + str(file))
          os.remove(file)
        except Exception as err:
          print('Error retry pushing realtime data')
          print(err)      

    return ret

  def upload_file(self, json_data):
    t = Thread(target=self.upload_file_t, args=[json_data])
    t.start()    

  def push_realtime_data(self, json_data):
    t = Thread(target=self.push_realtime_data_t, args=[json_data])
    t.start()    

  def start_polling_thread(self):    
    t = Thread(target=self.poll_command_t, args=())
    print('Start Edge Polling Thread')
    t.start()    

  def start_straming_server_t(self):
    self.streaming_server = StreamingServer()
    self.streaming_server.start_server(self)
