#!/bin/bash

# Colourise the output
RED='\033[0;31m'        # Red
GRE='\033[0;32m'        # Green
YEL='\033[1;33m'        # Yellow
NCL='\033[0m'           # No Color

file_specification() {
        FILE_NAME="$(basename "${entry}")"
        DIR="$(dirname "${entry}")"
        NAME="${FILE_NAME%.*}"
        EXT="${FILE_NAME##*.}"
        SIZE="$(du -sh "${entry}" | cut -f1)"

        if [ "$EXT" = "avi" ]
        then
                RESULTFILE_PATH="$DIR/$NAME.json"
                printf "${GRE}Looking for JSON Result File: $RESULTFILE_PATH\n"
                if [ -f "$RESULTFILE_PATH" ]
                then
                        printf "${GRE}Found JSON Result File: ($RESULTFILE_PATH), skip running.\n"
                else
                        printf "${RED}Running detection on file: $entry\n"

                        # Run detection                        
                        # python3 main_app.py -f "$entry" -r "$DIR"/bounding_box.json -d yolo -c 0.2 -l monitor
                        python3 main_app.py -b edge_noop -f "$entry" -r "$DIR"/bounding_box.json -d ssdmv2 -v 2 -e http://localhost:8080

                        # Run heatmap generation
                        # python3 heatmap_from_coords.py -f "$entry"
                fi
        fi
}

walk() {
        local indent="${2:-0}"
        printf "${YEL}Checking Directory: $1\n"
        BOUNDINGBOX_CFG_PATH="$1/bounding_box.json"
        printf "${GRE}Looking for Bounding Box configuration file: $BOUNDINGBOX_CFG_PATH\n"
        if [ -f "$BOUNDINGBOX_CFG_PATH" ]
        then
                printf "${YEL}Found Bounding Box configuration file: $BOUNDINGBOX_CFG_PATH\n"
                # Process all files
                for entry in "$1"/*; do [[ -f "$entry" ]] && file_specification; done

                #DIR="$1"
                #printf "${GRE}Running CSV generation in directory: $DIR"
                #python3 summarize_csv.py "$DIR"
        fi
        # Recursive looking in subdirectory
        for entry in "$1"/*; do [[ -d "$entry" ]] && walk "$entry" $((indent+4)); done        
}

xlsx_gen() {
        printf "${GRE}Running XLSX generation in directory"
        # python3 summarize_xlsx.py "$DIR"        
}

# If the path is empty use the current, otherwise convert relative to absolute; Exec walk()
[[ -z "${1}" ]] && ABS_PATH="${PWD}" || ABS_PATH="${1}"
while true; do printf "${YEL}Running Daemon...\n"; walk "${ABS_PATH}/videos"; xlsx_gen; printf "${NCL}Finished...\n"; sleep 10; done
