import cv2
import numpy as np
import os

from object_detector import ObjectDetector


class SSDMobileNetDetector(ObjectDetector):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('SSDMobileNetDetector::init_model')
        prototxt = "../mobilenet_ssd/MobileNetSSD_deploy.prototxt"
        caffe_model = "../mobilenet_ssd/MobileNetSSD_deploy.caffemodel"
        self.model = cv2.dnn.readNet(prototxt, caffe_model)
        if "use_myriad" in config and config["use_myriad"] == True:
            # Use MyRiad to perform inference
            print('Using NCS2 MyRiad Device.')
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)
        else:
            # Use CPU
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
        self.confidence = 0.1
        if ("confidence" in config):
            print('Use Confidence Threshold = ' + str(config["confidence"]))
            self.confidence = config["confidence"]
        self.PERSON_CLASS = 15

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        # print('SSDMobileNetDetector::perform_detection')
        W = image.shape[1]
        H = image.shape[0]
        blob = cv2.dnn.blobFromImage(image, 0.007843, (W, H), 127.5)
        self.model.setInput(blob)
        detections = self.model.forward()
        rects = []
        for i in np.arange(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > self.confidence:
                idx = int(detections[0, 0, i, 1])
                if idx != self.PERSON_CLASS:
                    continue
                box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])
                (startX, startY, endX, endY) = box.astype("int")
                rects.append((startX, startY, endX, endY, 'person'))
        return rects
