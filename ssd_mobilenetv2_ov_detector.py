import cv2
import numpy as np
import os
from rpi_utils.uploader import Uploader

from object_detector import ObjectDetector


class SSDMobileNetV2OpenVINODetector(ObjectDetector):
    def __init__(self, fp_num):
        self.model = None
        self.data_precision = fp_num
        self.error_count = 0

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('SSDMobileNetV2OpenVINODetector::init_model')
        xml_file = "../mobilenetv2_ssd/frozen_inference_graph.xml"
        bin_file = "../mobilenetv2_ssd/frozen_inference_graph.bin"
        if self.data_precision == 'fp32':
            xml_file = "../mobilenetv2_ssd/frozen_inference_graph_fp32.xml"
            bin_file = "../mobilenetv2_ssd/frozen_inference_graph_fp32.bin"
        self.model = cv2.dnn.readNet(xml_file, bin_file)
        if "use_myriad" in config and config["use_myriad"] == True:
            # Use MyRiad to perform inference
            print('Using NCS2 MyRiad Device.')
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)
        else:
            # Use CPU
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
        self.confidence = 0.05
        if ("confidence" in config):
            print('Use Confidence Threshold = ' + str(config["confidence"]))
            self.confidence = config["confidence"]
        self.PERSON_CLASS = 1

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        # print('SSDMobileNetV2Detector::perform_detection')
        W = image.shape[1]
        H = image.shape[0]
        blob = cv2.dnn.blobFromImage(image, size=(300, 300), swapRB=True)
        # blob = cv2.dnn.blobFromImage(image, 0.007843, (W, H), 127.5)
        self.model.setInput(blob)
        rects = []
        try:
            detections = self.model.forward()
            for i in np.arange(0, detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                idx = int(detections[0, 0, i, 1])
                # print('idx = ' + str(idx) + ', confidence = ' + str(confidence))
                if confidence > self.confidence:
                    if idx != self.PERSON_CLASS:
                        continue
                    box = detections[0, 0, i, 3:7] * np.array([W, H, W, H])
                    (startX, startY, endX, endY) = box.astype("int")
                    rects.append((startX, startY, endX, endY, 'person'))
            Uploader.system_status = 'OK'
            self.error_count = 0
        except Exception as err:
            print("========= ERROR IN OBJECT DETECTION INFERENCING ==========")
            print(err)
            Uploader.system_status = 'ED:' + str(self.error_count) + ':' + str(err)[:200]
            if self.error_count >= 1000:
                Uploader.running = False
                exit(1)
            else:
                self.error_count = self.error_count + 1
            return rects

        return rects
