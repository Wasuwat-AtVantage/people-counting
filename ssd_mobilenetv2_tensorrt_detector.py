import os
import sys
import cv2
import time
import ctypes
import numpy as np
import platform
import pycuda.autoinit
import pycuda.driver as cuda

import uff
import tensorrt as trt
import graphsurgeon as gs
from rpi_utils.uploader import Uploader

from jetson_utils import ssd_mobilenet_v2_tensorrt_converter as model

if platform.processor() == 'x86_64':
    ctypes.CDLL("../jetson_utils/libflattenconcat_x64.so")
else:
    ctypes.CDLL("../jetson_utils/libflattenconcat.so")

from object_detector import ObjectDetector


class SSDMobileNetV2TensorRTDetector(ObjectDetector):
    def __init__(self):
        self.error_count = 0

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('SSDMobileNetV2TensorRTDetector::init_model')

        TRT_LOGGER = trt.Logger(trt.Logger.INFO)
        trt.init_libnvinfer_plugins(TRT_LOGGER, '')
        runtime = trt.Runtime(TRT_LOGGER)

        # compile model into TensorRT
        if not os.path.isfile(model.TRTbin):
            print('Converting Tensorflow model to TensorRT model...')
            dynamic_graph = model.add_plugin(gs.DynamicGraph(model.path))
            uff_model = uff.from_tensorflow(dynamic_graph.as_graph_def(), model.output_name, output_filename='tmp.uff')

            with trt.Builder(TRT_LOGGER) as builder, builder.create_network() as network, trt.UffParser() as parser:
                builder.max_workspace_size = 1 << 28
                builder.max_batch_size = 1
                builder.fp16_mode = True

                parser.register_input('Input', model.dims)
                parser.register_output('MarkOutput_0')
                parser.parse('tmp.uff', network)
                engine = builder.build_cuda_engine(network)

                buf = engine.serialize()
                with open(model.TRTbin, 'wb') as f:
                    f.write(buf)

        # create engine
        print('Loading TensorRT model file...')
        with open(model.TRTbin, 'rb') as f:
            buf = f.read()
            engine = runtime.deserialize_cuda_engine(buf)

        # create buffer
        self.host_inputs = []
        self.cuda_inputs = []
        self.host_outputs = []
        self.cuda_outputs = []
        self.bindings = []
        self.stream = cuda.Stream()

        for binding in engine:
            size = trt.volume(engine.get_binding_shape(binding)) * engine.max_batch_size
            host_mem = cuda.pagelocked_empty(size, np.float32)
            cuda_mem = cuda.mem_alloc(host_mem.nbytes)

            self.bindings.append(int(cuda_mem))
            if engine.binding_is_input(binding):
                self.host_inputs.append(host_mem)
                self.cuda_inputs.append(cuda_mem)
            else:
                self.host_outputs.append(host_mem)
                self.cuda_outputs.append(cuda_mem)
        self.context = engine.create_execution_context()

        self.confidence = 0.7
        if ("confidence" in config):
            print('Use Confidence Threshold = ' + str(config["confidence"]))
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        boxes = []
        try:
            # inference
            W = image.shape[1]
            H = image.shape[0]

            # Resize and normalize image for network input
            frame = cv2.resize(image, (300, 300))
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = (2.0 / 255.0) * frame - 1.0
            frame = frame.transpose((2, 0, 1))
            np.copyto(self.host_inputs[0], frame.ravel())

            # start_time = time.time()
            cuda.memcpy_htod_async(self.cuda_inputs[0], self.host_inputs[0], self.stream)
            self.context.execute_async(bindings=self.bindings, stream_handle=self.stream.handle)
            cuda.memcpy_dtoh_async(self.host_outputs[1], self.cuda_outputs[1], self.stream)
            cuda.memcpy_dtoh_async(self.host_outputs[0], self.cuda_outputs[0], self.stream)
            self.stream.synchronize()
            # print("execute times "+str(time.time()-start_time))

            output = self.host_outputs[0]
            for i in range(int(len(output) / model.layout)):
                prefix = i * model.layout
                index = int(output[prefix + 0])
                label = int(output[prefix + 1])
                if label == 1:  # Detect only 'Person' class
                    conf = output[prefix + 2]
                    if conf > 0.3:  # self.confidence:
                        xmin = int(output[prefix + 3] * W)
                        ymin = int(output[prefix + 4] * H)
                        xmax = int(output[prefix + 5] * W)
                        ymax = int(output[prefix + 6] * H)
                        # print("Detected {} with confidence {}".format(COCO_LABELS[label], "{0:.0%}".format(conf)))
                        boxes.append((xmin, ymin, xmax, ymax,
                                      conf))  # 'person')) # We don't use the 'person' class anymore, so should remove it in future
            # boxes = self.non_max_suppression_fast(np.array(boxes), 0.1)
            boxes = self.nms(np.array(boxes), 0.1)
            Uploader.system_status = 'OK'
            self.error_count = 0
        except Exception as err:
            print("========= ERROR IN OBJECT DETECTION INFERENCING ==========")
            print(err)
            Uploader.system_status = 'ED:' + str(self.error_count) + ':' + str(err)[:200]
            if self.error_count >= 1000:
                Uploader.running = False
                exit(1)
            else:
                self.error_count = self.error_count + 1

        return boxes

    def nms(self, boxes, threshold, type='Union'):
        """Non-Maximum Supression

        # Arguments
          boxes: numpy array [:, 0:5] of [x1, y1, x2, y2, score]'s
          threshold: confidence/score threshold, e.g. 0.5
          type: 'Union' or 'Min'

        # Returns
          A list of indices indicating the result of NMS
        """
        if boxes.shape[0] == 0:
            return []
        xx1, yy1, xx2, yy2 = boxes[:, 0], boxes[:, 1], boxes[:, 2], boxes[:, 3]
        areas = np.multiply(xx2 - xx1 + 1, yy2 - yy1 + 1)
        sorted_idx = boxes[:, 4].argsort()

        pick = []
        while len(sorted_idx) > 0:
            # In each loop, pick the last box (highest score) and remove
            # all other boxes with IoU over threshold
            tx1 = np.maximum(xx1[sorted_idx[-1]], xx1[sorted_idx[0:-1]])
            ty1 = np.maximum(yy1[sorted_idx[-1]], yy1[sorted_idx[0:-1]])
            tx2 = np.minimum(xx2[sorted_idx[-1]], xx2[sorted_idx[0:-1]])
            ty2 = np.minimum(yy2[sorted_idx[-1]], yy2[sorted_idx[0:-1]])
            tw = np.maximum(0.0, tx2 - tx1 + 1)
            th = np.maximum(0.0, ty2 - ty1 + 1)
            inter = tw * th
            if type == 'Min':
                iou = inter / \
                      np.minimum(areas[sorted_idx[-1]], areas[sorted_idx[0:-1]])
            else:
                iou = inter / \
                      (areas[sorted_idx[-1]] + areas[sorted_idx[0:-1]] - inter)
            pick.append(sorted_idx[-1])
            sorted_idx = sorted_idx[np.where(iou <= threshold)[0]]
        return boxes[pick].astype("int")

    # Implementation of fast NMS using algorithm from: Malisiewicz et al.
    def non_max_suppression_fast(self, boxes, overlapThresh):
        # if there are no boxes, return an empty list
        if len(boxes) == 0:
            return []

        # if the bounding boxes integers, convert them to floats --
        # this is important since we'll be doing a bunch of divisions
        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")

        # initialize the list of picked indexes
        pick = []

        # grab the coordinates of the bounding boxes
        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]

        # compute the area of the bounding boxes and sort the bounding
        # boxes by the bottom-right y-coordinate of the bounding box
        area = (x2 - x1 + 1) * (y2 - y1 + 1)
        idxs = np.argsort(y2)

        # print('init = ' + str(idxs))

        # keep looping while some indexes still remain in the indexes
        # list
        while len(idxs) > 0:
            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)

            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])

            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)

            # compute the ratio of overlap
            overlap = (w * h) / area[idxs[:last]]

            # print('overlap = ' + str(overlap))

            # delete all indexes from the index list that have
            idxs = np.delete(idxs, np.concatenate(([last],
                                                   np.where(overlap > overlapThresh)[0])))

        # print('left = ' + str(pick))

        # return only the bounding boxes that were picked using the
        # integer data type
        return boxes[pick].astype("int")
