import os
import sys
import cv2
import time
import ctypes
import numpy as np
import tensorflow as tf
import tensorflow.contrib.tensorrt as trt
from rpi_utils.uploader import Uploader

# ctypes.CDLL("../jetson_utils/libflattenconcat.so")

from object_detector import ObjectDetector


class SSDMobileNetV2TFTensorRTDetector(ObjectDetector):
    def __init__(self):
        self.error_count = 0

    def get_frozen_graph(self, graph_file):
        """Read Frozen Graph file from disk."""
        with tf.gfile.FastGFile(graph_file, "rb") as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
        return graph_def

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('SSDMobileNetV2TFTensorRTDetector::init_model')

        # load frozen graph and create model
        MODEL_FROZEN_GRAPH = '../mobilenetv2_ssd/trt_graph.pb'

        print("Loaded model from disk")
        self.graph = tf.Graph()
        trt_graph = self.get_frozen_graph(MODEL_FROZEN_GRAPH)

        # Create session and load graph
        tf_config = tf.ConfigProto()
        tf_config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=tf_config, graph=self.graph)
        with self.graph.as_default():
            tf.import_graph_def(trt_graph, name='')
        print('Finish loading GraphDef')

        # input and output tensor names.
        self.input_tensor_name = "image_tensor:0"
        self.scores = self.sess.graph.get_tensor_by_name('detection_scores:0')
        self.boxes = self.sess.graph.get_tensor_by_name('detection_boxes:0')
        self.classes = self.sess.graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = self.sess.graph.get_tensor_by_name('num_detections:0')

        self.confidence = 0.7
        if ("confidence" in config):
            print('Use Confidence Threshold = ' + str(config["confidence"]))
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        boxes = []
        try:
            # inference
            W = image.shape[1]
            H = image.shape[0]

            # Resize and normalize image for network input
            frame = cv2.resize(image, (300, 300))
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            # frame = (2.0 / 255.0) * frame - 1.0
            # frame = frame.transpose((2, 0, 1))

            scores, boxes_, classes, num_detections = self.sess.run(
                [self.scores, self.boxes, self.classes, self.num_detections], feed_dict={
                    self.input_tensor_name: [frame]
                })

            # print(scores)

            boxes_ = boxes_[0]  # index by 0 to remove batch dimension
            scores = scores[0]
            classes = classes[0]
            num_detections = num_detections[0]

            for i in range(len(boxes_)):
                label = int(classes[i])
                if label == 1:  # Detect only 'Person' class
                    conf = scores[i]
                    if conf > 0.7:  # self.confidence:
                        xmin = int(boxes_[i][1] * W)
                        ymin = int(boxes_[i][0] * H)
                        xmax = int(boxes_[i][3] * W)
                        ymax = int(boxes_[i][2] * H)
                        # print("Detected {} with confidence {}".format(COCO_LABELS[label], "{0:.0%}".format(conf)))
                        boxes.append((xmin, ymin, xmax, ymax, 'person'))
            Uploader.system_status = 'OK'
            self.error_count = 0

        except Exception as err:
            print("========= ERROR IN OBJECT DETECTION INFERENCING ==========")
            print(err)
            Uploader.system_status = 'ED:' + str(self.error_count) + ':' + str(err)[:200]
            if self.error_count >= 1000:
                Uploader.running = False
                exit(1)
            else:
                self.error_count = self.error_count + 1

        return boxes
