import cv2
import numpy as np
import os
import tensorflow as tf

from object_detector import ObjectDetector


class SSDMobileNetV2TFLiteDetector(ObjectDetector):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('SSDMobileNetV2TFLiteDetector::init_model')

        # load json and create model
        MODEL_FLATBUFFER = '../mobilenetv2_ssd/model2.tflite'

        print("Loaded model from disk")
        self.interpreter = tf.lite.Interpreter(model_path=MODEL_FLATBUFFER)
        self.interpreter.allocate_tensors()
        self.input_details = self.interpreter.get_input_details()
        self.output_details = self.interpreter.get_output_details()

        self.confidence = 0.1
        if ("confidence" in config):
            print('Use Confidence Threshold = ' + str(config["confidence"]))
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        # print('SSDMobileNetV2Detector::perform_detection')
        W = image.shape[1]
        H = image.shape[0]

        # Resize and normalize image for network input
        frame = cv2.resize(image, (300, 300))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = frame.astype('float32')
        frame = np.expand_dims(frame, axis=0)
        frame = (2.0 / 255.0) * frame - 1.0
        # frame = (1.0 / 255.0) * frame

        # run model
        self.interpreter.set_tensor(self.input_details[0]['index'], frame)
        self.interpreter.invoke()

        # get results
        boxes = self.interpreter.get_tensor(
            self.output_details[0]['index'])
        classes = self.interpreter.get_tensor(
            self.output_details[1]['index'])
        scores = self.interpreter.get_tensor(
            self.output_details[2]['index'])
        num = self.interpreter.get_tensor(
            self.output_details[3]['index'])

        # Find detected boxes coordinates
        return self._boxes_coordinates(image,
                                       np.squeeze(boxes[0]),
                                       np.squeeze(classes[0] + 1).astype(np.int32),
                                       np.squeeze(scores[0]),
                                       min_score_thresh=self.confidence)

    def _boxes_coordinates(self,
                           image,
                           boxes,
                           classes,
                           scores,
                           max_boxes_to_draw=20,
                           min_score_thresh=.5):
        if not max_boxes_to_draw:
            max_boxes_to_draw = boxes.shape[0]
        number_boxes = min(max_boxes_to_draw, boxes.shape[0])
        person_boxes = []
        # person_labels = []
        for i in range(number_boxes):
            if scores is None or scores[i] > min_score_thresh:
                if classes[i] != 1:
                    continue
                box = tuple(boxes[i].tolist())
                ymin, xmin, ymax, xmax = box

                im_height, im_width, _ = image.shape
                left, right, top, bottom = [int(z) for z in (xmin * im_width, xmax * im_width,
                                                             ymin * im_height, ymax * im_height)]

                person_boxes.append((left, top, right, bottom, 'person'))
        return person_boxes
