# For OpenVINO on NCS2
python3 mo_tf.py --input_shape [1,224,224,3] --tensorflow_use_custom_operations_config ../model_optimizer/extensions/front/tf/ssd_v2_support.json --input input_1 --output dense_2/Softmax --data_type FP16 --input_model ~/work/atvantage/people-counting/staff_mobilenetv2_ssd/TF_model/tf_model.pb

# For OpenVINO on CPU
python3 mo_tf.py --input_shape [1,224,224,3] --tensorflow_use_custom_operations_config ../model_optimizer/extensions/front/tf/ssd_v2_support.json --input input_1 --output dense_2/Softmax --data_type FP32 --input_model ~/work/atvantage/people-counting/staff_mobilenetv2_ssd/TF_model/tf_model.pb