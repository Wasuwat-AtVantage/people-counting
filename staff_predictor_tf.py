import cv2
import numpy as np
import os
from keras.preprocessing.image import img_to_array
from keras.models import load_model, model_from_json
import dlib

from object_classifier import ObjectClassifier


class StaffPredictorTF(ObjectClassifier):
    def __init__(self):
        self.model = None
        self.STAFF_COLOR = 'yellow'

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('StaffPredictorTF::init_model')

        # load json and create model
        MODEL_JSON = '../staff_mobilenetv2_ssd/staff_model' + self.STAFF_COLOR + '.json'
        MODEL_WEIGHTS = '../staff_mobilenetv2_ssd/staff_model_weight' + self.STAFF_COLOR + '.h5'

        print("Loaded model from disk")
        json_file = open(MODEL_JSON, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)
        # load weights into new model
        self.model.load_weights(MODEL_WEIGHTS)

        self.img_size = 224
        self.class_label_map = {0: 'customer', 1: 'staff'}
        self.target_size = self.model.input_shape[1:3]
        self.confidence = 0.5
        if ("confidence" in config):
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    def preprocess_input(self, x, v2=True):
        x = x.astype('float32')
        x = x / 255.0
        if v2:
            x = x - 0.5
            x = x * 2.0
        return x

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('StaffPredictor::perform_classification')
        image = cv2.resize(image, (self.img_size, self.img_size))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        rgb_face = self.preprocess_input(image, False)
        rgb_face = np.expand_dims(rgb_face, 0)
        prediction = self.model.predict(rgb_face)
        print('STAFF PREDICTION (1) => ' + str(prediction))
        idx = np.argmax(prediction[0])
        conf = prediction[0][idx]

        return self.class_label_map[idx]
