import cv2
import numpy as np
import os

from object_classifier import ObjectClassifier

LOWER_PURPLE = np.array([120, 50, 30])
UPPER_PURPLE = np.array([350, 120, 120])
PURPLE_THRESHOLD = 0.165

LOWER_RED = np.array([170, 150, 75])
UPPER_RED = np.array([180, 255, 190])
LOWER_RED2 = np.array([0, 150, 75])
UPPER_RED2 = np.array([10, 255, 190])
LOWER_RED3 = np.array([345, 150, 75])
UPPER_RED3 = np.array([360, 255, 190])
RED_THRESHOLD = 0.16

LOWER_YELLOW = np.array([15, 115, 95])
UPPER_YELLOW = np.array([70, 185, 180])
YELLOW_THRESHOLD = 0.13

USE_RED = 1
USE_YELLOW = 2
USE_PURPLE = 3


class StaffPredictorWithCap(ObjectClassifier):
    def __init__(self):
        self.model = None
        self.mode = USE_YELLOW
        self.confidence = None
        if USE_RED:
            self.confidence = RED_THRESHOLD
        elif USE_YELLOW:
            self.confidence = YELLOW_THRESHOLD
        else:
            self.confidence = PURPLE_THRESHOLD
        # self.num = 0
        self.img_size = 224

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('StaffPredictor::init_model')
        if "confidence" in config:
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value
        elif name == 'mode':
            self.mode = int(value)

    # Function to perform object detection, return object is
    # class label
    def perform_classification(self, image):
        # print('GenderPredictor::perform_classification')

        image = cv2.resize(image, (self.img_size, self.img_size))
        # filename = 'test_img' + str(self.num) + '.png'
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')
        # print(filename)
        # cv2.imwrite('../test_images/' + filename, image)
        # self.num += 1
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')
        # print('----------------------------------------------------------------------------------------------------')

        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        kernel = np.ones((3, 3), np.float32) / 9
        if USE_RED:
            mask = cv2.inRange(hsv, LOWER_RED, UPPER_RED) + cv2.inRange(hsv, LOWER_RED2, UPPER_RED2) + \
                   cv2.inRange(hsv, LOWER_RED3, UPPER_RED3)
        elif USE_YELLOW:
            mask = cv2.inRange(hsv, LOWER_YELLOW, UPPER_YELLOW)
        else:
            mask = cv2.inRange(hsv, LOWER_PURPLE, UPPER_PURPLE)

        dilation = cv2.dilate(mask, kernel, iterations=1)
        num_labels, labels_im, stats, centroids = cv2.connectedComponentsWithStats(dilation)
        sizes = stats[:, -1]
        wearing_cap = True
        if len(sizes) > 2:
            body_index = np.where(sizes == np.amax(sizes[1:]))[0][0]  # index of the biggest
            head_index = np.where(sizes == np.partition(sizes.flatten(), -3)[-3])[0][0]  # index of the second largest
            body_lowest_point = stats[body_index][1] + stats[body_index][3]
            head_lowest_point = stats[head_index][1] + stats[head_index][3]
            cap_area = stats[head_index][-1]
            if head_lowest_point > body_lowest_point:
                wearing_cap = False
            dilation = dilation[: body_lowest_point][::]  # crop the pic
        else:
            wearing_cap = False

        color_dots = cv2.countNonZero(dilation)

        staff = 1 if wearing_cap and (round(color_dots / dilation.size, 2)) >= self.confidence else 0

        if staff == 1:
            return 'staff'
        else:
            return 'customer'
