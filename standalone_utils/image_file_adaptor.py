import os
import cv2
from datetime import datetime
import time

class ImageFileAdaptor:

  def __init__(self, system_setting, args):
    self.system_setting = system_setting
    self.imagePath = args["file"]
    self.image = cv2.imread(self.imagePath)
    self.image = cv2.resize(self.image, (640, 480))

  # Do initalization stuffs
  def initialize(self, args):
    pass

  # Get next frame from input source
  def get_next_frame(self):
    time.sleep(1)
    return self.image.copy()

  # Do dispose stuffs
  def dispose(self):
    pass

  # Get FPS (Fake)
  def get_fps(self):
    return 1
    
  # Get Date and Time string of image source
  def get_date_and_time(self):
    # Parsing Date/Time string from file path.
    # We will skip the image if we cannot parse file path into designated format correctly.
    # FORMAT:
    #
    # Case 1:
    # <File Dir>/YYYYMMDD_HHMMSS.mp4
    #
    # Case 2:
    # <File Dir>/YYYYMMDD/HHMMSS.mp4
    #
    this_time = datetime.today()
    date_string = this_time.strftime("%Y%m%d")
    time_string = this_time.strftime("%H%M%S")
    base_minute = 0
    meta_datum = []
    return (date_string, time_string, base_minute, meta_datum)

  # Get all logging path
  def get_all_log_paths(self):    
    DETECTION_PLAYBACK_DIR = '../detection_playback'
    DETECTED_OBJECTS_DIR = '../detected_objects'
    result_file_path = self.imagePath[:self.imagePath.rfind('.')] + '.json'
    # Heatmap file path
    heatmap_file_path = self.imagePath[:self.imagePath.rfind('.')] + '.heatmap.log'
    # Count log file path
    count_log_file_path = self.imagePath[:self.imagePath.rfind('.')] + '.count.log'
    # Raw CSV log file to be accumulated later
    raw_csv_log_file_path = '../' + self.system_setting.report_dir + self.imagePath[:self.imagePath.rfind('.')] + '_raw.csv'

    IS_WINDOWS_OS = (os.name == 'nt')
    if IS_WINDOWS_OS:
      DETECTION_PLAYBACK_PATH = os.path.join(DETECTION_PLAYBACK_DIR, self.imagePath[self.imagePath.rfind('/'):self.imagePath.rfind('.')] + '.avi')
    else:
      # DETECTION_PLAYBACK_DIR = '/root/sharedfolder/detection_playback' # This path is inside docker container
      print('DETECTION_PLAYBACK_DIR = ' + str(DETECTION_PLAYBACK_DIR))
      print('path = ' + str(self.imagePath))
      DETECTION_PLAYBACK_PATH = os.path.join(DETECTION_PLAYBACK_DIR, self.imagePath[self.imagePath.rfind('/') + 1:self.imagePath.rfind('.')] + '.mp4')
    
    return {
      'detection_playback_dir': DETECTION_PLAYBACK_DIR, 
      'detected_objects_dir': DETECTED_OBJECTS_DIR,
      'detection_playback_path': DETECTION_PLAYBACK_PATH,
      'result_file_path': result_file_path,
      'heatmap_file_path': heatmap_file_path,
      'count_log_file_path': count_log_file_path,
      'raw_csv_log_file_path': raw_csv_log_file_path,
      'video_path': self.imagePath,
    }
