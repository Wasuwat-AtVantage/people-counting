import os
import cv2

class VideoFileAdaptor:

  def __init__(self, system_setting, args):
    self.system_setting = system_setting
    self.videoPath = args["file"]
    self.capture = cv2.VideoCapture(self.videoPath)

  # Do initalization stuffs
  def initialize(self, args):
    pass

  # Get next frame from input source
  def get_next_frame(self):
    _, frame = self.capture.read()
    frame = cv2.resize(frame, (640, 480))
    return frame

  # Do dispose stuffs
  def dispose(self):
    self.capture.release()

  # Get FPS of video source
  def get_fps(self):
    fps_value = self.capture.get(cv2.CAP_PROP_FPS)
    return fps_value
    
  # Get Date and Time string of video source
  def get_date_and_time(self):
    # Parsing Date/Time string from file path.
    # We will skip the video if we cannot parse file path into designated format correctly.
    # FORMAT:
    #
    # Case 1:
    # <File Dir>/YYYYMMDD_HHMMSS.mp4
    #
    # Case 2:
    # <File Dir>/YYYYMMDD/HHMMSS.mp4
    #
    date_string, time_string, base_minute, meta_datum = self.system_setting.parse_date_time_from_path(self.videoPath)
    return (date_string, time_string, base_minute, meta_datum)

  # Get all logging path
  def get_all_log_paths(self):    
    DETECTION_PLAYBACK_DIR = '../detection_playback'
    DETECTED_OBJECTS_DIR = '../detected_objects'
    result_file_path = self.videoPath[:self.videoPath.rfind('.')] + '.json'
    # Heatmap file path
    heatmap_file_path = self.videoPath[:self.videoPath.rfind('.')] + '.heatmap.log'
    # Count log file path
    count_log_file_path = self.videoPath[:self.videoPath.rfind('.')] + '.count.log'
    # Raw CSV log file to be accumulated later
    raw_csv_log_file_path = '../' + self.system_setting.report_dir + self.videoPath[:self.videoPath.rfind('.')] + '_raw.csv'

    IS_WINDOWS_OS = (os.name == 'nt')
    if IS_WINDOWS_OS:
      DETECTION_PLAYBACK_PATH = os.path.join(DETECTION_PLAYBACK_DIR, self.videoPath[self.videoPath.rfind('/'):self.videoPath.rfind('.')] + '.avi')
    else:
      # DETECTION_PLAYBACK_DIR = '/root/sharedfolder/detection_playback' # This path is inside docker container
      print('DETECTION_PLAYBACK_DIR = ' + str(DETECTION_PLAYBACK_DIR))
      print('path = ' + str(self.videoPath))
      DETECTION_PLAYBACK_PATH = os.path.join(DETECTION_PLAYBACK_DIR, self.videoPath[self.videoPath.rfind('/') + 1:self.videoPath.rfind('.')] + '.mp4')
    
    return {
      'detection_playback_dir': DETECTION_PLAYBACK_DIR, 
      'detected_objects_dir': DETECTED_OBJECTS_DIR,
      'detection_playback_path': DETECTION_PLAYBACK_PATH,
      'result_file_path': result_file_path,
      'heatmap_file_path': heatmap_file_path,
      'count_log_file_path': count_log_file_path,
      'raw_csv_log_file_path': raw_csv_log_file_path,
      'video_path': self.videoPath,
    }
