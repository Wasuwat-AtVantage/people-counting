import os
import time
import cv2
from rpi_utils.uploader import Uploader

class WebcamCameraAdaptor:

  def __init__(self, system_setting, args):
    try:
      time.sleep(0.5) # Wait for camera hardware to be initialized.
      self.system_setting = system_setting
      self.capture = cv2.VideoCapture(0)
      Uploader.system_status = 'CY'
    except Exception as err:
      print("========= ERROR IN CAMERA INITIALIZATION ==========")
      print(err)  
      #os.system('sudo reboot now')
      Uploader.system_status = 'EC:' + str(err)[:100]
      return
      # os._exit(1) # This will trigger shell script to restart the program

  # Do initalization stuffs
  def initialize(self, args):
    pass

  # Get next frame from input source
  def get_next_frame(self):
    _, frame = self.capture.read()
    return frame

  # Do dispose stuffs
  def dispose(self):
    self.capture.release()

  # Get FPS of video source
  def get_fps(self):
    fps_value = self.capture.get(cv2.CAP_PROP_FPS)
    return fps_value
    
  # Get Date and Time string of video source
  def get_date_and_time(self):
    #TODO: Return Date/Time based on current system date and time
    return ('00/00/0000', '00:00:00', 0, [])

  # Get all logging path
  def get_all_log_paths(self):    
    DETECTION_PLAYBACK_DIR = '../detection_playback'
    DETECTED_OBJECTS_DIR = '../detected_objects'
    result_file_path = 'webcam_result.json'
    # Heatmap file path
    heatmap_file_path = 'webcam_result.heatmap.log'
    # Count log file path
    count_log_file_path = 'webcam_result.count.log'
    # Raw CSV log file to be accumulated later
    raw_csv_log_file_path = '../' + self.system_setting.report_dir + 'webcam_result_raw.csv'

    DETECTION_PLAYBACK_PATH = os.path.join(DETECTION_PLAYBACK_DIR, 'webcam_result.mp4')

    return {
      'detection_playback_dir': DETECTION_PLAYBACK_DIR, 
      'detected_objects_dir': DETECTED_OBJECTS_DIR,
      'detection_playback_path': DETECTION_PLAYBACK_PATH,
      'result_file_path': result_file_path,
      'heatmap_file_path': heatmap_file_path,
      'count_log_file_path': count_log_file_path,
      'raw_csv_log_file_path': raw_csv_log_file_path,
      'video_path': 'webcam.mp4',
    }
