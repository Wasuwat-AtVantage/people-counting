import numpy as np
import cv2
from imagezmq import imagezmq

imageHub = imagezmq.ImageHub()

while(True):
  (node_id, frame) = imageHub.recv_image()
  #print('Receiving frame for node: ' + node_id + ', ' + str(frame.shape))
  imageHub.send_reply(b'OK')

  cv2.imshow('Frame', frame)
  key = cv2.waitKey(1) & 0xFF
  if key == ord('q'):
    break
    