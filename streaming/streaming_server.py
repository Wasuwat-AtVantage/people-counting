from flask import Flask, request
import socket
from streaming.imagezmq import imagezmq
from threading import Thread

app = Flask(__name__)
server = None

@app.route('/start')
def start():
  global server
  try:
    print('server = ' + str(server))
    ip = request.args.get('ip')
    port = request.args.get('port')
    if port is None:
      port = '5555'
    server.start(ip, port)
    return "OK"
  except:
    return "FAIL"

@app.route('/start')
def stop():
  global server
  try:
    server.stop()
    return "OK"
  except:
    return "FAIL"

class StreamingServer():

  def __init__(self):
    self.sender = None
    self.target_ip = None

  def start_server(self, uploader):
    global server
    self.uploader = uploader
    server = self
    print('server = ' + str(server))
    app.run(host='0.0.0.0', port=5554)

  def start(self, ip, port):
    if ip is not None:      

      # Release existing sender if existed
      if self.sender is not None:
        pass

      print('Initiate streaming to IP: ' + ip + ', port: ' + port)
      self.sender = imagezmq.ImageSender(connect_to="tcp://{}:{}".format(ip, port))
      self.target_ip = ip
      self.start_send_thread(ip)  

  def stop(self):
    self.sender = None
    self.ip = None

  def start_send_thread(self, ip):    
    t = Thread(target=self.send_frame_t, args=[ip])
    print('Start Streaming Server Stream Thread')
    t.start()    

  def send_frame_t(self, ip):
    while self.target_ip == ip and self.sender is not None:
      frame = self.uploader.last_frame
      if frame is not None:
        # print('Streaming: ' + str(frame.shape))
        self.sender.send_image(self.uploader.node_id, frame)

## unit test ##
# s = StreamingServer()
# s.start_server(None)
