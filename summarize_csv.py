import os
import sys
import glob
import json
from system_setting import SystemSetting
from roi_area_config import ROIAreaConfig

dir_path = sys.argv[1]

# Load system setting
system_setting = SystemSetting()
if os.path.exists('system_setting.json'):
    system_setting.read_from_config_file('system_setting.json')

os.chdir("videos")


# Utility function to convert minute_passed into HH:MM string
def to_time_string(minutes_passed):
    log_minute = int(round(minutes_passed))
    log_hour = str(int(log_minute / 60))
    while len(log_hour) < 2:
        log_hour = '0' + log_hour
    log_minute = str(log_minute % 60)
    while len(log_minute) < 2:
        log_minute = '0' + log_minute
    this_time_string = log_hour + ':' + log_minute
    return this_time_string


# Load bounding_box.json file
bounding_box_path = os.path.join(dir_path, 'bounding_box.json')
if not os.path.exists(bounding_box_path):
    print('The bounding_box.json file is not found. Skip.')
    exit(0)

# ROIs configuration is array of below list:
# [TOP, LEFT, BOTTOM, RIGHT, X-DIRECTION, Y-DIRECTION, AGE_TRACKING, COUNT_THRESHOLD, INTEREST_THREHOLD, ENGAGEMENT_THRESHOLD]
ROI_AREA_CONFIGS = []
print('Reading ROIs JSON configuration from: ' + bounding_box_path)
with open(bounding_box_path) as f:
    roi_datum = json.load(f)
    for roi_data in roi_datum:
        print(roi_data)

        # Check for backward compatible ROI Data Format
        version = roi_data[0]
        if version != '_v2':
            # For V1 Data Format (2 vertices for rectangle ROIs)
            top = roi_data[1]
            left = roi_data[2]
            bottom = roi_data[3]
            right = roi_data[4]
            poly = [[top, left], [top, right], [bottom, right], [bottom, left]]
            constraint_rois = []
            if len(roi_data) > 12:
                rois = roi_data[12].split(',')
                for i in range(len(rois)):
                    try:
                        roi_id = int(rois[i].strip())
                        constraint_rois.append(roi_id)
                    except:
                        pass
            roi_config = ROIAreaConfig(type_=roi_data[0], poly=poly, direction_x=roi_data[5], direction_y=roi_data[6],
                                       tracking_age=roi_data[7], count_threshold=roi_data[8],
                                       interesting_threshold=roi_data[9], engagement_threshold=roi_data[10],
                                       unoccupied_threshold=roi_data[11],
                                       constraint_rois=constraint_rois)
            ROI_AREA_CONFIGS.append(roi_config)
        else:
            # For V2 Data Format (multi-vertices polygon)
            constraint_rois = []
            if len(roi_data) > 10:
                rois = roi_data[10].split(',')
                for i in range(len(rois)):
                    try:
                        roi_id = int(rois[i].strip())
                        constraint_rois.append(roi_id)
                    except:
                        pass
            roi_config = ROIAreaConfig(type_=roi_data[1], poly=roi_data[2], direction_x=roi_data[3],
                                       direction_y=roi_data[4],
                                       tracking_age=roi_data[5], count_threshold=roi_data[6],
                                       interesting_threshold=roi_data[7], engagement_threshold=roi_data[8],
                                       unoccupied_threshold=roi_data[9],
                                       constraint_rois=constraint_rois)
            ROI_AREA_CONFIGS.append(roi_config)

print(ROI_AREA_CONFIGS)

videoList = glob.glob(dir_path + '/*.avi') + glob.glob(dir_path + '/*.mp4')
videoList = sorted(videoList)
if len(videoList) > 0:

    for hotspot_id, hotspot_config in enumerate(ROI_AREA_CONFIGS):
        csv_path = os.path.join('/root/sharedfolder',
                                system_setting.report_dir + dir_path + '/summary_roi' + str(hotspot_id) + '.csv')
        print('Summarize file: ' + csv_path)

        unoccupied_report_path = os.path.join('/root/sharedfolder',
                                              system_setting.report_dir + dir_path + '/unoccupied_roi' + str(
                                                  hotspot_id) + '.csv')
        print('Unoccupied Report file: ' + unoccupied_report_path)

        with open(csv_path, 'w', encoding='utf-8') as fout:
            with open(unoccupied_report_path, 'w', encoding='utf-8') as fout_unoccupied:

                header_row = [
                    'File',
                    'Date',
                    'Time',
                    'Total',

                    'Right-Total',
                    'Right-M-Kids',
                    'Right-M-Adults',
                    'Right-M-Elders',
                    'Right-W-Kids',
                    'Right-W-Adults',
                    'Right-W-Elders',
                    'Right-Undefined',

                    'Left-Total',
                    'Left-M-Kids',
                    'Left-M-Adults',
                    'Left-M-Elders',
                    'Left-W-Kids',
                    'Left-W-Adults',
                    'Left-W-Elders',
                    'Left-Undefined',

                    'Interest-Total',
                    'Interest-M-Kids',
                    'Interest-M-Adults',
                    'Interest-M-Elders',
                    'Interest-W-Kids',
                    'Interest-W-Adults',
                    'Interest-W-Elders',
                    'Interest-Undefined',

                    'Engage-Total',
                    'Engage-M-Kids',
                    'Engage-M-Adults',
                    'Engage-M-Elders',
                    'Engage-W-Kids',
                    'Engage-W-Adults',
                    'Engage-W-Elders',
                    'Engage-Undefined',

                    '% Occcupied',
                ]
                fout.write(','.join(header_row) + '\n')

                fout_unoccupied.write('Date,Unoccupied' + '\n')

                for videoFile in videoList:

                    # Read date file from path
                    date_string, _, _, meta_datum = system_setting.parse_date_time_from_path(videoFile)

                    # Read CSV Log file for each video
                    raw_csv_log_file_path = '../' + system_setting.report_dir + videoFile[
                                                                                :videoFile.rfind('.')] + '_raw.csv'
                    print('Log Raw CSV Path: ' + raw_csv_log_file_path)
                    print('Reading: ' + raw_csv_log_file_path)

                    with open(raw_csv_log_file_path, 'r', encoding='utf-8') as f:
                        for line in f:
                            cols = line.strip().split(',')
                            hotspot_id_ = cols[4]
                            if str(hotspot_id) == str(hotspot_id_):
                                new_line = ','.join([col for i, col in enumerate(cols) if i != 3 and i != 4 and i != 5])
                                fout.write(new_line + '\n')

                    # Read Unoccupied Log file for each video
                    raw_json_log_file_path = videoFile[:videoFile.rfind('.')] + '.json'
                    print('Log Raw JSON Path: ' + raw_json_log_file_path)
                    print('Reading: ' + raw_json_log_file_path)
                    with open(raw_json_log_file_path, 'r', encoding='utf-8') as f:
                        json_data = json.load(f)
                        event_data = json_data['hotspot_stat'][hotspot_id]['events']
                        unoccupied_from = None
                        for event in event_data:
                            if event[0] == 'out':
                                unoccupied_from = to_time_string(event[1])
                            elif event[0] == 'in':
                                if unoccupied_from is not None:
                                    unoccupied_to = to_time_string(event[1])
                                    fout_unoccupied.write(
                                        date_string + ',' + str(unoccupied_from) + ' - ' + str(unoccupied_to) + '\n')
                                    unoccupied_from = None
