import os
import numpy as np
import json
from pathlib import Path
import glob
import csv
from system_setting import SystemSetting
from xlsxwriter.workbook import Workbook

print("Running XLSX Summarizer")

# Load system setting
system_setting = SystemSetting()
if os.path.exists('system_setting.json'):
    system_setting.read_from_config_file('system_setting.json')

rootdir = './' + system_setting.report_dir

print('Reading Root Directory: ' + rootdir)

directory_list = Path(rootdir).glob('**/*/')
directory_list = [d for d in directory_list if os.path.isdir(d)]
directory_list = sorted(directory_list)

for directory in directory_list:
    print('Processing: ' + str(directory))

    # Read all *_raw.csv file and combine them into single sheet
    # TODO: Find better way to sort CSV file by date/time path information in filename
    raw_csv_list = Path(str(directory)).glob('*_raw.csv')
    raw_csv_list = sorted(raw_csv_list)
    if len(raw_csv_list) > 0:

        # Construct xlsx file
        xlsx_path = str(directory) + '/summary.xlsx'
        print('Creating: ' + xlsx_path)
        workbook = Workbook(xlsx_path)
        summary_worksheet = workbook.add_worksheet('summary')

        row_idx = 0
        for idx, raw_csv in enumerate(raw_csv_list):
            print(' Adding data from: ' + str(raw_csv))
            with open(raw_csv, 'rt', encoding='utf8') as f:
                reader = csv.reader(f)
                for r, row in enumerate(reader):
                    if r == 0 and idx > 0: continue  # Skip header if not the first file
                    for c, col in enumerate(row):
                        summary_worksheet.write(row_idx, c, col)
                    row_idx = row_idx + 1

        # Read all unoccupied_roi*.csv files
        # TODO: Find better way to sort CSV file by date/time path information in filename
        uc_file_list = Path(str(directory)).glob('unoccupied_roi*.csv')
        uc_file_list = sorted(uc_file_list)
        unoccupied_worksheet = workbook.add_worksheet('unoccupied')

        row_idx = 0
        for idx, uc_file in enumerate(uc_file_list):
            print(' Adding data from: ' + str(uc_file))
            uc_file = str(uc_file)
            roi_id = uc_file[uc_file.rfind('_') + 1: uc_file.rfind('.')].replace('roi', '')
            with open(uc_file, 'rt', encoding='utf8') as f:
                reader = csv.reader(f)
                for r, row in enumerate(reader):
                    if r == 0 and idx > 0: continue  # Skip header if not the first file
                    for c, col in enumerate(row):
                        unoccupied_worksheet.write(row_idx, c + 1, col)
                    if r == 0:
                        unoccupied_worksheet.write(row_idx, 0, 'ROI')
                    else:
                        unoccupied_worksheet.write(row_idx, 0, roi_id)
                    row_idx = row_idx + 1

        print('Finished.')
        workbook.close()
