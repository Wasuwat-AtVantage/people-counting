import os
import sys
import json


class SystemSetting:
    # Constant for directory structure, DATE_IN_FILENAME means date format is in filename.
    # DATE_IN_DIRNAME means date format in directory name.
    DIRECTORY_STRUCTURE_DATE_IN_FILENAME = 1
    DIRECTORY_STRUCTURE_DATE_IN_DIRNAME = 2

    def __init__(self):
        # Default configuration
        self.log_frequency_minute = 15  # Interval for logging frequency (Effect every logging in the project)
        self.realtime_update_frequency_minute = 1  # Interval for realtime update
        self.directory_structure = SystemSetting.DIRECTORY_STRUCTURE_DATE_IN_FILENAME
        self.report_dir = 'report_data'  # Report data directory

        # Maximum number of meta data allowed in file path extracting
        self.MAX_METADATA_COUNT = 5

        # Node ID, for identify device id
        self.node_id = 'node-default'

    def read_from_config_file(self, config_path):
        with open(config_path, 'r', encoding='utf-8') as fin:
            json_data = json.load(fin)
            self.log_frequency_minute = json_data['log_frequency_minute']
            self.realtime_update_frequency_minute = json_data['realtime_update_frequency_minute']
            self.directory_structure = json_data['directory_structure']
            self.report_dir = json_data['report_dir']
            self.node_id = json_data['node_id']

    def parse_date_time_from_path(self, path):
        # Parsing Date/Time string from file path.
        # We will skip the video if we cannot parse file path into designated format correctly.
        # FORMAT:
        #
        # Case 1:
        # <File Dir>/YYYYMMDD_HHMMSS.mp4
        #
        # Case 2:
        # <File Dir>/YYYYMMDD/HHMMSS.mp4
        #
        date_string = None
        time_string = None
        base_minute = 0
        file_path_tokens = path.split('/')
        file_name = file_path_tokens[-1][0:-4]
        print('file_name = ' + str(file_name))
        meta_datum = file_path_tokens[:-1]
        meta_datum.reverse()
        meta_datum = meta_datum[:self.MAX_METADATA_COUNT]
        print('meta_datum = ' + str(meta_datum))
        if len(file_name) == 15 or len(file_name) == 22:  # Case 1
            date_string = file_name[0:4] + '/' + file_name[4:6] + '/' + file_name[6:8]
            time_string = file_name[9:11] + ':' + file_name[11:13]
            base_minute = int(file_name[9:11]) * 60 + int(file_name[11:13])
        elif len(file_name) == 6 or len(file_name) == 13:  # Case 2
            date_string = ' '  # TODO: Extract it from parent dir
            time_string = file_name[0:2] + ':' + file_name[2:4]
            base_minute = int(file_name[0:2]) * 60 + int(file_name[2:4])

        return (date_string, time_string, base_minute, meta_datum)
