import os
import sys
import csv

DIR = 'Camera3_20191130'
DATE = '2019/11/30'
DEVICE_ID = 'dc:a6:32:27:4c:86'

for roi in range(4):

  OUTPUT_PATH = os.path.join(DIR, '_summary_' + str(roi) + '.csv')

  with open(OUTPUT_PATH, 'w', encoding='utf-8', newline='') as fout:
    csv_writer = csv.writer(fout, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    # Write header row?
    headers = ['File','Date','Time','Hours','Device ID','ROI','ROI Type','Total','Total Men','Total Women','Right-Total','Right-M-Kids','Right-M-Adults','Right-M-Elders','Right-W-Kids','Right-W-Adults','Right-W-Elders','Right-Undefined','Left-Total','Left-M-Kids','Left-M-Adults','Left-M-Elders','Left-W-Kids','Left-W-Adults','Left-W-Elders','Left-Undefined','Pass-Through-Total','Pass-Through-M','Pass-Through-W','Interest-Total','Interest-M-Total','Interest-M-Kids','Interest-M-Adults','Interest-M-Elders','Interest-W-Total','Interest-W-Kids','Interest-W-Adults','Interest-W-Elders','Interest-Undefined','Engage-Total','Engage-M-Total','Engage-M-Kids','Engage-M-Adults','Engage-M-Elders','Engage-W-Total','Engage-W-Kids','Engage-W-Adults','Engage-W-Elders','Engage-Undefined','% Occupied','Mins of Unoccupied']
    in_headers = ['File','Date','Time','Meta Data','Area','ROI_Type','Total','Right-Total','Right-M-Kids','Right-M-Adults','Right-M-Elders','Right-W-Kids','Right-W-Adults','Right-W-Elders','Right-Undefined','Left-Total','Left-M-Kids','Left-M-Adults','Left-M-Elders','Left-W-Kids','Left-W-Adults','Left-W-Elders','Left-Undefined','Interest-Total','Interest-M-Kids','Interest-M-Adults','Interest-M-Elders','Interest-W-Kids','Interest-W-Adults','Interest-W-Elders','Interest-Undefined','Engage-Total','Engage-M-Kids','Engage-M-Adults','Engage-M-Elders','Engage-W-Kids','Engage-W-Adults','Engage-W-Elders','Engage-Undefined','% Occcupied']
    summable_cols = ['Total','Right-Total','Right-M-Kids','Right-M-Adults','Right-M-Elders','Right-W-Kids','Right-W-Adults','Right-W-Elders','Right-Undefined','Left-Total','Left-M-Kids','Left-M-Adults','Left-M-Elders','Left-W-Kids','Left-W-Adults','Left-W-Elders','Left-Undefined','Interest-Total','Interest-M-Kids','Interest-M-Adults','Interest-M-Elders','Interest-W-Kids','Interest-W-Adults','Interest-W-Elders','Interest-Undefined','Engage-Total','Engage-M-Kids','Engage-M-Adults','Engage-M-Elders','Engage-W-Kids','Engage-W-Adults','Engage-W-Elders','Engage-Undefined', '% Occcupied']

    in_map = {}
    out_map = {}
    for i in range(len(headers)):
      out_map[headers[i]] = i
    for i in range(len(in_headers)):
      in_map[in_headers[i]] = i

    # Map from hour to datarow
    map_data = {}
    count_data = {}
    
    csv_writer.writerow(headers)

    for root, dirs, files in os.walk(DIR):
      for name in files:
        if name.endswith((".csv")):
          if name[0] == '_': # Skip the output file
            continue
          full_path = os.path.join(root, name)
          print(full_path)

          # Extract time information from file path
          time_str = name.replace('_raw', '')
          time_str = time_str.replace('.csv', '')
          time_str = time_str[-6:]
          print(time_str)
          hour = time_str[0:2]
          minute = time_str[2:4]
          second = time_str[4:6]

          with open(full_path, 'r', encoding='utf-8') as fin:
            csv_reader = csv.reader(fin, delimiter=',', quotechar='"')
            last_row = None
            count = 0
            for row in csv_reader:
              if str(row[in_map['Area']]) == str(roi):
                last_row = row
                count = count + 1 # Skip header
            if last_row is not None and len(last_row) > 1:
              # Add row to data map
              sum_row = None
              if hour in map_data:
                sum_row = map_data[hour]
                for col in summable_cols:
                  sum_row[in_map[col]] = str(int(sum_row[in_map[col]]) + int(last_row[in_map[col]]))
                count_data[hour] = count_data[hour] + 1              
              else:
                sum_row = last_row
                map_data[hour] = sum_row
                count_data[hour] = 1

    # Write summary row to CSV file
    for hour in map_data:
      # print(hour)
      sum_row = map_data[hour]
      output_row = ['' for j in range(len(headers))]
      for col in in_headers:
        if col in headers:
          output_row[out_map[col]] = sum_row[in_map[col]]
      output_row[out_map['Date']] = DATE
      output_row[out_map['Time']] = hour + ':00:00-' + hour + ':59:59'
      hour_str = int(hour)
      if hour_str > 12:
        hour_str = str(hour_str - 12) + ' PM'
      else:
        hour_str = str(hour_str) + ' AM'
      output_row[out_map['Hours']] = hour_str
      output_row[out_map['ROI']] = sum_row[in_map['Area']]
      output_row[out_map['ROI Type']] = 'Customer'
      output_row[out_map['Device ID']] = DEVICE_ID
      output_row[out_map['Total Men']] = str(
        int(output_row[out_map['Right-M-Kids']]) +
        int(output_row[out_map['Right-M-Adults']]) +
        int(output_row[out_map['Right-M-Elders']]) +
        int(output_row[out_map['Left-M-Kids']]) +
        int(output_row[out_map['Left-M-Adults']]) +
        int(output_row[out_map['Left-M-Elders']])
      )
      output_row[out_map['Total Women']] = str(
        int(output_row[out_map['Right-W-Kids']]) +
        int(output_row[out_map['Right-W-Adults']]) +
        int(output_row[out_map['Right-W-Elders']]) +
        int(output_row[out_map['Left-W-Kids']]) +
        int(output_row[out_map['Left-W-Adults']]) +
        int(output_row[out_map['Left-W-Elders']])
      )
      output_row[out_map['Pass-Through-Total']] = output_row[out_map['Total']] 
      output_row[out_map['Pass-Through-M']] = output_row[out_map['Total Men']] 
      output_row[out_map['Pass-Through-W']] = output_row[out_map['Total Women']] 
      output_row[out_map['Interest-M-Total']] = str(
        int(output_row[out_map['Interest-M-Kids']]) +
        int(output_row[out_map['Interest-M-Adults']]) +
        int(output_row[out_map['Interest-M-Elders']])
      )
      output_row[out_map['Interest-W-Total']] = str(
        int(output_row[out_map['Interest-W-Kids']]) +
        int(output_row[out_map['Interest-W-Adults']]) +
        int(output_row[out_map['Interest-W-Elders']])
      )
      output_row[out_map['Engage-M-Total']] = str(
        int(output_row[out_map['Engage-M-Kids']]) +
        int(output_row[out_map['Engage-M-Adults']]) +
        int(output_row[out_map['Engage-M-Elders']])
      )
      output_row[out_map['Engage-W-Total']] = str(
        int(output_row[out_map['Engage-W-Kids']]) +
        int(output_row[out_map['Engage-W-Adults']]) +
        int(output_row[out_map['Engage-W-Elders']])
      )
      # Recalculate % Occupied
      percent_occupied = int(sum_row[in_map['% Occcupied']]) / count_data[hour]
      output_row[out_map['% Occupied']] = str(int(percent_occupied))
      output_row[out_map['Mins of Unoccupied']] = str(int(60.0 - percent_occupied * 0.6))

      csv_writer.writerow(output_row)
      print('|'.join(output_row))
