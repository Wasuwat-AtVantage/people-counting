import cv2
import numpy as np
import os

from object_detector import ObjectDetector


class YoloV3Detector(ObjectDetector):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('YoloV3Detector::init_model')
        classesFile = "../yolov3/coco.names"
        self.classes = None
        with open(classesFile, 'rt') as f:
            self.classes = f.read().rstrip('\n').split('\n')
        yoloV3cfg = '../yolov3/yolov3.cfg'
        yoloV3weights = '../yolov3/yolov3.weights'

        self.model = cv2.dnn.readNetFromDarknet(yoloV3cfg, yoloV3weights)

        if "use_myriad" in config and config["use_myriad"] == True:
            # Use MyRiad to perform inference
            print('Using NCS2 MyRiad Device.')
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_INFERENCE_ENGINE)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)
        else:
            # Use CPU
            self.model.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
            self.model.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

        self.confidence = 0.3
        if ("confidence" in config):
            print('Use Confidence Threshold = ' + str(config["confidence"]))
            self.confidence = config["confidence"]

        self.nmsThreshold = 0.3

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Get tensor name of output in last layer of the model
    def getOutputsNames(self):
        layersNames = self.model.getLayerNames()
        return [layersNames[i[0] - 1] for i in self.model.getUnconnectedOutLayers()]

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        # print('YoloV3Detector::perform_detection')
        W = image.shape[1]
        H = image.shape[0]
        blob = cv2.dnn.blobFromImage(image, 1.0 / 255, (288, 288), swapRB=True, crop=False)
        self.model.setInput(blob)
        detections = self.model.forward(self.getOutputsNames())

        classIDs = []
        confidences = []
        boxes = []

        for out in detections:
            for detection in out:

                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                if confidence > self.confidence:
                    # print('CONF = ' + str(confidence))
                    centerX = int(detection[0] * W)
                    centerY = int(detection[1] * H)

                    width = int(detection[2] * W)
                    height = int(detection[3] * H)

                    left = int(centerX - width / 2)
                    top = int(centerY - height / 2)

                    if self.classes[classID] != 'person':
                        continue

                    classIDs.append(self.classes[classID])
                    confidences.append(float(confidence))
                    boxes.append([left, top, width, height])

        indices = cv2.dnn.NMSBoxes(boxes, confidences, self.confidence, self.nmsThreshold)

        rects = []
        for i in indices:
            i = i[0]
            box = boxes[i]
            left = box[0]
            top = box[1]
            width = box[2]
            height = box[3]
            class_id = classIDs[i]
            rects.append((left, top, left + width, top + height, class_id))

        return rects
