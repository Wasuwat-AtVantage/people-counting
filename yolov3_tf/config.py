#! /usr/bin/env python
# coding=utf-8
#================================================================
#   Copyright (C) 2019 * Ltd. All rights reserved.
#
#   Editor      : VIM
#   File name   : config.py
#   Author      : YunYang1994
#   Created date: 2019-02-28 13:06:54
#   Description :
#
#================================================================

__C                             = {}
# Consumers can get config by: from config import cfg

cfg                             = __C

# YOLO options
__C['YOLO']                        = {}

# Set the class name
__C['YOLO']['CLASSES']                = "../yolov3/coco.names"
__C['YOLO']['ANCHORS']                = "../yolov3/basline_anchors.txt"
__C['YOLO']['MOVING_AVE_DECAY']       = 0.9995
__C['YOLO']['STRIDES']                = [8, 16, 32]
__C['YOLO']['ANCHOR_PER_SCALE']       = 3
__C['YOLO']['IOU_LOSS_THRESH']        = 0.5
__C['YOLO']['UPSAMPLE_METHOD']        = "resize"
#__C['YOLO']['ORIGINAL_WEIGHT']        = "./checkpoint/yolov3_coco.ckpt"
#__C['YOLO']['DEMO_WEIGHT']            = "./checkpoint/yolov3_coco_demo.ckpt"
