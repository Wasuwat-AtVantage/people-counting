import cv2
import numpy as np
import tensorflow as tf
import os
from yolov3_tf import utils

from object_detector import ObjectDetector


# Yolo V3 Detector for Tensorflow implementation (To be used with CUDA devices)
class YoloV3TFDetector(ObjectDetector):
    def __init__(self):
        self.model = None
        self.input_size = None
        self.return_tensors = None
        self.graph = None
        self.sess = None
        self.num_classes = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('YoloV3TFDetector::init_model')

        return_elements = ["input/input_data:0", "pred_sbbox/concat_2:0", "pred_mbbox/concat_2:0",
                           "pred_lbbox/concat_2:0"]
        pb_file = "../yolov3/yolov3_coco.pb"
        classesFile = "../yolov3/coco.names"
        self.classes = None
        with open(classesFile, 'rt') as f:
            self.classes = f.read().rstrip('\n').split('\n')
        self.num_classes = len(self.classes)
        self.input_size = 416
        self.graph = tf.Graph()
        self.return_tensors = utils.read_pb_return_tensors(self.graph, pb_file, return_elements)
        self.sess = tf.Session(graph=self.graph)

        # TODO: Use the value in real model inferencing
        self.confidence = 0.3
        if ("confidence" in config):
            print('Use Confidence Threshold = ' + str(config["confidence"]))
            self.confidence = config["confidence"]

        self.nmsThreshold = 0.3

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Get tensor name of output in last layer of the model
    def getOutputsNames(self):
        return None  # Not in used

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):

        frame_size = image.shape[:2]
        image_data = utils.image_preporcess(np.copy(image), [self.input_size, self.input_size])
        image_data = image_data[np.newaxis, ...]
        pred_sbbox, pred_mbbox, pred_lbbox = self.sess.run(
            [self.return_tensors[1], self.return_tensors[2], self.return_tensors[3]],
            feed_dict={self.return_tensors[0]: image_data})

        pred_bbox = np.concatenate([np.reshape(pred_sbbox, (-1, 5 + self.num_classes)),
                                    np.reshape(pred_mbbox, (-1, 5 + self.num_classes)),
                                    np.reshape(pred_lbbox, (-1, 5 + self.num_classes))], axis=0)

        bboxes = utils.postprocess_boxes(pred_bbox, frame_size, self.input_size, 0.3)
        bboxes = utils.nms(bboxes, 0.45, method='nms')

        rects = []
        for box in bboxes:
            left = int(box[0])
            top = int(box[1])
            right = int(box[2])
            bottom = int(box[3])
            class_id = int(box[5])
            if self.classes[class_id] != 'person':
                continue
            rects.append((left, top, right, bottom, self.classes[class_id]))

        return rects
