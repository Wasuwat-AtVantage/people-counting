import cv2
import numpy as np
import os
import tensorflow as tf

from object_detector import ObjectDetector


class YoloV3TFLiteDetector(ObjectDetector):
    def __init__(self):
        self.model = None

    # Function to perform model initialization (load model).
    def init_model(self, config):
        print('YoloV3TFLiteDetector::init_model')

        # load json and create model
        # MODEL_FLATBUFFER = '../yolov3/yolov3.tflite'
        MODEL_FLATBUFFER = '../yolov3/tiny_yolo_v3.tflite'

        print("Loaded model from disk")
        self.interpreter = tf.lite.Interpreter(model_path=MODEL_FLATBUFFER)
        self.interpreter.allocate_tensors()
        self.input_details = self.interpreter.get_input_details()
        self.output_details = self.interpreter.get_output_details()

        self.confidence = 0.1
        if ("confidence" in config):
            print('Use Confidence Threshold = ' + str(config["confidence"]))
            self.confidence = config["confidence"]

    # Set parameter of model after initialization
    def setParameter(self, name, value):
        if name == 'confidence':
            self.confidence = value

    # Function to perform object detection, return object is
    # bounding box with class.
    def perform_detection(self, image):
        # print('SSDMobileNetV2Detector::perform_detection')
        W = image.shape[1]
        H = image.shape[0]

        # Get input width/height from tensors
        height = self.input_details[0]['shape'][1]
        width = self.input_details[0]['shape'][2]

        # Resize and normalize image for network input
        frame = cv2.resize(image, (width, height))
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = frame.astype('float32')
        frame /= 255.
        # frame = np.expand_dims(frame, 0)  # Add batch dimension.

        # run model
        self.interpreter.set_tensor(self.input_details[0]['index'], [frame])
        self.interpreter.invoke()

        # get results
        boxes = self.interpreter.get_tensor(
            self.output_details[0]['index'])

        # Find detected boxes coordinates
        ratio_x = float(W) / float(width)
        ratio_y = float(H) / float(height)
        print('ratio_x=' + str(ratio_x))
        print('ratio_y=' + str(ratio_y))
        return self.handle_predictions(boxes[0], ratio_x, ratio_y, self.confidence)

    def handle_predictions(self, predictions, ratio_x, ratio_y, confidence=0.6, iou_threshold=0.5):
        print('Predition.shape = ' + str(predictions.shape))
        boxes = predictions[:, :, :4]
        print('boxes.shape = ' + str(boxes.shape))
        box_confidences = predictions[:, :, 4]
        box_confidences = np.expand_dims(box_confidences, 2)
        print('box_confidences.shape = ' + str(box_confidences.shape))
        box_class_probs = predictions[:, :, 5:]
        print('box_class_probs.shape = ' + str(box_class_probs.shape))

        box_scores = box_confidences * box_class_probs
        box_classes = np.argmax(box_scores, axis=-1)
        print(box_classes)
        box_class_scores = np.max(box_scores, axis=-1)
        # print(box_class_scores)
        pos = np.where(box_class_scores >= confidence)

        boxes = boxes[pos]
        classes = box_classes[pos]
        scores = box_class_scores[pos]

        # Boxes and Classes returned from NMS
        n_boxes = self.nms_boxes(boxes, classes, scores, iou_threshold, ratio_x, ratio_y)
        return n_boxes

    def nms_boxes(self, boxes, classes, scores, iou_threshold, ratio_x, ratio_y):
        nboxes = []
        for c in set(classes):
            inds = np.where(classes == c)
            b = boxes[inds]
            c = classes[inds]
            s = scores[inds]

            # Filter only person class
            # if c != 0:
            #  continue

            x = b[:, 0]
            y = b[:, 1]
            w = b[:, 2]
            h = b[:, 3]

            areas = w * h
            order = s.argsort()[::-1]

            keep = []
            while order.size > 0:
                i = order[0]
                keep.append(i)

                xx1 = np.maximum(x[i], x[order[1:]])
                yy1 = np.maximum(y[i], y[order[1:]])
                xx2 = np.minimum(x[i] + w[i], x[order[1:]] + w[order[1:]])
                yy2 = np.minimum(y[i] + h[i], y[order[1:]] + h[order[1:]])

                w1 = np.maximum(0.0, xx2 - xx1 + 1)
                h1 = np.maximum(0.0, yy2 - yy1 + 1)

                inter = w1 * h1
                ovr = inter / (areas[i] + areas[order[1:]] - inter)
                inds = np.where(ovr <= iou_threshold)[0]
                order = order[inds + 1]

            keep = np.array(keep)
            print(b[keep])
            nboxes = [[int(box[0] * ratio_x), int(box[1] * ratio_y), int((box[0] + box[2]) * ratio_x),
                       int((box[1] + box[3]) * ratio_y), 'person'] for box in b[keep]]
            print(nboxes)
        return nboxes
